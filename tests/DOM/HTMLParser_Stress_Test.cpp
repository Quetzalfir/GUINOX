#include <GUINOX/guinox.hpp>

int main(int argc,  char** argv) {
    if (argc < 2) {
        std::cout << "Not File in arguments" << std::endl;
        return 1;
    }

    auto window2 = GUINOX::createWindow(argv[1], "main2");

    //window2->run();

    //GUINOX::waitForTerminate();

    std::cout << "DONE!" << std::endl;
}