#include <gtest/gtest.h>
#include <GUINOX/guinox.hpp>
#include <algorithm>
#include "../../src/Utils/Utils.hpp"


static void removeLastSpaces(std::string &str) {
    size_t last = str.find_last_not_of(" \n\t\r");
    str = str.substr(0, (last + 1));
}

static std::string getTagName(Parser &parser) {
    return Utils::toLower( parser.getUntilFirstOf(" >/") );
}




TEST(DOMParser, TreeHierarchy) {
    // Build the Document, with the DOM tree
    auto document = DOMParser::parseFromFile("Assets/Parser/parser.html");

    auto jj = document->styleSheets[0];
    auto kk = jj->cssRules;

    for (auto & aa : kk) {
        std::string ss = aa->cssText;
        int i = 0;
    }

    // Re-parse the file to check the tree hierarchy
    std::ifstream htmlFile("Assets/Parser/parser.html", std::ifstream::in);

    std::string str((std::istreambuf_iterator<char>(htmlFile)), std::istreambuf_iterator<char>());

    Parser parser(str);
    parser.addCommentTag("<!--", "-->");
    parser.addCommentTag("<?", "?>");

    DOM_Node::sp_Node parent = document;
    DOM_Node::sp_Node ref = document->firstChild();

    while (parser.skipBlanks()) {

        if (parser.getChar() == '<') { // is a tag
            parser++;

            if (parser.getChar() == '/') { // is a close tag
                std::string tagname = getTagName(++parser);
                parser.goUntil(">")++;

                EXPECT_EQ(tagname, parent->nodeName) << "Is not the corresponding close tag";

                if (ref) {
                    EXPECT_FALSE(ref->nextSibling()) << "Has more childs than expected: " << ref->nodeName;
                }

                ref = parent->nextSibling();
                parent = parent->parentNode();

            } else { // is a new tag
                std::string tagname = getTagName(parser);
                parser.goUntil(">")++;

                ASSERT_TRUE(ref) << "Node doesn't exist: " << tagname;
                EXPECT_NE(ref->nodeType, DOM_Node::Node::TEXT_NODE);

                if (tagname == "!doctype") {
                    EXPECT_EQ(ref->nodeType, DOM_Node::Node::DOCUMENT_TYPE_NODE) << "Node is not a Document Type";
                } else {
                    EXPECT_EQ(tagname, ref->nodeName);
                }

                if (!Utils::isEmptyElement(tagname)) {
                    parent = ref;
                    ref = ref->firstChild();

                } else {
                    EXPECT_FALSE(ref->firstChild()) << "Empty element has a child: " << ref->nodeName;
                    ref = ref->nextSibling();
                }
            }

        } else { // is text
            std::string text = parser.getUntilFirstOf("<");
            removeLastSpaces(text);

            ASSERT_TRUE(ref) << "Text doesn't exist: " << text;
            ASSERT_EQ(ref->nodeType, DOM_Node::Node::TEXT_NODE) << "Is not a Text node";

            auto textNode = std::dynamic_pointer_cast<DOM_Node::Text>(ref);
            EXPECT_EQ(text, textNode->data);

            ref = ref->nextSibling();
        }
    }
}