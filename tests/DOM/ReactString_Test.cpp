#include <gtest/gtest.h>
#include <GUINOX/guinox.hpp>

TEST(ReactString, Constructors) {
    //todo, test the new getter function
    ReactString attr1( ReactString("value1") );
    EXPECT_EQ(attr1,"value1") << "Failed: ReactString(ReactString && ths);";

    std::string value2 = "value2";
    ReactString attr2(value2);
    EXPECT_EQ(attr2,"value2") << "Failed: ReactString(std::string value_);";

    ReactString attr3(attr1);
    EXPECT_EQ(attr3,"value1") << "Failed: ReactString(const ReactString & rhs);";

    ReactString attr4("value4");
    EXPECT_EQ(attr4,"value4") << "Failed: ReactString(const char* arr)";

    int cont1 = 0;

    ReactString attr5("value5", [&cont1](DOMString oldValue, DOMString newValue) {
        //std::cout << "\nInside attr5 reaction\n";
        //std::cout << newValue << std::endl;
        EXPECT_EQ(newValue, "value6");

        cont1++;
    });
    EXPECT_EQ(attr5,"value5") << "Failed: ReactString(std::string value, std::function<void(const std::string &)> reaction_);";

    ReactString attr6(attr5);

    int cont2 = 0;
    ReactString attr7( ReactString("value7",[&cont2](DOMString oldValue, DOMString newValue) {
        //std::cout << "\nInside attr7 reaction\n";
        //std::cout << newValue << std::endl;
        EXPECT_EQ(newValue, "value6");

        cont2++;
    }) );
    EXPECT_EQ(attr7,"value7");

    // check the reaction has been copied
    attr5 = "value5"; // nothing happend, same value
    attr5 = "value6"; // cont++
    attr6 = "value6"; // cont++
    attr7 = "value6"; // cont2++

    // with constructors the function reaction is copied
    EXPECT_EQ(cont1,2);
    EXPECT_EQ(cont2,1);
}

TEST(ReactString, GetterNSetter) {
    int cont1 = 0;
    int cont2 = 0;

    ReactString getterNSetter(
            [=] () mutable -> std::string { // GETTER
                std::string aux = std::to_string(++cont1);
                std::cout << "inside getter: " << aux << std::endl;
                return aux;
            },
            [&] (DOMString oldValue, DOMString newValue) { // REACTION
                cont2++;
            }
    );

    EXPECT_EQ(getterNSetter, "1");
    EXPECT_EQ(cont1, 0);
    EXPECT_EQ(cont2, 0);

    EXPECT_EQ(getterNSetter, "2");
    EXPECT_EQ(cont1, 0);
    EXPECT_EQ(cont2, 0);

    getterNSetter = "5";
    EXPECT_EQ(cont1, 0);
    EXPECT_EQ(cont2, 1);

    EXPECT_EQ(getterNSetter, "3");
    EXPECT_EQ(cont1, 0);
    EXPECT_EQ(cont2, 1);

    getterNSetter += "a";
    EXPECT_EQ(getterNSetter, "5");
    EXPECT_EQ(cont2, 2);

    ReactString cpy = getterNSetter; // calls constructor
    EXPECT_EQ(getterNSetter, "6");
    EXPECT_EQ(cont2, 2);
    EXPECT_EQ(cpy, "6");

    cpy = "1";
    EXPECT_EQ(cpy, "7");
    EXPECT_EQ(cont2, 3);

    ReactString react;
    react = getterNSetter; // calls assignment
    EXPECT_EQ(getterNSetter, "8");
    EXPECT_EQ(cont2, 3);
    EXPECT_EQ(react, "7");
}

TEST(ReactString, Assignments) {
    int cont = 0;
    ReactString attr("value0", [&cont](DOMString oldValue, DOMString newValue) {
        cont++;
    });

    std::string value1 = "value1";
    attr = value1;
    EXPECT_EQ(attr, "value1");
    EXPECT_EQ(cont, 1);

    attr = "value2";
    EXPECT_EQ(attr, "value2");
    EXPECT_EQ(cont, 2);

    int cont2 = 0;
    ReactString attr2("value3", [&cont2](DOMString oldValue, DOMString newValue) {
        cont2++;
    });

    // Unlike constructor, the function reaction is not copied with assignment
    attr = attr2;
    EXPECT_EQ(attr, "value3");
    EXPECT_EQ(cont, 3);
    EXPECT_EQ(cont2, 0);

    attr = "value3";
    EXPECT_EQ(attr, "value3");
    EXPECT_EQ(cont, 3);
    EXPECT_EQ(cont2, 0);

    int cont3 = 0;
    attr = ReactString("value4", [&cont3](DOMString oldValue, DOMString newValue) {
        cont3++;
    });
    EXPECT_EQ(attr, "value4");
    EXPECT_EQ(cont, 4);
    EXPECT_EQ(cont2, 0);
    EXPECT_EQ(cont3, 0);

    attr = "value5";
    EXPECT_EQ(attr, "value5");
    EXPECT_EQ(cont, 5);
    EXPECT_EQ(cont2, 0);
    EXPECT_EQ(cont3, 0);
}

TEST(ReactString, Operators) {
    //constructor by char* and assignment
    ReactString attr1 = "value1";

    std::string value1 = "value1";
    std::string value2 = "value2";
    EXPECT_TRUE(attr1 == value1);
    EXPECT_TRUE(attr1 != value2);
    EXPECT_FALSE(attr1 != value1);
    EXPECT_FALSE(attr1 == value2);

    EXPECT_TRUE(attr1 == "value1");
    EXPECT_TRUE(attr1 != "value2");
    EXPECT_FALSE(attr1 != "value1");
    EXPECT_FALSE(attr1 == "value2");

    ReactString attr2("value2",[](DOMString oldValue, DOMString newValue) {});
    ReactString attr1_2("value1",[](DOMString oldValue, DOMString newValue) {});
    EXPECT_TRUE(attr1 == attr1_2);
    EXPECT_TRUE(attr1 != attr2);
    EXPECT_FALSE(attr1 != attr1_2);
    EXPECT_FALSE(attr1 == attr2);

    // operator +=
    int cont1 = 0;
    ReactString attr3("Hello", [&cont1](DOMString oldValue, DOMString newValue) {
        cont1++;
    });
    attr3 += "World";
    EXPECT_EQ(attr3, "HelloWorld");
    EXPECT_EQ(cont1, 1);

    std::string aux = "IsA";
    attr3 += aux;
    EXPECT_EQ(attr3, "HelloWorldIsA");
    EXPECT_EQ(cont1, 2);

    ReactString attr4 = "CEReaction";
    attr3 += attr4;
    EXPECT_EQ(attr3, "HelloWorldIsACEReaction");
    EXPECT_EQ(cont1, 3);

    // operator +
    ReactString attr5("Hello", [&cont1](DOMString oldValue, DOMString newValue) {
        cont1++;
    });
    std::string res = attr5 + "World";
    EXPECT_EQ(res, "HelloWorld");
    EXPECT_EQ(cont1, 3);

    std::string aux2 = "World";
    res = attr5 + aux2;
    EXPECT_EQ(res, "HelloWorld");
    EXPECT_EQ(cont1, 3);

    ReactString attr6 = "World";
    res = attr5 + attr6;
    EXPECT_EQ(res, "HelloWorld");
    EXPECT_EQ(cont1, 3);

}

TEST(ReactString, StringFunctionality) {
    const ReactString attr = ReactString("Hello");
    const std::string hello = "Hello";

    std::string res1 = "World" + hello;
    std::string res2 = "World" + attr;
    EXPECT_EQ(res1, "WorldHello");
    EXPECT_EQ(res1, res2);

    res1 = hello + "World";
    res2 = attr + "World";
    EXPECT_EQ(res1, "HelloWorld");
    EXPECT_EQ(res1, res2);

    const ReactString attr2 = "World" + hello + attr;
    EXPECT_EQ(attr2, "WorldHelloHello");

    const ReactString attr3 = attr + "World" + hello;
    EXPECT_EQ(attr3, "HelloWorldHello");

    const ReactString attr4 = hello + attr + "World";
    EXPECT_EQ(attr4, "HelloHelloWorld");

    std::string str = attr4;
    EXPECT_EQ(str, "HelloHelloWorld");

    str = attr3;
    EXPECT_EQ(str, "HelloWorldHello");
}