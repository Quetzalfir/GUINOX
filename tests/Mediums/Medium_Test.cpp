#include <gtest/gtest.h>
#include <GUINOX/guinox.hpp>




TEST(Medium, Type_Check) {

    class MediumTest : public Presentation {

    public:

        MediumTest(unsigned type_) : Presentation(type_) {
            features["grid"] = "0";
            features["height"] = "600px";
            features["width"] = "800px";
        }
    };
    MediaList mediaList;

    MediumTest medium(Presentation::SCREEN | Presentation::PRINT);

    mediaList = "screen";
    EXPECT_TRUE(medium.check(mediaList));

    mediaList = "not screen";
    EXPECT_TRUE(medium.check(mediaList));

    mediaList = "only screen";
    EXPECT_FALSE(medium.check(mediaList));

    mediaList = "screen and (grid)";
    EXPECT_FALSE(medium.check(mediaList));

    mediaList = "only screen and (grid)";
    EXPECT_FALSE(medium.check(mediaList));

    MediumTest medium2(Presentation::SPEECH);

    mediaList = "speech";
    EXPECT_TRUE(medium2.check(mediaList));

    mediaList = "not speech";
    EXPECT_FALSE(medium2.check(mediaList));

    mediaList = "only speech";
    EXPECT_TRUE(medium2.check(mediaList));

    mediaList = "speech and (not (grid))";
    EXPECT_TRUE(medium2.check(mediaList));

    mediaList = "not speech and (grid)";
    EXPECT_FALSE(medium2.check(mediaList));
}

TEST(Medium, Range_Check) {

   class MediumTest : public Presentation {

    public:

        MediumTest(unsigned type_) : Presentation(type_) {
            features["grid"] = "0";
            features["height"] = "600px";
            features["width"] = "800px";
        }
    };
    MediaList mediaList;

    MediumTest medium(Presentation::SCREEN | Presentation::PRINT);

    mediaList = "(500 <= height <= 700)";
    EXPECT_TRUE(medium.check(mediaList));

    mediaList = "(width > 800px)";
    EXPECT_FALSE(medium.check(mediaList));

    mediaList = "(800px >= width)";
    EXPECT_TRUE(medium.check(mediaList));
}

TEST(Medium, Check) {
    class MediumTest : public Presentation {

    public:

        MediumTest(unsigned type_) : Presentation(type_) {

            features["color"] = "blue";
        }
    };
    MediaList mediaList;

    MediumTest medium(Presentation::SCREEN | Presentation::PRINT);

    mediaList = "color = red";
    EXPECT_FALSE(medium.check(mediaList));

}


TEST(Medium, Screen_Check) {

    class MediumTest : public Presentation {

    public:

        MediumTest(unsigned type_) : Presentation(type_) {
            features["hover"] = "hover";
            features["width"] = "900px";
        }
    };
    MediaList mediaList;

    MediumTest medium(Presentation::SCREEN);

    mediaList = "hover = none";
    EXPECT_FALSE(medium.check(mediaList));

    mediaList = "screen and (width:900px)";
    EXPECT_TRUE(medium.check(mediaList));

}

TEST(Medium, Not_Check) {

    class MediumTest : public Presentation {

    public:

        MediumTest(unsigned type_) : Presentation(type_) {
            features["color"] = "pink";
        }
    };
    MediaList mediaList;

    MediumTest medium(Presentation::ALL | Presentation::SCREEN);

//    mediaList = "not all and (monochrome)";
//    EXPECT_TRUE(medium.check(mediaList));

    mediaList = "not screen and (pink), print and (pink)";
    EXPECT_TRUE(medium.check(mediaList));

    mediaList = "only screen and (pink)";
    EXPECT_FALSE(medium.check(mediaList));

}
