#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <linmath.h>
#include <cstdio>
#include <string>
#include <iostream>


static const struct {
    float x, y;
    float r, g, b;
} vertices[3] =
        {
                { -0.6f, -0.4f, 1.f, 0.f, 0.f },
                {  0.6f, -0.4f, 0.f, 1.f, 0.f },
                {   0.f,  0.6f, 0.f, 0.f, 1.f }
        };

static const char* vertex_shader_text =
        "#version 110\n"
        "uniform mat4 MVP;\n"
        "attribute vec3 vCol;\n"
        "attribute vec2 vPos;\n"
        "varying vec3 color;\n"
        "void main()\n"
        "{\n"
        "    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
        "    color = vCol;\n"
        "}\n";

static const char* fragment_shader_text =
        "#version 110\n"
        "varying vec3 color;\n"
        "void main()\n"
        "{\n"
        "    gl_FragColor = vec4(color, 1.0);\n"
        "}\n";



static void error_callback(int error, const char* description) {
    fprintf(stderr, "Error %d: %s\n", error, description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    printf("Key Event: key = %c,%c,%d, scancode = %d, action = %d, mods = %d\n", glfwGetKeyName(key, scancode),key,key,scancode,action,mods);
}

void character_callback(GLFWwindow* window, unsigned int codepoint) {
    printf("Text Event: key = %c, %d\n", codepoint, codepoint);
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) {
    std::cout << "Cursor at x:" << xpos << " y:" << ypos <<std::endl;
}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    std::string btn;
    switch (button) {
        case GLFW_MOUSE_BUTTON_4: btn = "4";
            break;
        case GLFW_MOUSE_BUTTON_5: btn = "5";
            break;
        case GLFW_MOUSE_BUTTON_6: btn = "6";
            break;
        case GLFW_MOUSE_BUTTON_7: btn = "7";
            break;
        case GLFW_MOUSE_BUTTON_LAST: btn = "LAST"; //GLFW_MOUSE_BUTTON_8
            break;
        case GLFW_MOUSE_BUTTON_LEFT: btn = "LEFT"; //GLFW_MOUSE_BUTTON_1
            break;
        case GLFW_MOUSE_BUTTON_MIDDLE: btn = "MIDDLE"; //GLFW_MOUSE_BUTTON_3
            break;
        case GLFW_MOUSE_BUTTON_RIGHT: btn = "RIGHT"; //GLFW_MOUSE_BUTTON_2
            break;
    }

    std::string act;
    switch (action) {
        case GLFW_PRESS: act = "PRESSED";
            break;
        case GLFW_RELEASE: act = "RELEASED";
            break;
    }

    std::cout << "Mouse Event: The button " << btn << " was " << act << " with mods " << mods << std::endl;
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    std::cout << "xoffset: " << xoffset << ", yoffset: " << yoffset << std::endl;
}


int main() {
    GLFWwindow *window;
    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
    GLint mvp_location, vpos_location, vcol_location;

    /* Initialize the library */
    if (!glfwInit()) return -1;

    // pass function to print errors
    glfwSetErrorCallback(error_callback);

    /* Create a windowed mode window and its OpenGL context */
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    auto monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

    window = glfwCreateWindow(mode->width, mode->height, "Hello World", monitor, nullptr);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    gladLoadGL(glfwGetProcAddress);
    glfwSwapInterval(1);

    // pass cb to watch events
    //glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, cursor_position_callback);
    glfwSetCharCallback(window, character_callback);
    glfwSetScrollCallback(window, scroll_callback);


    // NOTE: OpenGL error checks have been omitted for brevity
    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_text, nullptr);
    glCompileShader(vertex_shader);

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_text, nullptr);
    glCompileShader(fragment_shader);

    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    mvp_location = glGetUniformLocation(program, "MVP");
    vpos_location = glGetAttribLocation(program, "vPos");
    vcol_location = glGetAttribLocation(program, "vCol");

    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
                          sizeof(vertices[0]), nullptr);
    glEnableVertexAttribArray(vcol_location);

    glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE,
                          sizeof(vertices[0]), (void*) (sizeof(float) * 2));


    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        /* Render here */
        float ratio;
        int width, height;
        mat4x4 m, p, mvp;

        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);

        mat4x4_identity(m);
        mat4x4_rotate_Z(m, m, (float) glfwGetTime());
        mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        mat4x4_mul(mvp, p, m);

        glUseProgram(program);
        glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*) mvp);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}