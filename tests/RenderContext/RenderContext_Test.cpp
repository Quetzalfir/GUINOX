#include <gtest/gtest.h>
#include <GUINOX/guinox.hpp>




TEST(RenderContext, RuleTree) {
    static const std::string HTML1 = R"(
            <!DOCTYPE html>
            <html>
            <head>
                <style>
                    div {margin:5px;color:black}
                    .err {color:red}
                    .big {margin-top:3px; color:pink;}
                    div span {margin-bottom:4px; color:red;}
                    #div1 {color:blue}
                    #div2 {color:green}
                </style>
            </head>
              <body>
                <div class="err" id="div1">
                  <p>
                    this is a <span class="big"> big error </span>
                    this is also a
                    <span class="big"> very  big  error</span> error
                  </p>
                </div>
                <div class="err" id="div2">another error</div>
              </body>
            </html>
    )";


    auto document = DOMParser::parseFromString(HTML1);

}
