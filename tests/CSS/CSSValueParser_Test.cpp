#include <gtest/gtest.h>
//#include "../../src/CSSOM/CSSValueParser.hpp"

/*static void print(const std::vector<CSSValueParser::Property> & properties, const std::string & original_text) {
    std::cout << original_text << std::endl;

    for (const auto &prop : properties) {
        std::cout << prop.name << " : " << prop.value << std::endl;
    }

    std::cout << std::endl;
}

TEST(CSSValueParser, Font) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("font", "12pt/14pt sans-serif");
    ASSERT_EQ(properties.size(), 3);

    EXPECT_EQ(properties[0].name, "font-size");
    EXPECT_EQ(properties[0].value, "12pt");

    EXPECT_EQ(properties[1].name, "line-height");
    EXPECT_EQ(properties[1].value, "14pt");

    EXPECT_EQ(properties[2].name, "font-family");
    EXPECT_EQ(properties[2].value, "sans-serif");


    properties = CSSValueParser::parseProperty("font", "bold italic large serif");
    print(properties, "font : bold italic large serif");
    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "font-weigth");
    EXPECT_EQ(properties[0].value, "bold");

    EXPECT_EQ(properties[1].name, "font-style");
    EXPECT_EQ(properties[1].value, "italic");

    EXPECT_EQ(properties[2].name, "font-size");
    EXPECT_EQ(properties[2].value, "large");

    EXPECT_EQ(properties[3].name, "font-family");
    EXPECT_EQ(properties[3].value, "serif");

    properties = CSSValueParser::parseProperty("margin", "2px 1em 0 auto");
    print(properties, "margin : 2px 1em 0 auto");

    properties = CSSValueParser::parseProperty("margin", "2px 3px 4px 5px 6px");
    print(properties, "margin : 2px 3px 4px 5px 6px");

    properties = CSSValueParser::parseProperty("margin", "5% auto");
    print(properties, "margin : 5% auto");

    properties = CSSValueParser::parseProperty("flex", "2 2");
    print(properties, "flex : 2 2");

    properties = CSSValueParser::parseProperty("flex", "2 2 10%");
    print(properties, "flex : 2 2 10%");

    properties = CSSValueParser::parseProperty("border", "1px solid #000");
    print(properties, "border : 1px solid #000");

    properties = CSSValueParser::parseProperty("border", "4px dotted blue");
    print(properties, "border : 4px dotted blue");

    properties = CSSValueParser::parseProperty("border", "double");
    print(properties, "border : double");

    properties = CSSValueParser::parseProperty("border", "dotted solid solid double 4px 3px 2px 1px blue red #f35 rgba(100,50,200,30)");
    print(properties, "border : dotted solid solid double 4px 3px 2px 1px blue red #f35 rgba(100,50,200,30)");

    properties = CSSValueParser::parseProperty("border-top", "none thick green");
    print(properties, "border-top : none thick green");

    properties = CSSValueParser::parseProperty("border-right", "4mm ridge rgb(170, 50, 220, .6)");
    print(properties, "border-right : 4mm ridge rgb(170, 50, 220, .6)");

    properties = CSSValueParser::parseProperty("border-bottom", "none thick green");
    print(properties, "border-bottom : none thick green");

    properties = CSSValueParser::parseProperty("border-left", "1px solid #000");
    print(properties, "border-left : 1px solid #000");

    properties = CSSValueParser::parseProperty("border-width", "thin");
    print(properties, "border-width : thin");

    properties = CSSValueParser::parseProperty("border-color", "blue red pink green");
    print(properties, "border-color : blue red pink green");

    properties = CSSValueParser::parseProperty("border-style", "solid dotted");
    print(properties, "border-style : solid dotted");

    properties = CSSValueParser::parseProperty("padding", "10px 5px 15px 20px");
    print(properties, "padding : 10px 5px 15px 20px");

    properties = CSSValueParser::parseProperty("padding", "10px 3% 20px");
    print(properties, "padding : 10px 3% 20px");

    properties = CSSValueParser::parseProperty("list-style", "square inside");
    print(properties, "list-style : square inside");

    properties = CSSValueParser::parseProperty("list-style", "square inside url(\"sqpurple.gif\")");
    print(properties, "list-style : square inside url(\"sqpurple.gif\")");

    properties = CSSValueParser::parseProperty("border-radius", "10px 40px 40px 10px");
    print(properties, "border-radius : 10px 40px 40px 10px");

    properties = CSSValueParser::parseProperty("border-radius", "13em/3em");
    print(properties, "border-radius : 13em/3em");
}

TEST(CSSValueParser, Background) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("background", "url(\"https://mdn.mozillademos.org/files/11983/starsolid.gif\") #00D repeat-y fixed");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "background-image");
    EXPECT_EQ(properties[0].value, "url(\"https://mdn.mozillademos.org/files/11983/starsolid.gif\")");

    EXPECT_EQ(properties[1].name, "background-color");
    EXPECT_EQ(properties[1].value, "#00D");

    EXPECT_EQ(properties[2].name, "background-repeat");
    EXPECT_EQ(properties[2].value, "repeat-y");

    EXPECT_EQ(properties[3].name, "background-attachment");
    EXPECT_EQ(properties[3].value, "fixed");

    properties = CSSValueParser::parseProperty("background", "lightblue url(\"img_tree.gif\") no-repeat fixed center");

    ASSERT_EQ(properties.size(), 5);

    EXPECT_EQ(properties[0].name, "background-color");
    EXPECT_EQ(properties[0].value, "lightblue");

    EXPECT_EQ(properties[1].name, "background-image");
    EXPECT_EQ(properties[1].value, "url(\"img_tree.gif\")");

    EXPECT_EQ(properties[2].name, "background-repeat");
    EXPECT_EQ(properties[2].value, "no-repeat");

    EXPECT_EQ(properties[3].name, "background-attachment");
    EXPECT_EQ(properties[3].value, "fixed");

    EXPECT_EQ(properties[4].name, "background-position");
    EXPECT_EQ(properties[4].value, "center");

}

TEST(CSSValueParser, Margin) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("margin", "2px 1em 0 auto");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "margin-top");
    EXPECT_EQ(properties[0].value, "2px");

    EXPECT_EQ(properties[1].name, "margin-right");
    EXPECT_EQ(properties[1].value, "1em");

    EXPECT_EQ(properties[2].name, "margin-bottom");
    EXPECT_EQ(properties[2].value, "0");

    EXPECT_EQ(properties[3].name, "margin-left");
    EXPECT_EQ(properties[3].value, "auto");
}

TEST(CSSValueParser, Border) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border", "1px solid #000");

    ASSERT_EQ(properties.size(), 12);

    EXPECT_EQ(properties[0].name, "border-width-top");
    EXPECT_EQ(properties[0].value, "1px");

    EXPECT_EQ(properties[1].name, "border-width-right");
    EXPECT_EQ(properties[1].value, "1px");

    EXPECT_EQ(properties[2].name, "border-width-bottom");
    EXPECT_EQ(properties[2].value, "1px");

    EXPECT_EQ(properties[3].name, "border-width-left");
    EXPECT_EQ(properties[3].value, "1px");

    EXPECT_EQ(properties[4].name, "border-style-top");
    EXPECT_EQ(properties[4].value, "solid");

    EXPECT_EQ(properties[5].name, "border-style-right");
    EXPECT_EQ(properties[5].value, "solid");

    EXPECT_EQ(properties[6].name, "border-style-bottom");
    EXPECT_EQ(properties[6].value, "solid");

    EXPECT_EQ(properties[7].name, "border-style-left");
    EXPECT_EQ(properties[7].value, "solid");

    EXPECT_EQ(properties[8].name, "border-color-top");
    EXPECT_EQ(properties[8].value, "#000");

    EXPECT_EQ(properties[9].name, "border-color-right");
    EXPECT_EQ(properties[9].value, "#000");

    EXPECT_EQ(properties[10].name, "border-color-bottom");
    EXPECT_EQ(properties[10].value, "#000");

    EXPECT_EQ(properties[11].name, "border-color-left");
    EXPECT_EQ(properties[11].value, "#000");

    properties = CSSValueParser::parseProperty("border", "4px dotted blue");

    ASSERT_EQ(properties.size(), 12);

    EXPECT_EQ(properties[0].name, "border-width-top");
    EXPECT_EQ(properties[0].value, "4px");

    EXPECT_EQ(properties[1].name, "border-width-right");
    EXPECT_EQ(properties[1].value, "4px");

    EXPECT_EQ(properties[2].name, "border-width-bottom");
    EXPECT_EQ(properties[2].value, "4px");

    EXPECT_EQ(properties[3].name, "border-width-left");
    EXPECT_EQ(properties[3].value, "4px");

    EXPECT_EQ(properties[4].name, "border-style-top");
    EXPECT_EQ(properties[4].value, "dotted");

    EXPECT_EQ(properties[5].name, "border-style-right");
    EXPECT_EQ(properties[5].value, "dotted");

    EXPECT_EQ(properties[6].name, "border-style-bottom");
    EXPECT_EQ(properties[6].value, "dotted");

    EXPECT_EQ(properties[7].name, "border-style-left");
    EXPECT_EQ(properties[7].value, "dotted");

    EXPECT_EQ(properties[8].name, "border-color-top");
    EXPECT_EQ(properties[8].value, "blue");

    EXPECT_EQ(properties[9].name, "border-color-right");
    EXPECT_EQ(properties[9].value, "blue");

    EXPECT_EQ(properties[10].name, "border-color-bottom");
    EXPECT_EQ(properties[10].value, "blue");

    EXPECT_EQ(properties[11].name, "border-color-left");
    EXPECT_EQ(properties[11].value, "blue");

    properties = CSSValueParser::parseProperty("border", "double");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "border-style-top");
    EXPECT_EQ(properties[0].value, "double");

    EXPECT_EQ(properties[1].name, "border-style-right");
    EXPECT_EQ(properties[1].value, "double");

    EXPECT_EQ(properties[2].name, "border-style-bottom");
    EXPECT_EQ(properties[2].value, "double");

    EXPECT_EQ(properties[3].name, "border-style-left");
    EXPECT_EQ(properties[3].value, "double");

    properties = CSSValueParser::parseProperty("border", "dotted solid solid double 4px 3px 2px 1px blue red #f35 rgba(100,50,200,30)");

    ASSERT_EQ(properties.size(), 12);

    EXPECT_EQ(properties[0].name, "border-style-top");
    EXPECT_EQ(properties[0].value, "dotted");

    EXPECT_EQ(properties[1].name, "border-style-right");
    EXPECT_EQ(properties[1].value, "solid");

    EXPECT_EQ(properties[2].name, "border-style-bottom");
    EXPECT_EQ(properties[2].value, "solid");

    EXPECT_EQ(properties[3].name, "border-style-left");
    EXPECT_EQ(properties[3].value, "double");

    EXPECT_EQ(properties[4].name, "border-width-top");
    EXPECT_EQ(properties[4].value, "4px");

    EXPECT_EQ(properties[5].name, "border-width-right");
    EXPECT_EQ(properties[5].value, "3px");

    EXPECT_EQ(properties[6].name, "border-width-bottom");
    EXPECT_EQ(properties[6].value, "2px");

    EXPECT_EQ(properties[7].name, "border-width-left");
    EXPECT_EQ(properties[7].value, "1px");

    EXPECT_EQ(properties[8].name, "border-color-top");
    EXPECT_EQ(properties[8].value, "blue");

    EXPECT_EQ(properties[9].name, "border-color-right");
    EXPECT_EQ(properties[9].value, "red");

    EXPECT_EQ(properties[10].name, "border-color-bottom");
    EXPECT_EQ(properties[10].value, "#f35");

    EXPECT_EQ(properties[11].name, "border-color-left");
    EXPECT_EQ(properties[11].value, "rgba(100,50,200,30)");

}

TEST(CSSValueParser, BorderTop) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-top", "none thick green");

    ASSERT_EQ(properties.size(), 3);

    EXPECT_EQ(properties[0].name, "border-top-style");
    EXPECT_EQ(properties[0].value, "none");

    EXPECT_EQ(properties[1].name, "border-top-width");
    EXPECT_EQ(properties[1].value, "thick");

    EXPECT_EQ(properties[2].name, "border-top-color");
    EXPECT_EQ(properties[2].value, "green");
}

TEST(CSSValueParser, BorderRight) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-right", "4mm ridge rgb(170, 50, 220, .6)");

    ASSERT_EQ(properties.size(), 3);

    EXPECT_EQ(properties[0].name, "border-right-width");
    EXPECT_EQ(properties[0].value, "4mm");

    EXPECT_EQ(properties[1].name, "border-right-style");
    EXPECT_EQ(properties[1].value, "ridge");

    EXPECT_EQ(properties[2].name, "border-right-color");
    EXPECT_EQ(properties[2].value, "rgb(170, 50, 220, .6)");
}

TEST(CSSValueParser, BorderButtom) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-bottom", "none thick green");

    ASSERT_EQ(properties.size(), 3);

    EXPECT_EQ(properties[0].name, "border-bottom-style");
    EXPECT_EQ(properties[0].value, "none");

    EXPECT_EQ(properties[1].name, "border-bottom-width");
    EXPECT_EQ(properties[1].value, "thick");

    EXPECT_EQ(properties[2].name, "border-bottom-color");
    EXPECT_EQ(properties[2].value, "green");
}

TEST(CSSValueParser, BorderLeft) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-left", "1px solid #000");

    ASSERT_EQ(properties.size(), 3);

    EXPECT_EQ(properties[0].name, "border-left-width");
    EXPECT_EQ(properties[0].value, "1px");

    EXPECT_EQ(properties[1].name, "border-left-style");
    EXPECT_EQ(properties[1].value, "solid");

    EXPECT_EQ(properties[2].name, "border-left-color");
    EXPECT_EQ(properties[2].value, "#000");
}

TEST(CSSValueParser, BorderWidth) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-width", "thin");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "border-width-top");
    EXPECT_EQ(properties[0].value, "thin");

    EXPECT_EQ(properties[1].name, "border-width-right");
    EXPECT_EQ(properties[1].value, "thin");

    EXPECT_EQ(properties[2].name, "border-width-bottom");
    EXPECT_EQ(properties[2].value, "thin");

    EXPECT_EQ(properties[3].name, "border-width-left");
    EXPECT_EQ(properties[3].value, "thin");
}

TEST(CSSValueParser, BorderColor) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-color", "blue red pink green");

    EXPECT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "border-color-top");
    EXPECT_EQ(properties[0].value, "blue");

    EXPECT_EQ(properties[1].name, "border-color-right");
    EXPECT_EQ(properties[1].value, "red");

    EXPECT_EQ(properties[2].name, "border-color-bottom");
    EXPECT_EQ(properties[2].value, "pink");

    EXPECT_EQ(properties[3].name, "border-color-left");
    EXPECT_EQ(properties[3].value, "green");
}

TEST(CSSValueParser, BorderStyle) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-style", "solid dotted");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "border-style-top");
    EXPECT_EQ(properties[0].value, "solid");

    EXPECT_EQ(properties[1].name, "border-style-right");
    EXPECT_EQ(properties[1].value, "dotted");

    EXPECT_EQ(properties[2].name, "border-style-bottom");
    EXPECT_EQ(properties[2].value, "solid");

    EXPECT_EQ(properties[3].name, "border-style-left");
    EXPECT_EQ(properties[3].value, "dotted");

}

TEST(CSSValueParser, Padding) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("padding", "10px 5px 15px 20px");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "padding-top");
    EXPECT_EQ(properties[0].value, "10px");

    EXPECT_EQ(properties[1].name, "padding-right");
    EXPECT_EQ(properties[1].value, "5px");

    EXPECT_EQ(properties[2].name, "padding-bottom");
    EXPECT_EQ(properties[2].value, "15px");

    EXPECT_EQ(properties[3].name, "padding-left");
    EXPECT_EQ(properties[3].value, "20px");

    properties = CSSValueParser::parseProperty("padding", "10px 3% 20px");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "padding-top");
    EXPECT_EQ(properties[0].value, "10px");

    EXPECT_EQ(properties[1].name, "padding-right");
    EXPECT_EQ(properties[1].value, "3%");

    //EXPECT_EQ(properties[2].name, "padding-bottom");
    //EXPECT_EQ(properties[2].value, "3%");

    EXPECT_EQ(properties[3].name, "padding-left");
    EXPECT_EQ(properties[3].value, "20px");
}

TEST(CSSValueParser, ListStyle) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("list-style", "square inside");

    ASSERT_EQ(properties.size(), 2);

    EXPECT_EQ(properties[0].name, "list-style-type");
    EXPECT_EQ(properties[0].value, "square");

    EXPECT_EQ(properties[1].name, "list-style-position");
    EXPECT_EQ(properties[1].value, "inside");

    properties = CSSValueParser::parseProperty("list-style", "square inside url(\"sqpurple.gif\")");

    ASSERT_EQ(properties.size(), 3);

    EXPECT_EQ(properties[0].name, "list-style-type");
    EXPECT_EQ(properties[0].value, "square");

    EXPECT_EQ(properties[1].name, "list-style-position");
    EXPECT_EQ(properties[1].value, "inside");

    EXPECT_EQ(properties[2].name, "list-style-image");
    EXPECT_EQ(properties[2].value, "url(\"sqpurple.gif\")");
}

TEST(CSSValueParser, BorderRadius) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("border-radius", "10px 40px 40px 10px");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "border-top-left-radius");
    EXPECT_EQ(properties[0].value, "10px");

    EXPECT_EQ(properties[1].name, "border-top-right-radius");
    EXPECT_EQ(properties[1].value, "40px");

    EXPECT_EQ(properties[2].name, "border-bottom-right-radius");
    EXPECT_EQ(properties[2].value, "40px");

    EXPECT_EQ(properties[3].name, "border-bottom-left-radius");
    EXPECT_EQ(properties[3].value, "10px");

    properties = CSSValueParser::parseProperty("border-radius", "13em/3em");

    ASSERT_EQ(properties.size(), 4);

    EXPECT_EQ(properties[0].name, "border-top-left-radius");
    EXPECT_EQ(properties[0].value, "13em");

    EXPECT_EQ(properties[1].name, "border-top-right-radius");
    EXPECT_EQ(properties[1].value, "3em");

    EXPECT_EQ(properties[2].name, "border-bottom-right-radius");
    EXPECT_EQ(properties[2].value, "13em");

    EXPECT_EQ(properties[3].name, "border-bottom-left-radius");
    EXPECT_EQ(properties[3].value, "3em");
}

TEST(CSSValueParser, Flex) {
    std::vector<CSSValueParser::Property> properties;

    properties = CSSValueParser::parseProperty("flex", "2 2");

    ASSERT_EQ(properties.size(), 2);

    EXPECT_EQ(properties[0].name, "flex-grow");
    EXPECT_EQ(properties[0].value, "2");

    EXPECT_EQ(properties[1].name, "flex-shrink");
    EXPECT_EQ(properties[1].value, "2");

    properties = CSSValueParser::parseProperty("flex", "2 2 10%");

    ASSERT_EQ(properties.size(), 3);

    EXPECT_EQ(properties[0].name, "flex-grow");
    EXPECT_EQ(properties[0].value, "2");

    EXPECT_EQ(properties[1].name, "flex-shrink");
    EXPECT_EQ(properties[1].value, "2");

    EXPECT_EQ(properties[2].name, "flex-basis");
    EXPECT_EQ(properties[2].value, "10%");
}
*/