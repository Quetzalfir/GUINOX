#include <gtest/gtest.h>
#include <GUINOX/guinox.hpp>


TEST(CSSStyleDeclaration, Background) {

    CSSStyleDeclaration declaration;

    declaration.setProperty("color", "red");
    declaration.setProperty("width", "90%");
    declaration.setProperty("border-right", "4mm ridge rgb(170, 50, 220)");

    // comment for Vane
    //std::cout << declaration["border"] << std::endl;

    std::cout << declaration.cssText << std::endl;

    declaration.setProperty("color", "blue");
    declaration.setProperty("width", "100px");
    declaration.removeProperty("border-right-style");

    std::cout << declaration.cssText << std::endl;

    declaration.removeProperty("border");

    std::cout << declaration.cssText << std::endl;

    declaration.cssText = "background: lightblue url(\"img_tree.gif\") no-repeat fixed center; color:pink;";
    std::cout << declaration.cssText << std::endl;

}

TEST(CSSStyleDeclaration, Font) {

    //p { font: 12pt/14pt sans-serif }
    CSSStyleDeclaration declaration;

    declaration.setProperty("font-size", "12pt");
    declaration.setProperty("font-family", "sans-serif");

    // comment for Vane
    std::cout << declaration["font"] << std::endl;

    std::cout << declaration.cssText << std::endl;

    declaration.setProperty("font-size", "14pt");
    declaration.setProperty("font-family", "sans-serif");

    std::cout << declaration.cssText << std::endl;

}

TEST(CSSStyleDeclaration, Array_Access_Operator) {

    CSSStyleDeclaration declaration;

    declaration["color"] = "red";
    declaration["width"] = "90%";
    declaration["border-right"] = "4mm ridge rgb(170, 50, 220)";

    // comment for Vane
    //std::cout << declaration["border"] << std::endl;

    std::cout << declaration.cssText << std::endl;

    declaration["color"] = "blue";
    declaration["width"] = "100px";
    declaration["border-right-style"] = ""; //removeProperty

    std::cout << declaration.cssText << std::endl;

    declaration["border"] = ""; //removeProperty

    std::cout << declaration.cssText << std::endl;




    CSSStyleDeclaration declaration2;

    declaration2["font-size"] = "12pt";
    declaration2["font-family"] = "sans-serif";

    // comment for Vane
    std::cout << declaration2["font"] << std::endl;

    std::cout << declaration2.cssText << std::endl;

    declaration2["font-size"] = "14pt";
    declaration2["font-family"] = "sans-serif";

    std::cout << declaration2.cssText << std::endl;

}
