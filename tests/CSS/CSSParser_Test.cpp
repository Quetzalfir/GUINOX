#include <gtest/gtest.h>
#include <GUINOX/guinox.hpp>
#include "../../src/CSSOM/CSSSelector.hpp"

TEST(CSSParser, Selector) {
    // First read de file
    std::ifstream htmlFile("Assets/Parser/css_parser.html", std::ifstream::in);

    std::string str((std::istreambuf_iterator<char>(htmlFile)), std::istreambuf_iterator<char>());

    // Build the Document, with the DOM tree
    auto document = DOMParser::parseFromString(str);

    auto res = CSSSelector::select("body [aa=bb] h3[foo=gggg]", document->documentElement);

    bool f = res[0]->matches("body [aa=bb] h3[foo=gggg]");

    int x = 0;

    //ref = f;
    //EXPECT_TRUE(ref->firstChild()) << "Empty element has a child: " << ref->nodeName;
}

TEST(CSSParser, Tokens){
    // First read de file
    std::ifstream htmlFile("Assets/Parser/css_parser.html", std::ifstream::in);

    std::string str((std::istreambuf_iterator<char>(htmlFile)), std::istreambuf_iterator<char>());

    // Build the Document, with the DOM tree
    auto document = DOMParser::parseFromString(str);

    auto res = CSSSelector::select("body [aa=bb] h3[foo=gggg]", document->documentElement);

    bool f = res[0]->matches("body [aa=bb] h3[foo=gggg]");

}

