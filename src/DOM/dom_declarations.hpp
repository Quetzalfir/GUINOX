/**
* @brief File where all the classes to be used in the DOM
* are defined to be defined later.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
*/
#ifndef GUINOX_DOM_DECLARATIONS_HPP
#define GUINOX_DOM_DECLARATIONS_HPP

#include <list>
#include <deque>
#include <memory>

namespace DOM_Node {

    class Node;
    class Document;
    class Element;
    class Text;
    class DocumentType;
    class XMLDocument;

    typedef std::shared_ptr<Node> sp_Node;
    typedef std::weak_ptr<Node> wp_Node;
    typedef std::list<sp_Node> NodeList;
    typedef std::shared_ptr<DOM_Node::Element> sp_Element;
    typedef std::deque<sp_Element> HTMLCollection;
    typedef std::shared_ptr<Document> sp_Document;
    typedef std::weak_ptr<Document> wp_Document;
    typedef std::shared_ptr<DocumentType> sp_DocumentType;
    typedef std::shared_ptr<Text> sp_Text;

}

typedef const std::string & DOMString;


class EventTarget;
typedef std::shared_ptr<EventTarget> sp_EventTarget;


class HTMLElement;
typedef std::shared_ptr<HTMLElement> sp_HTMLElement;



#endif //GUINOX_DOM_DECLARATIONS_HPP
