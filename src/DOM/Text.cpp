#include <iostream>
#include "Text.hpp"
#include "MutationAlgorithms.hpp"
#include "NodeTravel.hpp"


namespace DOM_Node {

    Text::Text(const std::string &str) {
        _nodeType = TEXT_NODE;
        _nodeName = "#text";

        data = str;
    }

    Text::~Text() {
        std::cout << "Text: destroy Text: " << data << std::endl;
    }

    /*void Text::render() {
        for (const Line &line : lines) {
            for (const Glyph &glyph : line.glyphs) {
                font->drawGlyph(glyph, border, factor);
            }
        }
    }*/

    DOM_Node::sp_Node Text::cloneNode(bool deep) const {
        auto res = std::shared_ptr<Text>( new Text(data) );

        clone(res.get(), deep);

        return res;
    }

    bool Text::isEqualNode(DOM_Node::sp_Node node) const {
        if (Node::isEqualNode(node)) {
            auto other = dynamic_cast<Text *>(node.get());

            return data == other->data;
        }

        return false;
    }

    /*const DOMRect &Text::prepare(const DOMRect &rec) {
        limitRec = rec;

        resetCursor(cursor);
        lines.clear();
        Line line0;
        line0.number = 0;
        lines.push_back(line0);


        int length = string.length();
        int wordSize = 0;
        Point point = {rec.left, rec.bottom};
        bool breakWord = false;

        if (fillBox) {
            for (int i = 0; i < length; i++) {
                const Char &glyph = getGlyph(string[i]);

                Fill_Analize:
                if (glyphFit(glyph)) {
                    cursor.x += addGlyph(glyph);
                } else {
                    if (nextLineFit()) {
                        nextLine();
                        goto Fill_Analize;
                    } else {
                        goto Exit;
                    }
                }
            }
        } else {

            for (int i = 0; i < length; i++) {
                const Char &glyph = getGlyph(string[i]);

                if (breakWord) goto Break_Word;

                if (breakable) {
                    if (string[i] != ' ' && !wordSize) {
                        Analizar_Palabra:
                        wordSize = wordFit(string, i);
                        if (wordSize < 0) {
                            if (cursor.x == point.x) {
                                Break_Word:
                                breakWord = true;
                                cursor.x += addGlyph(glyph);
                                if (cursor.x >= limitRec.right) {
                                    if (nextLineFit()) {
                                        nextLine();
                                        breakWord = false;
                                        wordSize = 0;
                                    } else {
                                        goto Exit;
                                    }
                                }
                            } else {
                                if (nextLineFit()) {
                                    nextLine();
                                    goto Analizar_Palabra;
                                } else {
                                    goto Exit;
                                }
                            }
                        } else {
                            goto Add_Glyph;
                        }
                    } else if (string[i] == ' ') {
                        cursor.x += addGlyph(glyph);
                        if (cursor.x >= limitRec.right) {
                            if (nextLineFit()) {
                                nextLine();
                            } else {
                                goto Exit;
                            }
                        }
                    } else if (wordSize) {
                        Add_Glyph:
                        cursor.x += addGlyph(glyph);
                        wordSize--;
                    }
                } else {    // no breakable
                    if (glyphFit(glyph)) {
                        cursor.x += addGlyph(glyph);
                    } else {
                        goto Exit;
                    }
                }
            }
        }

        Exit:

        return recBound;
    }*/


    /*void Text::setSize(int size_) {
        size = size_;
        //factor = ((double) size) / font->lineHeight;
    }*/

    /*double Text::addGlyph(const Char &cha) {
        Glyph glyph(cha);

        DOMRect rec = DOMRect::fromRect(
                cursor.x + cha.xoffset * factor,
                cursor.y + cha.yoffset * factor,
                cha.width * factor,
                cha.height * factor
        );
        const float *vertex = rec.setVertex();
        glyph.vertex[0] = vertex[0];
        glyph.vertex[1] = vertex[1];
        glyph.vertex[2] = vertex[7];
        glyph.vertex[3] = vertex[6];

        lines.back().glyphs.push_back(glyph);
        lines.back().mainSize += cha.xadvance * factor;

        return cha.xadvance * factor;
    }*/

    /*const Manager::Font::Font_Char &Text::getGlyph(int c) {
        return font->getChar(c);
    }*/

    /*bool Text::glyphFit(const Char &glyph) {
        double calc = cursor.x + (glyph.xoffset + glyph.width) * factor;

        return (calc <= limitRec.right);
    }*/

    /*void Text::resetCursor(Point &cursor) {
        cursor.x = limitRec.left;
        cursor.y = limitRec.top;
    }*/

    /*bool Text::nextLineFit() {
        return cursor.y + font->lineHeight * factor * 2 <= limitRec.bottom;
    }*/

    /*void Text::nextLine() {
        cursor.y += font->lineHeight * factor;
        cursor.x = limitRec.left;

        Line newLine;
        newLine.number = ++currentLine;
        lines.push_back(newLine);
    }*/

    /*int Text::wordFit(const std::string &str, int cont) {
        float size = 0;
        int length = str.length();

        int cont2 = cont;
        while (cont2 < length && str[cont2] != ' ') {
            size += getGlyph(str[cont2]).xadvance * factor;
            cont2++;
        }

        if (cursor.x + size <= limitRec.right) {
            return cont2 - cont;
        } else {
            return -1;
        }
    }*/

    /*void Text::alignLines(const std::string &align) {
        for (Line &line: lines) {
            if (line.empty()) { continue; }

            double space;
            if (line.glyphs[line.size() - 1].letter == ' ')
                space = limitRec.width() - line.mainSize - getGlyph(' ').xadvance * factor;
            else
                space = limitRec.width() - line.mainSize;

            if (space > 0) {
                if (align == "right") {

                    space = space * 2.0 / viewportWidth;
                    for (Glyph &glyph : line.glyphs) {
                        glyph.vertex[0] += space;
                        glyph.vertex[3] += space;
                    }

                } else if (align == "center") {
                    space /= 2;

                    space = space * 2.0 / viewportWidth;
                    for (Glyph &glyph : line.glyphs) {
                        glyph.vertex[0] += space;
                        glyph.vertex[3] += space;
                    }
                }
            }
        }
    }*/

    /*void Text::move(double x, double y) {
        x = x * -2.0 / viewportWidth;
        y = y * -2.0 / viewportHeight;

        for (Line &line : lines) {
            for (Glyph &glyph : line.glyphs) {
                float *vertex = glyph.vertex;

                vertex[0] += x,
                        vertex[1] += y;
                vertex[2] += y;
                vertex[3] += x;
            }
        }
    }*/

    sp_Text Text::splitText(unsigned long offset) {
        auto length = this->length();

        if (offset > length) {
            throw "IndexSizeError"; // DOMException.
        }

        auto count = length - offset;

        std::string newData = this->substringData(offset, count);

        auto newNode = std::shared_ptr<Text>( new Text(newData) );

        auto parent = this->parentNode();

        if (parent) {
            parent->insertBefore(newNode, this->nextSibling());

            MutationAlgorithms(this).forEachLiveRange([=](sp_Range & range) {
                if (range->start.node.get() == this && range->start.offset > offset) {
                    range->start.node = newNode;
                    range->start.offset -= offset;
                }

                if (range->end.node.get() == this && range->end.offset > offset) {
                    range->end.node = newNode;
                    range->end.offset -= offset;
                }

                if (range->start.node == parent && range->start.offset == NodeTravel(this).index() + 1 ) {
                    range->start.offset += 1;
                }

                if (range->end.node == parent && range->end.offset == NodeTravel(this).index() + 1 ) {
                    range->end.offset += 1;
                }
            });
        }

        this->replaceData(offset, count, "");

        return newNode;
    }

    std::string Text::wholeText() const {
        std::string s;
        auto parent = parentNode();

        for (const auto & child : parent->childNodes) {
            if (child->nodeType == Node::TEXT_NODE) {
                auto text = dynamic_cast<Text *>(child.get());
                s += text->data;
            }
        }

        return s;
    }

}