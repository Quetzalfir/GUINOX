#include "ReactString.hpp"
#include <iostream>


ReactString::ReactString(
        std::string value_,
        std::function<void(const std::string &, const std::string &)> reaction_) :
        value(std::move(value_)),
        reaction(std::move(reaction_))
{
    reactive = true;
}

ReactString::ReactString(std::function<std::string(void)> getter_,
                         std::function<void(const std::string &, const std::string &)> reaction_) {

    getterNSetter = true;
    reactive = true;
    getter = std::move(getter_);
    reaction = std::move(reaction_);
}

ReactString::ReactString(const ReactString & rhs) {
    reaction = rhs.reaction;
    reactive = rhs.reactive;
    getterNSetter = rhs.getterNSetter;
    getter = rhs.getter;
    if (!getterNSetter)
        value = rhs.value;
}

ReactString::ReactString(ReactString && ths) noexcept {
    reaction = std::move(ths.reaction);
    reactive = ths.reactive;
    getterNSetter = ths.getterNSetter;
    getter = std::move(ths.getter);
    if (!getterNSetter)
        value = std::move(ths.value);
}

ReactString::ReactString(const char *arr) : value(arr) {}

ReactString::ReactString() : ReactString("") {}



bool ReactString::operator==(const ReactString &rhs) const {
    return (getterNSetter ? getter() : value) == (rhs.getterNSetter ? rhs.getter() : rhs.value);
}

bool ReactString::operator==(const std::string &str) const {
    return (getterNSetter ? getter() : value) == str;
}

bool ReactString::operator==(const char *arr) const {
    return (getterNSetter ? getter() : value) == arr;
}

bool operator== (const std::string & lhs, const ReactString & rhs) {
    return (rhs.getterNSetter ? rhs.getter() : rhs.value) == lhs;
}




ReactString& ReactString::operator=(const std::string &str) {
    if (getterNSetter || value != str) {
        if (reactive) reaction(value, str);
        value = str;
    }

    return *this;
}

ReactString & ReactString::operator=(const char *arr) {
    if (getterNSetter || value != arr) {
        std::string aux = arr;
        if (reactive) reaction(value, aux);
        value = arr;
    }

    return *this;
}

ReactString & ReactString::operator=(char c) {
    if (getterNSetter || value.length() != 1 || value[0] != c) {
        std::string aux(1,c);
        if (reactive) reaction(value, aux);
        value = c;
    }

    return *this;
}

ReactString& ReactString::operator=(const ReactString &rhs) {
    if (&rhs != this) {
        std::string tmpValue2 = rhs.getterNSetter ? rhs.getter() : rhs.value;

        if (getterNSetter || value != tmpValue2) {
            if (reactive) reaction(value, tmpValue2);
            value = tmpValue2;
        }
    }

    return *this;
}

ReactString & ReactString::operator=(ReactString && ths) noexcept {
    std::string tmpValue2 = ths.getterNSetter ? ths.getter() : ths.value;

    if (getterNSetter || value != tmpValue2) {
        if (reactive) reaction(value, tmpValue2);
        value = tmpValue2;
    }

    return *this;
}

ReactString& ReactString::operator+=(const std::string &str) {
    std::string tmpValue = getterNSetter ? getter() : value ;
    std::string aux = tmpValue + str;

    if (aux != tmpValue) {
        if (reactive) reaction(value, aux);
        value = aux;
    }

    return *this;
}

ReactString& ReactString::operator+=(const char* arr) {
    std::string tmpValue = getterNSetter ? getter() : value ;
    std::string aux = tmpValue + arr;

    if (aux != tmpValue) {
        if (reactive) reaction(value, aux);
        value = aux;
    }

    return *this;
}

ReactString& ReactString::operator+=(char c) {
    std::string tmpValue = getterNSetter ? getter() : value ;
    std::string aux = tmpValue + c;

    if (aux != tmpValue) {
        if (reactive) reaction(value, aux);
        value = aux;
    }

    return *this;
}

ReactString & ReactString::operator+=(const ReactString &rhs) {
    std::string tmpValue = getterNSetter ? getter() : value ;
    std::string aux = tmpValue + (rhs.getterNSetter ? rhs.getter() : rhs.value);

    if (aux != tmpValue) {
        if (reactive) reaction(value, aux);
        value = aux;
    }

    return *this;
}

std::string ReactString::operator+(const ReactString &rhs) const {
    std::string aux = getterNSetter ? getter() : value;
    return aux + (rhs.getterNSetter ? rhs.getter() : rhs.value );
}


/*std::string operator+ (const ReactString& lhs, const std::string& rhs) {
    std::string aux = lhs.getterNSetter ? lhs.getter() : lhs.value;
    return aux + rhs;
}*/

std::string operator+ (ReactString&& lhs, std::string&& rhs) {
    std::string aux = lhs.getterNSetter ? lhs.getter() : lhs.value;
    return aux + rhs;
}

/*std::string operator+ (std::string&& lhs, const ReactString& rhs) {
    std::string aux = rhs.getterNSetter ? rhs.getter() : rhs.value;
    return lhs + aux;
}*/

/*std::string operator+ (const std::string& lhs, ReactString&& rhs) {
    std::string aux = rhs.getterNSetter ? rhs.getter() : rhs.value;
    return lhs + aux;
}*/


/*std::string operator+ (const ReactString& lhs, const char* rhs) {
    std::string aux = lhs.getterNSetter ? lhs.getter() : lhs.value;
    return aux + rhs;
}*/

std::string operator+ (ReactString&& lhs, const char* rhs) {
    std::string aux = lhs.getterNSetter ? lhs.getter() : lhs.value;
    return aux + rhs;
}

std::string operator+ (const char* lhs, const ReactString& rhs) {
    std::string aux = rhs.getterNSetter ? rhs.getter() : rhs.value;
    return lhs + aux;
}

std::string operator+ (const char* lhs, ReactString&& rhs) {
    std::string aux = rhs.getterNSetter ? rhs.getter() : rhs.value;
    return lhs + aux;
}


std::string operator+ (const ReactString& lhs, char rhs) {
    std::string aux = lhs.getterNSetter ? lhs.getter() : lhs.value;
    return aux + rhs;
}

std::string operator+ (ReactString&& lhs, char rhs) {
    std::string aux = lhs.getterNSetter ? lhs.getter() : lhs.value;
    return aux + rhs;
}

std::string operator+ (char lhs, const ReactString& rhs) {
    std::string aux = rhs.getterNSetter ? rhs.getter() : rhs.value;
    return lhs + aux;
}

std::string operator+ (char lhs, ReactString&& rhs) {
    std::string aux = rhs.getterNSetter ? rhs.getter() : rhs.value;
    return lhs + aux;
}


std::ostream& operator<<(std::ostream &out, const ReactString &hs) {
    out << (hs.getterNSetter ? hs.getter() : hs.value);
    return out;
}

std::istream & operator>>(std::istream &in, ReactString &hs) {
    std::string aux;
    in >> aux;

    std::string tmpValue = hs.getterNSetter ? hs.getter() : hs.value ;

    if (aux != tmpValue) {
        if (hs.reactive) hs.reaction(hs.value, aux);
        hs.value = aux;
    }

    return in;
}

ReactString::operator std::string () const {
    return getterNSetter ? getter() : value ;
}

size_t ReactString::size() const {
    return length();
}

size_t ReactString::length() const {
    std::string tmpValue = getterNSetter ? getter() : value ;
    return tmpValue.size();
}