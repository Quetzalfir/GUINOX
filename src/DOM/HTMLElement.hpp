/**
* @brief The HTMLElement interface represents any HTML element.
*
* Some elements implement this interface directly, others implement it through an interface that inherits from it.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
*/
#ifndef GUINOX_HTMLELEMENT_HPP
#define GUINOX_HTMLELEMENT_HPP


#include "Element.hpp"
#include "../CSSOM/ElementCSSInlineStyle.hpp"
#include "../RenderContext/ElementFrame.hpp"
#include "../RenderContext/ElementRenderizable.hpp"

class HTMLElement :
        public DOM_Node::Element,
        public ElementCSSInlineStyle,
        protected ElementFrame,
        protected ElementRenderizable {

    friend class Presentation;
    friend class RenderFrame::Frame;
    friend class DOM_Node::Element;
    friend class RenderFrame::Block;


    Element *_offsetParent;
    long _offsetTop;
    long _offsetLeft;
    long _offsetWidth;
    long _offsetHeight;


protected:
    std::string _accessKeyLabel;


    HTMLElement(const std::string & tagName);

    bool attributeChangedCallback(DOMString AttrName, DOMString oldvalue, DOMString newValue) override ;

    //Node Life Events
    //void insertion_steps() override ;
    void children_changed_steps() override ;


public:

    // CSSOM box model attributes
    const long & offsetTop = _offsetTop;
    const long & offsetLeft = _offsetLeft;
    const long & offsetWidth = _offsetWidth;
    const long & offsetHeight = _offsetHeight;

    // metadata attributes
    std::string title;
    std::string lang;
    bool translate;
    std::string dir;

    // user interaction
    bool hidden;
    char accessKey;
    const std::string & accessKeyLabel = _accessKeyLabel;
    bool draggable;
    bool spellcheck;
    std::string autocapitalize;




    DOM_Node::sp_Element offsetParent();

    void click();
    void blur();
    void focus();


    // Node inherit
    DOM_Node::sp_Node cloneNode(bool deep) const override;


};




/**
 * @brief The HTMLUnknownElement interface represents an invalid HTML element and derives
 * from the HTMLElement interface, but without implementing any additional properties or methods.
 *
 * @author GUINOX
 * @version 1.0
 * @date 24/02/2020
 *
 * @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/HTMLUnknownElement
 */
class HTMLUnknownElement : public HTMLElement {
    friend class ElementRegistry;


protected:
    explicit HTMLUnknownElement(const std::string & tag) : HTMLElement(tag) {}
};


#endif //GUINOX_HTMLELEMENT_HPP
