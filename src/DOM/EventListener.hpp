/**
* @brief This is EventListener class.
*
* The EventListener interface represents an object that
* can handle an event dispatched by an EventTarget object.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/EventListener
*/
#ifndef GUINOX_EVENTLISTENER_H
#define GUINOX_EVENTLISTENER_H

#include "../Events/Event.hpp"
#include <functional>


class EventListener {

public:
    std::string name;
    std::function<void(Event *)> fn;

    inline bool operator==( const EventListener & rhs ) const { return name == rhs.name; }
    inline bool operator!=( const EventListener & rhs ) const { return !(this->operator==(rhs)); }
    inline void operator()(Event * event) { fn(event); }

};


#endif //GUINOX_EVENTLISTENER_H
