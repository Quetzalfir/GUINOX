#ifndef GUINOX_NODEFILTER_HPP
#define GUINOX_NODEFILTER_HPP

#include "dom_declarations.hpp"
#include <functional>

/**
* @brief This is NodeFilter class.
*
* A NodeFilter interface represents an object used to filter
* the nodes in a NodeIterator or TreeWalker.
* A NodeFilter knows nothing about the document or traversing nodes;
* it only knows how to evaluate a single node against
* the provided filter.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/NodeFilter
*/
struct NodeFilter {

public:

    // Constants for acceptNode()
    enum FilterResp : unsigned short {
        FILTER_ACCEPT = 1,
        FILTER_REJECT = 2,
        FILTER_SKIP = 3
    };


private:
    static inline FilterResp acceptAll(const DOM_Node::sp_Node &node) { return FILTER_ACCEPT; }


public:

    // Constants for whatToShow
    enum WhatToShow : unsigned long {
        SHOW_ALL = 0xFFFFFFFF,
        SHOW_ELEMENT = 0x1,
        SHOW_ATTRIBUTE = 0x2,
        SHOW_TEXT = 0x4,
        SHOW_CDATA_SECTION = 0x8,
        SHOW_ENTITY_REFERENCE = 0x10, // historical
        SHOW_ENTITY = 0x20, // historical
        SHOW_PROCESSING_INSTRUCTION = 0x40,
        SHOW_COMMENT = 0x80,
        SHOW_DOCUMENT = 0x100,
        SHOW_DOCUMENT_TYPE = 0x200,
        SHOW_DOCUMENT_FRAGMENT = 0x400,
        SHOW_NOTATION = 0x800 // historical
    };

    std::function<FilterResp(const DOM_Node::sp_Node &)> acceptNode = acceptAll;


    NodeFilter() = default ;
    explicit NodeFilter(std::function<FilterResp(const DOM_Node::sp_Node &)> acceptNode_) : acceptNode(std::move(acceptNode_)) {}

    FilterResp operator()(const DOM_Node::sp_Node & node) const { return acceptNode(node); }

};


#endif //GUINOX_NODEFILTER_HPP
