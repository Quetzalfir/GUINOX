#include "Traversal.hpp"
#include "Node.hpp"


Traversal::Traversal(DOM_Node::sp_Node root_, unsigned long whatToShow_, const NodeFilter &filter_) {
    _root = std::move(root_);
    _whatToShow = whatToShow_;
    _filter = filter_;
}

NodeFilter::FilterResp Traversal::filtering(const DOM_Node::sp_Node & node) {
    if (active) throw "InvalidStateError"; // DOMException.
    if (node->nodeType == 0) throw "InvalidStateError"; // DOMException. todo, check type

    unsigned int n = node->nodeType;

    unsigned long a = 1;
    if (n <= 12 && !(_whatToShow & a << (n-1))) return NodeFilter::FILTER_SKIP;

    active = true;

    NodeFilter::FilterResp result;
    try {
        result = _filter(node);

    } catch (std::exception &e) {
        active = false;
        throw e;
    }

    active = false;

    return result;
}