/**
* @brief This is a NodeIterator class.
*
* The NodeIterator interface represents an iterator
* over the members of a list of the nodes in a subtree of the DOM.
* The nodes will be returned in document order.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/NodeIterator
*/
#ifndef GUINOX_NODEITERATOR_HPP
#define GUINOX_NODEITERATOR_HPP


#include <vector>
#include "Traversal.hpp"

class NodeIterator : public Traversal {

    friend class DOM_Node::Document;
    friend class MutationAlgorithms;

    DOM_Node::sp_Node _referenceNode;
    bool _pointerBeforeReferenceNode = true;
    std::vector<DOM_Node::sp_Node> collection;


    void pre_removing(DOM_Node::sp_Node toBeRemovedNode);
    DOM_Node::sp_Node traverse(bool next);


protected:
    NodeIterator(DOM_Node::sp_Node root, unsigned long whatToShow, const NodeFilter & filter);


public:
    const DOM_Node::sp_Node &referenceNode = _referenceNode;
    const bool & pointerBeforeReferenceNode = _pointerBeforeReferenceNode;


    ~NodeIterator();

    DOM_Node::sp_Node nextNode();
    DOM_Node::sp_Node previousNode();

};

typedef std::shared_ptr<NodeIterator> sp_NodeIterator;


#endif //GUINOX_NODEITERATOR_HPP
