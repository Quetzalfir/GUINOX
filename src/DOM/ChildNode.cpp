#include "ChildNode.hpp"
#include <utility>
#include "Text.hpp"
#include "MutationAlgorithms.hpp"
#include <algorithm>

void ChildNode::after(DOM_Node::sp_Node node) {
    auto parent = this->_parentNode;

    if (!parent) return;

    auto viableNextSibling = nextSibling();

    MutationAlgorithms(parent).pre_insert(node, viableNextSibling);
}

void ChildNode::after(DOM_Node::NodeList &nodes) {
    auto parent = this->_parentNode;

    if (!parent) return;

    auto viableNextSibling = nextSibling();

    //Let viableNextSibling be this’s first following sibling not in nodes, and null otherwise.
    auto nodesBegin = nodes.begin();
    auto nodesEnd = nodes.end();
    while (viableNextSibling && (std::find(nodesBegin, nodesEnd, viableNextSibling) != nodesEnd)) {
        viableNextSibling = viableNextSibling->nextSibling();
    }

    auto node = MutationAlgorithms::convert_nodes_into_a_node(nodes, _ownerDocument);

    MutationAlgorithms(parent).pre_insert(node, viableNextSibling);
}

void ChildNode::after(const std::string &text) {
    DOM_Node::sp_Node node = std::make_shared<DOM_Node::Text>(text);
    after(node);
}

void ChildNode::before(DOM_Node::sp_Node node) {
    auto parent = this->_parentNode;

    if (!parent) return;

    auto viablePreviousSibling = previousSibling();

    viablePreviousSibling = !viablePreviousSibling ? parent->firstChild() : viablePreviousSibling->nextSibling() ;

    MutationAlgorithms(parent).pre_insert(node, viablePreviousSibling);
}

void ChildNode::before(DOM_Node::NodeList &nodes) {
    auto parent = this->_parentNode;

    if (!parent) return;

    auto viablePreviousSibling = previousSibling();

    // Let viablePreviousSibling be this’s first preceding sibling not in nodes, and null otherwise.
    auto nodesBegin = nodes.begin();
    auto nodesEnd = nodes.end();
    while (viablePreviousSibling && (std::find(nodesBegin, nodesEnd, viablePreviousSibling) != nodesEnd)) {
        viablePreviousSibling = viablePreviousSibling->previousSibling();
    }

    auto node = MutationAlgorithms::convert_nodes_into_a_node(nodes, _ownerDocument);

    viablePreviousSibling = !viablePreviousSibling ? parent->firstChild() : viablePreviousSibling->nextSibling() ;

    MutationAlgorithms(parent).pre_insert(node, viablePreviousSibling);
}

void ChildNode::before(const std::string &text) {
    DOM_Node::sp_Node node = std::make_shared<DOM_Node::Text>(text);
    before(node);
}

void ChildNode::remove() {
    MutationAlgorithms::remove(this);
}

void ChildNode::replaceWith(DOM_Node::sp_Node node) {
    auto parent = this->_parentNode;

    if (!parent) return;

    auto viableNextSibling = nextSibling();

    if (this->_parentNode == parent) {
        MutationAlgorithms(parent).replace(this, node.get());

    } else {
        MutationAlgorithms(parent).pre_insert(node, viableNextSibling);
    }
}

void ChildNode::replaceWith(DOM_Node::NodeList &nodes) {
    auto parent = this->_parentNode;

    if (!parent) return;

    auto viableNextSibling = nextSibling();

    //Let viableNextSibling be this’s first following sibling not in nodes, and null otherwise.
    auto nodesBegin = nodes.begin();
    auto nodesEnd = nodes.end();
    while (viableNextSibling && (std::find(nodesBegin, nodesEnd, viableNextSibling) != nodesEnd)) {
        viableNextSibling = viableNextSibling->nextSibling();
    }

    auto node = MutationAlgorithms::convert_nodes_into_a_node(nodes, _ownerDocument);

    if (this->_parentNode == parent) {
        MutationAlgorithms(parent).replace(this, node.get());

    } else {
        MutationAlgorithms(parent).pre_insert(node, viableNextSibling);
    }
}

void ChildNode::replaceWith(const std::string &text) {
    DOM_Node::sp_Node node = std::make_shared<DOM_Node::Text>(text);
    replaceWith(node);
}