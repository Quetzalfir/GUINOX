#include "TreeWalker.hpp"
#include "Node.hpp"
#include <utility>


TreeWalker::TreeWalker(DOM_Node::sp_Node root_, unsigned long whatToShow_, const NodeFilter &filter_) :
                Traversal(root_, whatToShow_, filter_), currentNode(root_) {

}

DOM_Node::sp_Node TreeWalker::parentNode() {
    auto node = currentNode;

    while(node && node != _root) {
        node = node->parentNode();
        if (node && filtering(node) == NodeFilter::FILTER_ACCEPT) {
            currentNode = node;
            return node;
        }
    }

    return nullptr;
}

DOM_Node::sp_Node TreeWalker::traverse_children(bool first) {
    auto node = currentNode;

    node = first ? node->firstChild() : node->lastChild() ;

    while (node) {
        auto result = filtering(node);

        if (result == NodeFilter::FILTER_ACCEPT) {
            currentNode = node;
            return node;

        } else if (result == NodeFilter::FILTER_SKIP) {
            auto child = first ? node->firstChild() : node->lastChild() ;
            if (child) {
                node = child;
                continue;
            }
        }

        do {
            auto sibling = first ? node->nextSibling() : node->previousSibling() ;

            if (sibling) {
                node = sibling;
                break;
            }

            auto parent = node->parentNode();

            if (!parent || parent == _root || parent == currentNode) {
                return nullptr;
            }

            node = parent;
        } while (node);
    }

    return nullptr;
}

DOM_Node::sp_Node TreeWalker::firstChild() {
    return traverse_children(true);
}

DOM_Node::sp_Node TreeWalker::lastChild() {
    return traverse_children(false);
}

DOM_Node::sp_Node TreeWalker::traverse_siblings(bool next) {
    auto node = currentNode;

    if (node == _root) return nullptr;

    while (true) {
        auto sibling = next ? node->nextSibling() : node->previousSibling() ;

        while (sibling) {
            node = sibling;

            auto result = filtering(node); //todo, if filtering throw then return nullptr

            if (result == NodeFilter::FILTER_ACCEPT) {
                currentNode = node;
                return node;
            }

            sibling = next ? node->firstChild() : node->lastChild() ;

            if (result == NodeFilter::FILTER_REJECT || !sibling) {
                sibling = next ? node->nextSibling() : node->previousSibling() ;
            }
        }

        node = node->parentNode();

        if (!node || node == _root) return nullptr;

        if (filtering(node) == NodeFilter::FILTER_ACCEPT) return nullptr;
    }
}

DOM_Node::sp_Node TreeWalker::nextSibling() {
    return traverse_siblings(true);
}

DOM_Node::sp_Node TreeWalker::previousSibling() {
    return traverse_siblings(false);
}

DOM_Node::sp_Node TreeWalker::previousNode() {
    auto node = currentNode;

    while (node != _root) {
        while (auto sibling = node->previousSibling()) {
            node = sibling;

            auto result = filtering(node);

            while (result != NodeFilter::FILTER_REJECT && !node->childNodes.empty()) {
                node = node->lastChild();
                result = filtering(node);
            }

            if (result == NodeFilter::FILTER_ACCEPT) {
                currentNode = node;
                return node;
            }
        }

        if (node == _root || !node->parentNode()) {
            return nullptr;
        }

        node = node->parentNode();
        if (!node) return nullptr;

        if (filtering(node) == NodeFilter::FILTER_ACCEPT) {
            currentNode = node;
            return node;
        }
    }

    return nullptr;
}

DOM_Node::sp_Node TreeWalker::nextNode() {
    auto node = currentNode;
    auto result = NodeFilter::FILTER_ACCEPT;

    while (true) {
        while (result != NodeFilter::FILTER_REJECT && !node->childNodes.empty()) {
            node = node->firstChild();

            result = filtering(node);

            if (result == NodeFilter::FILTER_ACCEPT) {
                currentNode = node;
                return node;
            }
        }

        DOM_Node::sp_Node sibling = nullptr;
        auto temporary = node;

        do {
            if (temporary == _root) return nullptr;

            sibling = temporary->nextSibling();

            if (sibling) break;

            temporary = temporary->parentNode();
        } while (temporary);

        if (!sibling) break;
        node = sibling;

        result = filtering(node);

        if (result == NodeFilter::FILTER_ACCEPT) {
            currentNode = node;
            return node;
        }
    }

    return nullptr;
}