#include "HTMLElement.hpp"
#include "ElementRegistry.hpp"
#include "Document.hpp"


HTMLElement::HTMLElement(const std::string &tagName_) : ElementFrame(this) {
    _nodeName = tagName_;
}

DOM_Node::sp_Element HTMLElement::offsetParent() {
    if (_offsetParent) {
        return std::dynamic_pointer_cast<DOM_Node::Element>(_offsetParent->shared_from_this());
    } else {
        return nullptr;
    }
}

DOM_Node::sp_Node HTMLElement::cloneNode(bool deep) const {
    auto res = ElementRegistry::get(this->_nodeName);

    clone(res.get(), deep);

    return res;
}

bool HTMLElement::attributeChangedCallback(const std::string &AttrName, const std::string &oldvalue,
                                           const std::string &newValue) {

    if (!Element::attributeChangedCallback(AttrName, oldvalue, newValue)) {
        if (AttrName == "style") {
            //todo
            //If the readonly flag is set, then return.
            //If the updating flag is set, then return.
            //If localName is not "style", or namespace is not null, then return.
            style.cssText = newValue;
        }
    }

    if (_ownerDocument && !_ownerDocument->_building) {
        dirty();
    }

    return false;
}

void HTMLElement::children_changed_steps() {
    Element::children_changed_steps();

    if (_ownerDocument && !_ownerDocument->_building) {
        childrenDirty();
    }
}