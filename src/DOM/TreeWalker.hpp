/**
* @brief This is TreeWalker class.
*
* The TreeWalker object represents the nodes of a document
 * subtree and a position within them.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
* @details Is based on https://developer.mozilla.org/es/docs/Web/API/TreeWalker
*/
#ifndef GUINOX_TREEWALKER_HPP
#define GUINOX_TREEWALKER_HPP

#include "Traversal.hpp"

class TreeWalker : public Traversal {

    DOM_Node::sp_Node traverse_children(bool first);
    DOM_Node::sp_Node traverse_siblings(bool next);


public:

    DOM_Node::sp_Node currentNode;


    TreeWalker(DOM_Node::sp_Node root, unsigned long whatToShow, const NodeFilter & filter);


    DOM_Node::sp_Node parentNode();
    DOM_Node::sp_Node firstChild();
    DOM_Node::sp_Node lastChild();
    DOM_Node::sp_Node previousSibling();
    DOM_Node::sp_Node nextSibling();
    DOM_Node::sp_Node previousNode();
    DOM_Node::sp_Node nextNode();

};


#endif //GUINOX_TREEWALKER_HPP
