#ifndef GUINOX_SLOTABLE_HPP
#define GUINOX_SLOTABLE_HPP

#include <memory>
#include "Node.hpp"
#include <vector>

class HTMLSlotElement;

/**
* @brief This is a Slotable class.
*
* The Slotable mixin defines features that allow nodes to become the contents of a <slot> element — the following features are included in both Element and Text.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/es/docs/Web/API/Slotable
*/
class Slotable : public virtual DOM_Node::Node {

    friend class MutationAlgorithms;
    friend class HTMLSlotElement;


    static std::shared_ptr<HTMLSlotElement> find_slot(const DOM_Node::sp_Node & slotable, bool open = false);

    static void assign_slotables_for_tree(DOM_Node::sp_Node root);


    std::shared_ptr<HTMLSlotElement> _assignedSlot = nullptr;


protected:

    static void assign_slotables(std::shared_ptr<HTMLSlotElement> slot);
    static std::vector<DOM_Node::sp_Node> find_slotables(DOM_Node::sp_Node slot);


    Slotable() = default;

    void assign_slot();


public:

    const std::shared_ptr<HTMLSlotElement> & assignedSlot = _assignedSlot;
};


#endif //GUINOX_SLOTABLE_HPP
