/**
* @brief CharacterData is an abstract interface.
*
* The CharacterData abstract interface represents a Node object
 * that contains characters. This is an abstract interface,
 * meaning there aren't any object of type CharacterData:
 * it is implemented by other interfaces,
 * like Text, Comment, or ProcessingInstruction which aren't abstract.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/CharacterData
*/
#ifndef GUINOX_CHARACTERDATA_HPP
#define GUINOX_CHARACTERDATA_HPP


#include "Node.hpp"
#include "ChildNode.hpp"
#include "NonDocumentTypeChildNode.hpp"
#include "ReactString.hpp"

class CharacterData :
        public virtual DOM_Node::Node,
        public ChildNode,
        public NonDocumentTypeChildNode {

    std::string _data;

    std::string dataGetter();
    void dataSetter(const std::string & oldValue, const std::string & newValue);

    static std::string sanitizeData(const std::string & str);


public:
    CharacterData();

    ReactString data;
    inline size_t length() const override { return _data.size(); }

    std::string substringData(unsigned long offset, unsigned long count) const ;
    void appendData(const std::string & data);
    void insertData(unsigned long offset, const std::string & data);
    void deleteData(unsigned long offset, unsigned long count);
    void replaceData(unsigned long offset, unsigned long count, const std::string & data);


};


#endif //GUINOX_CHARACTERDATA_HPP
