/**
* @brief Interface
*
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://dom.spec.whatwg.org/#nonelementparentnode
*/
#ifndef GUINOX_NONELEMENTPARENTNODE_HPP
#define GUINOX_NONELEMENTPARENTNODE_HPP


#include "dom_declarations.hpp"


class NonElementParentNode {

public:
    virtual DOM_Node::sp_Element getElementById(const std::string & elementId) const = 0;

};


#endif //GUINOX_NONELEMENTPARENTNODE_HPP
