/**
 * @brief DOM_Node is a namespace that encompasses all types of nodes that belong to the DOM.
 */

#ifndef GUINOX_DOM_NODE_TEXT_H
#define GUINOX_DOM_NODE_TEXT_H

#include "Slotable.hpp"
#include "CharacterData.hpp"


namespace DOM_Node {
/**
* @brief The Text interface represents the textual content
* of Element or Attr.
*
* If an element has no markup within its content,
* it has a single child implementing Text that contains
* the element's text. However, if the element contains markup,
* it is parsed into information items and Text nodes that
* form its children.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Text
*/
    class Text :
            public CharacterData,
            public Slotable {

        short currentLine = 0;
        int size;
        double factor;
        bool breakable = true;
        bool changed = true;
        bool fillBox = false;
        bool border = false;


    public:


        explicit Text(const std::string &str);
        virtual ~Text();


        // Node inherit
        DOM_Node::sp_Node cloneNode(bool deep) const override;
        bool isEqualNode(sp_Node node) const override;


        sp_Text splitText(unsigned long offset);
        std::string wholeText() const ;
    };
}


#endif //GUINOX_DOM_NODE_TEXT_H
