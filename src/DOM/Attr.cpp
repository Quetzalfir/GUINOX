#include "Attr.hpp"
#include "MutationAlgorithms.hpp"
#include "Element.hpp"
#include <algorithm>
#include <Utils/Utils.hpp>


Attr::Attr(const std::string &name_) :
                _name(Utils::toLower(name_)),
                value("",[this](auto && PH1, auto && PH2) { handle_attribute_changes(PH1, PH2); }) {

}

void Attr::handle_attribute_changes(const std::string & oldValue, const std::string & newValue) {
    //todo, Queue a mutation record of "attributes" for element with attribute’s local name, attribute’s namespace, oldValue, « », « », null, and null.

    if (ownerElement) {
        MutationAlgorithms(ownerElement).attributeChangedCallback(_name, oldValue, newValue);
    }
}

Attr& Attr::operator=(const Attr &rhs) {
    if (&rhs != this) {
        _name = rhs._name;
        referenced = rhs.referenced;
        defalt = rhs.defalt;
        value = rhs.value;
    }

    return *this;
}

class Attr & Attr::operator=(class Attr && ths) noexcept {
    _name = std::move(ths._name);
    referenced = ths.referenced;
    defalt = std::move(ths.defalt);
    value = std::move(ths.value);

    return *this;
}