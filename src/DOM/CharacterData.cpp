#include "CharacterData.hpp"
#include "MutationAlgorithms.hpp"
#include <regex>



CharacterData::CharacterData() : data([this] { return dataGetter(); },
                                      [this](auto && PH1, auto && PH2) { dataSetter(PH1, PH2); }) {}

std::string CharacterData::dataGetter() {
    return _data;
}

void CharacterData::dataSetter(const std::string &oldValue, const std::string &newValue) {
    auto aux = sanitizeData(newValue);

    replaceData(0, aux.length(), aux);
}

std::string CharacterData::sanitizeData(const std::string &str) {
    //fixme, in nodes text of style it remove repeated spaces inside properties with quotes
    // example => image: url("path/ with    multiple  spaces   ");
    std::regex blanks(" |\n|\r|\t");

    std::string result;
    std::regex_replace (std::back_inserter(result), str.begin(), str.end(), blanks, " ");

    std::string::iterator new_end = std::unique(result.begin(), result.end(),
             [](char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); });
    result.erase(new_end, result.end());

    return result;
}




std::string CharacterData::substringData(unsigned long offset, unsigned long count) const {
    if (offset > _data.length()) {
        throw "IndexSizeError"; // DOMException.
    }

    return _data.substr(offset, offset+count);
}

void CharacterData::replaceData(unsigned long offset, unsigned long count, const std::string &str) {
    size_t size = _data.size();

    if (offset > size) {
        //throw an IndexSizeError.
        return;
    }

    if (offset + count > size) count = size - offset;

    auto oldValue = _data;

    _data.replace(offset, count, str);

    if (oldValue == _data) return;

    //todo, Queue a mutation record of "characterData" for node with oldValue node’s data.

    MutationAlgorithms(this).forEachLiveRange([=](sp_Range & range) {
        if (range->start.node.get() == this && range->startOffset > offset && range->startOffset <= offset + count) {
            range->start.offset = offset;
        }

        if (range->end.node.get() == this && range->endOffset > offset && range->endOffset <= offset + count) {
            range->end.offset = offset;
        }

        if (range->start.node.get() == this && range->startOffset > offset + count) {
            range->start.offset += _data.length();
            range->start.offset -= count;
        }

        if (range->end.node.get() == this && range->endOffset > offset + count) {
            range->end.offset += _data.length();
            range->end.offset -= count;
        }
    });

    if (_parentNode) {
        MutationAlgorithms(_parentNode).children_changed_steps();
    }
}

void CharacterData::appendData(const std::string &data) {
    replaceData(_data.length(), 0, data);
}

void CharacterData::insertData(unsigned long offset, const std::string &data) {
    replaceData(offset, 0, data);
}

void CharacterData::deleteData(unsigned long offset, unsigned long count) {
    replaceData(offset, count, "");
}