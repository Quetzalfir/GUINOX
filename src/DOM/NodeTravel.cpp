#include "NodeTravel.hpp"
#include "Node.hpp"
#include "Element.hpp"
#include "ShadowRoot.hpp"


void NodeTravel::ForEach::shadow_including_inclusive_descendants(const std::function<void(DOM_Node::Node * )> &cb) {
    cb(self);

    if (self->_nodeType == DOM_Node::Node::ELEMENT_NODE) {
        auto elem = dynamic_cast<DOM_Node::Element *>(self);
        if (elem->_shadowRoot) {
            NodeTravel(elem->_shadowRoot).forEach.shadow_including_inclusive_descendants(cb);
        }
    }

    for (auto & child : self->_childNodes) {
        NodeTravel(child).forEach.shadow_including_inclusive_descendants(cb);
    }
}

void NodeTravel::ForEach::shadow_including_descendants(const std::function<void(DOM_Node::Node * )> &cb) {
    if (self->_nodeType == DOM_Node::Node::ELEMENT_NODE) {
        auto elem = dynamic_cast<DOM_Node::Element *>(self);
        if (elem->_shadowRoot) {
            cb(elem->_shadowRoot.get());
            NodeTravel(elem->_shadowRoot).forEach.shadow_including_descendants(cb);
        }
    }

    for (auto & child : self->_childNodes) {
        cb(child.get());
        NodeTravel(child).forEach.shadow_including_descendants(cb);
    }
}

void NodeTravel::ForEach::inclusive_descendants(const std::function<void (DOM_Node::Node *)> &cb) {
    cb(self);

    for (auto & child : self->_childNodes) {
        NodeTravel(child).forEach.inclusive_descendants(cb);
    }
}

void NodeTravel::ForEach::descendants(const std::function<void (DOM_Node::Node *)> &cb) {
    for (auto & child : self->_childNodes) {
        cb(child.get());
        NodeTravel(child).forEach.descendants(cb);
    }
}

void NodeTravel::ForEach::inclusive_ancestor(const std::function<void(DOM_Node::Node *)> &cb) {
    DOM_Node::Node * aux = self;

    while (aux) {
        cb(aux);
        aux = aux->_parentNode;
    }
}

DOM_Node::Node * NodeTravel::root() const {
    auto * aux = const_cast<DOM_Node::Node *>(self);

    while (aux->_parentNode) {
        aux = aux->_parentNode;
    }

    return aux;
}

DOM_Node::Node * NodeTravel::shadow_including_root() const {
    DOM_Node::Node *root = this->root();

    if (root->nodeType == DOM_Node::Node::DOCUMENT_FRAGMENT_NODE) {
        auto shadow = dynamic_cast<ShadowRoot *>(root);
        if (shadow) {
            auto host = shadow->host();

            if (host) {
                return NodeTravel(host).shadow_including_root();
            }
        }
    }

    return root;
}

bool NodeTravel::host_including_inclusive_ancestor(const DOM_Node::Node *node) const {
    if (!node) return false;

    auto *aux = const_cast<DOM_Node::Node *>(node);

    while (aux->_parentNode && aux != self) {
        aux = aux->_parentNode;
    }

    bool f = aux == self;

    if (!f) {
        if (aux->_nodeType == DOM_Node::Node::DOCUMENT_FRAGMENT_NODE) {
            auto docFrag = dynamic_cast<DOM_Node::DocumentFragment *>(aux);
            auto host = const_cast<DOM_Node::Element *>(docFrag->host().get());
            f = this->host_including_inclusive_ancestor(host);
        }
    }

    return f;
}

bool NodeTravel::inclusive_descendant(const DOM_Node::Node *node) const {
    if (!node) throw "nullptr";

    const DOM_Node::Node *aux = self;

    while (aux && aux != node) {
        aux = aux->_parentNode;
    }

    return aux == node;
}

bool NodeTravel::inclusive_ancestor(const DOM_Node::Node *node) const {
    return NodeTravel(const_cast<DOM_Node::Node *>(node)).inclusive_descendant(self);
}

bool NodeTravel::ancestor(const DOM_Node::Node *node) const {
    return NodeTravel(const_cast<DOM_Node::Node *>(self)).inclusive_ancestor(node->parentNode());
}

size_t NodeTravel::index() const {
    auto aux = self;
    size_t cont = 0;


    while (aux->_previousSibling) {
        cont++;
        aux = aux->_previousSibling;
    }

    return cont;
}

DOM_Node::Node* NodeTravel::childAt(size_t index) const {
    DOM_Node::Node *aux = self->firstChild().get();

    while (aux && index--) {
        aux = aux->_nextSibling;
    }

    return aux;
}

DOM_Node::Node* NodeTravel::following(DOM_Node::Node * aRoot) const {
    DOM_Node::Node *node = self;

    if (!node->childNodes.empty()) {
        return node->firstChild().get();
    }

    DOM_Node::Node *sibling = nullptr;

    do {
        if (node == aRoot) return nullptr;

        sibling = node->_nextSibling;

        if (sibling) break;

        node = node->_parentNode;
    } while (node);

    return sibling;
}

DOM_Node::Node* NodeTravel::preceding(DOM_Node::Node * aRoot) const {
    auto node = self;

    if (node == aRoot) return nullptr;

    if (auto sibling = node->_previousSibling) {
        node = sibling;

        while (!node->childNodes.empty()) {
            node = node->lastChild().get();
        }

        return node;
    }

    return node->_parentNode;
}

std::deque<DOM_Node::Node *> NodeTravel::pathFromRoot() const {
    std::deque<DOM_Node::Node *> vec;

    DOM_Node::Node * aux = self;
    while (aux) {
        vec.push_front(aux);
        aux = aux->_parentNode;
    }

    return vec;
}

bool NodeTravel::isSibling(const DOM_Node::sp_Node &node) const {
    if (!node) return false;

    if (!self->_parentNode) return false;
    if (!node->_parentNode) return false;

    return self->_parentNode == node->_parentNode;
}