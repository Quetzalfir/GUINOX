#include "DocumentType.hpp"

namespace DOM_Node {

    DocumentType::DocumentType(std::string name) : _name(std::move(name)) {
        _nodeType = DOCUMENT_TYPE_NODE;
        _nodeName = _name;
    }

    sp_Node DocumentType::cloneNode(bool deep) const {
        auto res = std::shared_ptr<DocumentType>( new DocumentType(_name) );

        clone(res.get(), deep);

        return res;
    }

    void DocumentType::clone(Node *copy, bool deep) const {
        Node::clone(copy, deep);

        auto *docType = dynamic_cast<DocumentType *>(copy);

        docType->_publicId = _publicId;
        docType->_systemId = _systemId;
    }

    bool DocumentType::isEqualNode(DOM_Node::sp_Node node) const {
        if (Node::isEqualNode(node)) {
            auto docType = dynamic_cast<DocumentType *>(node.get());

            return _name == docType->_name &&
                   _publicId == docType->_publicId &&
                   _systemId == docType->_systemId;

        }

        return false;
    }

}