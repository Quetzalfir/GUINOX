/**
* @brief NonDocumentTypeChildNode is a raw interface and no object
* of this type can be created
*
* The NonDocumentTypeChildNode interface contains methods that
* are particular to Node objects that can have a parent, but not suitable for DocumentType.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/NonDocumentTypeChildNode
*/
#ifndef GUINOX_NONDOCUMENTTYPECHILDNODE_HPP
#define GUINOX_NONDOCUMENTTYPECHILDNODE_HPP


#include "Node.hpp"


class NonDocumentTypeChildNode : public virtual DOM_Node::Node {

public:
    DOM_Node::sp_Element nextElementSibling() const ;
    DOM_Node::sp_Element previousElementSibling() const ;

};

#endif //GUINOX_NONDOCUMENTTYPECHILDNODE_HPP
