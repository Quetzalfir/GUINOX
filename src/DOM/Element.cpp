#include "Element.hpp"
#include "Document.hpp"
#include "NodeTravel.hpp"
#include "MutationAlgorithms.hpp"
#include "Text.hpp"
#include "../Utils/Utils.hpp"
#include "XMLSerializer.hpp"
#include "DOMParser.hpp"
#include <algorithm>
#include <CSSOM/CSSSelector.hpp>


namespace DOM_Node {

    void Element::eachB(std::function<bool(sp_Element &)> toDo) {
        for (auto & child : _children) {
            if (toDo(child)) { break; }
        }
    }

    void Element::each(std::function<void(sp_Element &)> toDo) {
        for (auto & child : _children) {
            toDo(child);
        }
    }

    sp_Element Element::getElementById(const std::string &id_) const {
        return cast2sp_Element(getElementById2(id_));
    }

    Element* Element::getElementById2(const std::string &id_) const {
        if (this->id == id_) {
            return const_cast<Element *>(this);
        }

        for (auto & child : _children) {
            auto result = child->getElementById2(id_);
            if (result) {
                return result;
            };
        }

        return nullptr;
    }

    HTMLCollection Element::getElementsByTagName(const std::string &tagName_) const {
        HTMLCollection elements;
        getElementsByTagName(tagName_, elements);

        return elements;
    }

    void Element::getElementsByTagName(const std::string &tagName_, HTMLCollection &vec) const {
        for (auto & child : _children) {
            if (child->tagName == tagName_) {
                vec.push_back(child);
            }
            child->getElementsByTagName(tagName_, vec);
        }
    }

    HTMLCollection Element::getElementsByClassName(const std::string &clas) const {
        HTMLCollection elements;
        getElementsByClassName(clas, elements);

        return elements;
    }

    void Element::getElementsByClassName(const std::string &class_, HTMLCollection &vec) const {
        for (auto & child : _children) {
            if (child->classList.contains(class_)) {
                vec.push_back(child);
            }
            child->getElementsByClassName(class_, vec);
        }
    }

    HTMLCollection Element::getElementsBy(const std::function<bool(const sp_Element &)> &fn) {
        HTMLCollection vec;
        getElementsBy(fn, vec);
        return vec;
    }

    void Element::getElementsBy(const std::function<bool(const sp_Element &)>& fn, HTMLCollection &vec) {
        for (auto & child : _children) {
            if (fn(child)) {
                vec.push_back(child);
            }
            child->getElementsBy(fn, vec);
        }
    }

    Element *Element::getElementBy(std::function<bool(Element *)> fn) {
        for (auto & child : _children) {
            Element * aux = child.get();

            if (fn(aux)) {
                return aux;
            }

            aux = aux->getElementBy(fn);

            if (aux) return aux;
        }

        return nullptr;
    }

    std::string Element::textContentGetter() const {
        std::string text;

        NodeTravel((Node *)this).forEach.descendants([&text](Node *node) {
            if (node->nodeType == Node::TEXT_NODE) {
                auto textNode = dynamic_cast<Text *>(node);
                text += textNode->data;
            }
        });

        return text;
    }

    void Element::textContentSetter(const std::string &oldValue, const std::string & newValue) {
        MutationAlgorithms(this).string_replace_all(newValue);
    }

    std::string Element::outerHTMLGetter() {
        return XMLSerializer::serializeToString(shared_from_this());
    }

    void Element::outerHTMLSetter(const std::string &oldValue, const std::string &newValue) {
        if (!_parentNode) return;

        auto fragment = DOMParser::fragmentParse(newValue);

        _parentNode->replaceChild(fragment,shared_from_this());
    }

    sp_Node Element::insert_adjacent(const std::string &where, DOM_Node::sp_Node node) {
        std::string lower = Utils::toLower(where);

        if (lower == "beforebegin") {
            if (!_parentNode) return nullptr;
            auto res = MutationAlgorithms(_parentNode).pre_insert(node.get(),this);
            return res->shared_from_this();

        } else if (lower == "afterbegin") {
            auto res = MutationAlgorithms(this).pre_insert(node,firstChild());
            return res->shared_from_this();

        } else if (lower == "beforeend") {
            auto res = MutationAlgorithms(this).pre_insert(node, nullptr);
            return res->shared_from_this();

        } else if (lower == "afterend") {
            if (!_parentNode) return nullptr;
            auto res = MutationAlgorithms(_parentNode).pre_insert(node, nextSibling());
            return res->shared_from_this();

        } else {
            //Throw a "SyntaxError" DOMException.
            return nullptr;
        }
    }

    bool Element::attributeChangedCallback(const std::string &AttrName, const std::string &oldvalue,
                                           const std::string &newValue) {

        if (AttrName == "slot") {
            if (assignedSlot)
                assign_slotables(assignedSlot);

            assign_slot();

            return true;

        } else if (AttrName == "class") {
            className = newValue;

            return true;
        }

        return false;
    }

    Element::Element() :
            textContent([this] { return textContentGetter(); },
                        [this](const std::string & PH1, const std::string & PH2) { textContentSetter(PH1, PH2); }),
            outerHTML([this] { return outerHTMLGetter(); },
                      [this](const std::string & PH1, const std::string & PH2) { outerHTMLSetter(PH1, PH2); })
    {
        _nodeType = ELEMENT_NODE;
        _attributes.ownerElement = this;

    }

    ReactString& Element::setReferencedAttr(const std::string &name, const std::string &value) {
        return _attributes.setNamedItem(name, value);
    }

    void Element::setAttribute(const std::string &name, const std::string &value) {
        auto attr = Document::createAttribute(name);
        attr->value = value;

        _attributes.setNamedItem(attr);
    }

    std::string Element::getAttribute(const std::string &name) const {
        auto attr = _attributes.getNamedItem(name);

        return attr ? attr->value : "" ;
    }

    std::vector<std::string> Element::getAttributeNames() const {
        std::vector<std::string> names;

        int length = _attributes.length();
        names.reserve(length);

        for (int i = 0; i < length; i++) {
            names.push_back(_attributes[i]->name);
        }

        return names;
    }

    bool Element::toggleAttribute(const std::string & name, bool force) {
        auto attr = _attributes.getNamedItem(name);

        if (!attr) {
            if (!force) return false;

            attr = Document::createAttribute(name);
            _attributes.setNamedItem(attr);

            return true;
        }

        if (force) return true;

        removeAttribute(name);

        return false;
    }

    sp_Element Element::getNextElement(sp_Node &reference) {
        Node * aux = reference.get();
        while (aux && aux->nodeType != Node::ELEMENT_NODE) {
            aux = aux->nextSibling().get();
        }

        if (aux) {
            return cast2sp_Element(aux);
        }

        return nullptr;
    }

    void Element::appendElementChild(sp_Element &elem) {
        _children.push_back(elem);
    }

    void Element::removeElementChild(sp_Element &elem) {
        auto end = _children.end();
        for (auto it = _children.begin(); it != end; it++) {
            if ((*it) == elem) {
                _children.erase(it);

                return;
            }
        }
    }

    void Element::insertElementBefore(sp_Element &elem, sp_Element &reference) {
        auto end = _children.end();
        for (auto it = _children.begin(); it != end; it++) {
            if ((*it) == reference) {
                _children.insert(it, elem);

                return;
            }
        }

        appendElementChild(elem);
    }

    void Element::clone(Node *copy, bool deep) const {
        Node::clone(copy, deep);

        auto elem = dynamic_cast<Element *>(copy);

        elem->_attributes = attributes;
        //elem->domRect = domRect;
        elem->_classList = _classList;
        elem->_clientHeight = clientHeight;
        elem->_clientWidth = clientWidth;
        elem->_clientLeft = clientLeft;
        elem->_clientTop = clientTop;
        elem->_scrollHeight = scrollHeight;
        elem->_scrollWidth = scrollWidth;
        elem->scrollTop = scrollTop;
        elem->scrollLeft = scrollLeft;
        //elem->_hidden = hidden;
        //elem->localName = localName;
        //elem->prefix = prefix;
        //elem->namespaceURI = namespaceURI;
        //todo, slot
        //todo, _shadowRoot
    }

    sp_Node Element::appendChild(sp_Node aChild) {
        sp_Node aux = Node::appendChild(aChild);

        if (aux && aux->nodeType == Node::ELEMENT_NODE) {
            auto elem = std::dynamic_pointer_cast<Element>(aChild);
            appendElementChild(elem);
        }

        return aux;
    }

    sp_Node Element::removeChild(sp_Node child) {
        sp_Node aux = Node::removeChild(child);

        if (aux && aux->nodeType == Node::ELEMENT_NODE) {
            auto elem = std::dynamic_pointer_cast<Element>(child);

            removeElementChild(elem);
        }

        return aux;
    }

    DOM_Node::sp_Node Element::insertBefore(sp_Node newNode, sp_Node referenceNode) {
        sp_Node aux = Node::insertBefore(newNode, referenceNode);

        if (aux && aux->nodeType == Node::ELEMENT_NODE) {
            auto elem = std::dynamic_pointer_cast<Element>(aux);
            auto reference = getNextElement(referenceNode);

            insertElementBefore(elem, reference);
        }

        return aux;
    }

    DOM_Node::sp_Node Element::replaceChild(sp_Node newChild, sp_Node oldChild) {
        sp_Node aux = Node::replaceChild(newChild, oldChild);

        if (aux) {
            NodeType newChildType = newChild->nodeType;
            NodeType oldChildType = oldChild->nodeType;

            if (newChildType == NodeType::ELEMENT_NODE && oldChildType != NodeType::ELEMENT_NODE) {
                auto elem = std::dynamic_pointer_cast<Element>(newChild);
                auto reference = getNextElement(oldChild);
                insertElementBefore(elem, reference);

            } else if (newChildType != NodeType::ELEMENT_NODE && oldChildType == NodeType::ELEMENT_NODE) {
                auto elem = std::dynamic_pointer_cast<Element>(oldChild);
                removeElementChild(elem);

            } else if (newChildType == NodeType::ELEMENT_NODE && oldChildType == NodeType::ELEMENT_NODE) {
                auto newElem = std::dynamic_pointer_cast<Element>(newChild);
                auto oldElem = std::dynamic_pointer_cast<Element>(oldChild);

                auto end = _children.end();
                for (auto it = _children.begin(); it != end; it++) {
                    if ((*it) == oldElem) {
                        std::replace(it, it, (*it), newElem);

                        return aux;
                    }
                }
            }
        }

        return aux;
    }

    bool Element::isEqualNode(const sp_Node node) const {
        if (Node::isEqualNode(node)) {
            auto elem = dynamic_cast<Element *>(node.get());

            return //localName == elem->localName &&
                   //prefix == elem->prefix &&
                   //namespaceURI == elem->namespaceURI &&
                   classList.length == elem->classList.length &&
                   classList == elem->classList &&
                   attributes == elem->attributes ;

        }

        return false;
    }

    sp_ShadowRoot Element::attachShadow(ShadowRootMode init) {
        if (!(  isCustom() ||
                tagName == "article" ||
                tagName == "aside" ||
                tagName == "blockquote" ||
                tagName == "body" ||
                tagName == "div" ||
                tagName == "footer" ||
                tagName == "h1" ||
                tagName == "h2" ||
                tagName == "h3" ||
                tagName == "h4" ||
                tagName == "h5" ||
                tagName == "h6" ||
                tagName == "header" ||
                tagName == "main" ||
                tagName == "nav" ||
                tagName == "p" ||
                tagName == "section" ||
                tagName == "span"
        )) {
            //throw a "NotSupportedError" DOMException.
            return nullptr;
        }

        if (_shadowRoot) {
            // throw an "NotSupportedError" DOMException.
            return nullptr;
        }

        _shadowRoot = std::shared_ptr<ShadowRoot>( new ShadowRoot(init) );

        return _shadowRoot;
    }

    sp_Element Element::closest(const std::string &selectors) {
        HTMLCollection ancestors;
        auto aux = std::dynamic_pointer_cast<Element>(shared_from_this());

        while (aux) {
            ancestors.push_back(aux);
            aux = aux->parentElement();
        }

        HTMLCollection elements;
        for (auto & ancestor : ancestors) {
            if (CSSSelector::match(selectors, ancestor)) {
                elements.push_back(ancestor);
            }
        }

        if (!elements.empty()) {
            return elements.front();
        }

        return nullptr;
    }

    bool Element::matches(const std::string &selector) {
        return CSSSelector::match(selector, std::dynamic_pointer_cast<Element>(shared_from_this()));
    }

    sp_Element Element::insertAdjacentElement(const std::string &where, sp_Element element) {
        auto res = insert_adjacent(where, element);

        return std::dynamic_pointer_cast<Element>(res);
    }

    void Element::insertAdjacentText(const std::string &where, const std::string &data) {
        auto text = Document::createTextNode(data);

        insert_adjacent(where, text);
    }

    void Element::insertAdjacentHTML(const std::string &where, const std::string &html) {
        auto fragment = DOMParser::fragmentParse(html);

        insert_adjacent(where, fragment);
    }

    void Element::prepend(DOM_Node::sp_Node child) {
        auto reference = firstChild();
        insertBefore(child, reference);
    }

    DOM_Node::HTMLCollection Element::querySelectorAll(const std::string &selector) {
        return CSSSelector::select(selector, std::dynamic_pointer_cast<Element>(shared_from_this()));
    }

    const DOMRect & Element::getBoundingClientRect() const {
        static const DOMRect T;
        auto aux = const_cast<Element*>(this);

        if (auto html = dynamic_cast<HTMLElement *>(aux)) {
            if (!html->frames.empty()) {
                auto frame = html->frames.front();

                return frame->border;
            }
        }

        return T;
    }

    std::vector<DOMRect> Element::getClientRects() const {
        std::vector<DOMRect> rects;
        auto aux = const_cast<Element*>(this);

        if (auto html = dynamic_cast<HTMLElement *>(aux)) {
            if (!html->frames.empty()) {
                auto frame = html->frames.front();

                rects.push_back(frame->margin);
                rects.push_back(frame->border);
                rects.push_back(frame->padding);
                rects.push_back(frame->content);

                return rects;
            }
        }

        for (int i = 0; i < 4; i++) {
            rects.emplace_back();
        }

        return rects;
    }

    /*bool Element::cursorIn(double x, double y) {
        const DOMRect &box = domRect;

        return x >= box.left && x <= box.right &&
               y >= box.top && y <= box.bottom;
    }

    void Element::hide() {
        if (!_hidden) {
            _hidden = true;

            EventDispatcher *hide = new EventDispatcher(new Event("hide"));
            hide->setTargets({std::static_pointer_cast<Element>(this->lock())});

            EventDispatcher::addDispatcher(hide);
        }
    }

    void Element::show() {
        if (_hidden) {
            _hidden = false;

            EventDispatcher *show = new EventDispatcher(new Event("show"));
            show->setTargets({std::static_pointer_cast<Element>(this->lock())});

            EventDispatcher::addDispatcher(show);
        }
    }*/

}