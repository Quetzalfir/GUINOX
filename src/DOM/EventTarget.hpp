/**
* @brief This is EventTarget class.
*
* EventTarget is a DOM interface implemented by objects
* that can receive events and may have listeners for them.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/EventTarget
*/
#ifndef GUINOX_EVENTTARGET_HPP
#define GUINOX_EVENTTARGET_HPP


#include "EventListener.hpp"
#include <map>

class EventTarget {
    //friend class EventDispatcher;

public:
    struct Options {
        bool capture = false;
        bool once = false;
        bool passive = false;

        Options() {
            capture = false;
            once = false;
            passive = false;
        }
    };


protected:
    EventTarget() = default;


private:

    struct EventListenerType {
        EventListener eventListener;
        EventTarget::Options options;

        inline bool operator==( const EventListenerType & rhs ) const {
            return eventListener   ==   rhs.eventListener   &&
                   options.capture ==   rhs.options.capture;
        }
        inline bool operator!=( const EventListenerType & rhs ) const { return !(this->operator==(rhs)); }

        bool ejec(Event * event, bool capture);
    };

    std::map<std::string, std::deque<EventListenerType> > listeners;

    void ejecListener(Event * event, bool capture);


public:
    virtual ~EventTarget() = default;

    void addEventListener(const std::string & type, const EventListener& listener );
    void addEventListener(const std::string & type, const EventListener& listener, Options options );

    void removeEventListener(const std::string & type, const EventListener& listener, bool capture );
    void removeEventListener(const std::string & type, const std::string & listenerName, bool capture );

    void removeEventListeners(const std::string & type, const std::string & name, bool capture);
    void removeEventListeners(const std::string & type, const std::string & name);
    void removeEventListeners(const std::string & name);

    bool dispatchEvent(Event & event);

};


#endif //GUINOX_EVENTTARGET_HPP