/**
* @brief This is DOMImplementation class.
*
* The DOMImplementation interface represents an object
 * providing methods which are not dependent on any particular document.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/DOMImplementation
*/
#ifndef GUINOX_DOMIMPLEMENTATION_HPP
#define GUINOX_DOMIMPLEMENTATION_HPP


#include <string>
#include "dom_declarations.hpp"


class DOMImplementation {

public:
    static std::shared_ptr<DOM_Node::DocumentType> createDocumentType(const std::string &qualifiedName);
    static std::shared_ptr<DOM_Node::DocumentType> createDocumentType(const std::string &qualifiedName, const std::string &publicId);
    static std::shared_ptr<DOM_Node::DocumentType> createDocumentType(const std::string &qualifiedName, const std::string &publicId, const std::string &systemId);

    static std::shared_ptr<DOM_Node::XMLDocument> createDocument(const std::string &namespace_ = "");
    static std::shared_ptr<DOM_Node::XMLDocument> createDocument(const std::string &namespace_, const std::string &qualifiedName);
    static std::shared_ptr<DOM_Node::XMLDocument> createDocument(const std::string &namespace_, const std::string &qualifiedName, std::shared_ptr<DOM_Node::DocumentType> & doctype);

    static std::shared_ptr<DOM_Node::Document> createHTMLDocument();
    static std::shared_ptr<DOM_Node::Document> createHTMLDocument(const std::string & title);

};


#endif //GUINOX_DOMIMPLEMENTATION_HPP
