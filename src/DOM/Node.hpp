/**
 * @brief DOM_Node is a namespace that encompasses all types of nodes that belong to the DOM.
 */


#ifndef GUINOX_DOM_NODE_NODE_H
#define GUINOX_DOM_NODE_NODE_H


#include "EventTarget.hpp"
#include "dom_declarations.hpp"
#include "ReactString.hpp"
//#include "MutationObserver.h"
#include <vector>


//class DOMImplementation;
class NodeTravel;
class MutationAlgorithms;


namespace DOM_Node {

/**
* @brief This is Node class.
*
* Node is an interface from which various types of DOM API
* objects inherit, allowing those types to be treated similarly;
* for example, inheriting the same set of methods,
* or being testable in the same way.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Node
*/
    class Node :
            public EventTarget,
            public std::enable_shared_from_this<Node> {


        friend class Document;
        //friend class ::DOMImplementation;
        //friend class ::MutationObserver;
        friend class ::NodeTravel;
        friend class ::MutationAlgorithms;



        //std::vector<MutationRegistered> observers;


    public:

        enum NodeType : char {
            UNDEFINED = 0,
            ELEMENT_NODE = 1,
            //ATTRIBUTE_NODE = 2, // An Attribute of an Element. Attributes no longer implement the Node interface as of DOM4.
            TEXT_NODE = 3,
            //CDATA_SECTION_NODE = 4, // deprecated
            //ENTITY_REFERENCE_NODE = 5, // historical, An XML Entity Reference node, such as &foo;. Removed in DOM4.
            //ENTITY_NODE = 6, // historical, An XML <!ENTITY …> node. Removed in DOM4.
            //PROCESSING_INSTRUCTION_NODE = 7, // GUINOX doesn't occupy it
            //COMMENT_NODE = 8, // GUINOX doesn't occupy it
            DOCUMENT_NODE = 9,
            DOCUMENT_TYPE_NODE = 10,
            DOCUMENT_FRAGMENT_NODE = 11,
            //NOTATION_NODE = 12 //	An XML <!NOTATION …> node. Removed in DOM4.
        };


        static const unsigned short int DOCUMENT_POSITION_DISCONNECTED = 0x01;
        static const unsigned short int DOCUMENT_POSITION_PRECEDING = 0x02;
        static const unsigned short int DOCUMENT_POSITION_FOLLOWING = 0x04;
        static const unsigned short int DOCUMENT_POSITION_CONTAINS = 0x08;
        static const unsigned short int DOCUMENT_POSITION_CONTAINED_BY = 0x10;
        static const unsigned short int DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 0x20;


    protected:
        NodeType _nodeType;
        Document * _ownerDocument = nullptr;

        NodeList _childNodes; //DEVUELVE TODOS LOS HIJOS DE TODOS LOS TIPOS DE NODO
        Node *_parentNode = nullptr;
        Element *_parentElement = nullptr;
        Node *_previousSibling = nullptr;
        Node *_nextSibling = nullptr;


        std::string _nodeName;
        //std::string baseURI;


        explicit Node(NodeType type);
        Node() : Node(UNDEFINED) {}

        bool isCustom() const ;
        virtual void clone(Node *copy, bool deep) const;


        virtual void children_changed_steps() {}
        virtual void adopting_steps(Document * document) {}
        virtual void insertion_steps() {}
        virtual void removing_steps(Node * parent = nullptr) {}



        //todo, not be here, be in event loop class
        /*void queue_mutation_record( //Node *target,
                                    const std::string &type,
                                    const std::string &name,
                                    //const std::string &namespace_,
                                    const std::string &oldValue,
                                    const NodeList &addedNodes,
                                    const NodeList &removedNodes,
                                    sp_Node previousSibling,
                                    sp_Node nextSibling
                                    );*/

        //void queue_mutation_record(MutationRecord & record);
        //todo, A node’s get the parent algorithm, given an event, returns the node’s assigned slot, if node is assigned, and node’s parent otherwise.



    public:

        ~Node() override;

        const NodeList &childNodes = _childNodes;
        const std::string &nodeName = _nodeName;
        const NodeType &nodeType = _nodeType;
        //ReactString nodeValue; // Do not need it


        bool isConnected() const ;
        unsigned short int compareDocumentPosition(const sp_Node node) const ;
        bool contains(sp_Node node) const;

        sp_Node getRootNode(bool composed = false) const ;

        sp_Node firstChild() const ;
        sp_Node lastChild() const ;
        inline sp_Node nextSibling() const { return _nextSibling ? _nextSibling->shared_from_this() : nullptr; }
        inline sp_Node previousSibling() const { return _previousSibling ? _previousSibling->shared_from_this() : nullptr; }
        inline sp_Node parentNode() const { return _parentNode ? _parentNode->shared_from_this() : nullptr; }
        sp_Element parentElement() const ;
        sp_Document ownerDocument() const ;


        inline bool hasChildNodes() const { return !_childNodes.empty(); }

        virtual bool isEqualNode(const sp_Node node) const;

        bool isSameNode(const sp_Node node) const;

        inline virtual size_t length() const { return childNodes.size(); }

        //std::string lookupPrefix();
        //std::string lookupNamespaceURI();
        //bool isDefaultNamespace(const std::string &namespaceURI);

        void normalize();

        virtual sp_Node cloneNode(bool deep) const ;

        virtual sp_Node appendChild(sp_Node aChild);

        virtual sp_Node insertBefore(sp_Node newNode, sp_Node referenceNode);

        virtual sp_Node removeChild(sp_Node child);

        virtual sp_Node replaceChild(sp_Node newChild, sp_Node oldChild);


        bool operator==(Node & rhs) const;

        inline bool operator!=(Node &rhs) const { return !(this->operator==(rhs)); }
    };

}


#endif //GUINOX_DOM_NODE_NODE_H
