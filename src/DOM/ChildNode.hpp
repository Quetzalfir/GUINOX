/**
* @brief This is ChildNode class.
*
* The ChildNode contains methods and properties
* that are common to all types of Node objects that can
* have a parent. It's implemented by Element, DocumentType,
* and CharacterData objects.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/ChildNode
*/
#ifndef GUINOX_CHILDNODE_HPP
#define GUINOX_CHILDNODE_HPP

#include <string>
#include "Node.hpp"

class ChildNode : public virtual DOM_Node::Node {

protected:
    ChildNode() : DOM_Node::Node(DOM_Node::Node::UNDEFINED) {}


public:

    void after(DOM_Node::sp_Node node);
    void after(DOM_Node::NodeList & nodes);
    void after(const std::string & text);

    void before(DOM_Node::sp_Node node);
    void before(DOM_Node::NodeList & nodes);
    void before(const std::string & text);

    void remove();

    void replaceWith(DOM_Node::sp_Node child);
    void replaceWith(DOM_Node::NodeList & childs);
    void replaceWith(const std::string & text);
};

#endif //GUINOX_CHILDNODE_HPP
