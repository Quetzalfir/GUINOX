/**
* @brief The DocumentStyle interface provides a mechanism
* by which the style sheets embedded in a document can be retrieved.
*
* The expectation is that an instance of the DocumentStyle interface can be
* obtained by using binding-specific casting methods on an instance of the Document interface.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://www.w3.org/2003/01/dom2-javadoc/org/w3c/dom/stylesheets/DocumentStyle.html
*/

#ifndef GUINOX_DOCUMENTSTYLE_HPP
#define GUINOX_DOCUMENTSTYLE_HPP

#include <thread>
#include <list>
#include <functional>
#include "../CSSOM/StyleSheet.hpp"


class DocumentStyle {

    DOM_Node::Document * _document = nullptr;
    CSSStyleSheetList _styleSheets;


protected:
    void addStyleSheet(sp_CSSStyleSheet & sheet);
    void removeStyleSheet(sp_CSSStyleSheet & sheet);

    DocumentStyle(DOM_Node::Document * document);


public:
    const CSSStyleSheetList & styleSheets = _styleSheets;


};


#endif //GUINOX_DOCUMENTSTYLE_HPP
