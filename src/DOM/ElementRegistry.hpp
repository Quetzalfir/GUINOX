/**
* @brief This is a ElementRegistry class.
*
* Registers in the GUINOX library which elements are
* defined by the framework or by the standard.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
*/
#ifndef GUINOX_ELEMENTREGISTRY_HPP
#define GUINOX_ELEMENTREGISTRY_HPP


#include "HTMLElement.hpp"

class ElementRegistry {

protected:
    static std::map<std::string,std::function<std::shared_ptr<HTMLElement>()>> elements;


    ElementRegistry() = default;


public:

    ElementRegistry(const std::string &name, std::function<std::shared_ptr<HTMLElement>()> constructor);

    static std::shared_ptr<HTMLElement> get(const std::string & tag);

    //static void upgrade(DOM_Node::sp_Node root); // C++ implementation?
    //static Promise whenDefined(const std::string &name); // C++ implementation


};


#endif //GUINOX_ELEMENTREGISTRY_HPP
