/**
* @brief This is DOMTokenList class.
*
* The DOMTokenList interface represents a set of space-separated tokens.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/DOMTokenList
*/
#ifndef GUINOX_DOMTOKENLIST_HPP
#define GUINOX_DOMTOKENLIST_HPP

#include "ReactString.hpp"
#include <string>
#include <deque>
#include <vector>
#include <functional>


class DOMTokenList {
    typedef std::deque<std::string>::iterator iterator;
    typedef std::deque<std::string>::const_iterator const_iterator;


    std::deque<std::string> list;
    int _length = 0;

    std::string valueGetter();
    void valueSetter(const std::string &oldvalue, const std::string &newValue);


public:

    DOMTokenList();

    ReactString value;
    const int &length = _length;

    //bool supports(const std::string &str);
    //std::vector<std::string> entries();
    //std::vector<std::string> keys();
    //std::vector<std::string> values();

    void add(const std::vector<std::string> &str);

    void add(const std::string &str);

    void remove(const std::vector<std::string> &str);

    //void remove(const std::string &str);

    const std::string &item(int num) const { return list[num]; }

    bool toggle(const std::string &str);

    bool toggle(const std::string &str, bool force);

    bool contains(const std::string &str) const;

    void forEach(std::function<void(std::string, int, const std::deque<std::string>&)> callback);

    void replace(const std::string &oldClass, const std::string &newClass);

    inline const std::string &operator[](std::size_t idx) const { return list[idx]; }

    inline bool operator==(const DOMTokenList &rhs) const { return list == rhs.list; }

    inline bool operator!=(const DOMTokenList &rhs) const { return !(this->operator==(rhs)); }

    DOMTokenList &operator=(const DOMTokenList &other);


    inline iterator begin() noexcept { return list.begin(); }
    inline const_iterator cbegin() const noexcept { return list.cbegin(); }
    inline iterator end() noexcept { return list.end(); }
    inline const_iterator cend() const noexcept { return list.cend(); }


};


#endif //GUINOX_DOMTOKENLIST_HPP
