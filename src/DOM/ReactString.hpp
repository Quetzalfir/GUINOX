/**
* @brief
* Class that works as a string but when it changes value
* it is possible to execute a callback function.
*
* It is a string that when modifying value a function can be programmed to that value change.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*/
#ifndef GUINOX_REACTSTRING_HPP
#define GUINOX_REACTSTRING_HPP

#include <string>
#include <functional>

class ReactString {

    static void doNothing1(const std::string &, const std::string &) {}
    static std::string doNothing2() { return ""; }

    std::string value;
    bool reactive = false;
    bool getterNSetter = false;
    std::function<void(const std::string &, const std::string &)> reaction = doNothing1; //oldValue, newValue
    std::function<std::string(void)> getter = doNothing2;


public:
    ReactString(std::string value_) : value(std::move(value_)) {}
    ReactString(std::string value, std::function<void(const std::string &, const std::string &)> reaction_);
    ReactString(std::function<std::string(void)> getter_, std::function<void(const std::string &, const std::string &)> reaction_ = doNothing1);
    ReactString(const ReactString & rhs);
    ReactString(ReactString && ths) noexcept ;
    ReactString(const char* arr);
    ReactString();


    bool operator== (const ReactString &rhs) const ;
    inline bool operator!= (const ReactString &rhs) const { return !(this->operator==(rhs)); }
    bool operator== (const std::string &str) const ;
    inline bool operator!= (const std::string &str) const { return !(this->operator==(str)); }
    bool operator== (const char* arr) const ;
    inline bool operator!= (const char* arr) const { return !(this->operator==(arr)); }
    friend bool operator== (const std::string & lhs, const ReactString & rhs);
    friend inline bool operator!= (const std::string & lhs, const ReactString & rhs) { return !(lhs == rhs); }




    ReactString & operator= (const std::string & str);
    ReactString & operator= (const char* arr);
    ReactString & operator= (char c);
    ReactString & operator= (const ReactString & rhs);
    ReactString & operator= (ReactString && ths) noexcept ;

    ReactString & operator+= (const std::string & str);
    ReactString & operator+= (const char* arr);
    ReactString & operator+= (char c);
    ReactString & operator+= (const ReactString & rhs);

    std::string operator+ (const ReactString & rhs) const ;

    //friend std::string operator+ (const ReactString& lhs, const std::string& rhs);
    friend std::string operator+ (ReactString&&      lhs, std::string&&      rhs);
    //friend std::string operator+ (std::string&&      lhs, const ReactString& rhs);
    //friend std::string operator+ (const std::string& lhs, ReactString&&      rhs);

    //friend std::string operator+ (const ReactString& lhs, const char*        rhs);
    friend std::string operator+ (ReactString&&      lhs, const char*        rhs);
    friend std::string operator+ (const char*        lhs, const ReactString& rhs);
    friend std::string operator+ (const char*        lhs, ReactString&&      rhs);

    friend std::string operator+ (const ReactString& lhs, char               rhs);
    friend std::string operator+ (ReactString&&      lhs, char               rhs);
    friend std::string operator+ (char               lhs, const ReactString& rhs);
    friend std::string operator+ (char               lhs, ReactString&&      rhs);


    friend std::ostream & operator<< (std::ostream& out, const ReactString& hs); // output
    friend std::istream & operator>> (std::istream &in, ReactString &c); // input

    operator std::string() const ;

    size_t size() const ;
    size_t length() const ;

    //inline std::string::iterator begin();
    //inline std::string::const_iterator begin() const ;
    //inline std::string::iterator end();
    //inline std::string::const_iterator end() const ;

};


#endif //GUINOX_REACTSTRING_HPP
