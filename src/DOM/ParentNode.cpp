#include "ParentNode.hpp"

#include <utility>
#include "Element.hpp"
#include "Text.hpp"
#include "MutationAlgorithms.hpp"

DOM_Node::sp_Element ParentNode::firstElementChild() const {
    if (_children.empty()) return nullptr;

    return _children.front();
}

DOM_Node::sp_Element ParentNode::lastElementChild() const {
    if (_children.empty()) return nullptr;

    return _children.back();
}

size_t ParentNode::childElementCount() const {
    return _children.size();
}

DOM_Node::sp_Element ParentNode::querySelector(const std::string &selector) {
    auto aux = querySelectorAll(selector);

    return !aux.empty() ? aux[0] : nullptr ;
}

void ParentNode::append(DOM_Node::sp_Node node) {
    MutationAlgorithms(this).append(std::move(node));
}

void ParentNode::append(DOM_Node::NodeList &nodes) {
    DOM_Node::sp_Node node = MutationAlgorithms::convert_nodes_into_a_node(nodes, _ownerDocument);
    MutationAlgorithms(this).append(node);
}

void ParentNode::append(const std::string &text) {
    DOM_Node::sp_Node child = std::make_shared<DOM_Node::Text>(text);
    append(child);
}

void ParentNode::prepend(DOM_Node::sp_Node node) {
    MutationAlgorithms(this).pre_insert(node, firstChild());
}

void ParentNode::prepend(DOM_Node::NodeList &nodes) {
    DOM_Node::sp_Node node = MutationAlgorithms::convert_nodes_into_a_node(nodes, _ownerDocument);
    MutationAlgorithms(this).pre_insert(node, firstChild());
}

void ParentNode::prepend(const std::string &text) {
    DOM_Node::sp_Node child = std::make_shared<DOM_Node::Text>(text);
    prepend(child);
}