#include "Document.hpp"
#include "Text.hpp"
#include "CustomElementRegistry.hpp"
#include "NodeTravel.hpp"
#include "MutationAlgorithms.hpp"
#include <algorithm>
#include <utility>
#include <Utils/Utils.hpp>
#include <CSSOM/CSSParser.hpp>
#include <CSSOM/CSSSelector.hpp>


namespace DOM_Node {

    Document::Document() :
            DocumentStyle(this),
            DocumentPresentation(this)
            {

        _nodeType = DOCUMENT_NODE;
        _nodeName = "#document";
        _ownerDocument = this;
        //EventDispatcher::registDocument(this);
    }

    Document::~Document() {
        //EventDispatcher::quitDocument(this);
    }

    /*void Document::render() {
        //z_context.render();
    }

    void Document::stylize() {
        _z_context.clear();

        ((Stylizable *) body)->stylize();

        _z_context.sort();
    }*/

    void Document::manageNewChild(sp_Node &child) {
        switch (child->nodeType) {
            case Node::DOCUMENT_TYPE_NODE:
                _doctype = std::dynamic_pointer_cast<DocumentType>(child);
                break;
            case Node::ELEMENT_NODE:
                _documentElement = std::dynamic_pointer_cast<Element>(child);
                fullReFlow();
                break;
            default:
                break;
        }
    }

    void Document::checkLiveRanges() {
        //todo, implement a delayed thread that check the dead ranges and remove them from the vector
    }

    void Document::checkNodeIterators() {
        //todo, implement a delayed thread that check the dead ranges and remove them from the vector
    }

    sp_Element Document::getElementById(const std::string &id) const {
        if (documentElement) return documentElement->getElementById(id);
        return nullptr;
    }

    HTMLCollection Document::getElementsByTagName(const std::string &qualifiedName) {
        return documentElement->getElementsByTagName(qualifiedName);
    }

    HTMLCollection Document::getElementsByClassName(const std::string &className) {
        return documentElement->getElementsByClassName(className);
    }

    sp_Range Document::createRange() {
        auto doc = std::dynamic_pointer_cast<Document>(this->shared_from_this());
        sp_Range range(new Range(doc));

        liveRanges.push_back(range);

        return range;
    }

    sp_Element Document::createElement(const std::string &tagName) {
        std::string tag = Utils::toLower(tagName);

        sp_Element element = ElementRegistry::get(tag);
        //element->_ownerDocument = this;

        return element;
    }

    sp_Text Document::createTextNode(const std::string &data) {
        sp_Text node = std::make_shared<DOM_Node::Text>(data);
        //node->_ownerDocument = this;

        return node;
    }

    sp_DocumentFragment Document::createDocumentFragment() {
        auto documentFragment = std::shared_ptr<DocumentFragment>( new DocumentFragment() );
        //documentFragment->_ownerDocument = this;

        return documentFragment;
    }

    sp_Attr Document::createAttribute(const std::string &name) {
        return std::shared_ptr<Attr>( new Attr(name) );
    }

    sp_NodeIterator Document::createNodeIterator(sp_Node root, unsigned long whatToShow,
                                              const NodeFilter &filter) {

        sp_NodeIterator nodeIterator(new NodeIterator(std::move(root), whatToShow, filter));

        nodeIterators.push_back(nodeIterator);

        return nodeIterator;
    }

    sp_NodeIterator Document::createNodeIterator(DOM_Node::sp_Node root, unsigned long whatToShow) {
        NodeFilter filter;
        return createNodeIterator(std::move(root), whatToShow, filter);
    }

    TreeWalker Document::createTreeWalker(sp_Node root, unsigned long whatToShow, const NodeFilter &filter) {
        return TreeWalker(std::move(root), whatToShow, filter);
    }

    TreeWalker Document::createTreeWalker(sp_Node root, unsigned long whatToShow) {
        NodeFilter filter;
        return createTreeWalker(std::move(root), whatToShow, filter);
    }

    sp_Node Document::cloneNode(bool deep) const {
        auto res = std::shared_ptr<Document>( new Document() );

        clone(res.get(), deep);

        return res;
    }

    void Document::clone(Node *copy, bool deep) const {
        Node::clone(copy, deep);

        auto *doc = dynamic_cast<Document *>(copy);

        //doc->encoding = encoding
        doc->_contentType = _contentType;
        //doc->URL = URL;
        doc->_origin = _origin;
        //doc->type = type;
        //doc->mode = mode;
    }

    sp_Node Document::importNode(const DOM_Node::sp_Node node, bool deep) const {
        if (    !node ||
                node->nodeType == DOCUMENT_TYPE_NODE ||
                dynamic_cast<ShadowRoot *>(node.get())) {
            return nullptr;
        }

        auto res = node->cloneNode(deep);
        adoptNode(res);

        return res;
    }

    sp_Node Document::adoptNode(DOM_Node::sp_Node node) const {
        if (    !node ||
                node->nodeType == DOCUMENT_NODE ||
                dynamic_cast<ShadowRoot *>(node.get())) {
            return nullptr;
        }

        auto docFrag = dynamic_cast<DocumentFragment *>(node.get());

        if (docFrag && docFrag->host()) return nullptr;


        // adopt algorithm
        Document *oldDocument = node->_ownerDocument;

        if (node->_parentNode)
            node->_parentNode->removeChild(node);

        if (oldDocument != this) {
            auto * document = const_cast<Document *>(this);

            NodeTravel(node).forEach.shadow_including_inclusive_descendants([=](Node * aux) {
                MutationAlgorithms(aux).adoptedCallback(oldDocument, document);
                aux->_ownerDocument = document;

                aux->adopting_steps(oldDocument);
            });
        }

        return node;
    }

    sp_Node Document::appendChild(sp_Node aChild) {
        auto res = Node::appendChild(aChild);
        if (res) manageNewChild(res);
        return res;
    }

    sp_Node Document::insertBefore(sp_Node newNode, sp_Node referenceNode) {
        auto res = Node::insertBefore(newNode, referenceNode);
        if (res) manageNewChild(res);
        return res;
    }

    sp_Node Document::replaceChild(sp_Node newChild, sp_Node oldChild) {
        auto res = Node::replaceChild(newChild, oldChild);
        if (res) manageNewChild(newChild);
        return res;
    }

    DOM_Node::HTMLCollection Document::querySelectorAll(const std::string &selector) {
        return CSSSelector::select(selector, _documentElement);
    }

    sp_Node Document::removeChild(sp_Node child) {
        auto res = Node::removeChild(child);
        if (res) {
            switch (res->nodeType) {
                case Node::DOCUMENT_TYPE_NODE:
                    _doctype = nullptr;
                    break;
                case Node::ELEMENT_NODE:
                    _documentElement = nullptr;
                    break;
                default: break;
            }
        }
        return res;
    }
}