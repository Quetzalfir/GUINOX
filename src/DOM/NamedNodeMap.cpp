#include "NamedNodeMap.hpp"
#include "Document.hpp"
#include <algorithm>
#include <utility>
#include <Utils/Utils.hpp>

const static sp_Attr NULL_ATTR = nullptr;


bool NamedNodeMap::operator==(const NamedNodeMap &rhs) const {
    if (attributes.size() != rhs.attributes.size()) return false;

    auto lEnd = attributes.end();
    auto rEnd = rhs.attributes.end();
    auto it1 = attributes.begin();
    auto it2 = rhs.attributes.begin();

    while (it1 != lEnd && it2 != rEnd) {
        if (*(*it1) != *(*it2)) {
            return false;
        }

        it1++;
        it2++;
    }

    return true;
}

NamedNodeMap& NamedNodeMap::operator=(const NamedNodeMap &rhs) {
    if (&rhs == this) return *this;

    attributes.clear();

    for (const auto & attr : rhs.attributes) {
        sp_Attr aux = std::shared_ptr<Attr>( new Attr(attr->name) );
        *aux = *attr;
        //todo, check in test

        attributes.push_back(aux);
    }

    return *this;
}

sp_Attr NamedNodeMap::item(size_t index) {
    if (index >= attributes.size()) { return nullptr; }

    auto it = attributes.begin();
    while (index--) { it++; }

    return (*it);
}

const sp_Attr &NamedNodeMap::item(size_t index) const {
    if (index >= attributes.size()) { return NULL_ATTR; }

    auto it = attributes.cbegin();
    while (index--) { it++; }

    return (*it);
}


sp_Attr NamedNodeMap::getNamedItem(const std::string &name) const {
    std::string LName = Utils::toLower(name);

    auto end = attributes.end();
    for (auto it = attributes.begin(); it != end; it++) {
        if ((*it)->name == LName) {
            return (*it);
        }
    }

    return nullptr;
}

sp_Attr &NamedNodeMap::setNamedItem(const sp_Attr &attr) {
    if (!attr) throw "InUseAttributeError"; // DOMException.

    auto end = attributes.end();
    for (auto it = attributes.begin(); it != end; it++) {
        if ((*it)->name == attr->name) {
            (*it)->value = attr->value;
            return (*it);
        }
    }


    auto aux = DOM_Node::Document::createAttribute(attr->name);
    aux->ownerElement = ownerElement;
    aux->value = attr->value;
    attributes.push_back(aux);

    return (attributes.back());
}

ReactString& NamedNodeMap::setNamedItem(const std::string &name, const std::string &value) {
    auto attr = DOM_Node::Document::createAttribute(name);
    attr = setNamedItem(attr);

    attr->referenced = true;
    attr->defalt = value;
    attr->value = value;

    return attr->value;
}

sp_Attr NamedNodeMap::removeNamedItem(const std::string &name) {
    std::string LName = Utils::toLower(name);

    auto end = attributes.end();
    for (auto it = attributes.begin(); it != end; it++) {
        if ((*it)->name == LName) {
            if ((*it)->referenced) {
                (*it)->value = (*it)->defalt;
                return nullptr;

            } else {
                sp_Attr res = (*it);
                attributes.erase(it);

                res->handle_attribute_changes(res->value, "");
                res->ownerElement = nullptr;

                return res;
            }
        }
    }

    //todo, throw a "NotFoundError" DOMException.
    return nullptr;
}
