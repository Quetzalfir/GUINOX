/**
* @brief The DocumentOrShadowRoot mixin of the Shadow DOM API provides
* APIs that are shared between documents and shadow roots.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://dom.spec.whatwg.org/#mixin-documentorshadowroot
*/

#ifndef GUINOX_DOCUMENTORSHADOWROOT_HPP
#define GUINOX_DOCUMENTORSHADOWROOT_HPP

/**
 * Note: The DocumentOrShadowRoot interface is expected to be used by other standards that want to define APIs shared
 * between document and shadow roots.
 */
class DocumentOrShadowRoot {

public:
    //activeElement Read only
    //Returns the Element within the shadow tree that has focus.

    //fullscreenElement Read only
    //Returns the Element that's currently in full screen mode for this document.

    //pointerLockElement Read only
    //Returns the element set as the target for mouse events while the pointer is locked. It returns null if lock is pending, the pointer is unlocked, or if the target is in another document.

    //styleSheets Read only
    //Returns a StyleSheetList of CSSStyleSheet objects for stylesheets explicitly linked into, or embedded in a document.


    //caretPositionFromPoint()
    //Returns a CaretPosition object containing the DOM node containing the caret, and caret's character offset within that node.
    
    //elementFromPoint()
    //Returns the topmost element at the specified coordinates.

    //elementsFromPoint()
    //Returns an array of all elements at the specified coordinates.

    //getSelection()
    //Returns a Selection object representing the range of text selected by the user, or the current position of the caret.

    //nodeFromPoint()
    //Returns the topmost node at the specified coordinates.

    //nodesFromPoint()
    //Returns an array of all nodes at the specified coordinates.

};


#endif //GUINOX_DOCUMENTORSHADOWROOT_HPP
