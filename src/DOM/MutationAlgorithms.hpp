/**
* @brief It is an abstract static class that contains all
* the modifying algorithms of the DOM Tree.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
*/
#ifndef GUINOX_DOM_MUTATIONALGORITHMS_HPP
#define GUINOX_DOM_MUTATIONALGORITHMS_HPP


#include "dom_declarations.hpp"
#include "Range.hpp"


class MutationAlgorithms {

    DOM_Node::Node *self;

public:
    MutationAlgorithms(DOM_Node::Node *node) : self(node) {}
    MutationAlgorithms(const DOM_Node::sp_Node& node) : self(node.get()) {}


    bool pre_insertion_validity(DOM_Node::Node *node, DOM_Node::Node *child) const ;
    bool pre_insertion_validity(DOM_Node::sp_Node node, DOM_Node::sp_Node child) const ;

    DOM_Node::Node *pre_insert(DOM_Node::Node *node, DOM_Node::Node *child);
    DOM_Node::Node *pre_insert(const DOM_Node::sp_Node& node, DOM_Node::sp_Node child);

    void insert(DOM_Node::Node *node, DOM_Node::Node *child, bool suppress_observers = false);
    void insert(DOM_Node::sp_Node node, DOM_Node::sp_Node child, bool suppress_observers = false);

    DOM_Node::Node *append(DOM_Node::Node *node);
    DOM_Node::Node *append(DOM_Node::sp_Node node);

    DOM_Node::sp_Node replace(DOM_Node::Node *child, DOM_Node::Node *node);
    DOM_Node::sp_Node replace(DOM_Node::sp_Node child, DOM_Node::sp_Node node);

    void replace_all(DOM_Node::sp_Node node);
    void replace_all(DOM_Node::Node *node);

    DOM_Node::sp_Node pre_remove(DOM_Node::sp_Node child);
    DOM_Node::sp_Node pre_remove(DOM_Node::Node *child);

    static void remove(DOM_Node::Node *child, bool suppress_observers = false);
    static void remove(DOM_Node::sp_Node child, bool suppress_observers = false);

    static DOM_Node::sp_Node convert_nodes_into_a_node(DOM_Node::NodeList & nodes, DOM_Node::Document * document);

    void string_replace_all(const std::string & str);

    void forEachLiveRange(const std::function<void(std::shared_ptr<Range> &)> & toDo);

    void attributeChangedCallback(DOMString name, DOMString oldValue, DOMString newValue);
    void adoptedCallback(DOM_Node::Document *oldDocument, DOM_Node::Document *document);


    void children_changed_steps();


};


#endif //GUINOX_DOM_MUTATIONALGORITHMS_HPP
