#include "DocumentFragment.hpp"
#include "Element.hpp"
#include <CSSOM/CSSSelector.hpp>
#include <algorithm>

namespace DOM_Node {

    DocumentFragment::DocumentFragment() {
        _nodeType = DOCUMENT_FRAGMENT_NODE;
        _nodeName = "#document-fragment";
    }

    sp_Element DocumentFragment::host() const {
        if (_host) {
            return std::dynamic_pointer_cast<DOM_Node::Element>(_host->shared_from_this());
        } else {
            return nullptr;
        }
    }

    sp_Node DocumentFragment::cloneNode(bool deep) const {
        if (dynamic_cast<ShadowRoot *>( const_cast<DocumentFragment *>(this) ))
            return nullptr;

        auto res = std::shared_ptr<DocumentFragment>( new DocumentFragment() );

        clone(res.get(), deep);

        return res;
    }

    sp_Element DocumentFragment::getElementById(const std::string &id) const {
        for (const auto & child : children) {
            auto result = child->getElementById(id);

            if (result) {
                return result;
            }
        }

        return nullptr;
    }

    DOM_Node::HTMLCollection DocumentFragment::querySelectorAll(const std::string &selector) {
        HTMLCollection res;

        for (auto & child : children) {
            auto tmp = CSSSelector::select(selector, child);

            res.insert(res.end(), tmp.begin(), tmp.end());
        }

        std::sort( res.begin(), res.end() );
        res.erase( std::unique( res.begin(), res.end() ), res.end() );

        return res;
    }

}