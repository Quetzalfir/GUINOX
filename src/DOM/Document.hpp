/**
* @brief DOM_Node is a namespace that encompasses all types of nodes that belong to the DOM.
*
*/
#ifndef GUINOX_DOM_NODE_DOCUMENT_H
#define GUINOX_DOM_NODE_DOCUMENT_H


#include "Node.hpp"
#include "Attr.hpp"
#include "HTMLElement.hpp"
#include "DocumentType.hpp"
#include "DocumentFragment.hpp"
#include "DocumentOrShadowRoot.hpp"
#include "NonElementParentNode.hpp"
#include "TreeWalker.hpp"
#include "NodeIterator.hpp"
#include "Range.hpp"
#include "DocumentStyle.hpp"
#include "../RenderContext/DocumentPresentation.hpp"



class DOMParser;
class HTMLStyleElement;
class HTMLLinkElement;
class Window;
class CSSOMTravel;
class ElementStyleContext;
//class Stylizable;
//class Z_Context;



namespace DOM_Node {

    /**
     * @brief The Document interface represents any web page loaded
     * in the browser and serves as an entry point into the web page's
     * content, which is the DOM tree.
     *
     * @author GUINOX
     * @version 1.0
     * @date 24/02/2020
     *
     * @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Document
     */

    class Document :
            public virtual Node,
            public ParentNode,
            public NonElementParentNode,
            public DocumentOrShadowRoot,
            public DocumentStyle,
            protected DocumentPresentation {

        friend class ::DOMImplementation;
        friend class ::DOMParser;
        friend class ::Range;
        friend class ::NodeIterator;
        friend class ::MutationAlgorithms;
        friend class ::HTMLStyleElement;
        friend class ::HTMLLinkElement;
        friend class ::HTMLElement;
        friend class ::Window;
        friend class ::ElementStyleContext;
        friend class ::Presentation;
        //friend class Z_Context;



        std::string _URL;
        //std::string _documentURI;
        std::string _origin;
        //std::string _compatMode;
        std::string _contentType;


        sp_DocumentType _doctype = nullptr;
        sp_Element _documentElement = nullptr; //<html>



        bool _building = false;
        std::vector<std::string> _scripts;
        std::vector<std::weak_ptr<Range>> liveRanges;
        std::vector<std::weak_ptr<NodeIterator>> nodeIterators;


        //Z_Context _z_context;


        //NodeList _anchors;
        //std::string _characterSet;

        //StyleSheetSet _styleSheetSets;
        //std::string _visibilityState;
        //DocumentTimeline timeline;
        //Element *_scrollingElement;
        //std::string _preferredStyleSheetSet;
        //StyleSheetSet _lastStyleSheetSet;


        void manageNewChild(sp_Node &child);
        void checkLiveRanges();
        void checkNodeIterators();


    protected:

        Document();

        void clone(Node *copy, bool deep) const override ;


    public:
        ///const Z_Context &z_context = _z_context;
        const std::string & URL = _URL;
        const std::string & origin = _origin;
        const std::string & contentType = _contentType;
        const sp_DocumentType & doctype = _doctype;
        const std::shared_ptr<HTMLElement> body;
        const sp_Element & documentElement = _documentElement; //<html>
        static DOMImplementation implementation;
        // Element *const & scrollingElement = _scrollingElement;
        //const std::vector<std::string> &scripts = _scripts;


        //HTML5
        sp_Element getElementById(const std::string & id) const override ;
        HTMLCollection getElementsByTagName(const std::string & qualifiedName);
        //HTMLCollection getElementsByTagNameNS(const std::string & qualifiedName);
        HTMLCollection getElementsByClassName(const std::string & className);


        // Node inherit
        sp_Node cloneNode(bool deep) const override;
        //bool isEqualNode(sp_Node node) const override;
        sp_Node appendChild(sp_Node aChild) override;
        sp_Node insertBefore(sp_Node newNode, sp_Node referenceNode) override;
        sp_Node removeChild(sp_Node child) override;
        sp_Node replaceChild(sp_Node newChild, sp_Node oldChild) override;

        // ParentNode inherit
        DOM_Node::HTMLCollection querySelectorAll(const std::string & selector) override;

        static sp_Element createElement(const std::string & tagName);
        //sp_Element createElementNS(const std::string & ns, const std::string & tagName);
        static sp_DocumentFragment createDocumentFragment();
        static sp_Text createTextNode(const std::string & data);
        //sp_CDATASection CDATASection createCDATASection(const std::string & data);
        //sp_Comment createComment(const std::string & data);
        //sp_ProcessingInstruction createProcessingInstruction(const std::string & target, const std::string & data);


        sp_Node importNode(sp_Node node, bool deep = false) const ;
        sp_Node adoptNode(sp_Node node) const ;


        static sp_Attr createAttribute(const std::string & name);
        //Attr createAttributeNS(const std::string & name);


        Event createEvent(const std::string & interface); //todo


        sp_Range createRange();


        sp_NodeIterator createNodeIterator(sp_Node root, unsigned long whatToShow = 0xFFFFFFFF);
        sp_NodeIterator createNodeIterator(sp_Node root, unsigned long whatToShow, const NodeFilter &filter);

        static TreeWalker createTreeWalker(sp_Node root, unsigned long whatToShow, const NodeFilter &filter);
        static TreeWalker createTreeWalker(sp_Node root, unsigned long whatToShow = 0xFFFFFFFF);




        //bool hasFocus();
        //void open();
        //void close();
        //void write(const std::string & text);
        // void writeln(const std::string & text);


        //DocumentOrShadowRoot
        //Selection getSelection();
        //Element * elementFromPoint();
        //HTMLCollection elementsFromPoint();
        //CaretPosition caretPositionFromPoint()

        ~Document();

        //void render();
        //void stylize();


    };



/**
 * @brief The XMLDocument interface represents an XML document.
 *
 * It inherits from the generic Document and does not add any specific methods or properties to it:
 * nevertheless, several algorithms behave differently with the two types of documents.
 *
 * @author GUINOX
 * @version 1.0
 * @date 24/02/2020
 *
 * @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/XMLDocument
 */
    class XMLDocument : public Document {
        friend class ::DOMImplementation;

        XMLDocument() = default;
    };

}


#endif //GUINOX_DOM_NODE_DOCUMENT_H
