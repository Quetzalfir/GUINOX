#include "DocumentStyle.hpp"
#include <DOM/Element.hpp>
#include <CSSOM/html.css.h>
#include <CSSOM/CSSParser.hpp>
#include <CSSOM/CSSOMTravel.hpp>


DocumentStyle::DocumentStyle(DOM_Node::Document *document) {
    _document = document;

    auto defaultStyles = CSSParser::parse(HTML_CSS);
    defaultStyles->disabled = false;

    addStyleSheet(defaultStyles);
}

void DocumentStyle::addStyleSheet(sp_CSSStyleSheet &sheet) {
    if (!sheet) return;

    for (auto & s : _styleSheets) {
        if (s == sheet) return;
    }

    sheet->adopt(_document);

    if (styleSheets.size() > 1 && sheet->_ownerNode) {
        auto *ownerNode = dynamic_cast<DOM_Node::Node *>( sheet->_ownerNode );
        auto end = _styleSheets.end();
        auto it = _styleSheets.begin();
        it++; // skip default style sheet

        auto pos = ownerNode->compareDocumentPosition((*it)->ownerNode());
        while (!(pos & (DOM_Node::Node::DOCUMENT_POSITION_DISCONNECTED)) &&
                (pos & (DOM_Node::Node::DOCUMENT_POSITION_PRECEDING))) {

            it++;
            if (it == end) break;

            pos = ownerNode->compareDocumentPosition((*it)->ownerNode());
        }

        _styleSheets.insert(it, sheet);

    } else {
        _styleSheets.push_back(sheet);
    }
}

void DocumentStyle::removeStyleSheet(sp_CSSStyleSheet &sheet) {
    if (!sheet) return;

    sheet->disabled = true;

    auto end = _styleSheets.end();
    for (auto it = _styleSheets.begin(); it != end; it++) {
        if ((*it) == sheet) {
            (*it)->adopt(nullptr);
            _styleSheets.erase(it);
            break;
        }
    }
}