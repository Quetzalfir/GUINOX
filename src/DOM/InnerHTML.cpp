#include "InnerHTML.hpp"
#include "XMLSerializer.hpp"
#include "DOMParser.hpp"


InnerHTML::InnerHTML() : innerHTML(
        [this] { return getter(); },
        [this](const std::string & PH1, const std::string & PH2) { setter(PH1, PH2); }
        ) {}

std::string InnerHTML::getter() const {
    std::string str;

    for (const auto & child : childNodes) {
        str += "\n" + XMLSerializer::serializeToString(child);
    }

    return str;
}

void InnerHTML::setter(const std::string &olValue, const std::string &newValue) {
    auto fragment = DOMParser::fragmentParse(newValue);

    while (auto child = firstChild()) {
        removeChild(child);
    }

    appendChild(fragment);
}