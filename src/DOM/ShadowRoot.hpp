/**
* @brief The ShadowRoot interface of the Shadow DOM
* API is the root node of a DOM subtree that is rendered separately from a document's main DOM tree.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
* @details Is based on https://developer.mozilla.org/es/docs/Web/API/ShadowRoot
*/
#ifndef GUINOX_SHADOWROOT_HPP
#define GUINOX_SHADOWROOT_HPP


#include "DocumentFragment.hpp"
#include "DocumentOrShadowRoot.hpp"
#include "InnerHTML.hpp"

enum ShadowRootMode : char {
    OPEN,
    CLOSED
};


class ShadowRoot :
        public DOM_Node::DocumentFragment,
        public DocumentOrShadowRoot,
        public InnerHTML {

    friend class DOM_Node::Element;


private:
    ShadowRootMode _mode;


protected:
    ShadowRoot(ShadowRootMode mode_) {
        _mode = mode_;
    }


public:

    const ShadowRootMode & mode = _mode;
    //todo, attribute EventHandler onslotchange;

};


typedef std::shared_ptr<ShadowRoot> sp_ShadowRoot;


#endif //GUINOX_SHADOWROOT_HPP
