/**
* @brief The LinkStyle interface provides access to the
* associated CSS style sheet of a node.
*
* LinkStyle is a raw interface and no object of this type can be
* created; it is implemented by HTMLLinkElement and HTMLStyleElement objects.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/es/docs/Web/API/LinkStyle
*/

#ifndef GUINOX_LINKSTYLE_HPP
#define GUINOX_LINKSTYLE_HPP

#include "../CSSOM/StyleSheet.hpp"


class LinkStyle {

protected:
    sp_CSSStyleSheet _sheet = nullptr;


public:
    const sp_CSSStyleSheet & sheet = _sheet;

};


#endif //GUINOX_LINKSTYLE_HPP
