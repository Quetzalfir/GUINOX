/**
* @brief This is DOMParser class.
*
* The DOMParser interface provides the ability to parse
* XML or HTML source code from a string into a DOM Document.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/DOMParser
*/
#ifndef GUINOX_DOMPARSER_HPP
#define GUINOX_DOMPARSER_HPP

#include "../Utils/Parser.hpp"
#include "DocumentFragment.hpp"
#include <memory>

class DOMParser {

    struct Attribute {
        std::string name, value;
    };

    class Tree {
        DOM_Node::sp_Node tree;

    public:
        DOM_Node::NodeList nodes;


        void push(DOM_Node::sp_Node& node);
        void pop(const std::string & tagname);
    };


    static std::vector<Attribute> getAttributes(Parser &parser);
    static std::string getTagName(Parser &parser);
    static DOM_Node::sp_Node makeElement(Parser &parser);
    static void processAttributes(std::shared_ptr<DOM_Node::Element> & element, const std::vector<Attribute> &attr);
    static Tree parse(const char *buff, size_t length);
    static void tree2Document(Tree & tree, DOM_Node::sp_Document & document);


public:
    static DOM_Node::sp_Document parseFromFile(const std::string & path);

    static DOM_Node::sp_Document parseFromString(const std::string & html);
    static DOM_Node::sp_Document parseFromString(const char *buff, size_t length);

    static DOM_Node::sp_DocumentFragment fragmentParse(const std::string & html);
    static DOM_Node::sp_DocumentFragment fragmentParse(const char *buff, size_t length);

};


#endif //GUINOX_DOMPARSER_HPP
