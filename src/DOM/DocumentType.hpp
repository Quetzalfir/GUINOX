/**
* @brief DOM_Node is a namespace that encompasses all types of nodes that belong to the DOM.
*/
#ifndef GUINOX_DOCUMENTTYPE_HPP
#define GUINOX_DOCUMENTTYPE_HPP


#include "Node.hpp"
#include "ChildNode.hpp"
#include "DOMImplementation.hpp"

class DOMParser;

namespace DOM_Node {
    /**
    * @brief The DocumentType interface represents a Node containing a doctype.
    *
    * DocumentType nodes are simply known as doctypes.
    * Doctypes have an associated name, public ID, and system ID.
    * When a doctype is created, its name is always given.
    * Unless explicitly given when a doctype is created,
    * its public ID and system ID are the empty string.
    *
    * @author GUINOX
    * @version 1.0
    * @date 24/02/2020
    *
    * @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/DocumentType
     */
    class DocumentType :
            public virtual Node,
            public ChildNode {

        friend class ::DOMImplementation;


        std::string _name;
        std::string _publicId = "-//W3C//DTD HTML 4.01//EN";
        std::string _systemId = "http://www.w3.org/TR/html4/strict.dtd";


    protected:
        explicit DocumentType(std::string name);
        void clone(Node *copy, bool deep) const override ;

    public:
        const std::string &name = _name;
        const std::string &publicId = _publicId;
        const std::string &systemId = _systemId;


        // Node inherit
        inline size_t length() const override { return 0; }
        sp_Node cloneNode(bool deep) const override;
        bool isEqualNode(sp_Node node) const override;



    };
}


#endif //GUINOX_DOCUMENTTYPE_HPP
