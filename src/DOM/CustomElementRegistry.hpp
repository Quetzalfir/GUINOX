/**
* @brief This is CustomElementRegistry class.
*
* When the user defines his own element,
* he must identify a path of this class so that the
* analyzer can detect it.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
*/
#ifndef GUINOX_CUSTOMELEMENTREGISTRY_HPP
#define GUINOX_CUSTOMELEMENTREGISTRY_HPP


#include "ElementRegistry.hpp"

class CustomElementRegistry : public ElementRegistry {

public:
    static void define(const std::string & name, std::function<std::shared_ptr<HTMLElement>()> constructor);


    CustomElementRegistry(const std::string & tagName, std::function<std::shared_ptr<HTMLElement>()> constructor);
};


#endif //GUINOX_CUSTOMELEMENTREGISTRY_HPP
