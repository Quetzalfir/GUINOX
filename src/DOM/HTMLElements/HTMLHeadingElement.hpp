/**
* @brief The HTML <h1>–<h6> elements represent six levels of section headings.
*
* <h1> is the highest section level and <h6> is the lowest.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/heading_elements
*/

#ifndef GUINOX_HTMLHEADINGELEMENT_HPP
#define GUINOX_HTMLHEADINGELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLHeadingElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry registH1;
    static ElementRegistry registH2;
    static ElementRegistry registH3;
    static ElementRegistry registH4;
    static ElementRegistry registH5;
    static ElementRegistry registH6;


protected:
    explicit HTMLHeadingElement(const std::string & tag) : HTMLElement(tag) {}

};


#endif //GUINOX_HTMLHEADINGELEMENT_HPP
