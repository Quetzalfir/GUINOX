/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLHRELEMENT_HPP
#define GUINOX_HTMLHRELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLHRElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLHRElement() : HTMLElement("hr") {}


};


#endif //GUINOX_HTMLHRELEMENT_HPP
