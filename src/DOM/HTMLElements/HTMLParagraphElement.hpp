/**
* @brief The HTML <p> element represents a paragraph.
*
* Paragraphs are usually represented in visual media as blocks of text separated
* from adjacent blocks by blank lines and/or first-line indentation,
* but HTML paragraphs can be any structural grouping of related content, such as images or form fields.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p
*/

#ifndef GUINOX_HTMLPARAGRAPHELEMENT_HPP
#define GUINOX_HTMLPARAGRAPHELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLParagraphElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLParagraphElement() : HTMLElement("p") {}

};


#endif //GUINOX_HTMLPARAGRAPHELEMENT_HPP
