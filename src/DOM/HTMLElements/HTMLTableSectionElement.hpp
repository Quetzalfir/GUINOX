/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/tables.html#the-tbody-element
 */

#ifndef GUINOX_HTMLTABLESECTIONELEMENT_HPP
#define GUINOX_HTMLTABLESECTIONELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"
#include "HTMLTableRowElement.hpp"


class HTMLTableSectionElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry registTBody;
    static ElementRegistry registTHead;
    static ElementRegistry registTFoot;


    DOM_Node::HTMLCollection _rows;

    static DOM_Node::sp_Node getNextTRElement(DOM_Node::sp_Node & reference) {
        DOM_Node::sp_Node aux = reference;
        while (aux && aux->nodeName != "tr") {
            aux = aux->nextSibling();
        }

        return aux;
    }

    void insertTRElementBefore(std::shared_ptr<HTMLTableRowElement> & tr, DOM_Node::sp_Node reference) {
        auto end = _rows.end();

        for (auto it = _rows.begin(); it != end; it++) {
            if ((*it) == reference) {
                _rows.insert(it, tr);

                return;
            }
        }

        _rows.push_back(tr);
    }

    void removeTRElementChild(std::shared_ptr<HTMLTableRowElement> & tr) {
        auto end = _rows.end();
        for (auto it = _rows.begin(); it != end; it++) {
            if ((*it) == tr) {
                _rows.erase(it);

                return;
            }
        }
    }



protected:
    HTMLTableSectionElement(const std::string & tag) : HTMLElement(tag) {}



public:
    const DOM_Node::HTMLCollection & rows = _rows;

    std::shared_ptr<HTMLTableRowElement> insertRow(long index = -1) {
        if (index < -1 || index > _rows.size()) {
            // throw an "IndexSizeError" DOMException.
            return nullptr;
        }

        auto table_row = DOM_Node::Document::createElement("tr");

        if (index == -1 || index == _rows.size()) {
            appendChild(table_row);

        } else {
            insertBefore(table_row, _rows[index]);
        }

        return std::dynamic_pointer_cast<HTMLTableRowElement>( table_row );
    }


    void deleteRow(long index) {
        if (index < -1 || index > _rows.size()) {
            // throw an "IndexSizeError" DOMException.
            return;
        }

        if (index == -1 && !_rows.empty()) {
            removeChild(_rows.back());

        } else {
            removeChild(_rows[index]);
        }
    }




    // Overriding Node methods

    DOM_Node::sp_Node appendChild(DOM_Node::sp_Node aChild) override {
        auto child = HTMLElement::appendChild(aChild);

        // append to rows collection
        if (child && child->nodeType == Node::ELEMENT_NODE) {
            if (auto tr = std::dynamic_pointer_cast<HTMLTableRowElement>(child)) {
                _rows.push_back(tr);
            }
        }

        return child;
    }

    DOM_Node::sp_Node removeChild(DOM_Node::sp_Node child) override {
        auto aux = HTMLElement::removeChild(child);

        if (aux && aux->nodeType == Node::ELEMENT_NODE) {
            if (auto tr = std::dynamic_pointer_cast<HTMLTableRowElement>(child)) {
                // Remove from rows collection
                removeTRElementChild(tr);
            }
        }

        return aux;
    }

    DOM_Node::sp_Node insertBefore(DOM_Node::sp_Node newNode, DOM_Node::sp_Node referenceNode) override {
        auto aux = HTMLElement::insertBefore(newNode, referenceNode);

        if (aux && aux->nodeType == Node::ELEMENT_NODE) {
            if (auto tr = std::dynamic_pointer_cast<HTMLTableRowElement>(aux)) {
                auto reference = getNextTRElement(referenceNode);

                insertTRElementBefore(tr, reference);
            }
        }

        return aux;
    }

    DOM_Node::sp_Node replaceChild(DOM_Node::sp_Node newChild, DOM_Node::sp_Node oldChild) override {
        auto aux = HTMLElement::replaceChild(newChild, oldChild);

        if (aux) {
            auto newChildType = std::dynamic_pointer_cast<HTMLTableRowElement>( newChild );
            auto oldChildType = std::dynamic_pointer_cast<HTMLTableRowElement>( oldChild );

            if (newChildType && !oldChildType) {
                auto reference = getNextTRElement(oldChild);
                insertTRElementBefore(newChildType, reference);

            } else if (!newChildType && oldChildType) {
                removeTRElementChild(oldChildType);

            } else if (newChildType && oldChildType) {
                auto newElem = std::dynamic_pointer_cast<DOM_Node::Element>(newChild);

                auto end = _rows.end();
                for (auto it = _rows.begin(); it != end; it++) {
                    if ((*it) == oldChildType) {
                        std::replace(it, it, (*it), newElem);

                        return aux;
                    }
                }
            }
        }

        return aux;
    }

};


#endif //GUINOX_HTMLTABLESECTIONELEMENT_HPP
