/**
* @brief The HTML <meta> element represents metadata that cannot be represented by other HTML meta-related elements, like <base>, <link>, <script>, <style> or <title>.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta
*/

#ifndef GUINOX_HTMLMETAELEMENT_HPP
#define GUINOX_HTMLMETAELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLMetaElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLMetaElement() : HTMLElement("meta") {}


public:
    ReactString & name = setReferencedAttr("name", "");
    ReactString & httpEquiv = setReferencedAttr("httpEquiv", "");
    ReactString & content = setReferencedAttr("content", "");


};


#endif //GUINOX_HTMLMETAELEMENT_HPP
