/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLDIVELEMENT_HPP
#define GUINOX_HTMLDIVELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLDivElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLDivElement(const std::string & tag = "div") : HTMLElement(tag) {}


};


#endif //GUINOX_HTMLDIVELEMENT_HPP
