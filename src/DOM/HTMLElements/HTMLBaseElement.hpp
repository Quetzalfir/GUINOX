/**
* @brief The HTML <base> element specifies the base URL to use for all relative URLs in a document.
*
* There can be only one <base> element in a document.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base
*/

#ifndef GUINOX_HTMLBASEELEMENT_HPP
#define GUINOX_HTMLBASEELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLBaseElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLBaseElement() : HTMLElement("base") {}


public:
    ReactString & href = setReferencedAttr("href", "");
    ReactString & target = setReferencedAttr("target", "");


};


#endif //GUINOX_HTMLBASEELEMENT_HPP
