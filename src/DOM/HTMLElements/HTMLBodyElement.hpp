/**
* @brief The HTML <body> Element represents the content of an HTML document.
*
* There can be only one <body> element in a document.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/body
*/

#ifndef GUINOX_HTMLBODYELEMENT_HPP
#define GUINOX_HTMLBODYELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLBodyElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLBodyElement() : HTMLDivElement("body") {}


};


#endif //GUINOX_HTMLBODYELEMENT_HPP
