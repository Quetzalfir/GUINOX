/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/tables.html#the-caption-element
 */

#ifndef GUINOX_HTMLTABLECAPTIONELEMENT_HPP
#define GUINOX_HTMLTABLECAPTIONELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLTableCaptionElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLTableCaptionElement() : HTMLElement("caption") {}


};


#endif //GUINOX_HTMLTABLECAPTIONELEMENT_HPP
