/**
* @brief The HTML <img> element embeds an image into the document.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img
*/

#ifndef GUINOX_HTMLIMAGEELEMENT_HPP
#define GUINOX_HTMLIMAGEELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"
#include "../../Utils/PNG.hpp"
#include "../../OpenGL/Texture.hpp"


class HTMLImageElement : public HTMLElement {

    friend class ElementRegistry;
    static ElementRegistry regist;


    unsigned int texture;
    PNG *png = nullptr;
    long _x;
    long _y;


    void createNewTexture(const std::string & src);


protected:
    HTMLImageElement() : HTMLElement("img") {}


    void init(ViewPort &viewport) override ;
    void render(ViewPort &viewport) const override ;


    bool attributeChangedCallback(DOMString AttrName, DOMString oldvalue, DOMString newValue) override ;
    void insertion_steps() override ;
    void removing_steps(Node * parent = nullptr) override ;


public:
    ~HTMLImageElement();


    ReactString & width = setReferencedAttr("width", "0");
    ReactString & height = setReferencedAttr("height", "0");
    ReactString & src = setReferencedAttr("src", "");
    ReactString & alt = setReferencedAttr("alt", "");


    const long & x = _x;
    const long & y = _y;

};


#endif //GUINOX_HTMLIMAGEELEMENT_HPP
