/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLULISTELEMENT_HPP
#define GUINOX_HTMLULISTELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLUListElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLUListElement() : HTMLElement("ul") {}


};


#endif //GUINOX_HTMLULISTELEMENT_HPP
