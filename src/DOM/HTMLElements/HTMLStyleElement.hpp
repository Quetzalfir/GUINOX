/**
* @brief The HTML <style> element contains style information for a document, or part of a document.
*
* It contains CSS, which is applied to the contents of the document containing the <style> element.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/style
*/

#ifndef GUINOX_HTMLSTYLEELEMENT_HPP
#define GUINOX_HTMLSTYLEELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"
#include "../LinkStyle.hpp"
#include "../Document.hpp"
#include "../../CSSOM/CSSParser.hpp"


class HTMLStyleElement :
        public HTMLElement,
        public LinkStyle {

    friend class ElementRegistry;
    static ElementRegistry regist;


    std::string _type;


    void setMediums(const std::string & media) {
        if (_sheet) {
            _sheet->_media = media;

            if (_ownerDocument) {
                if (!(_ownerDocument->_building)) {
                    _ownerDocument->fullReFlow();
                }
            }
        }
    }


protected:
    HTMLStyleElement() : HTMLElement("style") { }

    void children_changed_steps() override {
        HTMLElement::children_changed_steps();

        normalize();

        if (_ownerDocument) {
            sp_CSSStyleSheet styleSheet = CSSParser::parse(textContent, _ownerDocument->URL);
            if (styleSheet) {
                styleSheet->_media = media;
                styleSheet->_ownerNode = dynamic_cast<DOM_Node::Element *>( this );

            }

            _ownerDocument->removeStyleSheet(_sheet);
            _ownerDocument->addStyleSheet(styleSheet);

            _sheet = styleSheet;

            if (!(_ownerDocument->_building)) {
                _ownerDocument->fullReFlow();
            }
        }
    }

    void adopting_steps(DOM_Node::Document * oldDocument) override {
        HTMLElement::adopting_steps(oldDocument);

        if (oldDocument) {
            oldDocument->removeStyleSheet(_sheet);
        }
    }

    void insertion_steps() override {
        HTMLElement::insertion_steps();


        auto styleSheet = CSSParser::parse(textContent, _ownerDocument->URL);
        if (styleSheet) {
            styleSheet->_media = media;
            styleSheet->_ownerNode = dynamic_cast<DOM_Node::Element *>( this );
        }

        _ownerDocument->removeStyleSheet(_sheet);
        _ownerDocument->addStyleSheet(styleSheet);

        _sheet = styleSheet;

        if (!(_ownerDocument->_building)) {
            _ownerDocument->fullReFlow();
        }
    }

    void removing_steps(Node * parent = nullptr) override {
        HTMLElement::removing_steps(parent);

        _ownerDocument->removeStyleSheet(_sheet);
    }

    bool attributeChangedCallback(DOMString AttrName, DOMString oldvalue, DOMString newValue) override {
        if (!HTMLElement::attributeChangedCallback(AttrName, oldvalue, newValue)) {
            if (AttrName == "media") {
                setMediums(newValue);

                return true;

            }
        }

        return false;
    }


public:
    ReactString & media = setReferencedAttr("media", "");
    const std::string & type = _type;

};


#endif //GUINOX_HTMLSTYLEELEMENT_HPP
