#include "HTMLImageElement.hpp"
#include "../Document.hpp"




void HTMLImageElement::createNewTexture(const std::string &src_) {
    if (_ownerDocument) {
        auto &url = _ownerDocument->URL;
        int found = url.find_last_of('/');
        std::string path = url.substr(0, found + 1) + src_;

        delete png;
        png = new PNG(path);
    }
}

HTMLImageElement::~HTMLImageElement() {
    delete png;
}


void HTMLImageElement::init(ViewPort &viewport) {
    if (png) {
        viewport.addTexture(src, png->getWidth(), png->getHeight(), (int*)png->getPixels(), png->getFormat());

        ElementRenderizable::_width = png->getWidth();
        ElementRenderizable::_height = png->getHeight();
        ElementRenderizable::_renderizable = true;
    }
}

void HTMLImageElement::render(ViewPort &viewport) const {
    auto rects = getClientRects();

    auto & content = rects.back();
    viewport.setVertex(content);

    viewport.renderTexture(src, content.getVertex());
}

bool HTMLImageElement::attributeChangedCallback(DOMString AttrName, DOMString oldvalue, DOMString newValue) {
    if (!HTMLElement::attributeChangedCallback(AttrName, oldvalue, newValue)) {
        if (AttrName == "src") {
            createNewTexture(newValue);
            return true;

        } else if (AttrName == "width") {

            return true;

        } else if (AttrName == "height") {

            return true;

        } else if (AttrName == "alt") {

            return true;
        }
    }

    return false;
}

void HTMLImageElement::insertion_steps() {
    HTMLElement::insertion_steps();
    createNewTexture(src);
}

void HTMLImageElement::removing_steps(Node *parent) {
    HTMLElement::removing_steps(parent);

    //fixme, cant remove, needs viewport
}


