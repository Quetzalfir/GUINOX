/**
* @brief The HTML Content Template (<template>) element is a mechanism
* for holding HTML that is not to be rendered immediately when a page is loaded.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/template
*/

#ifndef GUINOX_HTMLTEMPLATEELEMENT_HPP
#define GUINOX_HTMLTEMPLATEELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"
#include "../Document.hpp"
#include "../DocumentFragment.hpp"

class HTMLTemplateElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;

protected:
    DOM_Node::sp_DocumentFragment _content;


    HTMLTemplateElement() :
            HTMLElement("template"),
            _content(DOM_Node::Document::createDocumentFragment()) {}


public:
    const DOM_Node::sp_DocumentFragment & content = _content;

    bool isEqualNode(const DOM_Node::sp_Node node) const override {
        return _content->isEqualNode(node);
    }

    DOM_Node::sp_Node cloneNode(bool deep) const override {
        //todo, may return a legal clone
        return deep ? _content->cloneNode(true) : nullptr ;
    }

    DOM_Node::sp_Node appendChild(DOM_Node::sp_Node aChild) override {
        return _content->appendChild(aChild);
    }

    DOM_Node::sp_Node insertBefore(DOM_Node::sp_Node newNode, DOM_Node::sp_Node referenceNode) override {
        return _content->insertBefore(newNode, referenceNode);
    }

    DOM_Node::sp_Node removeChild(DOM_Node::sp_Node child) override {
        return _content->removeChild(child);
    }

    DOM_Node::sp_Node replaceChild(DOM_Node::sp_Node newChild, DOM_Node::sp_Node oldChild) override {
        return _content->replaceChild(newChild, oldChild);
    }


};


#endif //GUINOX_HTMLTEMPLATEELEMENT_HPP
