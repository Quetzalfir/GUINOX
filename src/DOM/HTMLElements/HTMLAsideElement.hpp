/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/sections.html#the-aside-element
*/


#ifndef GUINOX_HTMLASIDEELEMENT_HPP
#define GUINOX_HTMLASIDEELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLAsideElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLAsideElement() : HTMLDivElement("aside") {}


};


#endif //GUINOX_HTMLASIDEELEMENT_HPP
