/**
* @brief The HTML <slot> element—part of the Web Components technology
* suite—is a placeholder inside a web component that you can fill with
* your own markup, which lets you create separate DOM trees and present them together.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/slot
*/

#ifndef GUINOX_HTMLSLOTELEMENT_HPP
#define GUINOX_HTMLSLOTELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLSlotElement : public HTMLElement {
    friend class ElementRegistry;
    friend class Slotable;
    friend class MutationAlgorithms;
    static ElementRegistry regist;


    std::vector<DOM_Node::sp_Node> _assignedNodes;


    static std::vector<DOM_Node::sp_Node> find_flattened_slotables(DOM_Node::sp_Node slot) {
        std::vector<DOM_Node::sp_Node> result;

        auto shadow = std::dynamic_pointer_cast<ShadowRoot>( slot->getRootNode() );

        if (!shadow) return result;

        auto slotables = Slotable::find_slotables(slot);

        if (slotables.empty()) {
            for (auto & child : slot->childNodes) {
                auto slotable = std::dynamic_pointer_cast<Slotable>(child);

                if (slotable) {
                    slotables.push_back(slotable);
                }
            }
        }

        for (auto & node : slotables) {
            auto isSlot = std::dynamic_pointer_cast<HTMLSlotElement>(node);
            auto root = std::dynamic_pointer_cast<ShadowRoot>( node->getRootNode() );

            if (isSlot && root) {
                auto temporaryResult = find_flattened_slotables(node);

                result.insert(result.end(), temporaryResult.begin(), temporaryResult.end());

            } else {
                result.push_back(node);
            }
        }

        return result;
    }

    void signal_slot_change() {
        //Each similar-origin window agent has signal slots (a set of slots), which is initially empty. [HTML]

        //To signal a slot change, for a slot slot, run these steps:

        //1. Append slot to slot’s relevant agent’s signal slots.

        //2. Queue a mutation observer microtask.
    }


protected:
    HTMLSlotElement() : HTMLElement("slot") {}

    virtual bool attributeChangedCallback(DOMString AttrName, DOMString oldvalue, DOMString newValue) override {
        Element::attributeChangedCallback(AttrName, oldvalue, newValue);

        if (AttrName == "name") {
            Slotable::assign_slotables_for_tree( this->getRootNode() );
            return true;
        }

        return false;
    }


public:
    ReactString & name = setReferencedAttr("name","");



    std::vector<DOM_Node::sp_Node> assignedNodes(bool flatten = false) {
        if (!flatten) return _assignedNodes;

        return find_flattened_slotables(this->shared_from_this());
    }

    std::vector<DOM_Node::sp_Element> assignedElements(bool flatten = false) {
        std::vector<DOM_Node::sp_Element> result;

        for (const auto & node : assignedNodes(flatten)) {
            if (node->nodeType == DOM_Node::Node::ELEMENT_NODE) {
                result.push_back(std::dynamic_pointer_cast<DOM_Node::Element>(node));
            }
        }

        return result;
    }

};


#endif //GUINOX_HTMLSLOTELEMENT_HPP
