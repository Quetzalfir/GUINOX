/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/grouping-content.html#the-ol-element
 */


#ifndef GUINOX_HTMLOLISTELEMENT_HPP
#define GUINOX_HTMLOLISTELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLOListElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLOListElement() : HTMLElement("ol") {}


    ReactString & reversed = setReferencedAttr("reversed", "false");
    ReactString & cite = setReferencedAttr("start", "1");
    ReactString & type = setReferencedAttr("type", "decimal");


};


#endif //GUINOX_HTMLOLISTELEMENT_HPP
