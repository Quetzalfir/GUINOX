/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/tables.html#the-td-element
 */

#ifndef GUINOX_HTMLTABLECELLELEMENT_HPP
#define GUINOX_HTMLTABLECELLELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLTableCellElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry registTD;
    static ElementRegistry registTH;


protected:
    HTMLTableCellElement(const std::string & tag) : HTMLElement(tag) {}


    //[CEReactions] attribute unsigned long colSpan;
    //[CEReactions] attribute unsigned long rowSpan;
    //[CEReactions] attribute DOMString headers;
    //readonly attribute long cellIndex;

    //[CEReactions] attribute DOMString scope; // only conforming for th elements
    //[CEReactions] attribute DOMString abbr;  // only conforming for th elements

};


#endif //GUINOX_HTMLTABLECELLELEMENT_HPP
