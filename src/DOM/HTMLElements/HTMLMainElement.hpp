/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/grouping-content.html#the-main-element
*/

#ifndef GUINOX_HTMLMAINELEMENT_HPP
#define GUINOX_HTMLMAINELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLMainElement: public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLMainElement() : HTMLDivElement("main") {}


};


#endif //GUINOX_HTMLMAINELEMENT_HPP
