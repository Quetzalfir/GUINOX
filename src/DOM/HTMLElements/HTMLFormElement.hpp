/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/forms.html#the-form-element
*/

#ifndef GUINOX_HTMLFORMELEMENT_HPP
#define GUINOX_HTMLFORMELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLFormElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;

    DOMTokenList _relList;


protected:
    HTMLFormElement() : HTMLElement("form") {}


public:
    ReactString & acceptCharset = setReferencedAttr("acceptCharset", "");
    ReactString & action = setReferencedAttr("action", "");
    ReactString & autocomplete = setReferencedAttr("autocomplete", "");
    ReactString & enctype = setReferencedAttr("enctype", "");
    ReactString & encoding = setReferencedAttr("encoding", "");
    ReactString & method = setReferencedAttr("method", "");
    ReactString & name = setReferencedAttr("name", "");
    ReactString & noValidate = setReferencedAttr("noValidate", "");
    ReactString & target = setReferencedAttr("target", "");
    ReactString & rel = setReferencedAttr("rel", "");

    const DOMTokenList & relList = _relList;


    //[SameObject] readonly attribute HTMLFormControlsCollection elements;
    //readonly attribute unsigned long length;
    //getter Element (unsigned long index);
    //getter (RadioNodeList or Element) (DOMString name);

    //void submit();
    //void requestSubmit(std::shared_ptr<HTMLElement> submitter = nullptr);
    //void reset();
    //bool checkValidity();
    //bool reportValidity();

};


#endif //GUINOX_HTMLFORMELEMENT_HPP
