/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/grouping-content.html#the-figure-element
*/

#ifndef GUINOX_HTMLFIGUREELEMENT_HPP
#define GUINOX_HTMLFIGUREELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLFigureElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLFigureElement() : HTMLDivElement("figure") {}


};


#endif //GUINOX_HTMLFIGUREELEMENT_HPP
