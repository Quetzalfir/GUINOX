/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/sections.html#the-article-element
*/

#ifndef GUINOX_HTMLARTICLEELEMENT_HPP
#define GUINOX_HTMLARTICLEELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLArticleElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLArticleElement() : HTMLDivElement("article") {}


};


#endif //GUINOX_HTMLARTICLEELEMENT_HPP
