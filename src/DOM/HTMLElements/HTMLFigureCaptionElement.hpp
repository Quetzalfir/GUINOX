/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/grouping-content.html#the-figcaption-element
*/

#ifndef GUINOX_HTMLFIGURECAPTIONELEMENT_HPP
#define GUINOX_HTMLFIGURECAPTIONELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLFigureCaptionElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLFigureCaptionElement() : HTMLDivElement("figcaption") {}

};


#endif //GUINOX_HTMLFIGURECAPTIONELEMENT_HPP
