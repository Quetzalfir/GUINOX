/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/grouping-content.html#the-li-element
*/

#ifndef GUINOX_HTMLLIELEMENT_HPP
#define GUINOX_HTMLLIELEMENT_HPP


class HTMLLIElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLLIElement() : HTMLElement("li") {}


    ReactString & value = setReferencedAttr("value", "");


};


#endif //GUINOX_HTMLLIELEMENT_HPP
