/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/sections.html#the-address-element
*/
#ifndef GUINOX_HTMLADDRESSELEMENT_HPP
#define GUINOX_HTMLADDRESSELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLAddressElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLAddressElement() : HTMLDivElement("address") {}


};


#endif //GUINOX_HTMLADDRESSELEMENT_HPP
