/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/grouping-content.html#the-dl-element
*/


#ifndef GUINOX_HTMLDLISTELEMENT_HPP
#define GUINOX_HTMLDLISTELEMENT_HPP


class HTMLDListElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry registDL;
    static ElementRegistry registDT;
    static ElementRegistry registDD;


protected:
    HTMLDListElement(const std::string & tag) : HTMLElement(tag) {}


};


#endif //GUINOX_HTMLDLISTELEMENT_HPP
