/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/tables.html#the-tr-element
 */

#ifndef GUINOX_HTMLTABLEROWELEMENT_HPP
#define GUINOX_HTMLTABLEROWELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"
#include "HTMLTableCellElement.hpp"


class HTMLTableRowElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


    DOM_Node::HTMLCollection _cells;


    static DOM_Node::sp_Node getNextCellElement(DOM_Node::sp_Node & reference) {
        DOM_Node::sp_Node aux = reference;
        while (aux && !(std::dynamic_pointer_cast<HTMLTableCellElement>( aux ))) {
            aux = aux->nextSibling();
        }

        return aux;
    }

    void insertCellElementBefore(std::shared_ptr<HTMLTableCellElement> & td, DOM_Node::sp_Node reference) {
        auto end = _cells.end();

        for (auto it = _cells.begin(); it != end; it++) {
            if ((*it) == reference) {
                _cells.insert(it, td);

                return;
            }
        }

        _cells.push_back(td);
    }

    void removeCellElementChild(std::shared_ptr<HTMLTableCellElement> & td) {
        auto end = _cells.end();
        for (auto it = _cells.begin(); it != end; it++) {
            if ((*it) == td) {
                _cells.erase(it);

                return;
            }
        }
    }


protected:
    HTMLTableRowElement() : HTMLElement("tr") {}


public:
    const DOM_Node::HTMLCollection & cells = _cells;


    long rowIndex() const {
        //todo,

        return 0;
    }


    long sectionRowIndex() const  {
        //todo,

        return 0;
    }


    std::shared_ptr<HTMLTableCellElement> insertCell(long index = -1) {
        if (index < -1 || index > _cells.size()) {
            // throw an "IndexSizeError" DOMException.
            return nullptr;
        }

        auto table_cell = DOM_Node::Document::createElement("td");

        if (index == -1 || index == _cells.size()) {
            appendChild(table_cell);

        } else {
            insertBefore(table_cell, _cells[index]);
        }

        return std::dynamic_pointer_cast<HTMLTableCellElement>( table_cell );
    }

    void deleteCell(long index) {
        if (index < -1 || index > _cells.size()) {
            // throw an "IndexSizeError" DOMException.
            return;
        }

        if (index == -1 && !_cells.empty()) {
            removeChild(_cells.back());

        } else {
            removeChild(_cells[index]);
        }
    }



    // Overriding Node methods

    DOM_Node::sp_Node appendChild(DOM_Node::sp_Node aChild) override {
        auto child = HTMLElement::appendChild(aChild);

        // append to rows collection
        if (child && child->nodeType == Node::ELEMENT_NODE) {
            if (auto tr = std::dynamic_pointer_cast<HTMLTableCellElement>(child)) {
                _cells.push_back(tr);
            }
        }

        return child;
    }

    DOM_Node::sp_Node removeChild(DOM_Node::sp_Node child) override {
        auto aux = HTMLElement::removeChild(child);

        if (aux && aux->nodeType == Node::ELEMENT_NODE) {
            if (auto td = std::dynamic_pointer_cast<HTMLTableCellElement>(child)) {
                // Remove from rows collection
                removeCellElementChild(td);
            }
        }

        return aux;
    }

    DOM_Node::sp_Node insertBefore(DOM_Node::sp_Node newNode, DOM_Node::sp_Node referenceNode) override {
        auto aux = HTMLElement::insertBefore(newNode, referenceNode);

        if (aux && aux->nodeType == Node::ELEMENT_NODE) {
            if (auto td = std::dynamic_pointer_cast<HTMLTableCellElement>(aux)) {
                auto reference = getNextCellElement(referenceNode);

                insertCellElementBefore(td, reference);
            }
        }

        return aux;
    }

    DOM_Node::sp_Node replaceChild(DOM_Node::sp_Node newChild, DOM_Node::sp_Node oldChild) override {
        auto aux = HTMLElement::replaceChild(newChild, oldChild);

        if (aux) {
            auto newChildType = std::dynamic_pointer_cast<HTMLTableCellElement>( newChild );
            auto oldChildType = std::dynamic_pointer_cast<HTMLTableCellElement>( oldChild );

            if (newChildType && !oldChildType) {
                auto reference = getNextCellElement(oldChild);
                insertCellElementBefore(newChildType, reference);

            } else if (!newChildType && oldChildType) {
                removeCellElementChild(oldChildType);

            } else if (newChildType && oldChildType) {
                auto newElem = std::dynamic_pointer_cast<DOM_Node::Element>(newChild);

                auto end = _cells.end();
                for (auto it = _cells.begin(); it != end; it++) {
                    if ((*it) == oldChildType) {
                        std::replace(it, it, (*it), newElem);

                        return aux;
                    }
                }
            }
        }

        return aux;
    }
};


#endif //GUINOX_HTMLTABLEROWELEMENT_HPP
