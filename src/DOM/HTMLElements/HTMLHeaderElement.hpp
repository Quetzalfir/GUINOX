/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLHEADERELEMENT_HPP
#define GUINOX_HTMLHEADERELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLHeaderElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLHeaderElement() : HTMLDivElement("header") {}


};


#endif //GUINOX_HTMLHEADERELEMENT_HPP
