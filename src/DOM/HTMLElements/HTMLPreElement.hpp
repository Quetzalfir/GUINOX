/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLPREELEMENT_HPP
#define GUINOX_HTMLPREELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLPreElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLPreElement() : HTMLElement("pre") {}


};


#endif //GUINOX_HTMLPREELEMENT_HPP
