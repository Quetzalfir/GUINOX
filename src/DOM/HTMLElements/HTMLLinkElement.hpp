/**
* @brief The HTML External Resource Link element (<link>) specifies relationships between the current document and an external resource.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link
*/

#ifndef GUINOX_HTMLLINKELEMENT_HPP
#define GUINOX_HTMLLINKELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"
#include "../LinkStyle.hpp"
#include "../Document.hpp"
#include "../../CSSOM/CSSParser.hpp"


class HTMLLinkElement :
        public HTMLElement,
        public LinkStyle {

    friend class ElementRegistry;
    static ElementRegistry regist;

    DOMTokenList _relList;
    DOMTokenList _sizes;


    void setStyleSheet(const std::string & href) {

        if (_ownerDocument) {
            std::string path;
            auto & url = _ownerDocument->URL;
            int found = url.find_last_of('/');

            if (found != std::string::npos) {
                path = url.substr(0,found+1) + href;
            } else {
                path = href;
            }

            auto styleSheet = CSSParser::parseFromFile(path);

            if (styleSheet) {
                styleSheet->_media = media;
                styleSheet->_ownerNode = dynamic_cast<DOM_Node::Element *>( this );

            }

            _ownerDocument->removeStyleSheet(_sheet);
            _ownerDocument->addStyleSheet(styleSheet);

            _sheet = styleSheet;

            if (!(_ownerDocument->_building)) {
                _ownerDocument->fullReFlow();
            }
        }
    }

    void setRelation(const std::string &href) {
        if (href.empty()) return;

        if (rel == "stylesheet") {
            setStyleSheet(href);
        }
    }

    void setMediums(const std::string & media) {
        if (_sheet) {
            _sheet->_media = media;

            if (_ownerDocument) {
                if (!(_ownerDocument->_building)) {
                    _ownerDocument->fullReFlow();
                }
            }
        }
    }


protected:
    HTMLLinkElement() : HTMLElement("link") {}


    bool attributeChangedCallback(DOMString AttrName, DOMString oldvalue, DOMString newValue) override {
        if (!HTMLElement::attributeChangedCallback(AttrName, oldvalue, newValue)) {
            if (AttrName == "href") {
                setRelation(newValue);

                return true;

            } else if (AttrName == "rel") {
                setRelation(href);

                return true;

            } else if (AttrName == "media") {
                setMediums(newValue);

                return true;

            }
        }

        return false;
    }

    void adopting_steps(DOM_Node::Document * oldDocument) override {
        HTMLElement::adopting_steps(oldDocument);

        if (oldDocument) {
            oldDocument->removeStyleSheet(_sheet);
        }
    }

    void insertion_steps() override  {
        HTMLElement::insertion_steps();

        if (rel == "stylesheet") {
            setRelation(href);
        }
    }

    void removing_steps(Node * parent = nullptr) override  {
        HTMLElement::removing_steps(parent);

        if (rel == "stylesheet") {
            _ownerDocument->removeStyleSheet(_sheet);
        }
    }


public:
    ReactString & href = setReferencedAttr("href", "");
    ReactString & crossOrigin = setReferencedAttr("crossOrigin", "");
    ReactString & rel = setReferencedAttr("rel", "");
    ReactString & as = setReferencedAttr("as", "");
    const DOMTokenList & relList = _relList;
    ReactString & media = setReferencedAttr("media", "all");
    ReactString & integrity = setReferencedAttr("integrity", "");
    ReactString & hreflang = setReferencedAttr("hreflang", "");
    ReactString & type = setReferencedAttr("type", "");
    const DOMTokenList & sizes = _sizes;
    ReactString & imageSrcset = setReferencedAttr("imageSrcset", "");
    ReactString & imageSizes = setReferencedAttr("imageSizes", "");
    ReactString & referrerPolicy = setReferencedAttr("referrerPolicy", "");


};


#endif //GUINOX_HTMLLINKELEMENT_HPP
