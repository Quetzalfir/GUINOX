/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/sections.html#the-footer-element
*/

#ifndef GUINOX_HTMLFOOTERELEMENT_HPP
#define GUINOX_HTMLFOOTERELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLFooterElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLFooterElement() : HTMLDivElement("footer") {}


};


#endif //GUINOX_HTMLFOOTERELEMENT_HPP
