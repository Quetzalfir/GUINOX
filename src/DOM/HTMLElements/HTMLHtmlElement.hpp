/**
* @brief The HTML <html> element represents the root (top-level element) of an HTML document,
* so it is also referred to as the root element.
*
* All other elements must be descendants of this element.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/html
*/

#ifndef GUINOX_HTMLHTMLELEMENT_HPP
#define GUINOX_HTMLHTMLELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLHtmlElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLHtmlElement() : HTMLElement("html") {}

};


#endif //GUINOX_HTMLHTMLELEMENT_HPP
