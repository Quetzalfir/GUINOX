/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLSPANELEMENT_HPP
#define GUINOX_HTMLSPANELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLSpanElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;

protected:
    HTMLSpanElement() : HTMLElement("span") {}

};


#endif //GUINOX_HTMLSPANELEMENT_HPP
