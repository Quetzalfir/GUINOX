/**
* @brief The HTML Title element (<title>) defines the document's title that is shown
* in a browser's title bar or a page's tab.
*
* It only contains text; tags within the element are ignored.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/title
*/

#ifndef GUINOX_HTMLTITLEELEMENT_HPP
#define GUINOX_HTMLTITLEELEMENT_HPP


#include <iostream>
#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLTitleElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLTitleElement() : HTMLElement("title") {}


public:
    ReactString & text = setReferencedAttr("text", "");

};


#endif //GUINOX_HTMLTITLEELEMENT_HPP
