/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLQUOTEELEMENT_HPP
#define GUINOX_HTMLQUOTEELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLQuoteElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLQuoteElement() : HTMLElement("blockquote") {}


    ReactString & cite = setReferencedAttr("cite", "");

};


#endif //GUINOX_HTMLQUOTEELEMENT_HPP
