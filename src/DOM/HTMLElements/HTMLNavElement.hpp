/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/sections.html#the-nav-element
 */

#ifndef GUINOX_HTMLNAVELEMENT_HPP
#define GUINOX_HTMLNAVELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLNavElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLNavElement() : HTMLDivElement("nav") {}


};


#endif //GUINOX_HTMLNAVELEMENT_HPP
