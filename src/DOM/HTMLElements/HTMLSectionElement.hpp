/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/sections.html#the-section-element
 */

#ifndef GUINOX_HTMLSECTIONELEMENT_HPP
#define GUINOX_HTMLSECTIONELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLSectionElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLSectionElement() : HTMLDivElement("section") {}


};


#endif //GUINOX_HTMLSECTIONELEMENT_HPP
