/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/forms.html#the-label-element
*/

#ifndef GUINOX_HTMLLABELELEMENT_HPP
#define GUINOX_HTMLLABELELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLLabelElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLLabelElement() : HTMLElement("form") {}


public:
    //std::shared_ptr<HTMLFormElement> form();
    //std::shared_ptr<HTMLElement> control();


    ReactString & htmlFor = setReferencedAttr("htmlFor", "");

};


#endif //GUINOX_HTMLLABELELEMENT_HPP
