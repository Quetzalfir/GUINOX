/**
* @brief The HTML <head> element contains machine-readable information (metadata) about the document,
* like its title and style sheets.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/head
*/

#ifndef GUINOX_HTMLHEADELEMENT_HPP
#define GUINOX_HTMLHEADELEMENT_HPP


#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"

class HTMLHeadElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLHeadElement() : HTMLElement("head") {}

};


#endif //GUINOX_HTMLHEADELEMENT_HPP
