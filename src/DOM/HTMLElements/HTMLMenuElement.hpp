/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

#ifndef GUINOX_HTMLMENUELEMENT_HPP
#define GUINOX_HTMLMENUELEMENT_HPP


class HTMLMenuElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLMenuElement() : HTMLElement("menu") {}

};


#endif //GUINOX_HTMLMENUELEMENT_HPP
