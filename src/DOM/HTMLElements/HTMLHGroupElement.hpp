/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://html.spec.whatwg.org/multipage/sections.html#the-hgroup-element
*/

#ifndef GUINOX_HTMLHGROUPELEMENT_HPP
#define GUINOX_HTMLHGROUPELEMENT_HPP

#include "HTMLDivElement.hpp"


class HTMLHGroupElement : public HTMLDivElement {
    friend class ElementRegistry;
    static ElementRegistry regist;


protected:
    HTMLHGroupElement() : HTMLDivElement("hgroup") {}


};


#endif //GUINOX_HTMLHGROUPELEMENT_HPP
