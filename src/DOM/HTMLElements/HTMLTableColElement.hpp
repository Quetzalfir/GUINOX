/**
* @brief
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on
*/

/**
 * This class is based on https://html.spec.whatwg.org/multipage/tables.html#the-caption-element
 */

#ifndef GUINOX_HTMLTABLECOLELEMENT_HPP
#define GUINOX_HTMLTABLECOLELEMENT_HPP

#include "../HTMLElement.hpp"
#include "../ElementRegistry.hpp"


class HTMLTableColElement : public HTMLElement {
    friend class ElementRegistry;
    static ElementRegistry registColGroup;
    static ElementRegistry registCol;


protected:
    HTMLTableColElement(const std::string & tag) : HTMLElement(tag) {}


};


#endif //GUINOX_HTMLTABLECOLELEMENT_HPP
