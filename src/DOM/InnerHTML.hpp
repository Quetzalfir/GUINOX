/**
* @brief The InnerHTML property returns or sets the HTML
* syntax describing the descendants of the element.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details https://developer.mozilla.org/es/docs/Web/API/Element/innerHTML

*/
#ifndef GUINOX_INNERHTML_HPP
#define GUINOX_INNERHTML_HPP


#include "ReactString.hpp"
#include "Node.hpp"

/**
 * Note: La propiedad innerHTML de muchos tipos de elementos—incluyendo <body> o <html>—puede ser devuelta o establecida.
 * Esta propiedad se puede usar para ver el código HTML de la página actual, incluida la que haya sido modificada dinámicamente.
 */
class InnerHTML : public virtual DOM_Node::Node {

    std::string getter() const ;
    void setter(const std::string &olValue, const std::string &newValue);


protected:
    InnerHTML();


public:
    ReactString innerHTML;

};


#endif //GUINOX_INNERHTML_HPP
