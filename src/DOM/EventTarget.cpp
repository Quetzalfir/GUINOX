#include "EventTarget.hpp"

//#include <utility>
//#include "Events/EventOrigin/EventDispatcher.h"
//#include "Element.h"


void EventTarget::addEventListener(const std::string &type, const EventListener& listener,
                                   Options options) {

    EventListenerType set = {listener, options};
    auto & vec = listeners[type];

    if (    listener.name != "__Quetzalfir-funct" &&
            listener.name != "CQuery Listener"       ) {

        for (const auto &listen : vec) {
            if (set == listen) {
                return;
            }
        }
    }

    vec.push_back(set);
}

void EventTarget::addEventListener(const std::string &type, const EventListener& listener) {
    Options options;
    addEventListener(type, listener, options);
}


void EventTarget::removeEventListener(const std::string &type, const EventListener& listener,
                                      bool capture) {

    auto set = listeners.find(type);
    if (set != listeners.end()) {
        auto & vec = (*set).second;

        auto end = vec.end();
        for (auto it = vec.begin(); it != end; it++) {
            if ((*it).eventListener == listener &&
                (*it).options.capture == capture) {

                vec.erase(it);
                return;
            }
        }
    }
}

void EventTarget::removeEventListener(const std::string &type, const std::string &listenerName,
                                      bool capture) {

    auto set = listeners.find(type);
    if (set != listeners.end()) {
        auto & vec = (*set).second;

        auto end = vec.end();
        for (auto it = vec.begin(); it != end; it++) {
            if ((*it).eventListener.name == listenerName &&
                (*it).options.capture == capture) {

                vec.erase(it);
                return;
            }
        }
    }
}

void EventTarget::removeEventListeners(const std::string &type, const std::string &name,
                                       bool capture) {

    auto set = listeners.find(type);
    if (set != listeners.end()) {
        auto & vec = (*set).second;

        for (auto it = vec.begin(); it != vec.end(); ) {
            if ((*it).eventListener.name == name &&
                (*it).options.capture == capture) {

                it = vec.erase(it);
            } else {
                it++;
            }
        }
    }
}

void EventTarget::removeEventListeners(const std::string &type, const std::string &name) {
    auto set = listeners.find(type);
    if (set != listeners.end()) {
        auto & vec = (*set).second;

        for (auto it = vec.begin(); it != vec.end(); ) {
            if ((*it).eventListener.name == name) {
                it = vec.erase(it);
            } else {
                it++;
            }
        }
    }
}

void EventTarget::removeEventListeners(const std::string &name) {
    for (auto & listener : listeners) {
        auto & vec = listener.second;

        for (auto it = vec.begin(); it != vec.end(); ) {
            if ((*it).eventListener.name == name) {
                it = vec.erase(it);
            } else {
                it++;
            }
        }
    }
}

bool EventTarget::EventListenerType::ejec(Event * event, bool capture) {
    if (options.capture == capture || !event->bubbles) {
        if (event->eventPhase == Event::BUBBLING_PHASE) {
            event->passive = true;
        } else {
            event->passive = options.passive;
        }

        eventListener(event);

        return true;
    }

    return false;
}

void EventTarget::ejecListener(Event * event, bool capture) {

    auto set = listeners.find(event->type);
    if (set != listeners.end()) {
        auto & vec = (*set).second;

        for (auto it = vec.begin(); it != vec.end(); ) {
            if ( (*it).ejec(event, capture) ) {

                if ((*it).options.once) {
                    it = vec.erase(it);
                } else {
                    it++;
                }

                if (event->stop_immediate) {
                    return ;
                }
            } else {
                it++;
            }
        }

    }
}

bool EventTarget::dispatchEvent(Event & event) {
    if (event.dispatch || !event.initialized) {
        //throw an "InvalidStateError" DOMException

        return false;
    }


    event._isTrusted = false;
    //EventDispatcher::getTargets(/*(Element *)this, */event);

    //EventDispatcher::dispatch(event); //todo, verificar si se hace en paralelo

    return !event.defaultPrevented;
}