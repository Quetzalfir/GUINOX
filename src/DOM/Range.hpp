#ifndef GUINOX_RANGE_HPP
#define GUINOX_RANGE_HPP

#include <utility>
#include <list>
#include "dom_declarations.hpp"
#include "DocumentFragment.hpp"

class CharacterData;
/**
* @brief This is AbstractRange class.
*
* The AbstractRange abstract interface is the base class upon which all DOM range types are defined.
* A range is an object that indicates the start and end points of a section of content within the document.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/AbstractRange
*/
class AbstractRange {

    friend class ::MutationAlgorithms;
    friend class ::DOM_Node::Node;
    friend class ::CharacterData;
    friend class ::DOM_Node::Text;

protected:

    DOM_Node::wp_Node document;

    struct Boundary {
        DOM_Node::sp_Node node = nullptr;
        unsigned long offset = 0;

        Boundary() = default;

        Boundary(DOM_Node::Node *node_, unsigned long offset_) : Boundary(node_->shared_from_this(), offset_) {}

        Boundary(DOM_Node::sp_Node node_, unsigned long offset_) : node(std::move(node_)), offset(offset_) {}

        bool operator>(const Boundary &rhs) const;

        bool operator==(const Boundary &rhs) const;

        bool operator<(const Boundary &rhs) const;
    };

    Boundary start;
    Boundary end;


public:

    DOM_Node::sp_Node startContainer() const;

    const unsigned long &startOffset = start.offset;

    DOM_Node::sp_Node endContainer() const;

    const unsigned long &endOffset = end.offset;


    inline bool collapsed() const { return start == end; }
};


struct StaticRangeInit {
    DOM_Node::sp_Node startContainer;
    unsigned long startOffset;
    DOM_Node::sp_Node endContainer;
    unsigned long endOffset;
};

/**
* @brief This is StaticRange class.
*
* The DOM StaticRange interface extends AbstractRange to provide a method to specify
* a range of content in the DOM whose contents don't update to reflect changes which
* occur within the DOM tree. It offers the same set of properties and methods as AbstractRange.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/StaticRange
*/
class StaticRange : public AbstractRange {
public:
    explicit StaticRange(StaticRangeInit &init);
};

/**
* @brief This is Range class.
*
* The Range interface represents a fragment of a document that can contain nodes and parts of text nodes.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Range
*/
class Range : public AbstractRange {

    friend class ::DOM_Node::Document;

    Range() noexcept ;

    Range(DOM_Node::sp_Document & document) noexcept ;

    std::string stringifier();

    DOM_Node::Node *root() const;

    bool contained(DOM_Node::Node *node) const;

    bool partial_contained(DOM_Node::Node *node) const;

    //void forEachContained(std::function)


public:

    virtual ~Range();

    DOM_Node::sp_Node commonAncestorContainer() const;

    void setStart(DOM_Node::sp_Node node, unsigned long offset);

    void setEnd(DOM_Node::sp_Node node, unsigned long offset);

    void setStartBefore(DOM_Node::sp_Node node);

    void setStartAfter(DOM_Node::sp_Node node);

    void setEndBefore(DOM_Node::sp_Node node);

    void setEndAfter(DOM_Node::sp_Node node);

    void collapse(bool toStart = false);

    void selectNode(DOM_Node::sp_Node node);

    void selectNodeContents(DOM_Node::sp_Node node);


    enum How : unsigned short {
        START_TO_START = 0,
        START_TO_END = 1,
        END_TO_END = 2,
        END_TO_START = 3
    };


    short compareBoundaryPoints(How how, const Range &sourceRange) const;


    void deleteContents();

    DOM_Node::sp_DocumentFragment extractContents();

    DOM_Node::sp_DocumentFragment cloneContents() const;

    void insertNode(DOM_Node::sp_Node node);

    void surroundContents(DOM_Node::sp_Node newParent);

    Range cloneRange();


    bool isPointInRange(DOM_Node::sp_Node node, unsigned long offset) const;

    short comparePoint(DOM_Node::sp_Node node, unsigned long offset) const;


    bool intersectsNode(DOM_Node::sp_Node node) const;

    DOM_Node::sp_DocumentFragment createContextualFragment(DOMString fragment);

    //todo, The getClientRects(), and getBoundingClientRect()
    // methods are defined in other specifications. [DOM-Parsing] [CSSOM-VIEW]

};

typedef std::shared_ptr<Range> sp_Range;


#endif //GUINOX_RANGE_HPP
