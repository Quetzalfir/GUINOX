/**
* @brief The NamedNodeMap interface represents a collection
* of Attr objects
*
* Objects inside a NamedNodeMap are not in any particular
* order, unlike NodeList, although they may be accessed by
* an index as in an array.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details https://developer.mozilla.org/es/docs/Web/API/NamedNodeMap
*/
#ifndef GUINOX_NAMEDNODEMAP_HPP
#define GUINOX_NAMEDNODEMAP_HPP

#include "Attr.hpp"
#include <list>


class NamedNodeMap {
    friend class DOM_Node::Element;

    typedef std::list<std::shared_ptr<Attr> >::iterator iterator;
    typedef std::list<std::shared_ptr<Attr> >::const_iterator const_iterator;

     std::list<sp_Attr> attributes;
     DOM_Node::Element *ownerElement = nullptr;


    ReactString &setNamedItem(DOMString name, DOMString value);


public:

    inline size_t length() const { return attributes.size(); }
    sp_Attr item(size_t index);
    const sp_Attr &item(size_t index) const ;

    inline sp_Attr operator[] (size_t index) { return item(index); }
    inline sp_Attr operator[] (size_t index) const { return item(index); }
    inline sp_Attr operator[] (const std::string & name) { return getNamedItem(name); }
    inline sp_Attr operator[] (const std::string & name) const { return getNamedItem(name); }
    bool operator== (const NamedNodeMap &rhs) const ;
    inline bool operator!= (const NamedNodeMap &rhs) const { return !(this->operator==(rhs)); }
    NamedNodeMap& operator= (const NamedNodeMap &rhs);

    sp_Attr getNamedItem(DOMString qualifiedName) const ;
    //sp_Attr & getNamedItemNS(DOMString namespace, DOMString localName);
    /**
     * This function set the value of an attribute or append a newone to the object. It's important to notice
     * that the funtion append a clone of the attr parameter, so the parameter and the result returned are not the same
     * @param attr
     * @return
     */
    sp_Attr &setNamedItem(const sp_Attr & attr);
    //sp_Attr & setNamedItemNS(const Attr & attr);
    sp_Attr removeNamedItem(DOMString name);
    //sp_Attr & removeNamedItemNS(const std::string & ns, const std::string & localName);


    inline iterator begin() noexcept { return attributes.begin(); }
    inline const_iterator cbegin() const noexcept { return attributes.cbegin(); }
    inline iterator end() noexcept { return attributes.end(); }
    inline const_iterator cend() const noexcept { return attributes.cend(); }

};


#endif //GUINOX_NAMEDNODEMAP_HPP
