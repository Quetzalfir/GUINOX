#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"


#include "ElementRegistry.hpp"
#include "HTMLElements/HTMLBodyElement.hpp"
#include "HTMLElements/HTMLDivElement.hpp"
#include "HTMLElements/HTMLSpanElement.hpp"
#include "HTMLElements/HTMLHtmlElement.hpp"
#include "HTMLElements/HTMLHeadingElement.hpp"
#include "HTMLElements/HTMLImageElement.hpp"
#include "HTMLElements/HTMLLinkElement.hpp"
#include "HTMLElements/HTMLMetaElement.hpp"
#include "HTMLElements/HTMLParagraphElement.hpp"
#include "HTMLElements/HTMLTitleElement.hpp"
#include "HTMLElements/HTMLBaseElement.hpp"
#include "HTMLElements/HTMLHeadElement.hpp"
#include "HTMLElements/HTMLTemplateElement.hpp"
#include "HTMLElements/HTMLStyleElement.hpp"
#include "HTMLElements/HTMLHRElement.hpp"
#include "HTMLElements/HTMLPreElement.hpp"
#include "HTMLElements/HTMLQuoteElement.hpp"
#include "HTMLElements/HTMLOListElement.hpp"
#include "HTMLElements/HTMLUListElement.hpp"
#include "HTMLElements/HTMLMenuElement.hpp"
#include "HTMLElements/HTMLLIElement.hpp"
#include "HTMLElements/HTMLDListElement.hpp"
#include "HTMLElements/HTMLFigureElement.hpp"
#include "HTMLElements/HTMLFigureCaptionElement.hpp"
#include "HTMLElements/HTMLMainElement.hpp"
#include "HTMLElements/HTMLArticleElement.hpp"
#include "HTMLElements/HTMLSectionElement.hpp"
#include "HTMLElements/HTMLNavElement.hpp"
#include "HTMLElements/HTMLAsideElement.hpp"
#include "HTMLElements/HTMLHGroupElement.hpp"
#include "HTMLElements/HTMLHeaderElement.hpp"
#include "HTMLElements/HTMLFooterElement.hpp"
#include "HTMLElements/HTMLAddressElement.hpp"
#include "HTMLElements/HTMLFormElement.hpp"
#include "HTMLElements/HTMLLabelElement.hpp"
#include "HTMLElements/HTMLTableCaptionElement.hpp"
#include "HTMLElements/HTMLTableColElement.hpp"
#include "HTMLElements/HTMLTableSectionElement.hpp"
#include "HTMLElements/HTMLTableRowElement.hpp"
#include "HTMLElements/HTMLTableCellElement.hpp"


std::map<std::string,std::function<std::shared_ptr<HTMLElement>()>> ElementRegistry::elements;

ElementRegistry::ElementRegistry(const std::string &name, std::function<std::shared_ptr<HTMLElement>()> constructor) {
    elements[name] = std::move(constructor);
}

std::shared_ptr<HTMLElement> ElementRegistry::get(const std::string &tag) {
    auto it = elements.find(tag);
    if (it != elements.end()) {
        return (*it).second();
    }

    return std::shared_ptr<HTMLUnknownElement>(new HTMLUnknownElement(tag) );
}





ElementRegistry HTMLAddressElement::regist("address", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLAddressElement>( new HTMLAddressElement() );
});

ElementRegistry HTMLArticleElement::regist("article", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLArticleElement>( new HTMLArticleElement() );
});

ElementRegistry HTMLAsideElement::regist("aside", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLAsideElement>( new HTMLAsideElement() );
});

ElementRegistry HTMLBaseElement::regist("base", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLBaseElement>( new HTMLBaseElement() );
});

ElementRegistry HTMLBodyElement::regist("body", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLBodyElement>(new HTMLBodyElement() );
});

ElementRegistry HTMLDivElement::regist("div", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLDivElement>(new HTMLDivElement() );
});

ElementRegistry HTMLDListElement::registDL("dl", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLDListElement>(new HTMLDListElement("dl") );
});

ElementRegistry HTMLDListElement::registDT("dt", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLDListElement>(new HTMLDListElement("dt") );
});

ElementRegistry HTMLDListElement::registDD("dd", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLDListElement>(new HTMLDListElement("dd") );
});

ElementRegistry HTMLFigureElement::regist("figure", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLFigureElement>(new HTMLFigureElement() );
});

ElementRegistry HTMLFigureCaptionElement::regist("figcaption", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLFigureCaptionElement>(new HTMLFigureCaptionElement() );
});

ElementRegistry HTMLFooterElement::regist("footer", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLFooterElement>(new HTMLFooterElement() );
});

ElementRegistry HTMLFormElement::regist("form", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLFormElement>(new HTMLFormElement() );
});

ElementRegistry HTMLHeadingElement::registH1("h1", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeadingElement>(new HTMLHeadingElement( "h1") );
});

ElementRegistry HTMLHeadingElement::registH2("h2", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeadingElement>(new HTMLHeadingElement( "h2") );
});

ElementRegistry HTMLHeadingElement::registH3("h3", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeadingElement>(new HTMLHeadingElement( "h3") );
});

ElementRegistry HTMLHeadingElement::registH4("h4", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeadingElement>(new HTMLHeadingElement( "h4") );
});

ElementRegistry HTMLHeadingElement::registH5("h5", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeadingElement>(new HTMLHeadingElement( "h5") );
});

ElementRegistry HTMLHeadingElement::registH6("h6", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeadingElement>(new HTMLHeadingElement( "h6") );
});

ElementRegistry HTMLHGroupElement::regist("hgroup", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHGroupElement>(new HTMLHGroupElement() );
});

ElementRegistry HTMLHeadElement::regist("head", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeadElement>(new HTMLHeadElement() );
});

ElementRegistry HTMLHeaderElement::regist("header", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHeaderElement>(new HTMLHeaderElement() );
});

ElementRegistry HTMLHRElement::regist("hr", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHRElement>(new HTMLHRElement() );
});

ElementRegistry HTMLHtmlElement::regist("html", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLHtmlElement>(new HTMLHtmlElement() );
});

ElementRegistry HTMLImageElement::regist("img", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLImageElement>(new HTMLImageElement() );
});

ElementRegistry HTMLLabelElement::regist("label", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLLabelElement>(new HTMLLabelElement() );
});

ElementRegistry HTMLLIElement::regist("li", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLLIElement>(new HTMLLIElement() );
});

ElementRegistry HTMLLinkElement::regist("link", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLLinkElement>(new HTMLLinkElement() );
});

ElementRegistry HTMLMainElement::regist("main", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLMainElement>(new HTMLMainElement() );
});

ElementRegistry HTMLMenuElement::regist("menu", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLMenuElement>(new HTMLMenuElement() );
});

ElementRegistry HTMLMetaElement::regist("meta", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLMetaElement>(new HTMLMetaElement() );
});

ElementRegistry HTMLNavElement::regist("nav", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLNavElement>(new HTMLNavElement() );
});

ElementRegistry HTMLOListElement::regist("ol", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLOListElement>(new HTMLOListElement() );
});

ElementRegistry HTMLParagraphElement::regist("p", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLParagraphElement>(new HTMLParagraphElement() );
});

ElementRegistry HTMLPreElement::regist("pre", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLPreElement>(new HTMLPreElement() );
});

ElementRegistry HTMLQuoteElement::regist("blockquote", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLQuoteElement>(new HTMLQuoteElement() );
});

ElementRegistry HTMLSectionElement::regist("section", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLSectionElement>(new HTMLSectionElement() );
});

ElementRegistry HTMLSpanElement::regist("span", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLSpanElement>(new HTMLSpanElement() );
});

ElementRegistry HTMLStyleElement::regist("style", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLStyleElement>(new HTMLStyleElement() );
});

ElementRegistry HTMLTableCaptionElement::regist("caption", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableCaptionElement>(new HTMLTableCaptionElement() );
});

ElementRegistry HTMLTableCellElement::registTD("td", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableCellElement>(new HTMLTableCellElement("td") );
});

ElementRegistry HTMLTableCellElement::registTH("th", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableCellElement>(new HTMLTableCellElement("th") );
});

ElementRegistry HTMLTableColElement::registColGroup("colgroup", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableColElement>(new HTMLTableColElement("colgroup") );
});

ElementRegistry HTMLTableColElement::registCol("col", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableColElement>(new HTMLTableColElement("col") );
});

ElementRegistry HTMLTableRowElement::regist("tr", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableRowElement>(new HTMLTableRowElement() );
});

ElementRegistry HTMLTableSectionElement::registTBody("tbody", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableSectionElement>(new HTMLTableSectionElement("tbody") );
});

ElementRegistry HTMLTableSectionElement::registTHead("thead", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableSectionElement>(new HTMLTableSectionElement("thead") );
});

ElementRegistry HTMLTableSectionElement::registTFoot("tfoot", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTableSectionElement>(new HTMLTableSectionElement("tfoot") );
});

ElementRegistry HTMLTemplateElement::regist("template", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTemplateElement>(new HTMLTemplateElement() );
});

ElementRegistry HTMLTitleElement::regist("title", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLTitleElement>(new HTMLTitleElement() );
});

ElementRegistry HTMLUListElement::regist("ul", []() -> std::shared_ptr<HTMLElement> {
    return std::shared_ptr<HTMLUListElement>(new HTMLUListElement() );
});





#pragma clang diagnostic pop