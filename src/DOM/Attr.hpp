/**
* @brief This is a public class with any operators overloaded
* to use with the name & value of the attribute.
*
* The Attr interface represents one of a DOM element's
* attributes as an object.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Attr
*/
#ifndef GUINOX_DOM_ATTR_H
#define GUINOX_DOM_ATTR_H


#include <functional>
#include <string>
#include <memory>
#include "ReactString.hpp"
#include "dom_declarations.hpp"


class Attr {

    friend class NamedNodeMap;
    friend class DOM_Node::Document;


    std::string _name;
    bool referenced = false;
    std::string defalt;
    DOM_Node::Element *ownerElement = nullptr;

    explicit Attr(const std::string &name);

    void handle_attribute_changes(const std::string & oldValue, const std::string & newValue);


public:

    const std::string &name = _name;
    ReactString value;

    inline bool operator<(const Attr& rhs) const { return name < rhs.name; }
    inline bool operator==(const Attr &rhs) const { return name == rhs.name && value == rhs.value; }
    inline bool operator!=(const Attr &rhs) const { return !(this->operator==(rhs)); }

    Attr &operator=(const Attr &rhs);
    Attr &operator=(Attr && ths) noexcept ;

};

typedef std::shared_ptr<Attr> sp_Attr;


#endif //GUINOX_DOM_ATTR_H
