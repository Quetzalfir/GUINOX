/**
* @brief This is Traversal class.
*
* It is a class to inherit properties in common from other
* classes like tree walker or nodeiterator.
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*/
#ifndef GUINOX_TRAVERSAL_HPP
#define GUINOX_TRAVERSAL_HPP

#include "NodeFilter.hpp"


class Traversal {
    bool active = false;

protected:
    DOM_Node::sp_Node _root;
    unsigned long _whatToShow;
    NodeFilter _filter;


    Traversal(DOM_Node::sp_Node root, unsigned long whatToShow, const NodeFilter & filter);

    NodeFilter::FilterResp filtering(const DOM_Node::sp_Node & node);



public:
    const DOM_Node::sp_Node & root = _root;
    const unsigned long &whatToShow = _whatToShow;
    const NodeFilter &filter = _filter;

};


#endif //GUINOX_TRAVERSAL_HPP
