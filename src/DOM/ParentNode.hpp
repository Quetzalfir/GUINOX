/**
* @brief Interface to convert nodes into a node, given nodes and document.
*
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://dom.spec.whatwg.org/?fbclid=IwAR31NjWcO_Wv-Ng_GkDWoRtWAIz65TJQB--Y545AgH1X44ZWiJbPdc0kAGQ#parentnode
*/
#ifndef GUINOX_PARENTNODE_HPP
#define GUINOX_PARENTNODE_HPP


#include "Node.hpp"


class ParentNode : public virtual DOM_Node::Node {

protected:
    DOM_Node::HTMLCollection _children;


    ParentNode() : Node(Node::UNDEFINED) {}


public:
    //UNICAMENTE DEVUELVE A LOS HIJOS DE ELEMENTOS
    const DOM_Node::HTMLCollection & children = _children;

    DOM_Node::sp_Element firstElementChild() const ;
    DOM_Node::sp_Element lastElementChild() const ;
    size_t childElementCount() const ;

    virtual void append(DOM_Node::sp_Node node);
    void append(DOM_Node::NodeList & nodes);
    void append(const std::string & text);

    virtual void prepend(DOM_Node::sp_Node node);
    void prepend(DOM_Node::NodeList & nodes);
    void prepend(const std::string & text);

    DOM_Node::sp_Element querySelector(const std::string & selector);
    virtual DOM_Node::HTMLCollection querySelectorAll(const std::string & selector) = 0;
};


#endif //GUINOX_PARENTNODE_HPP
