#include "Slotable.hpp"
#include "Element.hpp"
#include "ShadowRoot.hpp"
#include "Document.hpp"
#include "HTMLElements/HTMLSlotElement.hpp"
#include "NodeTravel.hpp"

using namespace std::placeholders;


void Slotable::assign_slotables(std::shared_ptr<HTMLSlotElement> slot) {
    auto slotables = find_slotables(slot);

    auto assignedNodes = slot->assignedNodes();

    if (slotables != assignedNodes) {
        slot->signal_slot_change();
    }

    slot->_assignedNodes = slotables;

    for (auto & aux : slotables) {
        auto slotable = std::dynamic_pointer_cast<Slotable>(aux);

        slotable->_assignedSlot = slot;
    }
}

void Slotable::assign_slotables_for_tree(DOM_Node::sp_Node root) {
    NodeTravel(root).forEach.inclusive_descendants([](DOM_Node::Node * node) {
        if (auto slot = dynamic_cast<HTMLSlotElement *>(node)) {

        }
    });
}

void Slotable::assign_slot() {
    auto slot_aux = find_slot(this->shared_from_this());

    if (slot_aux) {
        assign_slotables(slot_aux);
    }
}

std::vector<DOM_Node::sp_Node> Slotable::find_slotables(DOM_Node::sp_Node slot) {
    std::vector<DOM_Node::sp_Node> result;

    sp_ShadowRoot shadowRoot = std::dynamic_pointer_cast<ShadowRoot>(slot->getRootNode());
    if (!shadowRoot) {
        return result;
    }

    auto host = shadowRoot->host();

    for (const auto & child : host->childNodes) {
        auto slotable = std::dynamic_pointer_cast<Slotable>(child);

        if (slotable) {
            auto foundSlot = find_slot(slotable);

            if (foundSlot == slot) {
                result.push_back(slotable);
            }
        }
    }

    return result;
}

std::shared_ptr<HTMLSlotElement> Slotable::find_slot(const DOM_Node::sp_Node &node, bool open) {
    auto slotable = std::dynamic_pointer_cast<DOM_Node::Element>( node );
    auto parent = std::dynamic_pointer_cast<DOM_Node::Element>( slotable->parentNode() );

    if (!parent) return nullptr;

    auto shadow = parent->shadowRoot();

    if (shadow) return nullptr;

    if (open && shadow->mode != ShadowRootMode::OPEN) {
        return nullptr;
    }

    NodeFilter filter;
    filter.acceptNode = [](const DOM_Node::sp_Node & node) -> NodeFilter::FilterResp {
        auto slot_aux = std::dynamic_pointer_cast<HTMLSlotElement>(node);

        return slot_aux ? NodeFilter::FILTER_ACCEPT : NodeFilter::FILTER_SKIP ;
    };

    // Init the TreeWalker
    auto tree = DOM_Node::Document::createTreeWalker(shadow, NodeFilter::SHOW_ELEMENT, filter);

    while (tree.nextNode()) {
        auto aux = std::dynamic_pointer_cast<HTMLSlotElement>(tree.currentNode);

        if (aux->name == slotable->slot) {
            return aux;
        }
    }

    return nullptr;
}