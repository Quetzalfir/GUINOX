#include "DOMImplementation.hpp"
#include "Document.hpp"
#include "DocumentType.hpp"
#include "Text.hpp"

/*DOMImplementation::DOMImplementation(DOM_Node::Document *contextObject) {
    this->contextObject = contextObject;
}*/

std::shared_ptr<DOM_Node::DocumentType> DOMImplementation::createDocumentType(const std::string &qualifiedName) {
    auto documenType = std::shared_ptr<DOM_Node::DocumentType>(new DOM_Node::DocumentType(qualifiedName) );

    //documenType->_ownerDocument = contextObject;

    return documenType;
}

std::shared_ptr<DOM_Node::DocumentType> DOMImplementation::createDocumentType(const std::string &qualifiedName,
                                                                              const std::string &publicId) {
    auto documenType = createDocumentType(qualifiedName);

    documenType->_publicId = publicId;

    return documenType;
}

std::shared_ptr<DOM_Node::DocumentType> DOMImplementation::createDocumentType(const std::string &qualifiedName,
                                                                              const std::string &publicId,
                                                                              const std::string &systemId) {
    auto documenType = createDocumentType(qualifiedName, publicId);

    documenType->_systemId = systemId;

    return documenType;
}

std::shared_ptr<DOM_Node::XMLDocument> DOMImplementation::createDocument(const std::string &namespace_) {
    auto document = std::shared_ptr<DOM_Node::XMLDocument>( new DOM_Node::XMLDocument() );

    //document->_origin = contextObject->_origin;
    document->_contentType = namespace_ == "application/xhtml+xml" || namespace_ == "image/svg+xml" ? namespace_ : "application/xml";

    return document;
}

std::shared_ptr<DOM_Node::XMLDocument> DOMImplementation::createDocument(const std::string &namespace_,
                                                                         const std::string &qualifiedName) {
    // https://dom.spec.whatwg.org/#dom-domimplementation-createdocument
    auto document = createDocument(namespace_);

    //todo, element = https://dom.spec.whatwg.org/#internal-createelementns-steps
    // if (element) document->appendChild(element);

    return document;
}

std::shared_ptr<DOM_Node::XMLDocument> DOMImplementation::createDocument(const std::string &namespace_,
                                                                         const std::string &qualifiedName,
                                                                         std::shared_ptr<DOM_Node::DocumentType> &doctype) {
    // https://dom.spec.whatwg.org/#dom-domimplementation-createdocument
    auto document = createDocument(namespace_);

    if (doctype) document->appendChild(doctype);
    //todo, element = https://dom.spec.whatwg.org/#internal-createelementns-steps
    // if (element) document->appendChild(element);

    return document;
}

std::shared_ptr<DOM_Node::Document> DOMImplementation::createHTMLDocument() {
    auto HTMLDocument = std::shared_ptr<DOM_Node::Document>( new DOM_Node::Document() );

    HTMLDocument->_contentType = "text/html";

    auto doctype = createDocumentType("html");
    HTMLDocument->appendChild(doctype);

    auto html = HTMLDocument->createElement("html");
    HTMLDocument->appendChild(html);

    auto head = HTMLDocument->createElement("head");
    html->appendChild(head);

    auto title = HTMLDocument->createElement("title");
    head->appendChild(title);

    title->appendChild(HTMLDocument->createTextNode("title"));

    html->appendChild(HTMLDocument->createElement("body"));

    //HTMLDocument->_origin = contextObject->_origin;

    return HTMLDocument;
}

std::shared_ptr<DOM_Node::Document> DOMImplementation::createHTMLDocument(const std::string &text) {
    auto HTMLDocument = createHTMLDocument();

    auto title = HTMLDocument->documentElement->firstChild()->firstChild();

    title->removeChild(title->firstChild());
    title->appendChild(HTMLDocument->createTextNode(text));

    return HTMLDocument;
}