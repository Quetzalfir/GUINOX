#include "NodeIterator.hpp"
#include "Node.hpp"
#include "NodeTravel.hpp"
#include "Document.hpp"

NodeIterator::NodeIterator(DOM_Node::sp_Node root_, unsigned long whatToShow_, const NodeFilter &filter_) :
        Traversal(std::move(root_), whatToShow_, filter_), _referenceNode(root_) {

    //root_->AddMutationObserver(this);
}

NodeIterator::~NodeIterator() {
    //if (_root) _root->RemoveMutationObserver(this);

    auto doc = _root->ownerDocument();

    if (doc) {
        doc->checkNodeIterators();
    }
}

void NodeIterator::pre_removing(DOM_Node::sp_Node toBeRemovedNode) {
    if (!(NodeTravel(toBeRemovedNode).inclusive_ancestor(_referenceNode)) ||
        toBeRemovedNode == _root) {

        return;
    }

    if (pointerBeforeReferenceNode) {
        auto next = NodeTravel(toBeRemovedNode).following(_root);

        while (next) {
            if (!(NodeTravel(next).inclusive_descendant(toBeRemovedNode))) {
                break;
            }

            next = NodeTravel(next).following(_root);
        }

        if (next) {
            _referenceNode = next->shared_from_this();
            return;
        }

        _pointerBeforeReferenceNode = false;
    }

    if (!toBeRemovedNode->previousSibling()) {
        _referenceNode = toBeRemovedNode->parentNode();

    } else {
        _referenceNode = NodeTravel(toBeRemovedNode).preceding(_root)->shared_from_this();
    }
}

DOM_Node::sp_Node NodeIterator::traverse(bool next) {
    auto node = _referenceNode;
    bool beforeNode = _pointerBeforeReferenceNode;

    while (true) {
        if (next) {
            if (!beforeNode) {
                node = NodeTravel(node).following(_root)->shared_from_this();
                if (!node) return nullptr;

            } else {
                beforeNode = false;
            }

        } else {
            if (beforeNode) {
                node = NodeTravel(node).preceding(_root)->shared_from_this();
                if (!node) return nullptr;

            } else {
                beforeNode = true;
            }
        }

        if (filtering(node) == NodeFilter::FILTER_ACCEPT) {
            break;
        }
    }

    _referenceNode = node;

    _pointerBeforeReferenceNode = beforeNode;
    return node;
}

DOM_Node::sp_Node NodeIterator::nextNode() {
    return traverse(true);
}

DOM_Node::sp_Node NodeIterator::previousNode() {
    return traverse(false);
}