/**
 * @brief DOM_Node is a namespace that encompasses all types of nodes that belong to the DOM.
 */

#ifndef GUINOX_ELEMENT_HPP
#define GUINOX_ELEMENT_HPP


#include "Node.hpp"
#include "ParentNode.hpp"
#include "ChildNode.hpp"
#include "DOMTokenList.hpp"
#include "NonDocumentTypeChildNode.hpp"
#include "NamedNodeMap.hpp"
#include "Slotable.hpp"
#include "ShadowRoot.hpp"
#include "InnerHTML.hpp"
#include "DOMRect.hpp"


class CSSSelector;

namespace DOM_Node {

/**
* @brief More specific classes inherit from Element.
*
* Element is the most general base class from which all
* element objects (i.e. objects that represent elements)
* in a Document inherit.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Element
*/

    class Element :
            public virtual Node,
            public ParentNode,
            public ChildNode,
            public NonDocumentTypeChildNode,
            public Slotable,
            public InnerHTML {

        friend class Node;
        friend class ::NodeTravel;
        friend class ::MutationAlgorithms;
        friend class ::CSSSelector;



        static inline sp_Element cast2sp_Element(Node * elem) { return std::dynamic_pointer_cast<Element>(elem->shared_from_this()); }

        Element * getElementById2(const std::string &id) const ;

        void getElementsByTagName(const std::string &tagName, HTMLCollection &vec) const ;

        void getElementsByClassName(const std::string &class_, HTMLCollection &vec) const ;

        Element *getElementBy(std::function<bool(Element *)> fn);

        HTMLCollection getElementsBy(const std::function<bool(const sp_Element &)> &fn);

        void getElementsBy(const std::function<bool(const sp_Element &)> & fn, HTMLCollection &vec);

        void eachB(std::function<bool(sp_Element &)> toDo);

        void each(std::function<void(sp_Element &)> toDo);

        //void travelTree(std::function<void(sp_Element &)> fn);

        static sp_Element getNextElement(sp_Node &reference);

        void appendElementChild(sp_Element &elem);

        void removeElementChild(sp_Element &elem);

        void insertElementBefore(sp_Element &elem, sp_Element &reference);

        std::string textContentGetter() const ;
        void textContentSetter(const std::string &oldValue, const std::string & newValue);

        std::string outerHTMLGetter() ;
        void outerHTMLSetter(const std::string &oldValue, const std::string & newValue);

        sp_Node insert_adjacent(DOMString where, sp_Node node);




    protected:
        NamedNodeMap _attributes;
        DOMTokenList _classList;
        double _clientHeight;
        double _clientLeft;
        double _clientTop;
        double _clientWidth;
        double _scrollHeight;
        double _scrollWidth;
        //bool _hidden = false;
        sp_ShadowRoot _shadowRoot = nullptr;
        std::string _custom_element_state = "undefined"; //, "failed", "uncustomized", or "custom"


        Element();

        ReactString & setReferencedAttr(const std::string & name, const std::string & value);
        void clone(Node *copy, bool deep) const override ;

        ///virtual bool cursorIn(double x, double y);

        // Custom Element Reactions
        virtual void connectedCallback() {}
        virtual void disconnectedCallback() {}
        virtual void adoptedCallback(Document *oldDocument, Document *document) {}
        virtual bool attributeChangedCallback(DOMString AttrName, DOMString oldvalue, DOMString newValue);
        virtual void formAssociatedCallback() {}
        virtual void formResetCallback() {}
        virtual void formDisabledCallback() {}
        virtual void formStateRestoreCallback() {}


    public:
        const NamedNodeMap &attributes = _attributes;
        const DOMTokenList &classList = _classList;
        const double &clientHeight = _clientHeight;   // CSS height + CSS padding - height of horizontal scrollbar (if present).
        const double &clientLeft = _clientLeft; // The width of the left border of an element in pixels. https://dev.mozilla.jp/localmdc/localmdc_2016.html
        const double &clientTop = _clientTop;
        const double &clientWidth = _clientWidth;
        ReactString & id = setReferencedAttr("id", "");
        ReactString & className = _classList.value;
        //const std::string & localName; // https://developer.mozilla.org/en-US/docs/Web/API/Element/localName
        //const std::string & prefix;
        //const std::string & namespaceURI;
        const double &scrollHeight = _scrollHeight;
        const double &scrollWidth = _scrollWidth;
        double scrollTop = 0;
        double scrollLeft = 0;
        const std::string &tagName = _nodeName;
        ReactString & slot = setReferencedAttr("slot", "");
        ReactString textContent;
        ReactString outerHTML;
        //const bool &hidden = _hidden;


        ~Element() override = default;


        inline sp_ShadowRoot shadowRoot() { return _shadowRoot && _shadowRoot->mode == ShadowRootMode::OPEN ? _shadowRoot : nullptr ; }


        // Node inherit
        bool isEqualNode(sp_Node node) const override;
        sp_Node appendChild(sp_Node aChild) override;
        sp_Node insertBefore(sp_Node newNode, sp_Node referenceNode) override;
        sp_Node removeChild(sp_Node child) override;
        sp_Node replaceChild(sp_Node newChild, sp_Node oldChild) override;


        // ParentNode inherit
        inline void append(DOM_Node::sp_Node child) override { appendChild(child); }
        void prepend(DOM_Node::sp_Node child) override ;
        DOM_Node::HTMLCollection querySelectorAll(const std::string & selector) override;


        // Attributes methods
        bool hasAttributes() const { return _attributes.length(); };
        std::vector<std::string> getAttributeNames() const ;
        std::string getAttribute(const std::string &name) const ;
        void setAttribute(const std::string &name, const std::string &value);
        inline void removeAttribute(const std::string &attrName) { _attributes.removeNamedItem(attrName); }
        inline bool hasAttribute(const std::string &name) const { return (const_cast<Element *>(this))->_attributes.getNamedItem(name) != nullptr; }
        bool toggleAttribute(const std::string & name, bool force = false);


        sp_ShadowRoot attachShadow(ShadowRootMode init);


        sp_Element closest(const std::string & selectors);
        bool matches(const std::string & selector);


        sp_Element getElementById(const std::string &id) const ;
        HTMLCollection getElementsByTagName(const std::string &tagName) const ;
        HTMLCollection getElementsByClassName(const std::string &classNames) const ;


        sp_Element insertAdjacentElement(const std::string & where, sp_Element element); // historical
        void insertAdjacentText(const std::string & where, const std::string & data); // historical
        void insertAdjacentHTML(const std::string & where, const std::string & html);



        const DOMRect &getBoundingClientRect() const ;
        std::vector<DOMRect> getClientRects() const ;


        //void releasePointerCapture(int pointerId);
        //void setPointerCapture(pointerId);
        ///void scrollIntoView();
        ///void scrollIntoView(bool alignToTop); // Boolean parameter
        ///void scrollIntoView(Object scrollIntoViewOptions); // Object parameter

    };

}



#endif //GUINOX_ELEMENT_HPP
