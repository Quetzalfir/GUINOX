#include "DOMParser.hpp"
#include "Element.hpp"
#include "Text.hpp"
#include "Document.hpp"
#include <iostream>
#include <algorithm>
#include <Utils/Utils.hpp>
#include <fstream>

using namespace DOM_Node;


//############################ DOMParser ############################//

//----------------------------- STATICS -----------------------------//

//----------------------------- PRIVATE -----------------------------//


std::vector<DOMParser::Attribute> DOMParser::getAttributes(Parser &parser) {
    std::vector<DOMParser::Attribute> attr;

    while (parser.skipBlanks() && !parser.charIsOneOf(">/")) {
        std::string name = parser.getUntilFirstOf(">= \n\r\t");

        Attribute aux;
        aux.name = name;

        if (parser.getChar() != '=') {
            parser.skipBlanks();
        }

        if (parser.getChar() == '=') {
            parser.goUntil("\"\'");
            aux.value = parser.getInsideQuotes();

        } else {
            aux.value = aux.name;
        }

        attr.push_back(aux);
    }

    return attr;
}

std::string DOMParser::getTagName(Parser &parser) {
    return parser.getUntilFirstOf(" >/");
}

sp_Node DOMParser::makeElement(Parser &parser) {
    std::string tagname = Utils::toLower( getTagName(parser) );
    auto attributes = getAttributes(parser);

    sp_Node node;

    if (tagname == "!doctype") {
        std::string name = attributes.empty() ? "html" : attributes[0].name;
        node = DOMImplementation::createDocumentType(name);

    } else {
        sp_Element element = DOM_Node::Document::createElement(tagname);
        processAttributes(element, attributes);

        node = element;
    }

    parser.goUntil(">")++;

    return node;
}

DOMParser::Tree DOMParser::parse(const char *buff, size_t length) {
    Parser parser(&buff, length);
    parser.addCommentTag("<!--", "-->");
    parser.addCommentTag("<?", "?>");

    Tree tree;

    while (parser.skipBlanks()) {

        if (parser.getChar() == '<') { // is a tag
            parser++;

            if (parser.getChar() == '/') { // is a close tag
                std::string tagname = getTagName(++parser);
                std::cout << "</" << tagname << ">" << std::endl;
                parser.goUntil(">")++;

                tree.pop(tagname);

            } else { // is a new tag
                sp_Node node = makeElement(parser);
                std::cout << "<" << node->nodeName << ">" << std::endl;

                tree.push(node);
            }

        } else { // is text
            parser.goBackUntilNotBlank();
            std::string text = parser.getUntilFirstOf("<");

            std::cout << text << std::endl;

            sp_Node node = DOM_Node::Document::createTextNode(text);

            tree.push(node);
        }
    }

    return tree;
}

void DOMParser::tree2Document(Tree &tree, DOM_Node::sp_Document &document) {
    document->_building = true;

    for (auto & node : tree.nodes) {
        document->appendChild(node);
    }

    document->_building = false;
}


//----------------------------- PUBLIC -----------------------------//


DOM_Node::sp_Document DOMParser::parseFromFile(const std::string &path) {
    std::ifstream htmlFile(path, std::ifstream::in);

    if (!htmlFile.is_open()) return nullptr;

    std::string str((std::istreambuf_iterator<char>(htmlFile)), std::istreambuf_iterator<char>());

    sp_Document document = DOMImplementation::createDocument();

    document->_URL = path;

    auto tree = parse(str.data(), str.size());

    tree2Document(tree, document);

    htmlFile.close();

    return document;
}

sp_Document DOMParser::parseFromString(const std::string &html) {
    return parseFromString(html.data(), html.size());
}

sp_Document DOMParser::parseFromString(const char *buff, size_t length) {
    auto tree = parse(buff, length);

    sp_Document document = DOMImplementation::createDocument();

    tree2Document(tree, document);

    return document;
}

DOM_Node::sp_DocumentFragment DOMParser::fragmentParse(const std::string &html) {
    return fragmentParse(html.data(), html.size());
}

DOM_Node::sp_DocumentFragment DOMParser::fragmentParse(const char *buff, size_t length) {
    auto tree = parse(buff, length);

    sp_DocumentFragment fragment = Document::createDocumentFragment();

    for (auto & node : tree.nodes) {
        fragment->appendChild(node);
    }

    return fragment;
}


//############################ END DOMParser ############################//



void DOMParser::processAttributes(std::shared_ptr<DOM_Node::Element> & element, const std::vector<DOMParser::Attribute> &attributes) {
    if (!element) { return ; }

    for (const auto & attr : attributes) {
        element->setAttribute(attr.name, attr.value);
    }
}


//########################### DOMParser::Tree ###########################//

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

void DOMParser::Tree::push(DOM_Node::sp_Node& node) {
    if (tree) {
        tree->appendChild(node);
    } else {
        nodes.push_back(node);
    }

    if (node->nodeType != Node::TEXT_NODE) {
        if (!Utils::isEmptyElement(node->nodeName) && node->nodeType != Node::DOCUMENT_TYPE_NODE) {
            tree = node;
        }
    }
}

void DOMParser::Tree::pop(const std::string & tagname) {
    while (tree && tree->nodeName != tagname) {
        tree = tree->parentNode();
    }

    if (tree) {
        tree = tree->parentNode();
    }
}

//########################### END DOMParser::Tree ###########################//
