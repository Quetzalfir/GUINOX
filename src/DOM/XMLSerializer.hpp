/**
* @brief This is XMLSerializer class.
*
* The XMLSerializer interface provides the serializeToString()
* method to construct an XML string representing a DOM tree.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/XMLSerializer
*/
#ifndef GUINOX_XMLSERIALIZER_HPP
#define GUINOX_XMLSERIALIZER_HPP


#include "dom_declarations.hpp"


class XMLSerializer {

public:

    static std::string serializeToString(DOM_Node::sp_Node root);

};


#endif //GUINOX_XMLSERIALIZER_HPP
