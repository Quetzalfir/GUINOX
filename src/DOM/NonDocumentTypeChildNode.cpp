#include "NonDocumentTypeChildNode.hpp"
#include "Element.hpp"

DOM_Node::sp_Element NonDocumentTypeChildNode::nextElementSibling() const {
    DOM_Node::sp_Node sibling = this->nextSibling();

    while (sibling) {
        if (sibling->nodeType == ELEMENT_NODE) {
            return std::dynamic_pointer_cast<DOM_Node::Element>(sibling);
        }

        sibling = sibling->nextSibling();
    }

    return nullptr;
}

DOM_Node::sp_Element NonDocumentTypeChildNode::previousElementSibling() const {
    DOM_Node::sp_Node sibling = this->previousSibling();

    while (sibling) {
        if (sibling->nodeType == ELEMENT_NODE) {
            return std::dynamic_pointer_cast<DOM_Node::Element>(sibling);
        }

        sibling = sibling->previousSibling();
    }

    return nullptr;
}