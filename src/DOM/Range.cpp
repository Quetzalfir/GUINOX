#include "Range.hpp"
#include "Node.hpp"
#include "NodeTravel.hpp"
#include "Text.hpp"
#include "Document.hpp"
#include "MutationAlgorithms.hpp"
#include "Document.hpp"
#include "DOMParser.hpp"


bool AbstractRange::Boundary::operator==(const AbstractRange::Boundary &rhs) const {
    if (NodeTravel(node).root() != NodeTravel(rhs.node).root()) return false;

    return node == rhs.node && offset == rhs.offset;
}

bool AbstractRange::Boundary::operator<(const AbstractRange::Boundary &rhs) const {
    //if (node->getRootNode() != rhs.node->getRootNode()) return false;

    if (node == rhs.node) {
        return offset < rhs.offset;
    }

    if (rhs.node->compareDocumentPosition(node) & DOM_Node::Node::DOCUMENT_POSITION_FOLLOWING) {
        return !(rhs < *this);
    }

    if (NodeTravel(node).ancestor(rhs.node)) {
        auto child = rhs.node;

        while (child->parentNode() != node)
            child = child->parentNode();

        if (NodeTravel(child).index() < offset)
            return false;
    }

    return true;
}

bool AbstractRange::Boundary::operator>(const AbstractRange::Boundary &rhs) const {
    //if (node->getRootNode() != rhs.node->getRootNode()) return false;

    if (node == rhs.node) {
        return offset > rhs.offset;
    }

    if (rhs.node->compareDocumentPosition(node) & DOM_Node::Node::DOCUMENT_POSITION_FOLLOWING) {
        return (rhs < *this);
    }

    if (NodeTravel(node).ancestor(rhs.node)) {
        auto child = rhs.node;

        while (child->parentNode() != node)
            child = child->parentNode();

        if (NodeTravel(child).index() < offset)
            return true;
    }

    return false;
}

DOM_Node::sp_Node AbstractRange::startContainer() const {
    return start.node ? start.node : nullptr ;
}

DOM_Node::sp_Node AbstractRange::endContainer() const {
    return end.node ? end.node : nullptr ;
}


StaticRange::StaticRange(StaticRangeInit &init) {
    if (init.startContainer->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE ||
        init.endContainer->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {

        // throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    start.node = init.startContainer;
    start.offset = init.startOffset;
    end.node = init.endContainer;
    end.offset = init.endOffset;
}



Range::Range(DOM_Node::sp_Document & document) noexcept {
    start.node = document;
    start.offset = 0;
    end.node = document;
    end.offset = 0;

    this->document = document;
}

Range::Range() noexcept {
    start.node = nullptr;
    start.offset = 0;
    end.node = nullptr;
    end.offset = 0;
}

Range::~Range() {
    if (!document.expired()) {
        auto aux = document.lock();

        if (aux) {
            auto doc = std::dynamic_pointer_cast<DOM_Node::Document>(aux);
            doc->checkLiveRanges();
        }
    }
}

DOM_Node::Node* Range::root() const {
    return NodeTravel(start.node).root();
}

bool Range::contained(DOM_Node::Node *node) const {
    return  NodeTravel(node).root() == root() &&
            Boundary(node, 0) > start &&
            Boundary(node, node->length()) < end ;
}

bool Range::partial_contained(DOM_Node::Node *node) const {
    return  (  NodeTravel(node).inclusive_ancestor(start.node) &&
             !(NodeTravel(node).inclusive_ancestor(end.node)) ) ||
            (!(NodeTravel(node).inclusive_ancestor(start.node)) &&
               NodeTravel(node).inclusive_ancestor(end.node)  );
}

DOM_Node::sp_Node Range::commonAncestorContainer() const {
    auto startPath = NodeTravel(start.node).pathFromRoot();
    auto endPath = NodeTravel(end.node).pathFromRoot();

    int i = 0;
    while (startPath[i] == endPath[i]) i++;

    return startPath[i-1]->shared_from_this();
}

void Range::setStart(DOM_Node::sp_Node node, unsigned long offset) {
    if (!node) return;

    if (node->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {
        //throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    if (offset > node->length()) {
        // throw an "IndexSizeError" DOMException.
        return;
    }

    auto bp = Boundary(node, offset);

    if (bp > end || root() != NodeTravel(node).root()) {
        end = bp;
    }

    start = bp;
}

void Range::setEnd(DOM_Node::sp_Node node, unsigned long offset) {
    if (!node) return;

    if (node->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {
        //throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    if (offset > node->length()) {
        // throw an "IndexSizeError" DOMException.
        return;
    }

    auto bp = Boundary(node, offset);

    if (bp < start || root() != NodeTravel(node).root()) {
        start = bp;
    }

    end = bp;
}

void Range::setStartBefore(DOM_Node::sp_Node node) {
    if (!node) return;

    auto parent = node->parentNode();

    if (!parent) {
        // throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    start = Boundary(parent, NodeTravel(node).index());
}

void Range::setStartAfter(DOM_Node::sp_Node node) {
    if (!node) return;

    auto parent = node->parentNode();

    if (!parent) {
        // throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    start = Boundary(parent, NodeTravel(node).index()+1);
}

void Range::setEndBefore(DOM_Node::sp_Node node) {
    if (!node) return;

    auto parent = node->parentNode();

    if (!parent) {
        // throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    end = Boundary(parent, NodeTravel(node).index());
}

void Range::setEndAfter(DOM_Node::sp_Node node) {
    if (!node) return;

    auto parent = node->parentNode();

    if (!parent) {
        // throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    end = Boundary(parent, NodeTravel(node).index()+1);
}

void Range::collapse(bool toStart) {
    if (toStart) {
        end = start;
    } else {
        start = end;
    }
}

void Range::selectNode(DOM_Node::sp_Node node) {
    if (!node) return;

    auto parent = node->parentNode();

    if (!parent) {
        throw "InvalidNodeTypeError"; // DOMException.
    }

    size_t index = NodeTravel(node).index();

    start = Boundary(parent, index);
    end = Boundary(parent, index+1);
}

void Range::selectNodeContents(DOM_Node::sp_Node node) {
    if (!node) return;

    if (node->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {
        // throw an "InvalidNodeTypeError" DOMException.
        return;
    }

    start = Boundary(node, 0);
    end = Boundary(node, node->length());
}

short Range::compareBoundaryPoints(How how, const Range &sourceRange) const {
    if (root() != sourceRange.root()) {
        throw "WrongDocumentError"; // DOMException
    }

    Boundary thisPoint, otherPoint;

    switch (how) {
        case START_TO_START:
            thisPoint = start;
            otherPoint = sourceRange.start;
            break;
        case START_TO_END:
            thisPoint = end;
            otherPoint = sourceRange.start;
            break;
        case END_TO_END:
            thisPoint = end;
            otherPoint = sourceRange.end;
            break;
        case END_TO_START:
            thisPoint = start;
            otherPoint = sourceRange.end;
            break;
    }

    if (thisPoint < otherPoint) {
        return -1;
    } else if (thisPoint == otherPoint) {
        return 0;
    } else {
        return 1;
    }
}

void Range::deleteContents() {
    // if collapsed, nothing to do
    if (collapsed()) return;

    // If original start node and original end node are the same, and they are a Text, ProcessingInstruction,
    // or Comment node, replace data with node original start node, offset original start offset, count original end
    // offset minus original start offset, and data the empty string, and then return.
    if (start.node == end.node && start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto *text = dynamic_cast<DOM_Node::Text *>(start.node.get());
        text->replaceData(startOffset, endOffset - startOffset, "");
        return;
    }


    // let's find the common ancestor
    auto startPath = NodeTravel(start.node).pathFromRoot();
    auto endPath = NodeTravel(end.node).pathFromRoot();

    int i = 0;
    while (startPath[i] == endPath[i]) i++;

    auto commonAncestor = startPath[i-1];


    Boundary newBoundary;
    if (NodeTravel(start.node).inclusive_ancestor(end.node)) {
        newBoundary = start;
    } else {
        newBoundary = Boundary(commonAncestor, NodeTravel(startPath[i]).index()+1);
    }

    if (start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto *text = dynamic_cast<DOM_Node::Text *>(start.node.get());
        text->replaceData(startOffset, start.node->length() - startOffset, "");
    }


    // Init the filter for the TreeWalker
    NodeFilter filter;
    filter.acceptNode = [this](const DOM_Node::sp_Node & node) -> NodeFilter::FilterResp {
        if (node == end.node) return NodeFilter::FILTER_ACCEPT;

        auto parent = node->parentNode();
        // if the parent is contained, reject it
        if (Boundary(parent,0) > start && Boundary(parent, parent->length()) < end) {
            return NodeFilter::FILTER_REJECT;

        } else { // if parent is NOT contained...
            // if node is contained, skip it
            if (Boundary(node,0) > start && Boundary(node, node->length()) < end) {
                return NodeFilter::FILTER_ACCEPT;

            } else { // if node is NOT contained, skip it
                return NodeFilter::FILTER_SKIP;
            }
        }
    };

    // Init the TreeWalker
    auto tree = DOM_Node::Document::createTreeWalker(commonAncestor->shared_from_this(), 0xFFFFFFFF, filter);

    tree.currentNode = start.node; // Move TreeWalker to original start node

    std::vector<DOM_Node::sp_Node> nodesToRemove;
    auto current = tree.nextNode();
    // Travel through contained nodes until original end node
    while (current != end.node) {
        nodesToRemove.push_back(current);
        current = tree.nextNode();
    }

    // Remove nodes from its parent
    for (auto & node : nodesToRemove) {
        node->parentNode()->removeChild(node);
    }


    if (end.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto *text = dynamic_cast<DOM_Node::Text *>(end.node.get());
        text->replaceData(0, endOffset, "");
    }

    start = end = newBoundary; // Collapse
}

DOM_Node::sp_DocumentFragment Range::extractContents() {
    auto fragment = DOM_Node::Document::createDocumentFragment();
    // fragment->document = start.node->document;

    if (collapsed()) return fragment;

    // If original start node and original end node are the same, and they are a Text, ProcessingInstruction,
    // or Comment node, replace data with node original start node, offset original start offset, count original end
    // offset minus original start offset, and data the empty string, and then return.
    if (start.node == end.node && start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto textNode = dynamic_cast<DOM_Node::Text *>( start.node.get() );

        DOM_Node::sp_Text clone = std::dynamic_pointer_cast<DOM_Node::Text>( textNode->cloneNode(false) );
        clone->data = textNode->substringData(startOffset, endOffset - startOffset);
        fragment->appendChild(clone);

        textNode->replaceData(startOffset, endOffset - startOffset, "");

        return fragment;
    }


    // let's find the common ancestor
    auto startPath = NodeTravel(start.node).pathFromRoot();
    auto endPath = NodeTravel(end.node).pathFromRoot();

    int i = 0;
    while (startPath[i] == endPath[i]) i++;

    auto commonAncestor = startPath[i-1];


    DOM_Node::Node *FPChild = nullptr;
    bool startCointainsEnd = NodeTravel(start.node).inclusive_ancestor(end.node);
    if (!startCointainsEnd) {
        FPChild = startPath[i];
    }

    DOM_Node::Node *LPChild = nullptr;
    if (!NodeTravel(end.node).inclusive_ancestor(start.node)) {
        LPChild = endPath[i];
    }

    // Let contained children be a list of all children of common ancestor that are contained in range, in tree order.
    std::vector<DOM_Node::sp_Node> containedChildren;
    auto aux = startPath[i]->nextSibling();
    while (aux.get() != endPath[i]) {
        if (aux->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {
            // If any member of contained children is a doctype, then throw a "HierarchyRequestError" DOMException.
            containedChildren.push_back(aux->cloneNode(false));
        } else {
            containedChildren.push_back(aux);
        }
    }


    Boundary newBoundary = startCointainsEnd ? start : Boundary(commonAncestor, NodeTravel(startPath[i]).index()+1);

    if (FPChild) {
        if (FPChild->nodeType == DOM_Node::Node::TEXT_NODE) {
            auto textNode = dynamic_cast<DOM_Node::Text *>( start.node.get() );

            DOM_Node::sp_Text clone = std::dynamic_pointer_cast<DOM_Node::Text>( textNode->cloneNode(false) );
            clone->data = textNode->substringData(startOffset, textNode->length() - startOffset);
            fragment->appendChild(clone);

            textNode->replaceData(startOffset, textNode->length() - startOffset, "");

        } else {
            auto clone = FPChild->cloneNode(false);

            fragment->appendChild(clone);

            Range subrange;
            subrange.setStart(start.node, startOffset);
            subrange.setEnd(FPChild->shared_from_this(), FPChild->length());

            clone->appendChild(subrange.extractContents());
        }
    }


    for (auto &child : containedChildren) {
        fragment->appendChild(child);
    }


    if (LPChild) {
        if (LPChild->nodeType == DOM_Node::Node::TEXT_NODE) {
            auto textNode = dynamic_cast<DOM_Node::Text *>( end.node.get() );

            DOM_Node::sp_Text clone = std::dynamic_pointer_cast<DOM_Node::Text>( textNode->cloneNode(false) );
            clone->data = textNode->substringData(0, endOffset);
            fragment->appendChild(clone);

            textNode->replaceData(0, endOffset, "");

        } else {
            auto clone = LPChild->cloneNode(false);

            fragment->appendChild(clone);

            Range subrange;
            subrange.setStart(LPChild->shared_from_this(), 0);
            subrange.setEnd(end.node, endOffset);

            clone->appendChild(subrange.extractContents());
        }
    }


    start = end = newBoundary;

    return fragment;
}

DOM_Node::sp_DocumentFragment Range::cloneContents() const {
    auto fragment = DOM_Node::Document::createDocumentFragment();
    // fragment->document = start.node->document;

    if (collapsed()) return fragment;

    // If original start node and original end node are the same, and they are a Text, ProcessingInstruction,
    // or Comment node, replace data with node original start node, offset original start offset, count original end
    // offset minus original start offset, and data the empty string, and then return.
    if (start.node == end.node && start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto textNode = dynamic_cast<DOM_Node::Text *>( start.node.get() );

        DOM_Node::sp_Text clone = std::dynamic_pointer_cast<DOM_Node::Text>( textNode->cloneNode(false) );
        clone->data = textNode->substringData(startOffset, endOffset - startOffset);
        fragment->appendChild(clone);

        return fragment;
    }


    // let's find the common ancestor
    auto startPath = NodeTravel(start.node).pathFromRoot();
    auto endPath = NodeTravel(end.node).pathFromRoot();

    int i = 0;
    while (startPath[i] == endPath[i]) i++;


    DOM_Node::Node *FPChild = nullptr;
    bool startCointainsEnd = NodeTravel(start.node).inclusive_ancestor(end.node);
    if (!startCointainsEnd) {
        FPChild = startPath[i];
    }

    DOM_Node::Node *LPChild = nullptr;
    if (!NodeTravel(end.node).inclusive_ancestor(start.node)) {
        LPChild = endPath[i];
    }

    // Let contained children be a list of all children of common ancestor that are contained in range, in tree order.
    std::vector<DOM_Node::sp_Node> containedChildren;
    auto aux = startPath[i]->nextSibling();
    while (aux.get() != endPath[i]) {
        if (aux->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {
            // If any member of contained children is a doctype, then throw a "HierarchyRequestError" DOMException.
            containedChildren.push_back(aux->cloneNode(false));
        } else {
            containedChildren.push_back(aux);
        }
    }


    if (FPChild) {
        if (FPChild->nodeType == DOM_Node::Node::TEXT_NODE) {
            auto textNode = dynamic_cast<DOM_Node::Text *>( start.node.get() );

            DOM_Node::sp_Text clone = std::dynamic_pointer_cast<DOM_Node::Text>( textNode->cloneNode(false) );
            clone->data = textNode->substringData(startOffset, textNode->length() - startOffset);
            fragment->appendChild(clone);

        } else {
            auto clone = FPChild->cloneNode(false);

            fragment->appendChild(clone);

            Range subrange;
            subrange.setStart(start.node, startOffset);
            subrange.setEnd(FPChild->shared_from_this(), FPChild->length());

            clone->appendChild(subrange.cloneContents());
        }
    }


    for (auto &child : containedChildren) {
        fragment->appendChild(child->cloneNode(true));
    }


    if (LPChild) {
        if (LPChild->nodeType == DOM_Node::Node::TEXT_NODE) {
            auto textNode = dynamic_cast<DOM_Node::Text *>( end.node.get() );

            DOM_Node::sp_Text clone = std::dynamic_pointer_cast<DOM_Node::Text>( textNode->cloneNode(false) );
            clone->data = textNode->substringData(0, endOffset);
            fragment->appendChild(clone);

        } else {
            auto clone = LPChild->cloneNode(false);

            fragment->appendChild(clone);

            Range subrange;
            subrange.setStart(LPChild->shared_from_this(), 0);
            subrange.setEnd(end.node, endOffset);

            clone->appendChild(subrange.cloneContents());
        }
    }

    return fragment;
}

void Range::insertNode(DOM_Node::sp_Node node) {
    if ((start.node->nodeType == DOM_Node::Node::TEXT_NODE && !start.node->parentNode()) ||
        start.node == node) {

        // throw a "HierarchyRequestError" DOMException.
        return;
    }

    DOM_Node::sp_Node reference = nullptr;
    if (start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        reference = start.node;
    } else {
        auto aux = NodeTravel(start.node).childAt(startOffset);
        reference = aux ? aux->shared_from_this() : nullptr;
    }

    auto parent = !reference ? start.node : reference->parentNode() ;

    if (!MutationAlgorithms(parent).pre_insertion_validity(node, reference)) {
        return;
    }

    if (start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto text = dynamic_cast<DOM_Node::Text *>(start.node.get());
        reference = text->splitText(startOffset);
    }

    auto newOffset = !reference ? parent->length() : NodeTravel(reference).index() ;
    newOffset += node->nodeType == DOM_Node::Node::DOCUMENT_FRAGMENT_NODE ? node->length() : 1 ;

    MutationAlgorithms(parent).pre_insert(node, reference);

    if (collapsed()) end = Boundary(parent, newOffset);
}

void Range::surroundContents(DOM_Node::sp_Node newParent) {
    if (!newParent) return;

    // If a non-Text node is partially contained in the context object,
    // then throw an "InvalidStateError" DOMException. Since Text nodes cannot have
    // any child, the path of common ancestor of both must contain just one node, the parent of both
    if (    start.node->nodeType != DOM_Node::Node::TEXT_NODE ||
            end.node->nodeType != DOM_Node::Node::TEXT_NODE ||
            start.node->parentNode() != end.node->parentNode() ) {

        throw "InvalidStateError"; // DOMException.
    }

    switch (newParent->nodeType) {
        case DOM_Node::Node::DOCUMENT_NODE:
        case DOM_Node::Node::DOCUMENT_TYPE_NODE:
        case DOM_Node::Node::DOCUMENT_FRAGMENT_NODE:
            throw "InvalidNodeTypeError"; // DOMException.
        default: ;
    }

    auto fragment = extractContents();

    if (!newParent->childNodes.empty()) {
        MutationAlgorithms(newParent).replace_all(nullptr);
    }

    insertNode(newParent);

    newParent->appendChild(fragment);

    selectNode(newParent);
}

Range Range::cloneRange() {
    return Range(*this);
}

bool Range::isPointInRange(DOM_Node::sp_Node node, unsigned long offset) const {
    if (NodeTravel(node).root() != root()) {
        throw "WrongDocumentError"; // DOMException.
    }

    if (node->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {
        throw "InvalidNodeTypeError"; // DOMException.
    }

    if (offset > node->length()) {
        throw "IndexSizeError"; // DOMException.
    }

    Boundary aux(node, offset);

    return !(aux < start || aux > end);
}

short Range::comparePoint(DOM_Node::sp_Node node, unsigned long offset) const {
    if (NodeTravel(node).root() != root()) {
        throw "WrongDocumentError"; // DOMException.
    }

    if (node->nodeType == DOM_Node::Node::DOCUMENT_TYPE_NODE) {
        throw "InvalidNodeTypeError"; // DOMException.
    }

    if (offset > node->length()) {
        throw "IndexSizeError"; // DOMException.
    }

    Boundary aux(node, offset);

    if (aux < start) return -1;

    if (aux > end) return  1;

    return 0;
}

bool Range::intersectsNode(DOM_Node::sp_Node node) const {
    if (NodeTravel(node).root() != root()) return false;

    auto parent = node->parentNode();

    if (!parent) return true;

    auto offset = NodeTravel(node).index();

    Boundary aux(parent, offset);

    return Boundary(parent, offset) < end && Boundary(parent, offset+1) > start ;
}

std::string Range::stringifier() {
    std::string s;

    if (start.node == end.node && start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto text = dynamic_cast<DOM_Node::Text *>(start.node.get());
        return text->substringData(startOffset, endOffset);
    }


    if (start.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto text = dynamic_cast<DOM_Node::Text *>(start.node.get());
        s += text->substringData(startOffset, text->length() - startOffset) + "\n";
    }


    // Init the filter for the TreeWalker
    NodeFilter filter;
    filter.acceptNode = [this](const DOM_Node::sp_Node & node) -> NodeFilter::FilterResp {
        if (node == end.node) return NodeFilter::FILTER_ACCEPT;

        if (node->nodeType == DOM_Node::Node::TEXT_NODE)
            return NodeFilter::FILTER_ACCEPT;

        return NodeFilter::FILTER_SKIP;
    };

    // Init the TreeWalker
    auto tree = DOM_Node::Document::createTreeWalker(commonAncestorContainer(), 0xFFFFFFFF, filter);

    tree.currentNode = start.node; // Move TreeWalker to original start node

    auto current = tree.nextNode();
    // Travel through contained nodes until original end node
    while (current != end.node) {
        auto text = dynamic_cast<DOM_Node::Text *>(current.get());

        s += text->data + "\n";

        current = tree.nextNode();
    }


    if (end.node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto text = dynamic_cast<DOM_Node::Text *>(end.node.get());
        s += text->substringData(0, endOffset);
    }

    return s;
}

DOM_Node::sp_DocumentFragment Range::createContextualFragment(DOMString fragment) {
    auto node = start.node;
    DOM_Node::sp_Element element = nullptr;

    switch (node->nodeType) {
        case DOM_Node::Node::DOCUMENT_NODE:
        case DOM_Node::Node::DOCUMENT_FRAGMENT_NODE:
            element = nullptr;
            break;
        case DOM_Node::Node::ELEMENT_NODE:
            element = std::dynamic_pointer_cast<DOM_Node::Element>(node);
            break;
        case DOM_Node::Node::TEXT_NODE:
            element = node->parentElement();
            break;
        default:
            break;
    }

    if (!element || (
            element->ownerDocument() &&
            element->ownerDocument()->contentType == "text/html" &&
            element->tagName == "html"
            )) {

        element = DOM_Node::Document::createElement("body");
        node->ownerDocument()->adoptNode(element);
    }

    auto fragmentNode = DOMParser::fragmentParse(fragment);

    return fragmentNode;
}