#include <DOM/HTMLElements/HTMLSlotElement.hpp>
#include "MutationAlgorithms.hpp"
#include "NodeTravel.hpp"
#include "Document.hpp"
#include "Text.hpp"

using namespace DOM_Node;


bool MutationAlgorithms::pre_insertion_validity(sp_Node node, sp_Node child) const {
    return pre_insertion_validity(node.get(), child.get());
}

bool MutationAlgorithms::pre_insertion_validity(Node *node, Node *child) const {
    if (!(self->_nodeType == Node::DOCUMENT_NODE ||
          self->_nodeType == Node::ELEMENT_NODE ||
          self->_nodeType == Node::DOCUMENT_FRAGMENT_NODE) ) {
        // throw a "HierarchyRequestError" DOMException.
        return false;
    }

    if (NodeTravel(node).host_including_inclusive_ancestor(self)) {
        // throw a "HierarchyRequestError" DOMException.
        return false;
    }

    if (child && child->_parentNode != self) {
        // throw a "NotFoundError" DOMException.
        return false;
    }

    if (!(node->_nodeType == Node::DOCUMENT_FRAGMENT_NODE ||
          node->_nodeType == Node::DOCUMENT_TYPE_NODE ||
          node->_nodeType == Node::ELEMENT_NODE ||
          node->_nodeType == Node::TEXT_NODE /*||
              node->_nodeType == PROCESSING_INSTRUCTION_NODE ||
              node->_nodeType == COMMENT_NODE*/) ) {
        // throw a "HierarchyRequestError" DOMException.
        return false;
    }

    if ((node->_nodeType == Node::TEXT_NODE && self->_nodeType == Node::DOCUMENT_NODE) ||
        (node->_nodeType == Node::DOCUMENT_TYPE_NODE && self->_nodeType != Node::DOCUMENT_NODE) ) {
        // throw a "HierarchyRequestError" DOMException.
        return false;
    }

    if (self->nodeType == Node::DOCUMENT_NODE) {
        bool f = false;
        const auto document = dynamic_cast<Document *>(const_cast<Node *>(self));

        switch (node->_nodeType) {
            case Node::DOCUMENT_FRAGMENT_NODE: {
                int cont = 0;
                for (const auto & nodeChild : node->_childNodes) {
                    if (nodeChild->_nodeType == Node::ELEMENT_NODE) {
                        if (++cont > 1) {
                            f = true;
                            break;
                        }

                    } else if (nodeChild->_nodeType == Node::TEXT_NODE) {
                        f = true;
                        break;
                    }
                }

                if (!f) {
                    f = (   document->documentElement ||
                            (child && child->_nodeType == Node::DOCUMENT_TYPE_NODE) ||
                            // this code expects that only Element and DoxumentType be inserted into Document
                            (child && child->_nextSibling && child->_nextSibling->_nodeType == Node::DOCUMENT_TYPE_NODE) );
                }

                break;
            }
            case Node::ELEMENT_NODE: {
                f = (   document->documentElement != nullptr ||
                        (child && child->_nodeType == Node::DOCUMENT_TYPE_NODE) ||
                        // this code expects that only Element and DoxumentType be inserted into Document
                        (child && child->_nextSibling && child->_nextSibling->_nodeType == Node::DOCUMENT_TYPE_NODE) );

                break;
            }
            case Node::DOCUMENT_TYPE_NODE: {
                f = (   document->doctype ||
                        // this code expects that only Element and DoxumentType be inserted into Document
                        (child && child->_previousSibling && child->_previousSibling->_nodeType == Node::ELEMENT_NODE) ||
                        (!child && document->documentElement) );

                break;
            }
            default: break;
        }

        if (f) {
            // throw a "HierarchyRequestError" DOMException.
            return false;
        }
    }

    return true;
}

Node* MutationAlgorithms::pre_insert(const sp_Node& node, sp_Node child) {
    return pre_insert(node.get(), child.get());
}

Node* MutationAlgorithms::pre_insert(Node *node, Node *child) {
    if (!node) return nullptr;
    if (node == child) return node;
    if (!pre_insertion_validity(node, child)) return nullptr;

    // 5. Insert node into parent before reference child.
    this->insert(node, child);

    return node;
}

void MutationAlgorithms::insert(sp_Node node, sp_Node child, bool suppress_observers) {
    return insert(node.get(), child.get(), suppress_observers);
}

void MutationAlgorithms::insert(Node *node, Node *child, bool suppress_observers) {
    if (!node) return;

    bool isDocFrag = node->nodeType == Node::DOCUMENT_FRAGMENT_NODE;
    NodeList nodes;

    if (isDocFrag) {
        nodes = node->_childNodes;

    } else {
        nodes.push_back(node->shared_from_this());
    }

    auto count = nodes.size();

    if (count == 0) return;

    if (isDocFrag) {
        for (auto & aux : node->_childNodes) {
            MutationAlgorithms::remove(aux, true);
        }

        //todo, queue a tree mutation record for node with « », nodes, null, and null.
    }

    if (child) {
        auto childIndex = NodeTravel(child).index();

        forEachLiveRange([=](sp_Range &range) {
            if (range->start.node.get() == self &&
                range->start.offset > childIndex) {

                range->start.offset += count;
            }

            if (range->end.node.get() == self &&
                range->end.offset > childIndex) {

                range->end.offset += count;
            }
        });
    }

    sp_Node previousSibling = child ? child->previousSibling() : self->lastChild() ;

    auto end = self->_childNodes.end();
    auto it = self->_childNodes.begin();

    if (child) {
        for (; it != end; it++) {
            if ((*it).get() == child)
                break;
        }

    } else {
        it = self->_childNodes.end();
    }

    auto actualPrevSibling = previousSibling.get();
    auto selfElement = dynamic_cast<Element *>(self);
    bool selfConnected = self->isConnected();

    for (auto & aux : nodes) {
        self->_ownerDocument->adoptNode(aux);

        remove(aux);

        aux->_previousSibling = actualPrevSibling;
        aux->_nextSibling = child;

        if (child) {
            child->_previousSibling = aux.get();
        }

        if (actualPrevSibling) {
            actualPrevSibling->_nextSibling = aux.get();
        }

        aux->_parentNode = self;
        if (selfElement) {
            aux->_parentElement = selfElement;
        }

        self->_childNodes.insert(it, aux);

        actualPrevSibling = aux.get();

        auto nodeSlotable = dynamic_cast<Slotable *>(aux.get());

        if (selfElement && selfElement->shadowRoot() && nodeSlotable) {
            nodeSlotable->assign_slot();
        }

        auto selfRoot = NodeTravel(self).root();
        auto parentShadowRoot = dynamic_cast<ShadowRoot *>(selfRoot);
        auto selfSlot = dynamic_cast<HTMLSlotElement *>(self);

        if (parentShadowRoot && selfSlot && selfSlot->assignedNodes().empty()) {
            selfSlot->signal_slot_change();
        }

        Slotable::assign_slotables_for_tree(aux->getRootNode());

        if (selfConnected) {
            NodeTravel(aux).forEach.shadow_including_inclusive_descendants([](Node *inclusiveDescendant) {
                inclusiveDescendant->insertion_steps();

                if (inclusiveDescendant->nodeType == Node::ELEMENT_NODE) {
                    auto *element = dynamic_cast<Element *>(inclusiveDescendant);
                    element->connectedCallback();
                }

                //if (!inclusiveDescendant->isCustom()) {
                    //todo, try to upgrade inclusiveDescendant.
                //}
            });
        }
    }

    if (!suppress_observers) {
        //todo,  queue a tree mutation record for parent with nodes, « », previousSibling, and child.
    }

    self->children_changed_steps();
}

Node * MutationAlgorithms::append(sp_Node node) {
    return pre_insert(node.get(), nullptr);
}

Node * MutationAlgorithms::append(Node *node) {
    return pre_insert(node, nullptr);
}

sp_Node MutationAlgorithms::replace(Node *child, Node *node) {
    return replace(child->shared_from_this(), node->shared_from_this());
}

sp_Node MutationAlgorithms::replace(sp_Node child, sp_Node node) {
    if (!child || !node) {
        //throw something

        return nullptr;
    }

    if (child == node) return child;
    

    const auto & selfType = self->_nodeType;

    if (selfType != Node::DOCUMENT_NODE &&
        selfType != Node::DOCUMENT_FRAGMENT_NODE &&
        selfType != Node::ELEMENT_NODE) {

        //throw a "HierarchyRequestError" DOMException.
        return nullptr;
    }

    if (NodeTravel(node).host_including_inclusive_ancestor(self)) {
        //throw a "HierarchyRequestError" DOMException.
        return nullptr;
    }

    if (child->_parentNode != self) {
        //throw a "NotFoundError" DOMException.
        return nullptr;
    }

    const auto & nodeType = node->_nodeType;

    if (nodeType != Node::DOCUMENT_FRAGMENT_NODE &&
        nodeType != Node::DOCUMENT_TYPE_NODE &&
        nodeType != Node::ELEMENT_NODE &&
        nodeType != Node::TEXT_NODE /*&&
        nodeType != Node::PROCESS_INSTRUCTION_NODE &&
        nodetype != Node::COMMENT_NODE*/) {

        //throw a "HierarchyRequestError" DOMException.
        return nullptr;
    }

    if ((nodeType == Node::TEXT_NODE && selfType == Node::DOCUMENT_NODE) ||
        (nodeType == Node::DOCUMENT_TYPE_NODE && selfType != Node::DOCUMENT_NODE)) {

        // throw a "HierarchyRequestError" DOMException.
        return nullptr;
    }

    if (selfType == Node::DOCUMENT_NODE) {
        bool f = false;

        auto document = dynamic_cast<Document *>(self);
        auto docElement = document->documentElement;
        auto nextChildSibling = child->_nextSibling;

        switch (nodeType) {
            case Node::DOCUMENT_FRAGMENT_NODE: {
                int cont = 0;
                for (const auto & nodeChild : node->_childNodes) {
                    if (nodeChild->_nodeType == Node::ELEMENT_NODE) {
                        if (++cont > 1) {
                            f = true;
                            break;
                        }

                    } else if (nodeChild->_nodeType == Node::TEXT_NODE) {
                        f = true;
                        break;
                    }
                }

                if (!f) {

                    f = (   (docElement && docElement != child) ||
                            (nextChildSibling && nextChildSibling->_nodeType == Node::DOCUMENT_TYPE_NODE ));
                }

                break;
            }
            case Node::ELEMENT_NODE: {
                f = (   (docElement && docElement != child) ||
                        (nextChildSibling && nextChildSibling->_nodeType == Node::DOCUMENT_TYPE_NODE ));

                break;
            }
            case Node::DOCUMENT_TYPE_NODE: {
                auto docType = document->doctype;

                f = (   (docType && docType != child) ||
                        // this code expects that only Element and DoxumentType be inserted into Document
                        (child->_previousSibling && child->_previousSibling->_nodeType == Node::ELEMENT_NODE));

                break;
            }
            default:
                break;
        }

        if (f) {
            // throw a "HierarchyRequestError" DOMException.
            return nullptr;
        }
    }

    auto referenceChild = child->_nextSibling;

    if (referenceChild == node.get()) {
        referenceChild = node->_nextSibling;
    }

    auto previousSibling = child->_previousSibling;

    std::vector<sp_Node> removedNodes;

    if (child->_parentNode) {
        removedNodes.push_back(child);
        remove(child, true);
    }

    NodeList nodes;

    if (nodeType == Node::DOCUMENT_FRAGMENT_NODE) {
        nodes = node->_childNodes;
    } else {
        nodes.push_back(node->shared_from_this());
    }

    insert(node.get(), referenceChild, true);

    //todo, qeue a tree mutation record for parent with nodes, removedNodes, previousSibling, and referenceChild.

    return child;
}

void MutationAlgorithms::replace_all(sp_Node node) {
    replace_all(node.get());
}

void MutationAlgorithms::replace_all(Node *node) {
    auto removedNodes = self->_childNodes;
    NodeList addedNodes;

    if (node) {
        if (node->_nodeType == Node::DOCUMENT_FRAGMENT_NODE) {
            addedNodes = node->_childNodes;
        } else {
            addedNodes.push_back(node->shared_from_this());
        }
    }

    for (auto & child : self->_childNodes) {
        remove(child, true);
    }

    if (node) {
        insert(node, nullptr);
    }

    //todo, Queue a tree mutation record for parent with addedNodes, removedNodes, null, and null.
}

sp_Node MutationAlgorithms::pre_remove(sp_Node child) {
    if (!child || child->_parentNode != self) {
        //throw a "NotFoundError" DOMException.
        return nullptr;
    }

    remove(child);

    return child;
}

sp_Node MutationAlgorithms::pre_remove(Node *child) {
    return pre_remove(child->shared_from_this());
}

void MutationAlgorithms::remove(DOM_Node::sp_Node child, bool suppress_observers) {
    remove(child.get(), suppress_observers);
}

void MutationAlgorithms::remove(DOM_Node::Node *node, bool suppress_observers) {
    auto parent = node->_parentNode;

    if (!parent) return;

    auto index = NodeTravel(node).index();

    MutationAlgorithms(parent).forEachLiveRange([=](sp_Range &range) {
        if (NodeTravel(range->start.node).inclusive_descendant(node)) {
            range->start = Range::Boundary(parent, index);
        }

        if (NodeTravel(range->end.node).inclusive_descendant(node)) {
            range->end = Range::Boundary(parent, index);
        }

        if (range->start.node.get() == parent && range->start.offset > index) {
            range->start.offset--;
        }

        if (range->end.node.get() == parent && range->end.offset > index) {
            range->end.offset--;
        }
    });

    auto sp_Node = node->shared_from_this();

    for (auto & wp_iterator : parent->_ownerDocument->nodeIterators) {
        if (auto iterator = wp_iterator.lock()) {
            if (iterator->root->_ownerDocument == node->_ownerDocument) {
                iterator->pre_removing(sp_Node);
            }
        }
    }

    auto oldPreviousSibling = node->_previousSibling;
    auto oldNextSibling = node->_nextSibling;

    if (oldPreviousSibling) {
        oldPreviousSibling->_nextSibling = oldNextSibling;
    }

    if (oldNextSibling) {
        oldNextSibling->_previousSibling = oldPreviousSibling;
    }

    node->_previousSibling = nullptr;
    node->_nextSibling = nullptr;
    node->_parentNode = nullptr;
    node->_parentElement = nullptr;

    parent->_childNodes.remove(sp_Node);

    auto slotable = dynamic_cast<Slotable *>(node);

    if (slotable && slotable->assignedSlot) {
        Slotable::assign_slotables(slotable->_assignedSlot);
    }

    auto shadowRoot = dynamic_cast<ShadowRoot *>(NodeTravel(parent).root());
    auto slot = dynamic_cast<HTMLSlotElement *>(parent);

    if (shadowRoot && slot && slot->assignedNodes().empty()) {
        slot->signal_slot_change();
    }

    NodeFilter filter;
    filter.acceptNode = [](const DOM_Node::sp_Node & node) -> NodeFilter::FilterResp {
        auto slot_aux = std::dynamic_pointer_cast<HTMLSlotElement>(node);

        return slot_aux ? NodeFilter::FILTER_ACCEPT : NodeFilter::FILTER_SKIP ;
    };

    // Init the TreeWalker
    auto tree = DOM_Node::Document::createTreeWalker(sp_Node, NodeFilter::SHOW_ELEMENT, filter);

    if (tree.nextNode()) {
        Slotable::assign_slotables_for_tree(parent->getRootNode());
        Slotable::assign_slotables_for_tree(sp_Node);
    }

    node->removing_steps(parent);

    bool isParentConnected = parent->isConnected();

    if (node->nodeType == Node::ELEMENT_NODE && isParentConnected) {
        auto *element = dynamic_cast<Element *>(node);
        element->disconnectedCallback();
    }

    NodeTravel(node).forEach.shadow_including_descendants([=](Node * descendant) {
        descendant->removing_steps();

        if (descendant->nodeType == Node::ELEMENT_NODE && isParentConnected) {
            auto *element = dynamic_cast<Element *>(descendant);
            element->disconnectedCallback();
        }
    });

    //todo, For each inclusive ancestor inclusiveAncestor of parent, and then for each registered of
    // inclusiveAncestor’s registered observer list, if registered’s options’s subtree is true,
    // then append a new transient registered observer whose observer is registered’s observer, options is
    // registered’s options, and source is registered to node’s registered observer list.

    if (!suppress_observers) {
        //todo,  queue a tree mutation record for parent with « », « node », oldPreviousSibling, and oldNextSibling.
    }

    parent->children_changed_steps();
}

sp_Node MutationAlgorithms::convert_nodes_into_a_node(DOM_Node::NodeList &nodes, DOM_Node::Document *document) {
    sp_Node node = nullptr;

    if (nodes.size() == 1) {
        node = nodes.back();

    } else {
        node = document->createDocumentFragment();

        MutationAlgorithms ma(node);

        for (auto & aux : nodes) {
            ma.append(aux);
        }
    }

    return node;
}

void MutationAlgorithms::string_replace_all(const std::string &str) {
    if (str.empty()) return;

    auto node = self->_ownerDocument->createTextNode(str);

    replace_all(node);
}

void MutationAlgorithms::forEachLiveRange(const std::function<void (std::shared_ptr<Range> &)>& toDo) {
    if (!self->_ownerDocument) return;

    for (auto & wp_range : self->_ownerDocument->liveRanges) {
        if (auto range = wp_range.lock()) {
            toDo(range);
        }
    }
}

void MutationAlgorithms::attributeChangedCallback(DOMString name, DOMString oldValue, DOMString newValue) {
    if (self && self->nodeType == Node::ELEMENT_NODE) {
        auto *element = dynamic_cast<Element *>(self);
        element->attributeChangedCallback(name, oldValue, newValue);
    }
}

void MutationAlgorithms::adoptedCallback(DOM_Node::Document *oldDocument, DOM_Node::Document *document) {
    if (self && self->nodeType == Node::ELEMENT_NODE) {
        auto *element = dynamic_cast<Element *>(self);
        element->adoptedCallback(oldDocument, document);
    }
}

void MutationAlgorithms::children_changed_steps() {
    self->children_changed_steps();
}