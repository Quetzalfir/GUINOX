#include "CustomElementRegistry.hpp"
#include <regex>


CustomElementRegistry::CustomElementRegistry(const std::string &tagName, std::function<std::shared_ptr<HTMLElement>()> constructor) {
    define(tagName, std::move(constructor));
}

void CustomElementRegistry::define(const std::string &name,
                                   std::function<std::shared_ptr<HTMLElement>()> constructor) {

    // https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name
    static const std::regex exp(
            "[a-z]"
            "( - | . | [0-9] | _ | [a-z] | \xB7 | [\xC0-\xD6] | [\xD8-\xF6] | [\xF8-\u037D ] | [\u037F-\u1FFF] | [\u200C-\u200D] | [\u203F-\u2040] | "
            "[\u2070-\u218F] | [\u2C00-\u2FEF] | [\u3001-\uD7FF] | [\uF900-\uFDCF] | [\uFDF0-\uFFFD] | [\u10000-\uEFFFF] )* "
            " - "
            "( - | . | [0-9] | _ | [a-z] | \xB7 | [\xC0-\xD6] | [\xD8-\xF6] | [\xF8-\u037D ] | [\u037F-\u1FFF] | [\u200C-\u200D] | [\u203F-\u2040] | "
            "[\u2070-\u218F] | [\u2C00-\u2FEF] | [\u3001-\uD7FF] | [\uF900-\uFDCF] | [\uFDF0-\uFFFD] | [\u10000-\uEFFFF] )* ");


    if (    std::regex_match(name, exp) &&
            name != "annotation-xml" &&
            name != "color-profile" &&
            name != "font-face" &&
            name != "font-face-src" &&
            name != "font-face-uri" &&
            name != "font-face-format" &&
            name != "font-face-name" &&
            name != "missing-glyph") {

        auto it = elements.find(name);
        if (it == elements.end()) {
            elements[name] = std::move(constructor);
        }
    }
}