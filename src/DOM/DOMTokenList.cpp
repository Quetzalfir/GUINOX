#include "DOMTokenList.hpp"
#include <algorithm>
#include <Utils/Utils.hpp>


DOMTokenList::DOMTokenList() : value([this] { return valueGetter(); },
                                     [this](const std::string & PH1, const std::string & PH2) { valueSetter(PH1, PH2); }) {}

std::string DOMTokenList::valueGetter() {
    std::string res;

    for (auto const & token : list) {
        res += token + " ";
    }

    return res.substr(0, res.length()-1);
}

void DOMTokenList::valueSetter(const std::string &oldvalue, const std::string &newValue) {
    list.clear();
    _length = 0;

    if (newValue.empty()) return;

    add(newValue);
}

void DOMTokenList::add(const std::vector<std::string> &tokens) {
    for (const std::string &s : tokens) {

        bool found = false;

        for (const std::string &clas : list) {
            if (s == clas) {
                found = true;
                break;
            }
        }

        if (!found) {
            list.push_back(s);
            _length++;
        }
    }
}

void DOMTokenList::add(const std::string &str) {
    add(Utils::split_string_by(str, ' '));
}

void DOMTokenList::remove(const std::vector<std::string> &tokens) {
    for (const std::string &s : tokens) {

        auto end = list.end();
        for (auto it = list.begin(); it != end; it++) {
            if ((*it) == s) {
                list.erase(it);
                _length--;
                break;
            }
        }
    }
}

void DOMTokenList::forEach(std::function<void(std::string, int, const std::deque<std::string> &)> callback) {
    for (size_t i = 0; i < list.size(); i++ ) {
        callback(list[i],i,list);
    }
}

bool DOMTokenList::contains(const std::string &str) const {
    for (const std::string &clas : list) {
        if (clas == str) {
            return true;
        }
    }

    return false;
}

bool DOMTokenList::toggle(const std::string &str) {
    auto end = list.end();
    for (auto it = list.begin(); it != end; it++) {
        if ((*it) == str) {
            list.erase(it);
            _length--;
            return false;
        }
    }

    list.push_back(str);
    _length++;
    return true;
}

bool DOMTokenList::toggle(const std::string &str, bool force) {
    if (force) {
        add({str});
    } else {
        remove({str});
    }

    return force;
}

void DOMTokenList::replace(const std::string &oldClass, const std::string &newClass) {
    auto end = list.end();
    for (auto it = list.begin(); it != end; it++) {
        if ((*it) == oldClass) {
            std::replace(it, it, (*it), newClass);
            break;
        }
    }
}

DOMTokenList& DOMTokenList::operator=(const DOMTokenList &other) {
    if (&other == this) {
        return *this;
    }

    list = other.list;
    _length = other.length;

    return *this;
}