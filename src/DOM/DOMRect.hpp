#ifndef GUINOX_DOMRECT_HPP
#define GUINOX_DOMRECT_HPP


#include "../Utils/Point.hpp"


class DOMRect {
    friend class ViewPort;

    float vertex[8] = {0};


public:

    static DOMRect fromRect(double x, double y, double width, double height) {
            DOMRect res;
            res.top = y;
            res.left = x;
            res.bottom = y + height;
            res.right = x + width;

            return res;
    }



    double top = 0;
    double left = 0;
    double bottom = 0;
    double right = 0;

    DOMRect() {top = left = bottom = right = 0;}

    void move(double x, double y) {
        left += x; right += x;
        top += y; bottom += y;
    }

    void moveTo(double x, double y) {
        double u = x - left;
        double v = y - top;

        move(u, v);
    }

    void width(double width_) { right = width_ >= 0 ? left+width_ : left ; }
    double width() const { return right-left; }
    void height(double height_) { bottom = height_ >= 0 ? top+height_ : top ; }
    double height() const { return bottom-top;}

    const float *getVertex() const { return vertex; }

};


#endif //GUINOX_DOMRECT_HPP
