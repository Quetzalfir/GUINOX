/**
* @brief Algorithms that define how to traverse the DOM tree.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*/
#ifndef GUINOX_NODETRAVEL_HPP
#define GUINOX_NODETRAVEL_HPP

#include <functional>
#include "dom_declarations.hpp"

class NodeTravel {

    class ForEach {
        DOM_Node::Node *const self;

    public:
        ForEach(DOM_Node::Node *node) : self(node) {}

        void shadow_including_inclusive_descendants(const std::function<void( DOM_Node::Node * )> &cb);
        void shadow_including_descendants(const std::function<void( DOM_Node::Node * )> &cb);
        void inclusive_descendants(const std::function<void( DOM_Node::Node * )> &cb);
        void descendants(const std::function<void( DOM_Node::Node * )> &cb);
        void inclusive_ancestor(const std::function<void( DOM_Node::Node * )> &cb);

    };


    DOM_Node::Node* const self;


public:

    ForEach forEach;


    NodeTravel(DOM_Node::Node *node) : self(node), forEach(node) {}
    NodeTravel(const DOM_Node::sp_Node& node) : NodeTravel(node.get()) {}


    DOM_Node::Node *root() const ;
    DOM_Node::Node *shadow_including_root() const ;

    bool inclusive_descendant(const DOM_Node::Node *node) const ;
    inline bool inclusive_descendant(const DOM_Node::sp_Node &node) const { return inclusive_descendant(node.get()); }

    bool inclusive_ancestor(const DOM_Node::Node *node) const ;
    inline bool inclusive_ancestor(const DOM_Node::sp_Node &node) const { return inclusive_ancestor(node.get()); }

    bool ancestor(const DOM_Node::Node *node) const ;
    inline bool ancestor(const DOM_Node::sp_Node &node) const { return ancestor(node.get()); }

    DOM_Node::Node *childAt(size_t index) const ;

    DOM_Node::Node *following(DOM_Node::Node * root) const ;
    inline DOM_Node::Node *following(const DOM_Node::sp_Node &root) const { return following(root.get()); }

    DOM_Node::Node *preceding(DOM_Node::Node * root) const ;
    DOM_Node::Node *preceding(const DOM_Node::sp_Node &root) const { return preceding(root.get()); }

    size_t index() const ;
    bool host_including_inclusive_ancestor(const DOM_Node::Node *node) const ;

    std::deque<DOM_Node::Node *> pathFromRoot() const ;

    bool isSibling(const DOM_Node::sp_Node & node) const ;


};


#endif //GUINOX_NODETRAVEL_HPP
