#include "XMLSerializer.hpp"
#include "Element.hpp"
#include "Text.hpp"
#include "DocumentType.hpp"
#include "Utils/Utils.hpp"

using namespace DOM_Node;

std::string XMLSerializer::serializeToString(DOM_Node::sp_Node root) {
    std::string str;
    bool emptyTag = false;

    if (!root) return str;

    switch (root->nodeType) {
        case Node::TEXT_NODE: {
            auto textNode = dynamic_cast<Text *>(root.get());
            str += textNode->data;
            break;
        }
        case Node::ELEMENT_NODE: {
            auto elemNode = dynamic_cast<Element *>(root.get());
            str += "<" + elemNode->tagName + " ";

            auto attributes = elemNode->attributes;
            int length = attributes.length();
            for (int i = 0; i < length; i++) {
                auto attr = attributes[i];
                str += attr->name + "=\"" + attr->value + "\" ";
            }

            if (Utils::isEmptyElement(elemNode->tagName)) {
                emptyTag = true;
                str += " />";

            } else {
                str += ">";
            }

            break;
        }
        case Node::DOCUMENT_TYPE_NODE: {
            auto doctype = dynamic_cast<DocumentType *>(root.get());
            str += "<!DOCTYPE " + doctype->name + ">";

            break;
        }
        default: break;
    }

    for (const auto & child : root->childNodes) {
        str += "\n" + serializeToString(child);
    }

    if (root->nodeType == Node::ELEMENT_NODE && !emptyTag) {
        str += "\n</" + root->nodeName + ">";
    }

    return str;
}