/**
* @brief DOM_Node is a namespace that encompasses all types of nodes that belong to the DOM.
*
*/
#ifndef GUINOX_DOCUMENTFRAGMENT_HPP
#define GUINOX_DOCUMENTFRAGMENT_HPP


#include "ParentNode.hpp"
#include "NonElementParentNode.hpp"

namespace DOM_Node {
/**
 * @brief The DocumentFragment interface represents a
 * minimal document object that has no parent.
 *
 * It is used as a lightweight version of Document that
 * stores a segment of a document structure comprised
 * of nodes just like a standard document.
 *
 * @author GUINOX
 * @version 1.0
 * @date 24/02/2020
 *
 * @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment
 */
    class DocumentFragment :
            public virtual Node,
            public ParentNode,
            public NonElementParentNode {

        friend class Document;

    protected:
        Element * _host = nullptr;


        DocumentFragment();


    public:

        sp_Element host() const ;
        sp_Node cloneNode(bool deep) const override;

        sp_Element getElementById(const std::string & id) const override ;


        // ParentNode inherit
        DOM_Node::HTMLCollection querySelectorAll(const std::string & selector) override;


    };


    typedef std::shared_ptr<DOM_Node::DocumentFragment> sp_DocumentFragment;

}

#endif //GUINOX_DOCUMENTFRAGMENT_HPP
