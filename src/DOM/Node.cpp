#include "Node.hpp"
#include "Text.hpp"
#include "Element.hpp"
#include "Document.hpp"
#include "NodeTravel.hpp"
#include <algorithm>
#include <iostream>
#include <utility>
#include "MutationAlgorithms.hpp"
#include "Range.hpp"


namespace DOM_Node {

    Node::Node(DOM_Node::Node::NodeType type) : _nodeType(type) {

    }

    Node::~Node() {
        if (nodeType != TEXT_NODE) {
            std::cout << "Node: destroy Node: " << _nodeName << std::endl;
        }
    }

    sp_Node Node::firstChild() const {
        return _childNodes.empty() ? nullptr : _childNodes.front() ;
    }

    sp_Node Node::lastChild() const {
        return _childNodes.empty() ? nullptr : _childNodes.back() ;
    }

    bool Node::isConnected() const {
        auto node = NodeTravel(const_cast<Node *>(this)).shadow_including_root();

        return node->nodeType == DOCUMENT_NODE;
    }

    sp_Node Node::appendChild(sp_Node aChild) {
        auto res = MutationAlgorithms(this).append(std::move(aChild));

        if (res) return res->shared_from_this();
        return nullptr;
    }

    void Node::normalize() {
        NodeTravel(this).forEach.descendants([this](Node *node) {
            if (node->nodeType == TEXT_NODE) {
                auto length = node->length();
                if (!length) {
                    MutationAlgorithms::remove(node);
                    return;
                }

                // Let data be the concatenation of the data of node’s contiguous exclusive Text nodes (excluding itself), in tree order.
                std::string data;
                auto aux = node->_nextSibling;

                std::vector<Node*> nodesToRemove;
                while (aux && aux->nodeType == TEXT_NODE) {
                    auto textNode = dynamic_cast<Text *>(aux);
                    data += textNode->data;

                    nodesToRemove.push_back(aux);
                    aux = aux->_nextSibling;
                }

                // Replace data with node node, offset length, count 0, and data data.
                auto textNode = dynamic_cast<Text *>(node);
                textNode->replaceData(length, 0, data);

                auto currentNode = node->_nextSibling;
                auto thisP = this->shared_from_this();
                while (currentNode && currentNode->nodeType == TEXT_NODE) {
                    MutationAlgorithms(this).forEachLiveRange([=](sp_Range &range) {
                        if (range->start.node.get() == currentNode) {
                            range->start.offset += length;
                            range->start.node = thisP;
                        }

                        if (range->end.node.get() == currentNode) {
                            range->end.offset += length;
                            range->end.node = thisP;
                        }

                        if (range->start.node == currentNode->parentNode() &&
                            range->start.offset == NodeTravel(currentNode).index()) {

                            range->start.offset = length;
                            range->start.node = thisP;
                        }

                        if (range->end.node == currentNode->parentNode() &&
                            range->end.offset == NodeTravel(currentNode).index()) {

                            range->end.offset = length;
                            range->end.node = thisP;
                        }
                    });

                    length += currentNode->length();
                    currentNode = _nextSibling;
                }

                for (auto nodeToRemove : nodesToRemove) {
                    MutationAlgorithms::remove(nodeToRemove);
                }
            }
        });
    }

    sp_Node Node::cloneNode(bool deep) const {
        auto res = std::shared_ptr<Node>(new Node(nodeType));

        clone(res.get(), deep);

        return res;
    }

    void Node::clone(Node *copy, bool deep) const {
        copy->_nodeName = nodeName;
        copy->_ownerDocument = _ownerDocument;

        if (deep) {
            for (const auto & child : childNodes) {
                sp_Node clone = child->cloneNode(true);
                copy->appendChild(clone);
            }
        }

        copy->_ownerDocument = _ownerDocument;
    }

    /**
     * The contains(other) method, when invoked, must return true if other is an inclusive
     * descendant of context object, and false otherwise (including when other is null).
     *
     * @param other
     * @return
     */
    bool Node::contains(const sp_Node other) const {
        return NodeTravel(other).inclusive_descendant(this);
    }

    sp_Node Node::getRootNode(bool composed) const {
        auto aux = const_cast<Node *>(this);

        auto travel = NodeTravel(aux);
        Node *node = composed ? travel.shadow_including_root() : travel.root() ;

        return node->shared_from_this();
    }

    sp_Node Node::insertBefore(sp_Node newNode, sp_Node referenceNode) {
        auto res = MutationAlgorithms(this).pre_insert(newNode, std::move(referenceNode));

        if (res) return res->shared_from_this();
        return nullptr;
    }

    std::shared_ptr<Node> Node::removeChild(sp_Node child) {
        return MutationAlgorithms(this).pre_remove(std::move(child));
    }

    bool Node::isEqualNode(const sp_Node node) const {
        if (node) {
            if (nodeType == node->nodeType &&
                nodeName == node->nodeName &&
                childNodes.size() == node->childNodes.size()) {

                auto lEnd = _childNodes.end();
                auto rEnd = node->_childNodes.end();
                auto it1 = _childNodes.begin();
                auto it2 = node->_childNodes.begin();

                while (it1 != lEnd && it2 != rEnd) {
                    if (!((*it1)->isEqualNode(*it2))) {
                        return false;
                    }

                    it1++;
                    it2++;
                }

                return true;
            }
        }

        return false;
    }

    bool Node::isSameNode(const sp_Node node) const {
        return node && node.get() == this;
    }

    sp_Node Node::replaceChild(sp_Node newChild, sp_Node oldChild) {
        return MutationAlgorithms(this).replace(std::move(oldChild), std::move(newChild));
    }

    //the position related from node to this;
    unsigned short int Node::compareDocumentPosition(const DOM_Node::sp_Node node) const {
        unsigned short int res = 0;

        if (this->isSameNode(node)) { return res; }

        if (!node || this->getRootNode() != node->getRootNode()) {
            res |= DOCUMENT_POSITION_DISCONNECTED;
            res |= DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC;
            res |= this <= node.get() ? DOCUMENT_POSITION_FOLLOWING : DOCUMENT_POSITION_PRECEDING;

            return res;
        }

        auto sp_this = std::const_pointer_cast<DOM_Node::Node>(this->shared_from_this());
        if (NodeTravel(node).ancestor(this)) {
            res |= DOCUMENT_POSITION_CONTAINS;
            res |= DOCUMENT_POSITION_PRECEDING;

            return res;
        }

        if (NodeTravel(node).inclusive_descendant(this)) {
            res |= DOCUMENT_POSITION_CONTAINED_BY;
            res |= DOCUMENT_POSITION_FOLLOWING;

            return res;
        }

        auto otherP = NodeTravel(node).pathFromRoot();
        auto thisP = NodeTravel(const_cast<Node *>(this)).pathFromRoot();


        size_t i = 0;
        while (otherP[i] == thisP[i]) { i++; }

        const NodeList & children = otherP[i-1]->_childNodes;

        for (const auto & child : children) {
            DOM_Node::Node *aux = child.get();
            if (aux == otherP[i]) {
                return DOCUMENT_POSITION_PRECEDING;
            }

            if (aux == thisP[i]) {
                return DOCUMENT_POSITION_FOLLOWING;
            }
        }

        return DOCUMENT_POSITION_FOLLOWING;
    }

    sp_Element Node::parentElement() const {
        if (_parentElement) {
            auto *parent = (DOM_Node::Node *)(_parentElement);
            return std::dynamic_pointer_cast<DOM_Node::Element>(parent->shared_from_this());
        } else {
            return nullptr;
        }
    }

    sp_Document Node::ownerDocument() const {
        if (_ownerDocument) {
            return std::dynamic_pointer_cast<DOM_Node::Document>(_ownerDocument->shared_from_this());
        } else {
            return nullptr;
        }
    }

    bool Node::operator==(DOM_Node::Node &rhs) const {
        DOM_Node::sp_Node aux = rhs.shared_from_this();
        return isEqualNode(aux);
    }

    bool Node::isCustom() const {
        return (    nodeName.find('-') != std::string::npos &&
                    nodeName != "annotation-xml" &&
                    nodeName != "color-profile" &&
                    nodeName != "font-face" &&
                    nodeName != "font-face-src" &&
                    nodeName != "font-face-uri" &&
                    nodeName != "font-face-format" &&
                    nodeName != "font-face-name" &&
                    nodeName != "missing-glyph" );
    }


    /*void Node::queue_mutation_record(const std::string &type, const std::string &name, const std::string &oldValue,
                                     const DOM_Node::NodeList &addedNodes, const DOM_Node::NodeList &removedNodes,
                                     DOM_Node::sp_Node previousSibling, DOM_Node::sp_Node nextSibling) {

        MutationRecord record;

        record.type = type;
        record.attributeName = name;
        record.oldValue = oldValue;
        record.addedNodes = addedNodes;
        record.removedNodes = removedNodes;
        record.previousSibling = previousSibling;
        record.nextSibling = nextSibling;
    }*/

//    void Node::queue_mutation_record(MutationRecord &record) {
//        std::unordered_map<MutationObserver, std::string> interestedObservers;
//
//        forEach_inclusive_ancestor([&](Node *node) {
//            for (auto &registered : node->observers) {
//                auto &options = registered.options;
//                auto &attrFilter = options.attributeFilter;
//
//                if (!(node != this && !options.childList) &&
//                    !(record.type == "attributes" && !options.attributes) &&
//                    !(  record.type == "attributes" &&
//                        !options.attributeFilter.empty() &&
//                            (   std::find(attrFilter.begin(), attrFilter.end(), record.attributeName) != attrFilter.end() /*||
//                                todo, (namespaces.size())*/
//                            )
//                     ) &&
//                    !(record.type == "characterData" && !options.characterData) &&
//                    !(record.type == "childList" && !options.childList)) {
//
//                    auto mo = registered.observer;
//
//                    interestedObservers[mo];
//
//                    if ((record.type == "attributes" && options.attributeOldValue) ||
//                        (record.type == "characterData" && options.characterDataOldValue)) {
//
//                        interestedObservers[mo] = record.oldValue;
//                    }
//
//                }
//            }
//        });
//
//        auto end = interestedObservers.end();
//        for (auto it = interestedObservers.begin(); it != end; it++) {
//            MutationRecord record2;
//
//            /*Let record be a new MutationRecord object with its type set to type, target set to target, attributeName
//             * set to name, attributeNamespace set to namespace, oldValue set to mappedOldValue, addedNodes set to
//             * addedNodes, removedNodes set to removedNodes, previousSibling set to previousSibling, and nextSibling
//             * set to nextSibling.*/
//
//            (*it).first.records.push(record2);
//        }
//
//        //todo, Queue a mutation observer microtask.
//    }

}