#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#endif


#include "Shader.hpp"
#include <fstream>
#include <utility>



bool Shader::frameBufferActive = false;
Point Shader::displacementVector;
Point Shader::newRatio;


Shader::Shader(
        GLuint prog,
        Camera *camera_, char numOfBuffers,
        const std::function<void(Shader *)>& init,
        std::function<void(Shader *, void*)> draw
) {
    drawShader = std::move(draw);
    VBOid = new GLuint[numOfBuffers];

    program = prog;
    camera = camera_;

    glUseProgram(program);
    if (numOfBuffers) {
        glGenBuffers(numOfBuffers, VBOid);
    }

    addUniforms();
    addAttributes();
    setCameraUniforms();

    init(this);

    unbind();
}

void Shader::draw(void *element) {
    glUseProgram(program);

    updateCamera();

    if (frameBufferActive) {
        glUniform4f(cameraUniforms.u_new_ratio, displacementVector.x, displacementVector.y, newRatio.x, newRatio.y);
    } else {
        glUniform4f(cameraUniforms.u_new_ratio, 0, 0, 1, 1);
    }

    drawShader(this, element);

    unbind();
}

void Shader::updateCamera() {
    glUniform3fv(cameraUniforms.u_camera_ang, 1, camera->angle);
    glUniform3fv(cameraUniforms.u_camera_pos, 1, camera->position);
    glUniform2fv(cameraUniforms.u_camera_ratio, 1, camera->ratio);
    glUniform4f(cameraUniforms.u_new_ratio, 0, 0, camera->ratio[0], camera->ratio[1]);
}

void Shader::addUniforms() {
    uniforms.clear();
    GLint numUniforms;
    GLint maxNameLenght;
    char *uniformName;

    glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &numUniforms);
    glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxNameLenght);
    uniformName = new char[maxNameLenght];

    for (GLuint i = 0; i < numUniforms; i++) {
        GLint  size;
        GLenum type;
        glGetActiveUniform(program, i, maxNameLenght, NULL, &size, &type, uniformName);

        Uniform uniform;
        uniform.name = uniformName;
        uniform.id = glGetUniformLocation(program, uniformName);

        uniforms.push_back(uniform);
    }

    delete uniformName;
}

void Shader::addAttributes() {
    attributes.clear();
    GLint numAttributes;
    GLint maxNameLenght;
    char *attributeName;

    glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &numAttributes);
    glGetProgramiv(program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxNameLenght);
    attributeName = new char[maxNameLenght];

    for (GLuint i = 0; i < numAttributes; i++) {
        GLint  size;
        GLenum type;
        glGetActiveAttrib(program, i, maxNameLenght, NULL, &size, &type, attributeName);

        Attribute attribute;
        attribute.name = attributeName;
        attribute.id = glGetAttribLocation(program, attributeName);

        attributes.push_back(attribute);
    }

    delete attributeName;
}

void Shader::setCameraUniforms() {
    cameraUniforms.u_camera_ang = glGetUniformLocation(program, "camera_ang");
    cameraUniforms.u_camera_pos = glGetUniformLocation(program, "camera_pos");
    cameraUniforms.u_camera_ratio = glGetUniformLocation(program, "camera_ratio");
    cameraUniforms.u_new_ratio = glGetUniformLocation(program, "camera_new_ratio");
}

void Shader::bindBuffer(GLenum target, char buffer) {
    glBindBuffer(target, VBOid[buffer]);
}

GLint Shader::getAttribute(std::string name) {
    auto end = attributes.end();

    for (auto a = attributes.begin(); a != end; a++) {
        if ((*a).name == name)
            return (*a).id;
    }

    return -1;
}

GLint Shader::getUniform(std::string name) {
    auto end = uniforms.end();

    for (auto u = uniforms.begin(); u != end; u++) {
        if ((*u).name == name)
            return (*u).id;
    }

    return -1;
}

void Shader::unbind() {
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Shader::newFrameBuffer(int x, int y, int width, int height, float xOffset, float yOffset) {

}

void Shader::unbindFrameBuffer() {
    //glBindFramebuffer(GL_FRAMEBUFFER, 0);
    //glViewport(0, 0, viewportWidth, viewportHeight);
}