#ifndef GUINOX_VIEWPORT_HPP
#define GUINOX_VIEWPORT_HPP


#include "../Utils/Point.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include <string>
#include <map>
#include <mutex>
#include <thread>
#include <deque>
#include "../DOM/DOMRect.hpp"
#include "Font.hpp"
#include "Texture.hpp"

class ViewPort {

    typedef struct {
        std::string pair[2];
        GLuint program;
    } Shader_Pair;

    std::vector<DOMRect> scissors;
    bool enabledScissors;

    bool locked = false;
    std::mutex mutex;
    std::thread::id blockingThread;
    std::thread::id goThread;
    std::deque<std::thread::id> waitingThreads;

    std::function<void(void)> blockingContext, freeingContext;
    std::vector<Camera *> cameras;
    std::map<std::string, Shader *> sh_store;
    std::vector<Shader_Pair> pairs;
    std::map<std::string, Texture*> textures;

    double _viewportWidth;
    double _viewportHeight;
    double _xdpi;
    double _ydpi;


    void scissorsClip(const DOMRect & rect);
    static DOMRect scissorsIntersect(const DOMRect & A, const DOMRect & B);


    bool createProgram(std::string sh, std::string fr, GLuint *prog);
    GLuint loadShader(GLenum type, std::string path);
    Camera * createCamera(const std::string & name);
    Point pixel2GLCoord(const Point & p) const ;




public:

    const double & viewportWidth = _viewportWidth;
    const double & viewportHeight = _viewportHeight;
    const double & xdpi = _xdpi;
    const double & ydpi = _ydpi;

    ViewPort() = default;
    ViewPort(std::function<void ()> block, std::function<void ()> free);


    void appendShader(const std::string & name,
                          const std::string & vrtx, const std::string & frg,
                          const std::string & camera, char buffers,
                          const std::function<void (Shader *)>& init,
                          const std::function<void (Shader *, void*)>& draw);

    Camera *getCamera(const std::string & name);

    void draw(const std::string & shader, void * data);

    const float* setVertex(DOMRect & rect) const ;

    void updateSizes(double width, double height);
    void updateResolution(double xdpi, double ydpi);
    void registFont(Font * font);


    void pushScissors(const DOMRect & rect);
    void popScissors();
    inline bool isDisabledScissors() const { return ! enabledScissors; }

    void lockContext();
    void freeContext();


    Texture * addTexture(const std::string &name, int w, int h, int pixels[], GLint format);
    //Texture *updateTexture(int w, int h, int pixels[], GLint format);
    //Texture *getTexture(GLuint handle);
    void renderTexture(const std::string & name, const GLfloat vertex[]);
    void renderTexture(const std::string & name, const GLfloat vertex[], GLfloat tCoord[]);
    void removeTexture(const std::string & name);

};


#endif //GUINOX_VIEWPORT_HPP
