#include "Texture.hpp"
#include "ViewPort.hpp"


std::string Texture::shader = "GUINOX_texture_shader";
GLfloat Texture::texCoord[8] = {
        0,1,
        0,0,
        1,1,
        1,0
};





void Texture::setShader(ViewPort *viewport) {
    viewport->appendShader(shader,
                           "textureShader.vert", "texture.frag", "static", 3,
                           [] (Shader * shader) {
                               static const GLushort indices[] = {0,1,2,3};

                               shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                               glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), 0, GL_STATIC_DRAW);
                               glEnableVertexAttribArray(shader->getAttribute("position"));

                               shader->bindBuffer(GL_ARRAY_BUFFER, 1);
                               glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), 0, GL_STATIC_DRAW);
                               glEnableVertexAttribArray(shader->getAttribute("a_texCoord"));

                               shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 2);
                               glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLushort), indices, GL_STATIC_DRAW);
                           },
                           [] (Shader *shader, void *element) {
                               Texture *texture = (Texture *)element;

                               glUniform2f(shader->getUniform("u_flip"), texture->hflip ? 1 : 0, texture->vflip ? 1 : 0);

                               glActiveTexture(GL_TEXTURE0);
                               glBindTexture(GL_TEXTURE_2D, texture->handle);

                               glUniform1i(shader->getUniform("s_texture"), 0);
                               glUniform1f(shader->getUniform("scale"), texture->scale);

                               glUniform2f(shader->getUniform("pivotPoint"), texture->vertex[0]+((texture->vertex[4]-texture->vertex[0])/2), texture->vertex[3]+((texture->vertex[1]-texture->vertex[3])/2));
                               glUniform4fv(shader->getUniform("u_color"), 1, texture->color.getFloat());

                               // vertex
                               shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                               glBufferSubData(GL_ARRAY_BUFFER, 0, 8 * sizeof(GLfloat), texture->vertex);
                               glVertexAttribPointer(shader->getAttribute("position"), 2, GL_FLOAT, GL_FALSE, 0, 0);

                               // texture coordinates
                               shader->bindBuffer(GL_ARRAY_BUFFER, 1);
                               glBufferSubData(GL_ARRAY_BUFFER, 0, 8 * sizeof(GLfloat), texture->tCoord);
                               glVertexAttribPointer(shader->getAttribute("a_texCoord"), 2, GL_FLOAT, GL_FALSE, 0, 0);
                               ////GL_HALF_FLOAT_OES

                               // indices
                               shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 2);
                               glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, 0);
                           });
}


GLuint Texture::registPixels(int w, int h, int *pixels, GLint format) {
    GLuint handle;

    glGenTextures(1, &handle);
    glBindTexture(GL_TEXTURE_2D, handle);

    /**  @ANDROID
     * In order to cover the 100% of devices, we need to use ARGB_to_RGBA()
     *
     * We could improve performance by just using GL_BGRA_EXT, but this seems to not cover
     * all devices
     */
    /*if (format == GL_BGRA_EXT) {
        ARGB_to_RGBA(w * h, pixels);
        format = GL_RGBA;
    }*/

    ///BGRA because of BITMAP class of java-android give it in that format
    ///glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, pixels);
    glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, pixels);


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


    glBindTexture(GL_TEXTURE_2D, 0);

    return handle;
}



Texture::Texture(ViewPort * viewPort, int w, int h, int *pixels, GLint format) {
    _width = w;
    _height = h;
    color.setColor(0xffffffff);

    _handle = registPixels(w, h, pixels, format);

    setShader(viewPort);
}

Texture::~Texture() {
    glDeleteTextures(1, &handle);
}

Texture * Texture::render(ViewPort * viewport, const GLfloat *vertex_, const GLfloat *tCoord_) {
    if (!visible)
        return this;

    for (char i = 0; i < 8; i++) {
        vertex[i] = vertex_[i];
        tCoord[i] = tCoord_[i];
    }

    viewport->draw(shader, this);

    return this;
}