#ifndef GUINOX_FONT_HPP
#define GUINOX_FONT_HPP


#include <vector>
#include <string>
#include "../../libs/freetype-gl/texture-atlas.h"
#include "../../libs/freetype-gl/texture-font.h"
#include "../DOM/DOMRect.hpp"


class ViewPort;

class Font {
    friend class ViewPort;

    static std::vector<Font*> fonts;

    std::string name;
    std::string _path;


    ftgl::texture_font_t * font;


private:
    Font(ftgl::texture_font_t *font, ftgl::texture_atlas_t *atlas);


public:
    ftgl::texture_atlas_t *atlas;
    const std::string & path = _path;
    const double lineHeight;
    const double baseLine;

    ~Font();

    static Font * getFont(const std::string & path);


    ftgl::texture_glyph_t * getGlyph(char c);
};


#endif //GUINOX_FONT_HPP
