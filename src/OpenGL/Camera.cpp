#include "Camera.hpp"

#include <utility>



Camera::Camera(std::string name_) : _name(std::move(name_)) {

}

Camera* Camera::setAngle(float ang, char axis) {
    for (char i = 0; i < 3; i++) {
        if ((1 << i) & axis)
            angle[i] = ang;
    }

    //keepInCircle();

    return this;
}

Camera* Camera::addAngle(float ang, char axis) {
    for (char i = 0; i < 3; i++) {
        if ((1 << i) & axis)
            angle[i] += ang;
    }

    //keepInCircle();

    return this;
}

Camera* Camera::setPosition(float pos, char axis) {
    for (char i = 0; i < 3; i++) {
        if ((1 << i) & axis)
            position[i] = pos;
    }

    return this;
}

float Camera::getAngle(char axis) {
    switch (axis) {
        case X: return angle[0];
        case Y: return angle[1];
        case Z: return angle[2];
        default: return 0;
    }
}

float Camera::getPosition(char axis) {
    switch (axis) {
        case X: return position[0];
        case Y: return position[1];
        case Z: return position[2];
        default: return 0;
    }
}