attribute vec2 position;
attribute vec2 a_texCoord;
uniform vec4 u_color;
uniform vec2 u_flip;
uniform vec3 camera_ang;
uniform vec3 camera_pos;
uniform vec2 camera_ratio;
uniform vec4 camera_new_ratio;
uniform float scale;
uniform vec2 pivotPoint;
varying vec2 v_textCoord;
varying vec4 v_color;

const int TWO_I = 2;
const int ONE_I = 1;
const int ZERO_I = 0;

vec4 scaleF(in float scale_, in vec2 center, in vec2 position);

void main() {
    vec4 pos = vec4(position, ZERO_I, ONE_I);
    pos.x = (pos.x - camera_new_ratio.x) * camera_new_ratio.z;
    pos.y = (pos.y - camera_new_ratio.y) * camera_new_ratio.w;

    pos = scaleF(scale, pivotPoint, pos.xy);

    gl_Position = pos;

    if (u_flip.x != 0.0) {
        v_textCoord = vec2(a_texCoord.s, a_texCoord.t);
    } else {
        v_textCoord = a_texCoord;
    }

    if (u_flip.y != 0.0) {
        v_textCoord = vec2(a_texCoord.s, 1.0 - a_texCoord.t);
    } else {
        v_textCoord = a_texCoord;
    }

    v_color = u_color;
}

vec4 scaleF(in float scale, in vec2 center, in vec2 position) {
    vec2 tmp = position - center;

    mat4 scaleM = mat4(scale);
    scaleM[3][3] = 1.0;

    vec4 aux = vec4(tmp, 0, 1);

    return vec4(center, 0, 0) + (scaleM * aux);
}