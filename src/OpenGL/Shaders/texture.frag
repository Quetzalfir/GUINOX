varying vec2 v_textCoord;
varying vec4 v_color;
uniform sampler2D s_texture;

void main() {
    gl_FragColor = v_color * texture2D(s_texture, v_textCoord);
}