attribute vec2 a_pos;
attribute vec4 a_color;
uniform float u_scale;
uniform vec3 camera_ang;
uniform vec3 camera_pos;
uniform vec2 camera_ratio;
uniform vec4 camera_new_ratio;
varying vec4 v_color;

void main() {
    vec4 pos = vec4(a_pos, 0, 1);
    pos.x = (pos.x - camera_new_ratio.x) * camera_new_ratio.z;
    pos.y = (pos.y - camera_new_ratio.y) * camera_new_ratio.w;

    gl_Position = pos;
    v_color = a_color;
}
