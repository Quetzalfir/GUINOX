attribute vec2 position;
attribute vec2 a_texCoord;
uniform vec4 u_color;
uniform vec3 camera_ang;
uniform vec3 camera_pos;
uniform vec2 camera_ratio;
uniform vec4 camera_new_ratio;
varying vec2 v_textCoord;
varying vec4 v_color;

const int TWO_I = 2;
const int ONE_I = 1;
const int ZERO_I = 0;


void main() {
    vec4 pos = vec4(position, ZERO_I, ONE_I);
    pos.x = (pos.x - camera_new_ratio.x) * camera_new_ratio.z;
    pos.y = (pos.y - camera_new_ratio.y) * camera_new_ratio.w;

    gl_Position = pos;

    v_textCoord = a_texCoord;
    v_color = u_color;
}