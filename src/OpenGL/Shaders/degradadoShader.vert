precision highp float;
precision highp int;

attribute vec2 a_radioAngulo;
attribute vec4 a_color;
uniform float scale;
uniform vec3 camera_ang;
uniform vec3 camera_pos;
uniform vec2 camera_ratio;
uniform vec4 camera_new_ratio;
varying vec4 v_color;

const int THREE_I = 3;
const int TWO_I = 2;
const int ONE_I = 1;
const int ZERO_I = 0;

mat4 translation(in vec3 delta);

void main() {
    mat4 scaleM = mat4(1);
    scaleM[ZERO_I][ZERO_I] = scale;
    scaleM[ONE_I][ONE_I] = scale;

    float x = a_radioAngulo.x * cos(radians(a_radioAngulo.y));
    float y = a_radioAngulo.x * sin(radians(a_radioAngulo.y));

    vec4 pos = vec4(x, y, 0, 1);
    pos.x = (pos.x - camera_new_ratio.x) * camera_new_ratio.z;
    pos.y = (pos.y - camera_new_ratio.y) * camera_new_ratio.w;

    gl_Position = translation(camera_pos) * scaleM * pos;
    v_color = a_color;
}

mat4 translation(in vec3 delta) {
    mat4 m = mat4(ONE_I);

    m[THREE_I][ZERO_I] = delta.x;
    m[THREE_I][ONE_I] = delta.y;
    m[THREE_I][TWO_I] = delta.z;

    return m;
}