varying vec2 v_textCoord;
varying vec4 v_color;
uniform sampler2D s_texture;
uniform float outlineDistance;
uniform float smoothing;

void main() {
    float distance = texture2D(s_texture, v_textCoord).r;
    float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    gl_FragColor = vec4(v_color.rgb, v_color.a * alpha);
}
/**
 *
 * float dist = texture2D(u_texture, gl_TexCoord[0].st).r;
    float width = fwidth(dist);
    float alpha = smoothstep(0.5-width, 0.5+width, dist);
    gl_FragColor = vec4(gl_Color.rgb, alpha*gl_Color.a);
 */
