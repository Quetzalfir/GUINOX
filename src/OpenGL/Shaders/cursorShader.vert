precision highp float;
precision highp int;

attribute vec2 position;
uniform vec4 u_color;
uniform float u_angulo;
uniform float scale;
uniform vec3 camera_ang;
uniform vec3 camera_pos;
uniform vec2 camera_ratio;
uniform vec4 camera_new_ratio;
varying vec4 v_color;

mat4 translacion = mat4(1);

// cada literal crea una variable nueva
const int THREE_I = 3;
const int TWO_I = 2;
const int ONE_I = 1;
const int ZERO_I = 0;
const float depth = 60.0;

mat4 rotateAngle(in vec3 angle);
mat4 translation(in vec3 delta);

void main() {
    vec4 pos = vec4(position, 0, 1);
    pos.x = (pos.x - camera_new_ratio.x) * camera_new_ratio.z;
    pos.y = (pos.y - camera_new_ratio.y) * camera_new_ratio.w;

    v_color = u_color;
    mat4 ratio = mat4(1);
    mat4 scaleM = mat4(scale);
    scaleM[THREE_I][THREE_I] = 1.0;
    ratio[ZERO_I][ZERO_I] = camera_ratio.y/camera_ratio.x;
    translacion[THREE_I][ONE_I] = .20;

    vec4 tmp = scaleM * ratio * rotateAngle(camera_ang) * rotateAngle(vec3(0,0,u_angulo - 90.0)) * translacion * pos;

    tmp.z = tmp.z/depth;
    tmp.y = tmp.y * (tmp.z+1.0);
    tmp.x = tmp.x * (tmp.z+1.0);

    gl_Position = tmp;
}

mat4 rotateAngle(in vec3 angle) {
    mat4 mx = mat4(ONE_I);
    mat4 my = mat4(ONE_I);
    mat4 mz = mat4(ONE_I);

    mx[ONE_I][ONE_I] = cos(radians(angle.x));
    mx[ONE_I][TWO_I] = sin(radians(angle.x));
    mx[TWO_I][ONE_I] = -mx[ONE_I][TWO_I];
    mx[TWO_I][TWO_I] = mx[ONE_I][ONE_I];

    my[ZERO_I][ZERO_I] = cos(radians(angle.y));
    my[ZERO_I][TWO_I] = -sin(radians(angle.y));
    my[TWO_I][ZERO_I] = -my[ZERO_I][TWO_I];
    my[TWO_I][TWO_I] = my[ZERO_I][ZERO_I];

    mz[ZERO_I][ZERO_I] = cos(radians(angle.z));
    mz[ZERO_I][ONE_I] = sin(radians(angle.z));
    mz[ONE_I][ZERO_I] = -mz[ZERO_I][ONE_I];
    mz[ONE_I][ONE_I] = mz[ZERO_I][ZERO_I];

    return mx * my * mz;
}

mat4 translation(in vec3 delta) {
    mat4 m = mat4(ONE_I);

    m[THREE_I][ZERO_I] = delta.x;
    m[THREE_I][ONE_I] = delta.y;
    m[THREE_I][TWO_I] = delta.z;

    return m;
}
