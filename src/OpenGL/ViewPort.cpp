#include <glad/gl.h>
#define GLFW_INCLUDE_NONE

#include <fstream>
#include <iostream>
#include "ViewPort.hpp"
#include <cmath>
#include <algorithm>


void ViewPort::scissorsClip(const DOMRect &rect) {
    glScissor(rect.left, viewportHeight - rect.bottom, rect.width(), rect.height());
    glEnable(GL_SCISSOR_TEST);
    enabledScissors = true;
}

DOMRect ViewPort::scissorsIntersect(const DOMRect &A, const DOMRect &B) {
    DOMRect res;

    res.left = std::max(A.left, B.left);
    res.right = std::min(A.right, B.right);
    if (res.left > res.right) { res.right = res.left; }

    res.top = std::max(A.top, B.top);
    res.bottom = std::min(A.bottom, B.bottom);
    if (res.top > res.bottom) { res.bottom = res.top; }

    return res;
}

// regresa false si la funcion no creo nada, y true si creo el programa
bool ViewPort::createProgram(std::string vtx, std::string frg, GLuint *prog) {
    *prog = 0;

    if (!pairs.empty()) {
        auto end = pairs.end();

        for (auto p = pairs.begin(); p != end; p++) {
            if ((*p).pair[0] == vtx && (*p).pair[1] == frg) {
                *prog = (*p).program;
                return false;
            }
        }
    }

    Shader_Pair actual;
    actual.pair[0] = vtx;
    actual.pair[1] = frg;

    const char* path = "Shaders/";
    vtx.insert(0, path);
    frg.insert(0, path);

    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, vtx);
    if (!vertexShader) return false;

    GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, frg);
    if(!pixelShader) return false;

    actual.program = glCreateProgram();
    if (actual.program) {
        //printf("program = %d", actual.program);
        glAttachShader(actual.program, vertexShader);
        glAttachShader(actual.program, pixelShader);

        glLinkProgram(actual.program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(actual.program, GL_LINK_STATUS, &linkStatus);
        //printf("link = %d", linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(actual.program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char *buf = new char[bufLength];
                glGetProgramInfoLog(actual.program, bufLength, NULL, buf);
                printf("Could not link program:\n%s\n", buf);
                delete [] buf;
            }
            glDeleteProgram(actual.program);
            printf("Error al compilar shaders");

            return false;
        }
    } else {
        GLenum err;
        err = glGetError();
        printf("program = 0");
        printf(" err return: %d \n", err);
    }

    pairs.push_back(actual);
    *prog = actual.program;
    return true;
}

GLuint ViewPort::loadShader(GLenum type, std::string path) {
    GLuint shader = glCreateShader(type);
    ///checkGlError("OpenGLMng: glCreateShader");
    if (shader) {
        std::ifstream file(path, std::ifstream::in);
        if (!file.is_open())
            printf("Asset in %s, can't be opened", path.c_str());

        std::string buff((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
        const char *buff2 = buff.data();

        glShaderSource(shader, 1, &buff2, NULL);
        glCompileShader(shader);

        // todo, almacena variables que se repiten, uniforms y attributes
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char *buf = new char[infoLen];
                glGetShaderInfoLog(shader, infoLen, NULL, buf);
                printf("Could not compile shader %d:\n%s\n", type, buf);
                glDeleteShader(shader);
                shader = 0;
                delete [] buf;
            }
        }

        file.close();
    }

    return shader;
}

Camera * ViewPort::createCamera(const std::string &name) {
    for (auto & cam : cameras) {
        if (cam->name == name)
            return cam;
    }

    auto * camera = new Camera(name);
    camera->ratio[0] = viewportWidth;
    camera->ratio[1] = viewportHeight;

    cameras.push_back(camera);

    return camera;
}

Point ViewPort::pixel2GLCoord(const Point & p) const {
    Point res;

    res.x = (2*p.x/viewportWidth) - 1;
    res.y = 1 - (2*p.y/viewportHeight);

    return res;
}


const float* ViewPort::setVertex(DOMRect & rect) const {
    Point p1 = pixel2GLCoord({rect.left, rect.top});
    Point p2 = pixel2GLCoord({rect.right, rect.bottom});

    rect.vertex[0] = p1.x; rect.vertex[1] = p1.y;
    rect.vertex[2] = p1.x; rect.vertex[3] = p2.y;
    rect.vertex[4] = p2.x; rect.vertex[5] = p1.y;
    rect.vertex[6] = p2.x; rect.vertex[7] = p2.y;

    return rect.vertex;
}

ViewPort::ViewPort(
        std::function<void ()> block,
        std::function<void ()> free) {

    blockingContext = block;
    freeingContext = free;
}

void
ViewPort::appendShader(
        const std::string & name,
        const std::string & vrtx, const std::string & frg,
        const std::string & cameraName, char buffers,
        const std::function<void (Shader* )>& init,
        const std::function<void (Shader*, void *)>& draw
        ) {

    auto it = sh_store.find(name);
    if (it == sh_store.end()) {
        lockContext();

        GLuint program;
        if (createProgram(vrtx, frg, &program)) {
            auto * camera = createCamera(cameraName);
            auto * shader = new Shader(program, camera, buffers, init, draw);

            sh_store[name] = shader;

        }

        freeContext();
    }
}

Camera * ViewPort::getCamera(const std::string &name) {
    for (auto & cam : cameras) {
        if (cam->name == name)
            return cam;
    }

    return nullptr;
}

void ViewPort::draw(const std::string &shader, void *data) {
    auto it = sh_store.find(shader);
    if (it != sh_store.end()) {
        (*it).second->draw(data);
    }
}

void ViewPort::updateSizes(double width, double height) {
    _viewportWidth = width;
    _viewportHeight = height;

    for (auto & cam : cameras) {
        cam->ratio[0] = width;
        cam->ratio[1] = height;
    }
}

void ViewPort::updateResolution(double xdpi, double ydpi) {
    _xdpi = xdpi;
    _ydpi = ydpi;
}

void ViewPort::pushScissors(const DOMRect &rect) {
    DOMRect intersected;

    if (!scissors.empty()) {
        intersected = scissorsIntersect(scissors.back(), rect);
    } else {
        intersected = rect;
    }

    scissors.push_back(intersected);
    scissorsClip(intersected);
}

void ViewPort::popScissors() {
    if (!scissors.empty()) {
        scissors.pop_back();

        if (!scissors.empty()) {
            scissorsClip(scissors.back());
        } else {
            glDisable(GL_SCISSOR_TEST);
            enabledScissors = false;
        }
    }
}

void ViewPort::lockContext() {
    auto currentThread = std::this_thread::get_id();


    if (locked) {

        if (blockingThread == currentThread) {
            return;

        } else {
            waitingThreads.push_back(currentThread);
            while (goThread != currentThread) ;
            waitingThreads.pop_front();
        }


    } else {
        if (!waitingThreads.empty()) {
            waitingThreads.push_back(currentThread);
            while (goThread != currentThread) ;
            waitingThreads.pop_front();
        }
    }

    while (!(mutex.try_lock())) {
        waitingThreads.push_back(currentThread);
        while (goThread != currentThread) ;
        waitingThreads.pop_front();
    }

    goThread = currentThread;

    locked = true;
    blockingThread = std::this_thread::get_id();

    blockingContext();
}

void ViewPort::freeContext() {
    if (!locked) return;
    if (std::this_thread::get_id() != blockingThread) return;

    freeingContext();

    mutex.unlock();

    locked = false;

    if (!waitingThreads.empty()) {
        goThread = waitingThreads.front();
    }
}

Texture * ViewPort::addTexture(const std::string & name, int w, int h, int *pixels, GLint format) {
    auto it = textures.find(name);
    if (it == textures.end()) {
        lockContext();
        auto texture = new Texture(this, w, h, pixels, format);
        freeContext();

        textures[name] = texture;

        return texture;
    }

    return it->second;
}

void ViewPort::renderTexture(const std::string &name, const GLfloat *vertex) {
    auto it = textures.find(name);
    if (it != textures.end()) {
        it->second->render(this, vertex);
    }
}

void ViewPort::renderTexture(const std::string &name, const GLfloat *vertex, GLfloat *tCoord) {
    auto it = textures.find(name);
    if (it != textures.end()) {
        (*it).second->render(this, vertex, tCoord);
    }
}

//todo, lento, arreglar con smartpointers
void ViewPort::removeTexture(const std::string &name) {
    auto it = textures.find(name);
    if (it != textures.end()) {
        lockContext();
        delete (*it).second;
        freeContext();

        textures.erase(it);
    }
}