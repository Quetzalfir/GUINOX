#include "Font.hpp"
#include "ViewPort.hpp"

static const int ATLAS_HEIGHT = 720;
static const int ATLAS_WIDTH = 700;
static const char * ASCII = " !\"#$%&'()*+,-./0123456789:;<=>?"
                            "@ABCDEñúüFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
                            "`abcdefghijklmnopqrstuvwxyz{|}~"
                            "ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜø£Ø×ƒ"
                            "áíóúñÑªº¿®¬½¼¡«»░▒▓│┤ÁÂÀ©╣║╗╝¢¥┐"
                            "└┴┬├─┼ãÃ╚╔╩╦╠═╬¤ðÐÊËÈıÍÎÏ┘┌█▄¦Ì▀"
                            "ÓßÔÒõÕµþÞÚÛÙýÝ¯´≡±‗¾¶§÷¸°¨·¹³²■";



std::vector<Font*> Font::fonts;



Font::Font(ftgl::texture_font_t *font_, ftgl::texture_atlas_t *atlas_) :
            lineHeight(font_->height + font_->linegap),
            baseLine(font_->linegap - font_->descender) {

    font = font_;
    atlas = atlas_;
    font->rendermode = ftgl::RENDER_SIGNED_DISTANCE_FIELD;

    texture_font_load_glyphs(font, ASCII);
}

Font::~Font() {
    texture_font_delete( font );
    //todo need context
    //glDeleteTextures( 1, &atlas->id );
    atlas->id = 0;
    texture_atlas_delete( atlas );

    auto end = fonts.end();
    for (auto it = fonts.begin(); it != end; it++) {
        if ((*it) == this) {
            fonts.erase(it);
            break;
        }
    }
}

Font * Font::getFont(const std::string &path) {
    for (auto & font : fonts) {
        if (font->_path == path)
            return font;
    }

    ftgl::texture_atlas_t *atlas = ftgl::texture_atlas_new( ATLAS_WIDTH, ATLAS_HEIGHT, 1 );
    ftgl::texture_font_t *font = texture_font_new_from_file( atlas, 65, path.data() );

    Font *aux = nullptr;

    if (font) {
        aux = new Font(font, atlas);
        aux->_path = path;

        fonts.push_back(aux);
    }

    return aux;
}

ftgl::texture_glyph_t * Font::getGlyph(char c) {
    if (!c) return nullptr;

    ftgl::texture_glyph_t *glyph = texture_font_find_glyph( font, &c );

    if (!glyph)
        glyph = texture_font_find_glyph( font, "█" );

    return glyph;
}