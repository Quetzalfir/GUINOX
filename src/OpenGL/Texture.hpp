#ifndef GUINOX_TEXTURE_HPP
#define GUINOX_TEXTURE_HPP

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#endif

#include "Shader.hpp"
#include "../Utils/Color.hpp"

class ViewPort;


class Texture {
    friend class ViewPort;



    static GLfloat texCoord[8];
    static std::string shader;
    static GLuint registPixels(int width, int height, int pixels[], GLint format);



    bool vflip = true;   // in android images are flipped
    bool hflip = false;
    GLuint _handle;
    int _width;
    int _height;
    float scale = 1;
    bool _visible = true;

    GLfloat vertex[8];
    GLfloat tCoord[8];


    void setShader(ViewPort *viewport);


protected:
    Texture(ViewPort * viewPort, int w, int h, int pixels[], GLint format);
    virtual ~Texture();


public:
    enum FLIP_MODE : char {
        VERTICAL,
        HORIZONTAL
    };


    const GLuint & handle = _handle;
    const int & width = _width;
    const int & height = _height;
    const bool & visible = _visible;


    //static void loadTextures();
    //static void ARGB_to_RGBA(int length, int pixel[]);
    //static void flipTexture(int w, int h, int pixel[]);
    //static Texture *addTexture(int w, int h, int pixels[], const std::string & name, const std::string & type, GLint format);
    //static Texture *updateTexture(int w, int h, int pixels[], const std::string & name, const std::string & type, GLint format);
    //static Texture *getTexture(const std::string & name, const std::string & type);
    //static void deleteType(const std::string & type);
    //static void remove(const std::string & name, const std::string & type);



    Color color;


    double getRatio() const { return ((double)width)/height;}
    void setScale(float scale_) {scale = scale_;}

    Texture *render(ViewPort * viewport, const GLfloat vertex[], const GLfloat tCoord[] = texCoord);
    //void updatePixels(int width, int height, int* pixels, GLint format);
    //void erase();

    Texture *flip(FLIP_MODE mode);
};

#endif //GUINOX_TEXTURE_HPP
