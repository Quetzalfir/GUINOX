#ifndef GUINOX_SHADER_HPP
#define GUINOX_SHADER_HPP



#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#endif


#include <string>
#include <vector>
#include <functional>
#include "Camera.hpp"
#include "../Utils/Point.hpp"


class Shader {
    friend class FrameBuffer;

    typedef struct {
        std::string name;
        GLint id;
    } Attribute, Uniform;

    typedef struct {
        GLint u_camera_ang;
        GLint u_camera_pos;
        GLint u_camera_ratio;
        GLint u_new_ratio;
    } Camera_Uniforms;

    static bool frameBufferActive;
    static Point displacementVector, newRatio;

    GLuint program;
    GLuint *VBOid = nullptr;
    Camera_Uniforms cameraUniforms;
    Camera *camera;
    std::function<void (Shader *, void *)> drawShader;
    std::vector<Attribute> attributes;
    std::vector<Uniform> uniforms;

    void addUniforms();
    void addAttributes();
    void setCameraUniforms();
    void updateCamera();
    static void unbind();

public:
    Shader( GLuint program,
            Camera *camera, char buffers,
            const std::function<void (Shader * shader)>& init,
            std::function<void (Shader*, void*)> draw
    );
    ~Shader() { delete VBOid; }

    static void newFrameBuffer(int x, int y, int width, int height, float xOffset, float yOffset);
    static void unbindFrameBuffer();

    GLint getAttribute(std::string name);
    GLint getUniform(std::string name);
    void bindBuffer(GLenum target, char buffer);
    void draw(void *element);

};


#endif //GUINOX_SHADER_HPP
