#ifndef GUINOX_CAMERA_HPP
#define GUINOX_CAMERA_HPP

#include <vector>
#include <string>


class Camera {
    friend class ViewPort;
    friend class Shader;


    std::string _name;



    Camera(std::string name);

protected:

    /*void keepInCircle() {
        for (int i = 0; i < 3; i++) {
            while (angle[i] >= 360)
                angle[i] -= 360;
            while (angle[i] <= -360)
                angle[i] += 360;
        }
    }*/

public:

    const std::string & name = _name;
    float ratio[2] = {1,1};
    float angle[3] = {0};
    float position[3] = {0};

    enum AXIS : char { X=1, Y=2, Z=4 };

    Camera *setRatio(float width, float height) {ratio[0] = width; ratio[1] = height; return this;}
    Camera *setAngle(float ang, char axis);
    Camera *addAngle(float ang, char axis);
    Camera *setPosition(float pos, char axis);
    float getAngle(char axis);
    float getPosition(char axis);
};


#endif //GUINOX_CAMERA_HPP
