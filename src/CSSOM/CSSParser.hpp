/**
* @brief Returns style sheet with all the rules it found.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
*/

#ifndef GUINOX_CSSPARSER_HPP
#define GUINOX_CSSPARSER_HPP

#include <string>
#include "css_declarations.hpp"
#include "../Utils/Parser.hpp"


class CSSParser {

public:

    static sp_CSSStyleSheet parseFromFile(const std::string & path);
    static sp_CSSStyleSheet parse(const std::string & css, const std::string & documentPath = "");
};


#endif //GUINOX_CSSPARSER_HPP
