/**
* @brief The ElementCSSInlineStyle mixin provides access to inline style properties of an element.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
* @details Is based on https://drafts.csswg.org/cssom-1/#the-elementcssinlinestyle-mixin
*/

#ifndef GUINOX_ELEMENTCSSINLINESTYLE_HPP
#define GUINOX_ELEMENTCSSINLINESTYLE_HPP

#include "CSSStyleDeclaration.hpp"

class ElementCSSInlineStyle {

public:
    CSSStyleDeclaration style;
};

#endif //GUINOX_ELEMENTCSSINLINESTYLE_HPP
