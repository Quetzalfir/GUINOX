#include "CSSSelector.hpp"
#include <Utils/Parser.hpp>
#include <Utils/Utils.hpp>
#include <DOM/Element.hpp>
#include <algorithm>
#include <DOM/NodeTravel.hpp>
#include <DOM/Document.hpp>
#include <DOM/Text.hpp>
#include <regex>
#include <utility>


static void processList(std::forward_list<DOM_Node::sp_Element> &list,
                        const std::function<DOM_Node::HTMLCollection(const DOM_Node::sp_Element &)>& process) {

    std::vector<DOM_Node::sp_Element> res;

    for (auto & element : list) {
        auto vec = process(element);
        res.insert(res.end(), vec.begin(), vec.end());
    }

    list.clear();
    list.insert_after(list.before_begin(), res.begin(), res.end());
}

static std::string getAttrValue(Parser &parser) {
    parser.skipSpaces();

    if (parser.charIsOneOf("\'\"")) {
        return parser.getInsideQuotes();
    } else {
        return parser.getUntilEnd();
    }
}

static void make_unique(DOM_Node::HTMLCollection & collection) {
    std::sort( collection.begin(), collection.end() );
    collection.erase( std::unique( collection.begin(), collection.end() ), collection.end() );
}

static void make_unique(std::forward_list<DOM_Node::sp_Element> & list) {
    list.sort();
    list.unique();
}



DOM_Node::HTMLCollection CSSSelector::select(const std::string &query, DOM_Node::sp_Element root) {
    auto selectors = Utils::split_string_by(query, ',');
    DOM_Node::HTMLCollection elements;

    for (const auto & selector : selectors) {
        auto rules = getRules(selector);

        auto res = search(rules, {root}, false);

        elements.insert(elements.end(), res.begin(), res.end());
    }

    make_unique(elements);

    return elements;
}

bool CSSSelector::match(const std::string &selector, DOM_Node::sp_Element element) {
    DOM_Node::HTMLCollection elements = { std::move(element) };
    auto rules = getRules(selector);

    auto rend = rules.rend();
    for (auto it = rules.rbegin(); it != rend && !elements.empty(); it++) {

        switch ((*it).token) {
            case INSIDE: {
                DOM_Node::HTMLCollection ancesters;
                for (auto & elem : elements) {
                    auto parent = elem->parentElement();

                    while (parent) {
                        ancesters.push_front(parent);
                        parent = parent->parentElement();
                    }
                }

                make_unique(ancesters);
                elements = ancesters;

                break;
            }

            case DIRECT_CHILD: {
                DOM_Node::HTMLCollection parents;
                for (auto & elem : elements) {
                    auto parent = elem->parentElement();
                    if (parent) {
                        parents.push_back(parent);
                    }
                }

                make_unique(parents);
                elements = parents;

                break;
            }

            case DIRECT_AFTER: {
                DOM_Node::HTMLCollection siblings;
                for (auto & elem : elements) {
                    auto previous = elem->previousElementSibling();
                    if (previous) {
                        siblings.push_back(previous);
                    }
                }

                make_unique(siblings);
                elements = siblings;

                break;
            }

            case AFTER: {
                DOM_Node::HTMLCollection siblings;
                for (auto & elem : elements) {
                    auto sibling = elem->previousElementSibling();

                    while (sibling) {
                        siblings.push_front(sibling);
                        sibling = sibling->previousElementSibling();
                    }
                }

                make_unique(siblings);
                elements = siblings;
                break;
            }

            default: {
                elements = search({(*it)}, elements, true);
            }
        }
    }

    return !elements.empty();
}

unsigned int  CSSSelector::getSpecificity(const std::string &selector) {
    auto selectors = Utils::split_string_by(selector, ',');

    if (selectors.empty()) return 0;

    auto rules = getRules(selectors[0]); // just the first selector
    unsigned int specificity = 0;

    for (const auto & rule : rules) {
        if (rule.token == ID) specificity += 0x100;
        else if (rule.token == ATTR || rule.token == CLASS || rule.token == PSEUDO_CLASS) specificity += 0x10;
        else if (rule.token != ALL) specificity += 0x1;
    }

    return specificity;
}

std::vector<CSSSelector::Rule> CSSSelector::getRules(const std::string & selector) {
    std::vector<CSSSelector::Rule> rules;
    Parser parser(selector);

    parser.skipBlanks();
    while (parser) {
        Rule rule;

        Analyze:
        switch (parser.getChar()) {
            case '#' : //ID
                parser++;
                rule.token = ID;
                rule.value = parser.getUntilFirstOf(" #.*[>+~:");
                break;

            case '.': //CLASS
                parser++;
                rule.token = CLASS;
                rule.value = parser.getUntilFirstOf("#.*[>+~:");
                break;

            case '*': //ALL
                parser++;
                rule.token = ALL;
                break;

            case '[': //ATTR
                parser++;
                rule.token = ATTR;
                rule.value = parser.getUntilFirstOf("]");
                parser++;
                break;

            case ' ': //DESCENDANT
                parser.skipSpaces();
                if (parser.charIsOneOf(">+~")) {
                    goto Analyze;
                } else {
                    rule.token = INSIDE;
                }
                break;

            case '>': //DIRECT CHILD
                parser++;
                rule.token = DIRECT_CHILD;
                parser.skipSpaces();
                break;

            case '+': //NEXT-SIBLING
                parser++;
                rule.token = DIRECT_AFTER;
                parser.skipSpaces();
                break;

            case '~'://SUBSEQUENT-SIBLING
                parser++;
                rule.token = AFTER;
                parser.skipSpaces();
                break;

            case ':': //PSEUDOs
                parser++;

                if (parser.getChar() == ':') { // PSEUDO ELEMENT
                    parser++;
                    parser.skipSpaces();
                    rule.token = PSEUDO_ELEMENT;

                } else { //PSEUDO CLASS
                    rule.token = PSEUDO_CLASS;

                }

                rule.value = parser.getUntilFirstOf(" #.*[>+~:(");

                if (parser.getChar() == '(') {
                    rule.value += '(' + parser.getInsideParentheses() + ')';
                }

                break;

            default: //TAG
                rule.token = TAG;
                rule.value = parser.getUntilFirstOf(" #.*[>+~:");
                break;
        }

        rules.push_back(rule);
    }

    return rules;
}

DOM_Node::HTMLCollection CSSSelector::search(const std::vector<Rule> &rules, DOM_Node::HTMLCollection collection, bool filter) {
    bool inside = !filter;

    //init the list with the collection
    std::forward_list<DOM_Node::sp_Element> list;
    auto rend = collection.rend();
    for (auto it = collection.rbegin(); it != rend; it++) {
        list.push_front((*it));
    }


    for (const Rule & rule : rules) {
        if (list.empty()) break;

        switch (rule.token) {
            case ID:
                searchByID(list, rule.value, inside);
                inside = false;
                break;

            case CLASS:
                searchByClass(list, rule.value, inside);
                inside = false;
                break;

            case TAG:
                searchBy(list, inside, [&](const DOM_Node::sp_Element & element) -> bool {
                    return element->tagName == rule.value;
                });
                inside = false;
                break;

            case ATTR:
                searchByAttr(list, rule.value, inside, false);
                inside = false;
                break;

            case DIRECT_CHILD: //> CHILD COMBINATOR
                searchByDirectChildren(list);
                inside = false;
                break;

            case DIRECT_AFTER: //+ NEXT-SIBLING COMBINATOR
                searchByDirectAfter(list);
                inside = false;
                break;

            case AFTER: //~ SUBSEQUENT-SIBLING COMBINATOR
                searchByAfter(list);
                inside = false;
                break;

            case PSEUDO_ELEMENT:
                searchByPseudoElement(list, rule.value);
                inside = false;
                break;

            case PSEUDO_CLASS:
                searchByPseudoClass(list, rule.value, inside, false);
                inside = false;
                break;

            case INSIDE:
                inside = true;
                break;

            case ALL:
                searchBy(list, inside, [&](const DOM_Node::sp_Element & element) -> bool {
                    return true;
                });
                inside = false;

            default:
                break;
        }

        make_unique(list);
    }

    DOM_Node::HTMLCollection elements;

    auto end = list.end();
    for (auto it = list.begin(); it != end; it++) {
        elements.push_back(*it);
    }

    return elements;
}

void CSSSelector::searchBy(std::forward_list<DOM_Node::sp_Element> & list, bool inside,
                         const std::function<bool(const DOM_Node::sp_Element &)> & fn) {

    if (inside) {
        processList(list , [&]( const DOM_Node::sp_Element& element) -> DOM_Node::HTMLCollection {
            return element->getElementsBy(fn);
        });

    } else {
        list.remove_if([&](const DOM_Node::sp_Element & element) -> bool {
            return !(fn(element));
        });
    }
}

void CSSSelector::searchByID(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value, bool inside) {
    if (inside) {
        DOM_Node::sp_Element res = nullptr;

        for (auto &element : list) {
            res = element->getElementById(value);

            if (res) {
                break;
            }
        }

        list.clear();

        if (res) {
            list.push_front(res);
        }

    } else {
        list.remove_if([&](DOM_Node::sp_Element & element) -> bool {
            return element->id != value;
        });
    }
}

void CSSSelector::searchByClass(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value, bool inside) {
    if (inside) {
        processList(list , [&](const DOM_Node::sp_Element& element) -> DOM_Node::HTMLCollection {
            return element->getElementsByClassName(value);
        });

    } else { // filter
        list.remove_if([&](const DOM_Node::sp_Element& element) -> bool {
            return !element->classList.contains(value);
        });
    }
}

void CSSSelector::searchByAttr(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value, bool inside, bool no) {
    Parser parser(value);

    std::string attrName = parser.getUntilFirstOf(" =~|^$*");
    parser.skipSpaces();

    switch (parser.getChar()) {
        case '=': { //ATTR WITH VAL
            parser++;
            std::string attrValue = getAttrValue(parser);

            searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                if (no) {
                    return element->getAttribute(attrName) != attrValue;
                } else {
                    return element->getAttribute(attrName) == attrValue;
                }
            });

            break;
        }

        case '~': { //ATTR WITH VAL LIKE A LIST OF WORDS
            parser += 2;
            std::string attrValue = getAttrValue(parser);

            searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                std::vector<std::string> words = Utils::split_string_by(element->getAttribute(attrName), ' ');

                for (const auto &str : words) {
                    if (str == attrValue) {
                        return !no;
                    }
                }

                return no;
            });

            break;
        }

        case '|': {
            parser += 2;
            std::string attrValue = getAttrValue(parser);

            searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                const std::string &aux = element->getAttribute(attrName);
                Parser parser(aux);

                bool res = parser.getUntilFirstOf("-") == attrValue;
                return no == !res;
            });
            break;
        }

        case '^': {
            parser += 2;
            std::string attrValue = getAttrValue(parser);

            searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                const std::string &aux = element->getAttribute(attrName);

                bool res = aux.substr(0, attrValue.length()) == attrValue;
                return no == !res;
            });
            break;
        }

        case '$': {
            parser += 2;
            std::string attrValue = getAttrValue(parser);
            int attrValueL = attrValue.length();

            searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                const std::string &aux = element->getAttribute(attrName);
                int auxL = aux.length();
                bool res = false;
                if (auxL >= attrValueL) {
                    res = aux.substr(auxL - attrValueL, std::string::npos) == attrValue;
                }

                return no == !res;
            });
            break;
        }

        case '*': {
            parser += 2;
            std::string attrValue = getAttrValue(parser);

            searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                const std::string &aux = element->getAttribute(attrName);
                bool res = aux.find(attrValue) != std::string::npos;
                return no == !res;
            });
            break;
        }

        default : //ATTR NAME
            searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                bool res = element->hasAttribute(attrName);
                return no == !res;
            });
            break;
    }
}

void CSSSelector::searchByDirectChildren(std::forward_list<DOM_Node::sp_Element> &list) {
    processList(list , [&](const DOM_Node::sp_Element& element) -> DOM_Node::HTMLCollection {
        return element->children;
    });
}

void CSSSelector::searchByDirectAfter(std::forward_list<DOM_Node::sp_Element> &list) {
    processList(list,[&](const DOM_Node::sp_Element & element) -> DOM_Node::HTMLCollection {
        DOM_Node::HTMLCollection collection = { element->nextElementSibling() };
        return collection;
    });
}

void CSSSelector::searchByAfter(std::forward_list<DOM_Node::sp_Element> &list) {
    processList(list,[&](const DOM_Node::sp_Element & element) -> DOM_Node::HTMLCollection {
        DOM_Node::HTMLCollection collection;

        auto next = element->nextElementSibling();
        while (next) {
            collection.push_back(next);
            next = next->nextElementSibling();
        }

        return collection;
    });
}

void CSSSelector::searchByPseudoElement(std::forward_list<DOM_Node::sp_Element> &list, const std::string & value) {

    if (value == "after") {
        processList(list, [&](const DOM_Node::sp_Element & element) -> DOM_Node::HTMLCollection {
            DOM_Node::sp_Text content = element->ownerDocument()->createTextNode("");
            element->appendChild(content);

            DOM_Node::HTMLCollection collection;
            //collection.push_back(content);
            //todo, create Pseudo Element class

            return collection;
        });

    } else if (value == "before") {


    } else if (value == "") {


    }

    //todo, temp none element is selected
    list.clear();
}

void CSSSelector::search_first_of_type(std::forward_list<DOM_Node::sp_Element> &list, bool inside, bool no) {
    searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
        auto aux = element->previousElementSibling();

        while (aux && aux->tagName != element->tagName) {
            aux = aux->previousElementSibling();
        }

        return (no == (aux != nullptr));
    });
}

void CSSSelector::search_last_of_type(std::forward_list<DOM_Node::sp_Element> &list, bool inside, bool no) {
    searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
        auto aux = element->nextElementSibling();

        while(aux && aux->tagName != element->tagName) {
            aux = aux->nextElementSibling();
        }

        return (no == (aux != nullptr));
    });
}

void CSSSelector::search_first_child(std::forward_list<DOM_Node::sp_Element> &list, bool inside, bool no) {
    searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
        return (no == (element->previousElementSibling() != nullptr));
    });
}

void CSSSelector::search_last_child(std::forward_list<DOM_Node::sp_Element> &list, bool inside, bool no) {
    searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
        return (no == (element->nextElementSibling() != nullptr));
    });
}

void CSSSelector::search_not(std::forward_list<DOM_Node::sp_Element> &list, const std::string & value, bool inside, bool no) {
    Parser parser(value);
    parser.goUntil("(");

    std::string newSelector = parser.getInsideParentheses();
    auto rules = getRules(newSelector);

    for (const Rule & rule : rules) {
        switch (rule.token) {
            case ID:
                searchBy(list, inside, [&](const DOM_Node::sp_Element & element) -> bool {
                    return no == !(element->id != rule.value);
                });
                break;

            case CLASS:
                searchBy(list, inside, [&](const DOM_Node::sp_Element & element) -> bool {
                    return no == element->classList.contains(rule.value);
                });
                break;

            case TAG:
                searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                    return no == !(element->tagName != rule.value);
                });
                break;

            case ATTR:
                searchByAttr(list, rule.value, inside, !no);
                break;

            case ALL:
                if (no) {
                    list.clear();

                } else {
                    searchBy(list, inside, [&](const DOM_Node::sp_Element &element) -> bool {
                        return true;
                    });
                }
                break;

            case PSEUDO_CLASS:
                searchByPseudoClass(list, rule.value, inside, true);
                break;

            default:
                break;
        }
    }
}

void CSSSelector::search_root(std::forward_list<DOM_Node::sp_Element> &list, bool no) {
    processList(list, [&](const DOM_Node::sp_Element & element) -> DOM_Node::HTMLCollection {
        DOM_Node::HTMLCollection collection;

        auto parent = element->parentElement();

        if (no == (parent != nullptr)) {
            collection.push_back(element);
        }

        return collection;
    });
}

void CSSSelector::search_nth_child(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value, bool inside, bool no) {
    Parser parser(value);
    parser.goUntil("(");

    std::string index = parser.getInsideParentheses();

    if (std::regex_match(index, std::regex("\\d+"))) { // simple digit index
        int idx = std::stoi(index);

        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            return (no == (NodeTravel(element).index() + 1 != idx));
        });

    } else if (value == "odd") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            return (no == (NodeTravel(element).index() % 2));
        });

    } else if (value == "even") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            return (no == !(NodeTravel(element).index() % 2));
        });

    } else if (std::regex_match(index, std::regex("(.*)(n+)(.*)"))) { // by formula
        //todo
    }
}

void CSSSelector::search_nth_last_child(std::forward_list<DOM_Node::sp_Element> &list,
                                      const std::string &value, bool inside, bool no) {

    Parser parser(value);
    parser.goUntil("(");

    std::string index = parser.getInsideParentheses();

    if (std::regex_match(index, std::regex("\\d+"))) { // simple digit index
        int idx = std::stoi(index);

        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            DOM_Node::HTMLCollection collection;
            auto parent = element->parentNode();
            size_t children_size = parent ? parent->childNodes.size() : 1;

            return (no == (children_size - NodeTravel(element).index() != idx));
        });

    } else if (value == "odd") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            return (no == (NodeTravel(element).index() % 2));
        });

    } else if (value == "even") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            return (no == !(NodeTravel(element).index() % 2));
        });

    } else if (std::regex_match(index, std::regex("(.*)(n+)(.*)"))) { // by formula
        //todo
    }
}

void CSSSelector::search_nth_of_type(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value,
                                   bool inside, bool no) {

    Parser parser(value);
    parser.goUntil("(");

    std::string index = parser.getInsideParentheses();

    if (std::regex_match(index, std::regex("\\d+"))) { // simple digit index
        int idx = std::stoi(index);

        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            int cont = 1;

            auto prev = element->previousElementSibling();
            while (prev && cont <= idx) {
                if (prev->tagName == element->tagName) {
                    cont++;
                }

                prev = prev->previousElementSibling();
            }

            return no == (cont != idx);
        });

    } else if (value == "odd") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            auto parent = element->parentElement();
            auto aux = parent ? parent->firstElementChild() : nullptr ;
            int i = 0;

            while (aux && aux != element) {
                if (aux->tagName == element->tagName) {
                    i++;
                }

                aux = aux->nextElementSibling();
            }

            return no == !(i % 2);
        });

    } else if (value == "even") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            auto parent = element->parentElement();
            auto aux = parent ? parent->firstElementChild() : nullptr ;
            int i = 0;

            while (aux && aux != element) {
                if (aux->tagName == element->tagName) {
                    i++;
                }

                aux = aux->nextElementSibling();
            }

            return no == (i % 2);
        });

    } else if (std::regex_match(index, std::regex("(.*)(n+)(.*)"))) { // by formula
        //todo
    }
}

void CSSSelector::search_nth_last_of_type(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value,
                                        bool inside, bool no) {

    Parser parser(value);
    parser.goUntil("(");

    std::string index = parser.getInsideParentheses();

    if (std::regex_match(index, std::regex("\\d+"))) { // simple digit index
        int idx = std::stoi(index);

        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            int cont = 1;

            auto next = element->nextElementSibling();
            while (next && cont <= idx) {
                if (next->tagName == element->tagName) {
                    cont++;
                }

                next = next->nextElementSibling();
            }

            return no == (cont != idx);
        });

    } else if (value == "odd") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            auto parent = element->parentElement();
            auto aux = parent ? parent->firstElementChild() : nullptr ;
            int i = 0;

            while (aux && aux != element) {
                if (aux->tagName == element->tagName) {
                    i++;
                }

                aux = aux->nextElementSibling();
            }

            return no == !(i % 2);
        });

    } else if (value == "even") {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            auto parent = element->parentElement();
            auto aux = parent ? parent->firstElementChild() : nullptr ;
            int i = 0;

            while (aux && aux != element) {
                if (aux->tagName == element->tagName) {
                    i++;
                }

                aux = aux->nextElementSibling();
            }

            return no == (i % 2);
        });

    } else if (std::regex_match(index, std::regex("(.*)(n+)(.*)"))) { // by formula
        //todo
    }
}

void CSSSelector::search_only_of_type(std::forward_list<DOM_Node::sp_Element> &list, bool inside, bool no) {
    searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
        auto parent = element->parentElement();
        auto aux = parent ? parent->firstElementChild() : nullptr ;
        bool other = false;

        while (aux)  {
            if (aux->tagName == element->tagName && aux != element) {
                other = true;
                break;
            }

            aux = aux->nextElementSibling();
        }

        return no == other;
    });
}

void CSSSelector::search_only_child(std::forward_list<DOM_Node::sp_Element> &list, bool inside, bool no) {
    searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
        DOM_Node::HTMLCollection collection;
        auto parent = element->parentElement();
        size_t n = parent ? parent->childElementCount() : 1;

        return no == (n != 1);
    });
}

void CSSSelector::search_matches(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value, bool inside, bool no) {
    Parser parser(value);
    parser.goUntil("(");

    std::string selector = parser.getInsideParentheses();
    auto selectors = Utils::split_string_by(selector, ',');

    for (const auto & s : selectors) {
        searchBy(list, inside, [&](const DOM_Node::sp_Element & element) {
            return no == !(element->matches(s));
        });
    }
}

void CSSSelector::searchByPseudoClass(std::forward_list<DOM_Node::sp_Element> &list, const std::string &value, bool inside, bool no) {

    if (value == "first-of-type") {
        search_first_of_type(list, inside, no);

    } else if (value == "last-of-type") {
        search_last_of_type(list, inside, no);

    } else if (value == "first-child") {
        search_first_child(list, inside, no);

    } else if (value == "last-child") {
        search_last_child(list, inside, no);

    } else if (std::regex_match(value, std::regex("not\\((.*)\\)"))) {
        search_not(list, value, inside, no);

    } else if (value == "root") {
        search_root(list, no);

    } else if (std::regex_match(value, std::regex("nth-child\\((.*)\\)"))) {
        search_nth_child(list, value, inside, no);

    } else if (std::regex_match(value, std::regex("nth-last-child\\((.*)\\)"))) {
        search_nth_last_child(list, value, inside, no);

    }  else if (std::regex_match(value, std::regex("nth-of-type\\((.*)\\)"))) {
        search_nth_of_type(list, value, inside, no);

    }  else if (std::regex_match(value, std::regex("nth-last-of-type\\((.*)\\)"))) {
        search_nth_last_of_type(list, value, inside, no);

    } else if (value == "only-of-type") {
        search_only_of_type(list, inside, no);

    } else if (value == "only-child") {
        search_only_child(list, inside, no);

    } else if (std::regex_match(value, std::regex("matches\\((.*)\\)"))) {
        search_matches(list, value, inside, no);

    } else {
        list.clear();
    }
}