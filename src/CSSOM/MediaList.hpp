/**
* @brief An object that implements the MediaList interface has an associated collection of media queries.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
* @details Is based on https://drafts.csswg.org/cssom-1/#the-medialist-interface
*/

#ifndef GUINOX_MEDIALIST_HPP
#define GUINOX_MEDIALIST_HPP

#include <string>
#include <deque>
#include "../DOM/ReactString.hpp"


class MediaList {
    typedef std::deque<std::string>::iterator iterator;
    typedef std::deque<std::string>::const_iterator const_iterator;


    std::deque<std::string> list;
    unsigned long _length = 0;


    std::string serialize() const ;
    void setter(const std::string & olValue, const std::string & newValue);


public:

    ReactString mediaText;
    const unsigned long length = _length;


    MediaList();

    const std::string & item(unsigned long index) const ;
    void appendMedium(const std::string & medium);
    void deleteMedium(const std::string & medium);
    bool empty() const ;


    const std::string & operator[](size_t index) const ;
    MediaList & operator=(const std::string & mediums);


    inline auto begin() noexcept { return list.begin(); }
    inline auto begin() const noexcept { return list.cbegin(); }
    inline auto cbegin() const noexcept { return list.cbegin(); }
    inline auto end() noexcept { return list.end(); }
    inline auto end() const noexcept { return list.cend(); }
    inline auto cend() const noexcept { return list.cend(); }
};


#endif //GUINOX_MEDIALIST_HPP
