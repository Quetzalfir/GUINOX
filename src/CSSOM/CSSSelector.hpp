/**
* @brief When receiving a tree or query, check if it matches the desired element.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
*/

#ifndef GUINOX_CSSSELECTOR_HPP
#define GUINOX_CSSSELECTOR_HPP

#include <string>
#include <vector>
#include <forward_list>
#include <functional>
#include "../DOM/dom_declarations.hpp"

class CSSSelector {

    enum Token : char {
        NONE = 0,
        TAG,
        ID,             // #
        CLASS,          // .
        ALL,            // *
        ATTR,           // [
        INSIDE,         // ' '
        DIRECT_CHILD,   // >
        DIRECT_AFTER,   // +
        AFTER,          // ~
        PSEUDO_ELEMENT, // ::
        PSEUDO_CLASS    // :
    };

    struct Rule {
        Token token = NONE;
        std::string value;
    };


    static std::vector<Rule> getRules(const std::string & selector);
    static DOM_Node::HTMLCollection search(const std::vector<Rule> & rules, DOM_Node::HTMLCollection collection, bool filter);

    static void searchBy(std::forward_list<DOM_Node::sp_Element> & list, bool inside, const std::function<bool(const DOM_Node::sp_Element &)> & fn);

    static void searchByID(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside);
    static void searchByClass(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside );
    static void searchByAttr(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside, bool no);

    static void searchByDirectChildren(std::forward_list<DOM_Node::sp_Element> & list);
    static void searchByDirectAfter(std::forward_list<DOM_Node::sp_Element> & list);
    static void searchByAfter(std::forward_list<DOM_Node::sp_Element> & list);

    static void searchByPseudoElement(std::forward_list<DOM_Node::sp_Element> &list, const std::string & value);
    static void searchByPseudoClass(std::forward_list<DOM_Node::sp_Element> &list, const std::string & value, bool inside, bool no);

    //Pseudo-Class Methods
    static void search_first_of_type(std::forward_list<DOM_Node::sp_Element> & list, bool inside, bool no);
    static void search_last_of_type(std::forward_list<DOM_Node::sp_Element> & list, bool inside, bool no);
    static void search_first_child(std::forward_list<DOM_Node::sp_Element> & list, bool inside, bool no);
    static void search_last_child(std::forward_list<DOM_Node::sp_Element> & list, bool inside, bool no);
    static void search_not(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside, bool no);
    static void search_root(std::forward_list<DOM_Node::sp_Element> & list, bool no);
    static void search_nth_child(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside, bool no);
    static void search_nth_last_child(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside, bool no);
    static void search_nth_of_type(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside, bool no);
    static void search_nth_last_of_type(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside, bool no);
    static void search_only_of_type(std::forward_list<DOM_Node::sp_Element> & list, bool inside, bool no);
    static void search_only_child(std::forward_list<DOM_Node::sp_Element> & list, bool inside, bool no);
    static void search_matches(std::forward_list<DOM_Node::sp_Element> & list, const std::string & value, bool inside, bool no);


public:
    //ENTRADA QUERY CSS Y DEVUELVE EL VECTOR DE ELEMENTOS SELECCIONADOS
    static DOM_Node::HTMLCollection select(const std::string & query, DOM_Node::sp_Element root);

    static bool match(const std::string & selector, DOM_Node::sp_Element element);

    /**
     * Returns the specificity of one selector in a list, the first one
     *
     * @param selector
     * @return
     */
    static unsigned int getSpecificity(const std::string & selector);

};


#endif //GUINOX_CSSSELECTOR_HPP
