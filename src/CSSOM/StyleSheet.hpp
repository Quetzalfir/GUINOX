#ifndef GUINOX_STYLESHEET_HPP
#define GUINOX_STYLESHEET_HPP

#include "../DOM/dom_declarations.hpp"
#include "css_declarations.hpp"
#include "MediaList.hpp"
#include <iterator>


/**
* @brief The StyleSheet interface represents an abstract, base style sheet.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
* @details Is based on https://drafts.csswg.org/cssom-1/#the-stylesheet-interface
*/
class StyleSheet : public std::enable_shared_from_this<StyleSheet> {

protected:

    DOM_Node::Document *_ownerDocument = nullptr;
    std::string _url;
    std::string _type = "text/css";
    std::string _href;
    DOM_Node::Element *_ownerNode = nullptr;
    CSSStyleSheet * _parentStyleSheet = nullptr; //optional
    std::string _title;
    MediaList _media;
    bool alternate_flag = false;
    bool origin_clean_flag = true;


public:
    virtual ~StyleSheet() = default;

    const std::string & url = _url;
    const std::string & type = _type;
    const std::string & href = _href;
    const std::string & title = _title;
    const MediaList & media = _media; // [SameObject, PutForwards=mediaText]
    bool disabled = false;

    DOM_Node::sp_Element ownerNode() const ;
    sp_CSSStyleSheet parentStyleSheet() const ;


};


class CSSStyleSheet : public StyleSheet {
    friend class HTMLStyleElement;
    friend class DocumentStyle;
    friend class HTMLLinkElement;
    friend class CSSParser;
    friend class CSSImportRule;
    friend class CSSGroupingRule;
    friend class CSSOMTravel;


    CSSRule *_ownerRule = nullptr;
    CSSRuleList _cssRules;


    static sp_CSSRule createStyleRule(const std::string & text, CSSStyleSheet *cssStyleSheet);
    static sp_CSSRule createImportRule(const std::string & text, CSSStyleSheet *cssStyleSheet);
    static sp_CSSRule createMediaRule(const std::string & text, CSSStyleSheet *cssStyleSheet);
    static sp_CSSRule createFontRule(const std::string & text, CSSStyleSheet *cssStyleSheet);

    static sp_CSSRule createRule(const std::string & text, CSSStyleSheet *cssStyleSheet);
    void adopt(DOM_Node::Document *document);


public:
    ~CSSStyleSheet() override = default;

    sp_CSSRule ownerRule() const ;
    const CSSRuleList & cssRules = _cssRules;

    unsigned long insertRule(const std::string & rule, long index = -1);
    void deleteRule(unsigned long index);
};


class CSSStyleSheetList {
    typedef std::list<sp_CSSStyleSheet>::iterator iterator;
    typedef std::list<sp_CSSStyleSheet>::const_iterator const_iterator;



    std::list<sp_CSSStyleSheet> list;


public:

    sp_CSSStyleSheet operator[](size_t index) const ;


    void push_back(const sp_CSSStyleSheet & sheet);
    iterator insert(iterator & it, const sp_CSSStyleSheet & sheet);
    iterator erase(iterator & position);
    inline bool empty() const { return list.empty(); }
    inline size_t size() const { return list.size(); }

    inline iterator begin() noexcept { return list.begin(); }
    inline const_iterator begin() const noexcept { return list.cbegin(); }
    inline const_iterator cbegin() const noexcept { return list.cbegin(); }
    inline iterator end() noexcept { return list.end(); }
    inline const_iterator end() const noexcept { return list.cend(); }
    inline const_iterator cend() const noexcept { return list.cend(); }
};


#endif //GUINOX_STYLESHEET_HPP
