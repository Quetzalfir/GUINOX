/**
* @brief Auxiliary class for traversing the CSSOM tree.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
*/

#ifndef GUINOX_CSSOMTRAVEL_HPP
#define GUINOX_CSSOMTRAVEL_HPP

#include "StyleSheet.hpp"
#include "CSSRule.hpp"


class Presentation;

class CSSOMTravel {

    CSSStyleSheet* const self;
    const Presentation* const presentation;

    bool checkMedium(const MediaList & media) const ;
    void includingRules(const sp_CSSRule& rule, const std::function<void(sp_CSSRule)>& cb, bool omitMediumCheck);
    void includingStyleSheets(const sp_CSSRule& rule, const std::function<void(sp_CSSStyleSheet)>& cb, bool omitMediumCheck);


public:

    CSSOMTravel(CSSStyleSheet *sheet, const Presentation * presentation_) : self(sheet), presentation(presentation_) {}
    CSSOMTravel(const sp_CSSStyleSheet & sheet, const Presentation * presentation_) : CSSOMTravel(sheet.get(), presentation_) {}



    void forEachCSSRule(const std::function<void(sp_CSSRule rule)>& cb, bool omitMediumCheck = false);
    void forEachStyleSheet(const std::function<void(sp_CSSStyleSheet)>& cb, bool omitMediumCheck = false);
    void forEachFontRule(const std::function<void(sp_CSSFontFaceRule)>& cb, bool omitMediumCheck = false);
    void forEachCSSStyleRule(const std::function<void(sp_CSSStyleRule)>& cb, bool omitMediumCheck = false);
};


#endif //GUINOX_CSSOMTRAVEL_HPP
