#include "CSSParser.hpp"
#include "StyleSheet.hpp"
#include <fstream>


sp_CSSStyleSheet CSSParser::parseFromFile(const std::string &path) {
    std::ifstream cssFile(path, std::ifstream::in);

    if (!cssFile.is_open()) return nullptr;

    std::string str((std::istreambuf_iterator<char>(cssFile)), std::istreambuf_iterator<char>());

    sp_CSSStyleSheet styleSheet = parse(str, path);
    styleSheet->_href = path;

    cssFile.close();

    return styleSheet;
}

sp_CSSStyleSheet CSSParser::parse(const std::string &css, const std::string & documentPath) {
    //todo, need to initialize de CSSModules in order to create just a document without a window

    Parser parser(css);
    parser.addCommentTag("/*", "*/");

    sp_CSSStyleSheet styleSheet = std::make_shared<CSSStyleSheet>();
    styleSheet->_url = documentPath;

    while (parser.skipBlanks()) {

        auto rule = parser.getUntilFirstOf("{;");

        if (parser.getChar() == '{') {
            rule += '{' + parser.getInsideParentheses() + '}';

        } else if (parser.getChar() == ';') {
            parser++;
        }

        styleSheet->insertRule(rule);
    }

    return styleSheet;
}