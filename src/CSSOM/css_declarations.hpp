/**
* @brief Statements to be defined later.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
*/

#ifndef GUINOX_CSS_DECLARATIONS_HPP
#define GUINOX_CSS_DECLARATIONS_HPP


#include <list>
#include <memory>


class CSSRule;
typedef std::shared_ptr<CSSRule> sp_CSSRule;

typedef std::list<sp_CSSRule> CSSRuleList;


class StyleSheet;
typedef std::shared_ptr<StyleSheet> sp_StyleSheet;


class CSSStyleSheet;
typedef std::shared_ptr<CSSStyleSheet> sp_CSSStyleSheet;



#endif //GUINOX_CSS_DECLARATIONS_HPP
