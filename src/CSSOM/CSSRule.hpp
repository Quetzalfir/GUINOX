/**
* @brief A CSS rule is an abstract concept that denotes a rule as defined by the CSS specification.
* A CSS rule is represented as an object that implements a subclass of the CSSRule interface.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
* @details Is based on https://drafts.csswg.org/cssom-1/#css-rules
*/

#ifndef GUINOX_CSSRULE_HPP
#define GUINOX_CSSRULE_HPP

#include <string>
#include "../DOM/ReactString.hpp"
#include "css_declarations.hpp"
#include "MediaList.hpp"
#include "CSSStyleDeclaration.hpp"


class CSSRule : public std::enable_shared_from_this<CSSRule> {
    friend class CSSStyleSheet;
    friend class CSSGroupingRule;


public:
    enum Type : unsigned short {
        UNKNOWN = 0,
        STYLE_RULE = 1,
        CHARSET_RULE = 2,
        IMPORT_RULE = 3,
        MEDIA_RULE = 4,
        FONT_FACE_RULE = 5,
        PAGE_RULE = 6,
        KEYFRAMES_RULE = 7,
        KEYFRAME_RULE = 8,
        MARGIN_RULE = 9,
        NAMESPACE_RULE = 10,
        COUNTER_STYLE_RULE = 11,
        SUPPORTS_RULE = 12,
        FONT_FEATURE_VALUES_RULE = 14,
        VIEWPORT_RULE = 15
    };

private:
    Type _type;


protected:
    CSSRule *_parentRule = nullptr;
    CSSStyleSheet *_parentStyleSheet = nullptr;


    CSSRule(Type type, CSSStyleSheet *styleSheet);

    virtual std::string serialize() = 0;


public:

    virtual ~CSSRule() = default;

    ReactString cssText;
    sp_CSSRule parentRule() const ;
    sp_CSSStyleSheet parentStyleSheet() const ;
    const Type & type = _type;

};



/**
 * @brief The CSSStyleRule interface represents a style rule.
 *
 * @author GUINOX
 * @version 1.0
 * @date 30/03/2020
 *
 * @details Is based on https://drafts.csswg.org/cssom-1/#the-cssstylerule-interface
 */
class CSSStyleRule : public CSSRule {
    friend class CSSStyleSheet;

    CSSStyleDeclaration _style;


protected:
    CSSStyleRule(CSSStyleSheet *styleSheet);

    std::string serialize() override ;


public:
    std::string selectorText;
    const CSSStyleDeclaration & style = _style; //[SameObject, PutForwards=cssText]
};

typedef std::shared_ptr<CSSStyleRule> sp_CSSStyleRule;




class CSSFontFaceRule : public CSSRule {
    friend class CSSStyleSheet;

    CSSStyleDeclaration _style;

    CSSFontFaceRule(CSSStyleSheet *styleSheet);

    std::string serialize() override ;

public:
    const CSSStyleDeclaration & style = _style;
};

typedef std::shared_ptr<CSSFontFaceRule> sp_CSSFontFaceRule;


/*
class CSSCharsetRule : public CSSRule {

public:
    std::string encoding;
// raises(DOMException) on setting

};*/

/**
 * @brief The CSSImportRule interface represents an @import at-rule.
 *
 * @author GUINOX
 * @version 1.0
 * @date 30/03/2020
 *
 * @details Is based on https://drafts.csswg.org/cssom-1/#the-cssimportrule-interface
 */
class CSSImportRule : public CSSRule {
    friend class CSSStyleSheet;

    //todo, include "supports" css syntax
    std::string _href;
    MediaList _media;
    sp_CSSStyleSheet _styleSheet;


    CSSImportRule(const std::string & path, const std::string & mediums, CSSStyleSheet * parentStylesheet);

    virtual std::string serialize() override ;


public:

    const std::string & href = _href;
    const MediaList & media = _media; //[SameObject, PutForwards=mediaText]
    const sp_CSSStyleSheet & styleSheet = _styleSheet;
};

/**
 * @brief The CSSGroupingRule interface represents an at-rule that contains other rules nested inside itself.
 *
 * @author GUINOX
 * @version 1.0
 * @date 30/03/2020
 *
 * @details Is based on https://drafts.csswg.org/cssom-1/#the-cssgroupingrule-interface
 */
class CSSGroupingRule : public CSSRule {


protected:
    CSSRuleList _cssRules;


    CSSGroupingRule(Type type, CSSStyleSheet * styleSheet);


public:

    const CSSRuleList & cssRules = _cssRules;

    unsigned long insertRule(const std::string & rule, unsigned long index = 0);
    void deleteRule(unsigned long index);
};

/**
 * @brief The CSSConditionRule interface represents all the "conditional" at-rules,
 * which consist of a condition and a statement block.
 *
 * @author GUINOX
 * @version 1.0
 * @date 30/03/2020
 *
 * @details Is based on https://www.w3.org/TR/css3-conditional/#the-cssconditionrule-interface
 */
class CSSConditionRule : public CSSGroupingRule {

protected:
    CSSConditionRule(Type type, CSSStyleSheet * styleSheet);


public:
    std::string conditionText;
};

/**
 * @brief The CSSMediaRule interface is defined in CSS Conditional Rules.
 *
 * @author GUINOX
 * @version 1.0
 * @date 30/03/2020
 *
 * @details Is based on https://drafts.csswg.org/cssom-1/#the-cssmediarule-interface
 */
class CSSMediaRule : public CSSConditionRule {

    friend class CSSStyleSheet;


    MediaList _media;


    virtual std::string serialize() override ;


    CSSMediaRule(const std::string & mediums, CSSStyleSheet * parentStyleSheet);

public:

    const MediaList & media = _media; //[SameObject, PutForwards=mediaText]
};

/*
class CSSPageRule : public CSSGroupingRule {

    CSSStyleDeclaration _style;

public:
    ReactString selectorText;
    const CSSStyleDeclaration & style = _style; //[SameObject, PutForwards=cssText]
};


class CSSMarginRule : public CSSRule {
    std::string _name;
    CSSStyleDeclaration _style;

public:
    const std::string &name = _name;
    const CSSStyleDeclaration & style = _style; //[SameObject, PutForwards=cssText]
};


class CSSNamespaceRule : public CSSRule {
    std::string _namespaceURI;
    std::string _prefix;

public:
    const std::string & namespaceURI = _namespaceURI;
    const std::string & prefix = _prefix;

};*/


#endif //GUINOX_CSSRULE_HPP
