#include <Utils/Parser.hpp>
#include <Utils/Utils.hpp>
#include "StyleSheet.hpp"
#include "CSSRule.hpp"
#include "../DOM/Element.hpp"
#include "CSSOMTravel.hpp"
#include <peglib.h>
#include <utility>


static std::vector<std::pair<std::string, std::string>> getTokens(std::shared_ptr<peg::Ast> &ast) {
    std::vector<std::pair<std::string, std::string>> properties;

    for (auto & node : ast->nodes) {
        if (node->is_token) {
            properties.emplace_back(node->name, Utils::trim( node->token ));
        } else {
            auto aux = getTokens(node);
            properties.insert(properties.end(), aux.begin(), aux.end());
        }
    }

    return  properties;
}




DOM_Node::sp_Element StyleSheet::ownerNode() const {
    if (_ownerNode)
        return std::dynamic_pointer_cast<DOM_Node::Element>( _ownerNode->shared_from_this() );

    return nullptr;
}

sp_CSSStyleSheet StyleSheet::parentStyleSheet() const {
    if (_parentStyleSheet) {
        return std::dynamic_pointer_cast<CSSStyleSheet>( _parentStyleSheet->shared_from_this() );
    }

    return nullptr;
}





sp_CSSRule CSSStyleSheet::createRule(const std::string &css, CSSStyleSheet *cssStyleSheet) {
    sp_CSSRule rule = nullptr;

    if (css[0] == '@') { // AT-Rule
        Parser parser(css);
        parser++;

        auto type = parser.getUntilFirstOf(" ");

        if (type == "import") {
            rule = createImportRule(css, cssStyleSheet);

        } else if (type == "media") {
            rule = createMediaRule(css, cssStyleSheet);

        } else if (type == "font-face") {
            rule = createFontRule(css, cssStyleSheet);

        }

    } else { // Style-Rule
        rule = createStyleRule(css, cssStyleSheet);
    }

    return rule;
}


sp_CSSRule CSSStyleSheet::ownerRule() const {
    if (_ownerRule)
        return _ownerRule->shared_from_this();

    return nullptr;
}

unsigned long CSSStyleSheet::insertRule(const std::string &css, long index) {
    if (!origin_clean_flag) {
        //todo, throw a SecurityError exception.
        return 0;
    }

    if (css.empty()) return -1;

    sp_CSSRule rule = createRule(css, this);

    if (rule) {
        if (index < 0 || index >= _cssRules.size()) {
            _cssRules.push_back(rule);
            return _cssRules.size() - 1 ;

        } else {
            auto it = _cssRules.begin();

            while (index) {
                it++;
                index--;
            }

            _cssRules.insert(it, rule);
            return index;
        }
    }

    return -1;
}

void CSSStyleSheet::deleteRule(unsigned long index) {
    if (index >= 0 && index < _cssRules.size()) {
        auto it = _cssRules.begin();

        while (index) {
            it++;
            index--;
        }

        _cssRules.erase(it);
    }
}

sp_CSSRule CSSStyleSheet::createStyleRule(const std::string & text, CSSStyleSheet *cssStyleSheet) {
    Parser parser(text);
    auto *rule = new CSSStyleRule(cssStyleSheet);

    std::string selector = Utils::trim( parser.getUntilFirstOf("{;") );
    selector.erase(std::remove(selector.begin(), selector.end(), '\n'), selector.end());

    rule->selectorText = selector;

    if (parser.getChar() == '{') {
        rule->_style.cssText = parser.getInsideParentheses();
    }

    return std::shared_ptr<CSSRule>(rule);
}

sp_CSSRule CSSStyleSheet::createImportRule(const std::string & text, CSSStyleSheet *cssStyleSheet) {
    static peg::parser parser(R"(
            IMPORT                      <- "@import" (STRING / URL) MEDIA_QUERY_LIST?

            MEDIA_QUERY_LIST            <- < MEDIA_QUERY (',' _ MEDIA_QUERY)* >

            MEDIA_QUERY                 <- MEDIA_CONDITION /
                                           ("not"/"only")? MEDIA_TYPE _ ("and" _ MEDIA_CONDITION_WITHOUT_OR)?

            MEDIA_TYPE                  <- IDENT

            MEDIA_CONDITION             <- MEDIA_NOT / MEDIA_IN_PARENS (MEDIA_AND / MEDIA_OR)*
            MEDIA_CONDITION_WITHOUT_OR  <- MEDIA_NOT / MEDIA_IN_PARENS MEDIA_AND*

            MEDIA_NOT                   <- "not" MEDIA_IN_PARENS
            MEDIA_AND                   <- "and" MEDIA_IN_PARENS
            MEDIA_OR                    <- "or"  MEDIA_IN_PARENS


            MEDIA_IN_PARENS	        	<-  MEDIA_FEATURE /
					                        '(' MEDIA_CONDITION ')' /
                                            GENERAL_ENCLOSED


            MEDIA_FEATURE		        <- '(' ( MF_PLAIN / MF_RANGE / MF_BOOLEAN ) _ ')'

            GENERAL_ENCLOSED	        <- FUNCTION /
					                       '(' IDENT (!')' .)* ')'

            FUNCTION		        	<- IDENT '(' (!')' .)* ')'

            MF_PLAIN 			        <- MF_NAME _ ':' MF_VALUE

            MF_BOOLEAN			        <- MF_NAME

            MF_RANGE			        <- MF_VALUE MF_LT MF_NAME MF_LT MF_VALUE /
					                       MF_VALUE MF_GT MF_NAME MF_GT MF_VALUE /
                                           MF_NAME MF_COMPARISON MF_VALUE /
					                       MF_VALUE MF_COMPARISON MF_NAME

            MF_COMPARISON 		        <- MF_LT / MF_GT / MF_EQ

            MF_LT				        <- _ '<' '='? _
            MF_GT				        <- _ '>' '='? _
            MF_EQ				        <- _ '=' _

            MF_NAME				        <- IDENT
            MF_VALUE			        <- RATIO / DIMENSION / NUMBER / IDENT

            RATIO				        <- NUMBER &'/' ('/' NUMBER)?

            DIMENSION			        <- LENGTH / RESOLUTION

            RESOLUTION			        <- '0' RESOLUTION_UNIT? / NUMBER RESOLUTION_UNIT
            RESOLUTION_UNIT		        <- "dpi" | "dpcm" | "dppx" | "x"

            LENGTH                      <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
            LENGTH_UNIT                 <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

            NUMBER                      <- FLOAT / INTEGER
            FLOAT                       <- ('+'|'-')? [0-9]* '.' [0-9]+
            INTEGER                     <- ('+'|'-')? [0-9]+


            IDENT				        <- ("--" / '-'? [a-zA-Z_]) [a-zA-Z0-9-_]*

            URL                         <- "url(" STRING ')'

            STRING                      <- < $q<QUOTE> (!$q .)* $q >
            QUOTE                       <- "'" | '"'

            ~_    				        <- [ \t]*
            %whitespace                 <- [ \t\r\n]*)");
    static bool init = false;

    if (!init) {
        init = true;
        parser.enable_ast();
    }

    std::shared_ptr<peg::Ast> ast;
    if (parser.parse(text.data(), ast)) {
        std::string href;
        std::string mediums;

        for (auto & p : getTokens(ast)) {
            if (p.first == "STRING") {
                href = p.second.substr(1, p.second.size()-2);

            } else if (p.first == "MEDIA_QUERY_LIST") {
                mediums = p.second;

            }
        }

        std::string path;
        int found = cssStyleSheet->url.find_last_of('/');

        if (found != std::string::npos) {
            path = cssStyleSheet->url.substr(0,found+1) + href;
        } else {
            path = href;
        }

        auto *rule = new CSSImportRule(path, mediums, cssStyleSheet);
        rule->_href = href;

        return std::shared_ptr<CSSRule>(rule);
    }

    return nullptr;
}

sp_CSSRule CSSStyleSheet::createMediaRule(const std::string &text, CSSStyleSheet *cssStyleSheet) {
    Parser parser(text);

    parser.goUntil(" ");
    parser++;

    auto mediums = parser.getUntilFirstOf("{");

    auto * rule = new CSSMediaRule(mediums, cssStyleSheet);

    parser++;
    while (parser.skipBlanks() && parser.getChar() != '}') {

        auto subRule = parser.getUntilFirstOf("{;");

        if (parser.getChar() == '{') {
            subRule += '{' + parser.getInsideParentheses() + '}';

        } else if (parser.getChar() == ';') {
            parser++;
        }

        rule->insertRule(subRule);
    }

    return std::shared_ptr<CSSRule>(rule);
}

sp_CSSRule CSSStyleSheet::createFontRule(const std::string &text, CSSStyleSheet *cssStyleSheet) {
    Parser parser(text);
    auto *rule = new CSSFontFaceRule(cssStyleSheet);

    parser.getUntilFirstOf("{;");

    if (parser.getChar() == '{') {
        rule->_style.cssText = parser.getInsideParentheses();
    }

    return std::shared_ptr<CSSRule>(rule);
}


void CSSStyleSheet::adopt(DOM_Node::Document *document) {
    CSSOMTravel(this, nullptr).forEachStyleSheet([=](sp_CSSStyleSheet sheet) {
        sheet->_ownerDocument = document;
    }, true);
}






sp_CSSStyleSheet CSSStyleSheetList::operator[](size_t index) const {
    auto end = list.end();
    auto it = list.begin();
    index += 1;

    while (it != end && index) {
        it++;
        index--;
    }

    if (it == end) return nullptr;
    return (*it);
}

void CSSStyleSheetList::push_back(const sp_CSSStyleSheet &sheet) {
    list.push_back(sheet);
}

CSSStyleSheetList::iterator CSSStyleSheetList::insert(iterator & it, const sp_CSSStyleSheet &sheet) {
    return list.insert(it, sheet);
}

CSSStyleSheetList::iterator CSSStyleSheetList::erase(iterator & it) {
    return list.erase(it);
}