#include <Utils/Utils.hpp>
#include "MediaList.hpp"


MediaList::MediaList() :
                mediaText([this] { return serialize(); },
                          [this](auto && PH1, auto && PH2) { setter(PH1, PH2); }) {

}

std::string MediaList::serialize() const {
    if (list.empty()) return "";

    std::string res;

    auto size = list.size();
    for (int i = 0; i < size-1; i++) {
        res += list[i] + ", ";
    }

    res += list.back();

    return res;
}

void MediaList::setter(const std::string &olValue, const std::string &newValue) {
    list.clear();
    _length = 0;

    if (newValue.empty()) return;

    auto mediums = Utils::split_string_by(newValue, ',');

    for (auto & medium : mediums) {
        appendMedium(medium);
    }
}

const std::string & MediaList::item(unsigned long index) const {
    static const std::string T;

    if (index >= list.size()) return T;

    return list[index];
}

const std::string & MediaList::operator[](size_t index) const {
    return item(index);
}

MediaList & MediaList::operator=(const std::string &mediums) {
    mediaText = mediums;

    return *this;
}

void MediaList::appendMedium(const std::string &medium) {
    for (auto & m : list) {
        if (m == medium)
            return;;
    }

    list.push_back(medium);
    _length++;
}

void MediaList::deleteMedium(const std::string &medium) {
    auto end = list.end();
    for (auto it = list.begin(); it != end; it++) {
        if ((*it) == medium) {
            list.erase(it);
            _length--;

            return;
        }
    }
}

bool MediaList::empty() const {
    return list.empty();
}