#include "CSSOMTravel.hpp"
#include <DOM/Document.hpp>


bool CSSOMTravel::checkMedium(const MediaList & media) const {
    if (self->disabled) return false;

    if (presentation) {
        return presentation->check(media);
    }

    return true;
}


void CSSOMTravel::forEachStyleSheet(const std::function<void (sp_CSSStyleSheet)>& cb, bool omitMediumCheck) {
    if (omitMediumCheck || checkMedium(self->media)) {
        cb(std::dynamic_pointer_cast<CSSStyleSheet>( self->shared_from_this()));

        for (const auto & rule : self->cssRules) {
            includingStyleSheets(rule, cb, omitMediumCheck);
        }
    }
}

void CSSOMTravel::includingStyleSheets(const sp_CSSRule& rule, const std::function<void (sp_CSSStyleSheet)> &cb, bool omitMediumCheck) {
    switch (rule->type) {
        case CSSRule::IMPORT_RULE: {
            auto import = std::dynamic_pointer_cast<CSSImportRule>(rule);

            CSSOMTravel(import->styleSheet, presentation).forEachStyleSheet(cb, omitMediumCheck);
            break;
        }
        case CSSRule::MEDIA_RULE: {
            auto media = std::dynamic_pointer_cast<CSSMediaRule>(rule);

            for (const auto & sub_rule : media->cssRules) {
                includingStyleSheets(sub_rule, cb, omitMediumCheck);
            }
            break;
        }
        default: ;
    }
}

void CSSOMTravel::forEachFontRule(const std::function<void (sp_CSSFontFaceRule)>& cb, bool omitMediumCheck) {
    forEachCSSRule([&](const sp_CSSRule& rule) {
        if (rule->type == CSSRule::FONT_FACE_RULE) {
            auto style = std::dynamic_pointer_cast<CSSFontFaceRule>(rule);

            cb(style);
        }
    }, omitMediumCheck);
}

void CSSOMTravel::forEachCSSStyleRule(const std::function<void (sp_CSSStyleRule)>& cb, bool omitMediumCheck) {
    forEachCSSRule([&](const sp_CSSRule& rule) {
        if (rule->type == CSSRule::STYLE_RULE) {
            auto style = std::dynamic_pointer_cast<CSSStyleRule>(rule);

            cb(style);
        }
    }, omitMediumCheck);
}

void CSSOMTravel::forEachCSSRule(const std::function<void(sp_CSSRule)>& cb, bool omitMediumCheck) {
    if (omitMediumCheck || checkMedium(self->media)) {
        for (const auto & rule : self->cssRules) {
            cb(rule);
            includingRules(rule, cb, omitMediumCheck);
        }
    }
}

void CSSOMTravel::includingRules(const sp_CSSRule& rule, const std::function<void(sp_CSSRule)>& cb, bool omitMediumCheck) {
    switch (rule->type) {
        case CSSRule::IMPORT_RULE: {
            auto import = std::dynamic_pointer_cast<CSSImportRule>(rule);

            CSSOMTravel(import->styleSheet, presentation).forEachCSSRule(cb, omitMediumCheck);
            break;
        }
        case CSSRule::MEDIA_RULE: {
            auto media = std::dynamic_pointer_cast<CSSMediaRule>(rule);

            for (const auto & sub_rule : media->cssRules) {
                cb(rule);
                includingRules(sub_rule, cb, omitMediumCheck);
            }

            break;
        }
        default: ;
    }
}