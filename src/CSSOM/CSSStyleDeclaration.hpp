/**
* @brief The CSSStyleDeclaration interface represents a CSS declaration block,
* including its underlying state, where this underlying state depends upon the source of the
* CSSStyleDeclaration instance.
*
* @author GUINOX
* @version 1.0
* @date 30/03/2020
*
* @details Is based on https://drafts.csswg.org/cssom-1/#the-cssstyledeclaration-interface
*/

#ifndef GUINOX_CSSSTYLEDECLARATION_HPP
#define GUINOX_CSSSTYLEDECLARATION_HPP

#include "css_declarations.hpp"
#include "../DOM/ReactString.hpp"
#include <deque>
#include <vector>

class CSSStyleDeclaration {
    friend class CSSStyleRule;


    unsigned long _length = 0;
    CSSRule *_parentRule = nullptr;
    bool case_sensitive = false;
    bool readonly = false;


    struct Declaration {
        std::string name;
        std::string value;
        bool priority;

        Declaration(std::string _name, std::string  _value, bool _priority);
    };

    typedef std::list<std::shared_ptr<Declaration>>::const_iterator const_iterator;



    std::list<std::shared_ptr<Declaration>> declarations;

    bool setCSSDeclaration(std::vector<std::pair<std::string,std::string>> & properties, bool important);
    std::string serialize();
    void parse(const std::string & oldValue, const std::string & newValue);
    std::string getFloat();
    void setFloat(const std::string & oldValue, const std::string & newValue);


public:
    ReactString cssText;
    const unsigned long & length = _length;
    ReactString cssFloat;


    CSSStyleDeclaration();
    CSSStyleDeclaration(const CSSStyleDeclaration & rhs);


    sp_CSSRule parentRule();
    inline bool empty() const { return declarations.empty(); }
    const std::string & item(unsigned long index) const ;
    std::string getPropertyValue(const std::string & property) const ;
    const std::string & getPropertyPriority(const std::string & property) const ;
    void setProperty(const std::string & property, const std::string & value, const std::string & priority = ""); //[CEReactions]
    std::string removeProperty(const std::string & property); //[CEReactions]

    const std::string &operator[](std::size_t idx) const ;
    ReactString operator[](const std::string & property);
    ReactString operator[](const std::string & property) const ;
    CSSStyleDeclaration & operator+= (const CSSStyleDeclaration & rhs);
    CSSStyleDeclaration & operator= (const CSSStyleDeclaration & rhs);


    //inline iterator begin() noexcept { return declarations.begin(); }
    inline const_iterator begin() const noexcept { return declarations.begin(); }
    inline const_iterator cbegin() const noexcept { return declarations.cbegin(); }
    //inline iterator end() noexcept { return declarations.end(); }
    inline const_iterator end() const noexcept { return declarations.cend(); }
    inline const_iterator cend() const noexcept { return declarations.cend(); }

};


#endif //GUINOX_CSSSTYLEDECLARATION_HPP
