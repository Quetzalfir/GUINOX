#include <Utils/Utils.hpp>
#include <Utils/Parser.hpp>
#include <utility>
#include "CSSStyleDeclaration.hpp"
#include "CSSRule.hpp"
#include "../RenderContext/CSSModuleRegistry.hpp"

using namespace std::placeholders;


static std::map<std::string, std::vector<std::string>> longhandMap = {
        {"font",           {"font-style",
                            "font-variant",
                            "font-weight",
                            "font-size",
                            "line-height",
                            "font-family"}},
        {"transition",     {"transition-property",
                            "transition-duration",
                            "transition-timing-function",
                            "transition-delay"}},
        {"animation",      {"animation-name",
                            "animation-duration",
                            "animation-timing-function",
                            "animation-delay",
                            "animation-iteration-count",
                            "animation-direction"}},
        {"padding",        {"padding-top",
                            "padding-right",
                            "padding-bottom",
                            "padding-left"}},
        {"list-style",     {"list-style-type",
                            "list-style-image",
                            "list-style-position"}},
        {"border-radius",  {"border-top-left-radius",
                            "border-top-right-radius",
                            "border-bottom-right-radius",
                            "border-bottom-left-radius"}},
        {"flex",           {"flex-grow",
                            "flex-shrink",
                            "flex-basis"}}
};



CSSStyleDeclaration::Declaration::Declaration(std::string _name, std::string _value, bool _priority) :
                name(std::move(_name)), value(std::move(_value)), priority(_priority) {

}


CSSStyleDeclaration::CSSStyleDeclaration() :
        cssText([this] { return serialize(); },
                [this](const std::string & PH1, const std::string & PH2) { parse(PH1, PH2); }),
        cssFloat([this] { return getFloat(); },
                 [this](const std::string & PH1, const std::string & PH2) { setFloat(PH1, PH2); }) {


}

CSSStyleDeclaration::CSSStyleDeclaration(const CSSStyleDeclaration &rhs) :
        cssText([this] { return serialize(); },
                [this](const std::string & PH1, const std::string & PH2) { parse(PH1, PH2); }),
        cssFloat([this] { return getFloat(); },
                 [this](const std::string & PH1, const std::string & PH2) { setFloat(PH1, PH2); }
        ) {


    declarations = rhs.declarations;
    _length = rhs._length;
    _parentRule = rhs._parentRule;
    case_sensitive = rhs.case_sensitive;
    readonly = rhs.readonly;
}

std::string CSSStyleDeclaration::serialize() {
    std::string res;

    for (auto & d : declarations) {
        res += d->name + " : " + d->value + (d->priority ? "!important" : "") + ";\n";
    }

    return res;
}

void CSSStyleDeclaration::parse(const std::string &oldValue, const std::string &newValue) {
    declarations.clear();
    _length = 0;

    Parser parser(newValue);

    while (parser.skipBlanks()) {
        std::string property = parser.getUntilFirstOf(" :");
        parser.skip(" :\t\r\n");

        std::string value = Utils::trim( parser.getUntilFirstOf("!;") );

        std::string priority;
        if (parser.getChar() == '!') {
            parser++;
            priority = Utils::trim( parser.getUntilFirstOf(";") );
        }

        setProperty(property, value, priority);

        parser++;
    }
}

std::string CSSStyleDeclaration::getFloat() {
    return getPropertyValue("float");
}

void CSSStyleDeclaration::setFloat(const std::string &oldValue, const std::string &newValue) {
    setProperty("float", newValue);
}

sp_CSSRule CSSStyleDeclaration::parentRule() {
    return _parentRule->shared_from_this();
}

bool CSSStyleDeclaration::setCSSDeclaration(std::vector<std::pair<std::string,std::string>> & props, bool important) {
    bool modified = false;

    for (auto & p : props) {

        auto end = declarations.end();
        auto it = declarations.begin();
        for ( ; it != end; it++) {
            if ((*it)->name == p.first) {
                modified = (*it)->value != p.second || (*it)->priority != important;
                (*it)->value = p.second;
                (*it)->priority = important;
                break;
            }
        }

        if (it == end) {
            auto ptr = std::make_shared<CSSStyleDeclaration::Declaration>(p.first, p.second, important);
            declarations.push_back(ptr);
            _length++;
            modified = true;
        }
    }

    return modified;
}

const std::string & CSSStyleDeclaration::item(unsigned long index) const {
    static const std::string T;

    if (index >= declarations.size()) return T;

    auto it = declarations.begin();
    std::advance(it, index);

    return (*it)->name;
}

std::string CSSStyleDeclaration::getPropertyValue(const std::string &property) const {
    bool first = true;
    bool priority = false;
    auto props = CSSModuleRegistry::getLonghandsProperty( Utils::toLower(property) );

    std::string value;

    for (auto & p : props) {
        for (auto & declaration : declarations) {
            if (declaration->name == p) {
                if (first) {
                    first = false;
                    priority = declaration->priority;

                } else {
                    if (priority != declaration->priority) return "";
                }

                value += declaration->value + " ";
            }
        }
    }

    return Utils::trim(value);
}

const std::string& CSSStyleDeclaration::getPropertyPriority(const std::string &property) const {
    static const std::string T;
    static const std::string IMPORTANT = "important";

    bool found = false;
    auto props = CSSModuleRegistry::getLonghandsProperty( Utils::toLower(property) );

    for (auto & p : props) {
        for (auto & p2 : declarations) {
            if (p2->name == p) {
                found = true;
                if (!p2->priority) return T;
                break;
            }
        }
    }

    return found ? IMPORTANT : T ;
}

void CSSStyleDeclaration::setProperty(const std::string &property, const std::string &value, const std::string &priority) {
    if (readonly) {
        //todo, throw a NoModificationAllowedError exception.
    }

    if (value.empty()) {
        removeProperty(property);
        return;
    }

    //parse possible shorthands
    auto props = CSSModuleRegistry::mapProperty(property, value);

    // set the declaration
    bool updated = setCSSDeclaration(props, Utils::toLower(priority) == "important");

    if (updated) {
        //todo, update style attribute for the CSS declaration block.
    }
}

std::string CSSStyleDeclaration::removeProperty(const std::string &property) {
    if (readonly) {
        //todo, throw a NoModificationAllowedError exception.
    }

    auto value = getPropertyValue(property);
    auto props = CSSModuleRegistry::getLonghandsProperty( Utils::toLower(property) );

    // remove declarations
    for (auto & p : props) {
        for (auto it = declarations.begin(); it != declarations.end(); it++) {
            if ((*it)->name == p) {
                declarations.erase(it);
                _length--;
                break;
            }
        }
    }

    return value;
}

const std::string & CSSStyleDeclaration::operator[](std::size_t idx) const {
    return item(idx);
}

ReactString CSSStyleDeclaration::operator[](const std::string & property) {
    return ReactString([&]() -> std::string {
                           return getPropertyValue(property);
                       },
                       [&](const std::string & oldValue, const std::string &newValue) {
                           setProperty(property, newValue);
                       });
}

ReactString CSSStyleDeclaration::operator[](const std::string & property) const {
    return ReactString([&]() -> std::string {
                           return getPropertyValue(property);
                       });
}

CSSStyleDeclaration & CSSStyleDeclaration::operator+=(const CSSStyleDeclaration &rhs) {
    for (const auto & d : rhs.declarations) {
        std::vector<std::pair<std::string,std::string>> prop;

        prop.emplace_back(d->name, d->value);

        setCSSDeclaration(prop , d->priority);
    }

    return *this;
}

CSSStyleDeclaration & CSSStyleDeclaration::operator=(const CSSStyleDeclaration &rhs) {
    if (&rhs != this) {
        declarations = rhs.declarations;
        _length = rhs._length;
        _parentRule = rhs._parentRule;
        case_sensitive = rhs.case_sensitive;
        readonly = rhs.readonly;
    }

    return *this;
}