#ifndef GUINOX_MEDIUM_HPP
#define GUINOX_MEDIUM_HPP

#include <peglib.h>
#include "MediaList.hpp"


class Medium {

    struct Op {
        bool op = false; // 0 = AND, 1 = OR
        bool value;
    };

    enum ValueType {
        IDENT, DIMENSION, NUMBER
    };

    peg::parser parser;
    void initMediaParser();




    static ValueType getValueType(const std::string& value);
    static bool compValues(const std::string & a, const std::string & comp, const std::string & b);
    static bool compNumbers(const double & a, const std::string & comp, const double & b);
    static void initStaticParser();
    static std::string getDimension(const std::string & value);




    class Map {
        std::map<std::string,std::string> features;

    public:
        Map();

        std::string & operator[](const std::string & feature);
        const std::string & operator[](const std::string & feature) const;
    };


    std::string getHoWoRIn(const std::string & feature, const std::string & dimension) const ;
    bool plainValues(const std::string &feature, const std::string & value) const ;


public:
    enum Type : unsigned {
        NONE = 0,
        SCREEN = 1,
        PRINT = 2,
        SPEECH = 4,
        ALL = 7
    };


protected:

    unsigned _type;
    double _h_resolution;
    double _v_resolution;


public:

    Map features;
    const unsigned & type = _type;


    Medium(unsigned type_);

    bool check(const MediaList & mediaList) const ;
};


#endif //GUINOX_MEDIUM_HPP
