#include "Medium.hpp"
#include "../Utils/Utils.hpp"


static peg::parser PARSER(R"(
            MEDIA_QUERY                 <- MEDIA_CONDITION /
                                           MEDIA_TYPE _ ("and" _ MEDIA_CONDITION_WITHOUT_OR)?

            MEDIA_TYPE                  <- NOT_ONLY? IDENT
            NOT_ONLY                    <- "not" | "only"

            MEDIA_CONDITION             <- MEDIA_NOT / MEDIA_IN_PARENS (MEDIA_AND / MEDIA_OR)*
            MEDIA_CONDITION_WITHOUT_OR  <- MEDIA_NOT / MEDIA_IN_PARENS MEDIA_AND*

            MEDIA_NOT                   <- "not" MEDIA_IN_PARENS
            MEDIA_AND                   <- "and" MEDIA_IN_PARENS
            MEDIA_OR                    <- "or"  MEDIA_IN_PARENS


            MEDIA_IN_PARENS	        	<-  MEDIA_FEATURE /
					                        '(' MEDIA_CONDITION ')' /
                                            GENERAL_ENCLOSED


            MEDIA_FEATURE		        <- '(' ( MF_PLAIN / MF_RANGE / MF_BOOLEAN ) _ ')'

            GENERAL_ENCLOSED	        <- FUNCTION /
					                       '(' IDENT (!')' .)* ')'

            FUNCTION		        	<- IDENT '(' (!')' .)* ')'

            MF_PLAIN 			        <- MF_NAME _ ':' MF_VALUE

            MF_BOOLEAN			        <- MF_NAME

            MF_RANGE			        <- MF_VALUE MF_LT MF_NAME MF_LT MF_VALUE /
					                       MF_VALUE MF_GT MF_NAME MF_GT MF_VALUE /
                                           MF_NAME MF_COMPARISON MF_VALUE /
					                       MF_VALUE MF_COMPARISON MF_NAME


            MF_COMPARISON 		        <- MF_LT / MF_GT / MF_EQ

            MF_LT				        <- _ '<' '='? _
            MF_GT				        <- _ '>' '='? _
            MF_EQ				        <- _ '=' _

            MF_NAME				        <- < IDENT >
            MF_VALUE			        <- RATIO / DIMENSION / NUMBER / IDENT

            RATIO				        <- < NUMBER &'/' ('/' NUMBER)? >

            DIMENSION			        <- < LENGTH / RESOLUTION >

            RESOLUTION			        <- '0' RESOLUTION_UNIT? / NUMBER RESOLUTION_UNIT
            RESOLUTION_UNIT		        <- "dpi" | "dpcm" | "dppx" | "x"

            LENGTH                      <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
            LENGTH_UNIT                 <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "rem" | "cm" | "in" | "pt" | "pc"

            NUMBER                      <- < FLOAT / INTEGER >
            FLOAT                       <- ('+'|'-')? [0-9]* '.' [0-9]+
            INTEGER                     <- ('+'|'-')? [0-9]+

            IDENT				        <- < ("--" / '-'? [a-zA-Z_]) [a-zA-Z0-9-_]* >

            STRING                      <- < $q<QUOTE> (!$q .)* $q >
            QUOTE                       <- "'" | '"'

            ~_    				        <- [ \t]*
            %whitespace                 <- [ \t\r\n]*)");




Medium::Map::Map() {
    features["any-hover"] = "none";
    features["any_pointer"] = "none";
    features["aspect-ratio"] = "0";
    features["color"] = "0";
    features["color-gamut"] = "srgb";
    features["color-index"] = "0";
    features["grid"] = "0";
    features["height"] = "0px";
    features["display-mode"] = "standalone"; //
    features["hover"] = "none";
    features["inverted-colors"] = "none";
    features["light-level"] = "normal";
    features["monochrome"] = "0";
    features["max-aspect-ratio"] = "0";
    features["max-color"] = "0";
    features["max-color-index"] = "0";
    features["max-height"] = "0px";
    features["max-monochrome"] = "0";
    features["max-resolution"] = "0dpi";
    features["max_width"] = "0px";
    features["min_aspect_ratio"] = "0";
    features["min_color"] = "1";
    features["min_color_index"] = "0";
    features["min_height"] = "0px";
    features["min_monochrome"] = "0";
    features["min_resolution"] = "0dpi";
    features["orientation"] = "landscape";
    features["overflow_block"] = "none";
    features["overflow_inline"] = "none";
    features["pointer"] = "none";
    features["resolution"] = "0dpi";
    features["scan"] = "progressive";
    features["scripting"] = "none";
    features["update"] = "none";
    features["width"] = "0px";
}

std::string & Medium::Map::operator[](const std::string &feature) {
    static std::string T;

    auto it = features.find(feature);
    if (it != features.end()) {
        return it->second;
    }

    return T;
}

const std::string & Medium::Map::operator[](const std::string &feature) const {
    const static std::string T;

    auto it = features.find(feature);
    if (it != features.end()) {
        return it->second;
    }

    return T;
}






bool Medium::compNumbers(const double &a, const std::string &comp, const double &b) {
    if (comp == "<") {
        return a < b;
    } else if (comp == "<=") {
        return a <= b;
    } else if (comp == ">") {
        return a > b;
    } else if (comp == ">=") {
        return a >= b;
    } else if (comp == "=") {
        return a == b;
    } else {
        return false;
    }
}

Medium::ValueType Medium::getValueType(const std::string &value) {
    if (!isdigit(value.front())) return IDENT;
    if (isdigit(value.back())) return NUMBER;
    return DIMENSION;
}

bool Medium::compValues(const std::string &a, const std::string &comp, const std::string &b) {
    ValueType aType = getValueType(a);
    ValueType bType = getValueType(b);

    switch (aType) {
        case NUMBER:
        case DIMENSION: {
            if (bType == IDENT) return false;
            if (bType == NUMBER || bType == DIMENSION) {
                double ad = std::stod(a);
                double bd = std::stod(b);

                return compNumbers(ad, comp, bd);
            }
        }
        case IDENT: {
            if (bType != IDENT) return false;

            if (comp == "=" || comp == ">=" || comp == "<=") {
                return a == b;
            }

            return false;
        }
    }

    return false;
}

void Medium::initStaticParser() {
    static bool init = false;

    if (init) return;

    init = true;

    PARSER.enable_packrat_parsing();


    PARSER["MEDIA_QUERY"] = [](const peg::SemanticValues& sv) -> bool {
        switch (sv.choice()) {
            case 0:  // MEDIA_CONDITION
                return peg::any_cast<bool>(sv[0]);

            default: { // NOT_ONLY? MEDIA_TYPE _ ("and" _ MEDIA_CONDITION_WITHOUT_OR)?
                bool b = true;

                for (auto &s : sv) {
                    b &= peg::any_cast<bool>(s);
                }

                return b;
            }
        }
    };

    PARSER["MEDIA_CONDITION"] = [](const peg::SemanticValues& sv) -> bool {
        switch (sv.choice()) {
            case 0:  // MEDIA_NOT
                return peg::any_cast<bool>(sv[0]);

            default: { // MEDIA_IN_PARENS (MEDIA_AND / MEDIA_OR)*
                int size = sv.size();
                bool b = peg::any_cast<bool>(sv[0]);

                for (int i = 1; i < size; i++) {
                    Op op = peg::any_cast<Op>(sv[i]);

                    if (op.op) { //OR
                        b |= op.value;
                    } else { //AND
                        b &= op.value;
                    }
                }

                return b;
            }
        }
    };

    PARSER["MEDIA_CONDITION_WITHOUT_OR"] = [](const peg::SemanticValues& sv) -> bool {
        switch (sv.choice()) {
            case 0:  // MEDIA_NOT
                return peg::any_cast<bool>(sv[0]);

            default: { // MEDIA_IN_PARENS MEDIA_AND*
                int size = sv.size();
                bool b = peg::any_cast<bool>(sv[0]);

                if (!b) return false;

                for (int i = 1; i < size; i++) {
                    Op op = peg::any_cast<Op>(sv[i]);
                    b &= op.value;

                    if (!b) return false;
                }

                return b;
            }
        }
    };

    PARSER["NOT_ONLY"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };

    PARSER["MEDIA_NOT"] = [](const peg::SemanticValues & sv) -> bool {
        return !(peg::any_cast<bool>(sv[0]));
    };

    PARSER["MEDIA_OR"] = [](const peg::SemanticValues & sv) -> Op {
        Op op;
        op.value = peg::any_cast<bool>(sv[0]);
        op.op = true;

        return op;
    };

    PARSER["MEDIA_AND"] = [](const peg::SemanticValues & sv) -> Op {
        Op op;
        op.value = peg::any_cast<bool>(sv[0]);

        return op;
    };

    PARSER["MEDIA_IN_PARENS"] = [](const peg::SemanticValues & sv) -> bool {
        return peg::any_cast<bool>(sv[0]);
    };

    PARSER["MEDIA_FEATURE"] = [](const peg::SemanticValues & sv) -> bool {
        return peg::any_cast<bool>(sv[0]);
    };

    PARSER["MF_COMPARISON"] = [](const peg::SemanticValues & sv) -> std::string {
        return peg::any_cast<std::string>(sv[0]);
    };

    PARSER["MF_LT"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };

    PARSER["MF_GT"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };

    PARSER["MF_EQ"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };

    PARSER["GENERAL_ENCLOSED"] = [](const peg::SemanticValues & sv) -> bool {
        //todo

        return true;
    };

    PARSER["MF_NAME"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };

    PARSER["MF_VALUE"] = [](const peg::SemanticValues & sv) -> std::string {
        return peg::any_cast<std::string>(sv[0]);
    };

    PARSER["RATIO"] = [](const peg::SemanticValues & sv) -> std::string {
        return std::to_string(Utils::parseMath(sv.token(0)));
    };

    PARSER["DIMENSION"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };

    PARSER["NUMBER"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };

    PARSER["IDENT"] = [](const peg::SemanticValues & sv) -> std::string {
        return sv.token(0);
    };
}

std::string Medium::getDimension(const std::string &value) {
    int i = 0;
    int size = value.size();
    while (value[i] >= '0'  && value[i] <='9' && i < size) i++;

    if (i == size) return "px";
    std::string dim = value.substr(i);

    if (dim != "px" && dim != "em" && dim != "ex" && dim != "ch" && dim != "vh" && dim != "vw" && dim != "vmin" &&
        dim != "pc" && dim != "vmax" && dim != "mm" && dim != "rem" && dim != "cm" && dim != "in" && dim != "pt" &&
        dim != "dpi" && dim != "dpcm" && dim != "dpx" && dim != "x") {

        return "px";
    }

    return dim;
}




std::string Medium::getHoWoRIn(const std::string &feature, const std::string &dim) const {
    double res = std::stod(features[feature]); //px/dpi

    if (feature.find("resolution") != std::string::npos) {
        if (dim == "dpcm") res /= 2.54;
        else if (dim == "dpx" || dim == "x") res /= 96;

    } else { // width or height
        double height = std::stod(features["height"]);
        double width = std::stod(features["width"]);
        double r;

        if (feature.find("height") != std::string::npos) {
            r = _v_resolution;
        } else {
            r = _h_resolution;
        }

        if (dim == "mm") res /= r;
        else if (dim == "cm") res = (res / r) / 10;
        else if (dim == "in") res = (res / r) / 25.4;
        else if (dim == "pt") res = (res / r) / (25.4 * 72);
        else if (dim == "pc") res = 12 * ((res / r) / (25.4 * 72));
        else if (dim == "vh") res = (res * 100) / height;
        else if (dim == "vw") res = (res * 100) / width;
        else if (dim == "vmin") res = (res * 100) / std::min(height, width);
        else if (dim == "vmax") res = (res * 100) / std::max(height, width);
        else if (dim == "em") res = (res / r) / 6; // 1em =  6mm
        else if (dim == "rem") res = (res / r) / 6; // 1rem =  6mm
        else if (dim == "ex") res = (res / r) / 6; // 1ex =  6mm
        else if (dim == "ch") res = (res / r) / 2; // 1ch =  2mm
    }

    return std::to_string(res) + dim;
}

bool Medium::plainValues(const std::string &feature, const std::string &value) const {
    static const std::vector<std::string> DIMENSIONALS = {"height", "width", "resolution"};
    std::string l_value;

    for (const auto & x : DIMENSIONALS) {
        if (feature.find(x) != std::string::npos) {
            l_value = getHoWoRIn(x, getDimension(value));
            break;
        }
    }

    if (l_value.empty()) {
        l_value = features[feature];
    }

    std::string comp = "=";

    if (feature.find("max") != std::string::npos) {
        comp = "<=";
    } else if (feature.find("min") != std::string::npos) {
        comp = ">=";
    }

    return compValues(l_value, comp, value);
}




Medium::Medium(unsigned type_) : _type(type_) {
    initMediaParser();
}


void Medium::initMediaParser() {
    initStaticParser();

    parser = PARSER;

    parser["MEDIA_TYPE"] = [this](const peg::SemanticValues & sv) -> bool {
        int size = sv.size();
        std::string not_only;
        int i = 0;

        if (size == 2) {
            not_only = peg::any_cast<std::string>(sv[0]);
            i = 1;
        }

        const auto& mType = peg::any_cast<std::string>(sv[i]);

        unsigned lType = 0;
        if (mType == "screen") {
            lType = SCREEN;
        } else if (mType == "speech") {
            lType = SPEECH;
        } else if (mType == "print") {
            lType = PRINT;
        } else if (mType == "all") {
            lType = ALL;
        }


        if (not_only == "not") {
            return (~lType) & _type;

        } else if (not_only == "only") {
            return lType == _type;

        } else {
            return lType & _type;
        }
    };

    parser["MF_RANGE"] = [this](const peg::SemanticValues & sv) -> bool {
        switch (sv.choice()) {
            case 0: // MF_VALUE MF_LT MF_NAME MF_LT MF_VALUE
            case 1: { // MF_VALUE MF_GT MF_NAME MF_GT MF_VALUE
                const auto& value1 = Utils::trim( peg::any_cast<std::string>(sv[0]) );
                const auto& comp1 = Utils::trim( peg::any_cast<std::string>(sv[1]) );
                const auto& name = Utils::trim( peg::any_cast<std::string>(sv[2]) );
                const auto& comp2 = Utils::trim( peg::any_cast<std::string>(sv[3]) );
                const auto& value2 = Utils::trim( peg::any_cast<std::string>(sv[4]) );

                std::string l_value1;
                std::string l_value2;
                if (name.find("height") != std::string::npos ||
                    name.find("width") != std::string::npos ||
                    name.find("resolution") != std::string::npos) {
                    l_value1 = getHoWoRIn(name, getDimension(value1));
                    l_value2 = getHoWoRIn(name, getDimension(value2));
                } else {
                    l_value1 = features[name];
                    l_value2 = features[name];
                }

                return compValues(value1, comp1, l_value1) && compValues(l_value2, comp2, value2);
            }
            case 2: { // MF_NAME MF_COMPARISON MF_VALUE
                const auto& name = Utils::trim( peg::any_cast<std::string>(sv[0]) );
                const auto& comp = Utils::trim( peg::any_cast<std::string>(sv[1]) );
                const auto& value = Utils::trim( peg::any_cast<std::string>(sv[2]) );

                std::string l_value;
                if (name.find("height") != std::string::npos ||
                    name.find("width") != std::string::npos ||
                    name.find("resolution") != std::string::npos) {
                    l_value = getHoWoRIn(name, getDimension(value));
                } else {
                    l_value = features[name];
                }

                return compValues(l_value, comp, value);

            }
            default: { // MF_VALUE MF_COMPARISON MF_NAME
                const auto& name = Utils::trim( peg::any_cast<std::string>(sv[2]) );
                const auto& comp = Utils::trim( peg::any_cast<std::string>(sv[1]) );
                const auto& value = Utils::trim( peg::any_cast<std::string>(sv[0]) );

                std::string l_value;
                if (name.find("height") != std::string::npos ||
                    name.find("width") != std::string::npos ||
                    name.find("resolution") != std::string::npos) {
                    l_value = getHoWoRIn(name, getDimension(value));
                } else {
                    l_value = features[name];
                }

                return compValues(value, comp, l_value);

            }
        }
    };

    parser["MF_PLAIN"] = [this](const peg::SemanticValues & sv) -> bool {
        const auto& name = peg::any_cast<std::string>(sv[0]);
        const auto& value = peg::any_cast<std::string>(sv[1]);

        return plainValues(name, value);
    };

    parser["MF_BOOLEAN"] = [this](const peg::SemanticValues & sv) -> bool {
        const auto& name = peg::any_cast<std::string>(sv[0]);

        const auto & l_value = features[name];

        return !(l_value == "0" || l_value == "none");
    };
}


bool Medium::check(const MediaList & mediaList) const {
    if (mediaList.empty()) {
        return true;
    }

    for (auto const & media : mediaList) {
        bool val;
        parser.parse(media.data(), val);

        if (val) return true;
    }

    return false;
}
