#include "CSSRule.hpp"
#include "CSSParser.hpp"
#include "StyleSheet.hpp"

static void doNothing() { }


CSSRule::CSSRule(Type type_, CSSStyleSheet *styleSheet) :
            _type(type_),
            cssText([this] { return serialize(); }) {


    _parentStyleSheet = styleSheet;
}

sp_CSSRule CSSRule::parentRule() const {
    if (_parentRule)
        return _parentRule->shared_from_this();

    return nullptr;
}

sp_CSSStyleSheet CSSRule::parentStyleSheet() const {
    if (_parentStyleSheet)
        return std::dynamic_pointer_cast<CSSStyleSheet>( _parentStyleSheet->shared_from_this() );

    return nullptr;
}





CSSStyleRule::CSSStyleRule(CSSStyleSheet *styleSheet) : CSSRule(STYLE_RULE, styleSheet) {
    _style._parentRule = this;
}

std::string CSSStyleRule::serialize() {
    std::string data = selectorText;

    data += "{ " + _style.cssText + " }";

    return data;
}




CSSImportRule::CSSImportRule(const std::string & path, const std::string & mediums, CSSStyleSheet * parentStylesheet) :
                    CSSRule(IMPORT_RULE, parentStylesheet) {

    _styleSheet = CSSParser::parseFromFile(path);
    _href = path; //todo, currently is the full path not the current value; it can be "url(s)"
    _media = mediums;

    if (_styleSheet) {
        _styleSheet->_ownerRule = this;
        _styleSheet->_parentStyleSheet = parentStylesheet;
        _styleSheet->_media = mediums;
    }
}

std::string CSSImportRule::serialize() {
    std::string data = "@import ";

    data += _href;

    data += " " + _media.mediaText + " ;";

    return data;
}



CSSFontFaceRule::CSSFontFaceRule(CSSStyleSheet *styleSheet) : CSSRule(FONT_FACE_RULE, styleSheet) {

}

std::string CSSFontFaceRule::serialize() {
    std::string data = "@import ";

    data += "{ " + _style.cssText + " }";

    return data;
}




CSSConditionRule::CSSConditionRule(Type type_, CSSStyleSheet * styleSheet) : CSSGroupingRule(type_, styleSheet) {

}




CSSMediaRule::CSSMediaRule(const std::string & mediums, CSSStyleSheet * styleSheet) : CSSConditionRule(MEDIA_RULE, styleSheet) {
    _media = mediums;
    conditionText = mediums;
}

std::string CSSMediaRule::serialize() {
    std::string data = "@media ";

    data += conditionText + "{ ";

    for (const auto & rule : _cssRules) {
        data += rule->cssText + " \n";
    }

    data += "}";

    return data;
}



CSSGroupingRule::CSSGroupingRule(Type type_, CSSStyleSheet * styleSheet) : CSSRule(type_, styleSheet) {

}

unsigned long CSSGroupingRule::insertRule(const std::string &css, unsigned long index) {
    if (css.empty()) return -1;

    sp_CSSRule rule = CSSStyleSheet::createRule(css, _parentStyleSheet);
    rule->_parentRule = this;

    if (rule) {

        if (index < 0 || index >= _cssRules.size()) {
            _cssRules.push_back(rule);
            return _cssRules.size() - 1 ;

        } else {
            auto it = _cssRules.begin();

            while (index) {
                it++;
                index--;
            }

            _cssRules.insert(it, rule);
            return index;
        }
    }

    return -1;
}

void CSSGroupingRule::deleteRule(unsigned long index) {
    if (index >= 0 && index < _cssRules.size()) {
        auto it = _cssRules.begin();

        while (index) {
            it++;
            index--;
        }

        _cssRules.erase(it);
    }
}