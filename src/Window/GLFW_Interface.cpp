#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include "GLFW_Interface.hpp"
#include "Window.hpp"

bool        GLFW_Interface::_initialized = false;
bool   &    GLFW_Interface::initialized = _initialized;
std::thread GLFW_Interface::GLFWThread;
bool        GLFW_Interface::canFinish = true;
std::queue<std::pair<std::function<void(void)>, std::function<void(void)>>> GLFW_Interface::homework;


static void error_callback(int error, const char* description) {
    fprintf(stderr, "Error %d: %s\n", error, description);
}


void GLFW_Interface::doInMainGLFWThread(const std::function<void(void)> &hw, bool sync) {
    const static std::function<void(void)> doNothing = [](){};

    if (!_initialized) init();

    if (sync) {
        bool done = false;

        homework.emplace(hw, [&](){ done = true; });

        while (!done) {}

    } else {
        homework.emplace(hw, doNothing);
    }
}

void GLFW_Interface::init() {
    if (!initialized) {
        _initialized = true;
        canFinish = false;

        GLFWThread = std::thread([]() {
            glfwInit();

            // pass function to print errors
            glfwSetErrorCallback(error_callback);

            while (initialized) {
                while (!homework.empty()) {
                    auto & hw = homework.front();
                    hw.first();
                    hw.second();
                    homework.pop();
                }

                if (!Window::glfwWindows.empty()) {
                    glfwPollEvents();
                }
            }

            glfwTerminate();
            canFinish = true;
        });

        GLFWThread.detach();
    }
}

void GLFW_Interface::terminate() {
    initialized = false;
}

void GLFW_Interface::waitForOut() {
    while (!canFinish) {}
}