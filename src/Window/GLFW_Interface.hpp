#ifndef GUINOX_GLFW_INTERFACE_HPP
#define GUINOX_GLFW_INTERFACE_HPP

#include <thread>
#include <queue>
#include <functional>


class GLFW_Interface {

    static bool _initialized;
    static std::thread GLFWThread;
    static std::queue<std::pair<std::function<void(void)>, std::function<void(void)>>> homework;
    static bool canFinish;


public:

    static bool & initialized;


    static void doInMainGLFWThread(const std::function<void(void)>& hw, bool sync = false);
    static void init();
    static void terminate();
    static void waitForOut();

};


#endif //GUINOX_GLFW_INTERFACE_HPP
