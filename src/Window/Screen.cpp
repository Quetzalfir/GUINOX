#include "Screen.hpp"

#ifndef __ANDROID__
#include "GLFW_Interface.hpp"


std::map<GLFWmonitor *, std::vector<Screen *>> Screen::screens;


void Screen::monitor_callback(GLFWmonitor *monitor, int event) {
    auto it = screens.find(monitor);
    if (it != screens.end()) {
        auto & vec = (*it).second;

        for (Screen * screen : vec) {
            //todo, dispatch event;

            if (event == GLFW_CONNECTED) {
                // The monitor was connected

            } else if (event == GLFW_DISCONNECTED) {
                // The monitor was disconnected
            }
        }
    }
}


Screen::Screen(GLFWmonitor *_monitor) {
    GLFW_Interface::doInMainGLFWThread([=]() {
        this->initProperties(_monitor);
    }, true);

    screens[monitor].push_back(this);
}


void Screen::initProperties(GLFWmonitor *_monitor) {
    monitor = _monitor;
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    _height = mode->height;
    _width = mode->width;
    _refreshRate = mode->refreshRate;
    _colorDepth[0] = mode->redBits;
    _colorDepth[1] = mode->greenBits;
    _colorDepth[2] = mode->blueBits;
    _name = glfwGetMonitorName(monitor);

    glfwGetMonitorPhysicalSize(monitor, &_widthMM, &_heightMM);
    glfwGetMonitorPos(monitor, &_left, &_top);

    glfwSetMonitorCallback(monitor_callback);
}
#endif



/*std::vector<Screen> Screen::getAllScreens() {
    int count;
    GLFWmonitor** monitors;
    GLFW_Interface::doInMainGLFWThread([&]() {
        monitors = glfwGetMonitors(&count);
    }, true);

    std::vector<Screen> vec;
    vec.reserve(count);
    for (int i = 0; i < count; i++) {
        vec.push_back(Screen(monitors[i]));
    }

    return vec;
}*/


Screen::Screen() {
    /*GLFW_Interface::doInMainGLFWThread([this]() {
        this->initProperties(glfwGetPrimaryMonitor());
    }, true);

    screens[monitor].push_back(this);*/
}



Screen::~Screen() {
    /*auto & vec = screens[monitor];

    auto end = vec.end();
    for (auto it = vec.begin(); it != end; it++) {
        if ((*it) == this) {
            vec.erase(it);
            break;
        }
    }*/
}

Screen & Screen::operator=(const Screen &rh) {
    if (&rh != this) {
        _height = rh._height;
        _heightMM = rh._heightMM;
        _name = rh._name;
        _refreshRate = rh._refreshRate;
        _width = rh._width;
        _widthMM = rh._widthMM;

        for (int i = 0; i < 3; i++) {
            _colorDepth[i] = rh._colorDepth[i];
        }
    }

    return *this;
}

bool Screen::operator==(const Screen &rhs) const {
    //return monitor == rhs.monitor;
    return false;
}