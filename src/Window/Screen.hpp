/**
* @brief The Screen interface represents a screen, usually the one on which the
* current window is being rendered, and is obtained using window.screen.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
* @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Screen
*
*/

#ifndef GUINOX_SCREEN_HPP
#define GUINOX_SCREEN_HPP

#ifndef __ANDROID__
#include <GLFW/glfw3.h>
#endif


#include "../DOM/EventTarget.hpp"
#include <string>
#include <vector>
#include <map>

class Screen : public EventTarget {

    friend class Window;


#ifndef __ANDROID__
    static std::map<GLFWmonitor *, std::vector<Screen *>> screens;


    static void monitor_callback(GLFWmonitor *monitor, int event);

    GLFWmonitor *monitor = nullptr;

    explicit Screen(GLFWmonitor *monitor);

    void initProperties(GLFWmonitor *monitor);

#endif




    int _colorDepth[3] = {0};
    int _height = 0;
    int _heightMM = 0;
    int _left = 0;
    std::string _name;
    int _refreshRate = 0;
    int _top = 0;
    int _width = 0;
    int _widthMM = 0;




    Screen();


public:

    //static std::vector<Screen> getAllScreens();


    const int *const colorDepth = _colorDepth;
    const int & height = _height;
    const int & heightMM = _heightMM;
    const int & left = _left;
    const std::string & name = _name;
    //ScreenOrientation orientation
    const int & refreshRate = _refreshRate;
    const int & top = _top;
    const int & width = _width;
    const int & widthMM = _widthMM;


    ~Screen() override;

    bool operator== (const Screen &rhs) const ;
    inline bool operator!= (const Screen &rhs) const { return !(this->operator==(rhs)); }

    Screen & operator= (const Screen & rh);

    //void lockOrientation();
    //void unlockOrientation()
};


#endif //GUINOX_SCREEN_HPP
