/**
* @brief The Window interface represents a window containing a DOM document;
* the document property points to the DOM document loaded in that window.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
 *
 * @details Is based on https://developer.mozilla.org/en-US/docs/Web/API/Window
*
*/

#ifndef GUINOX_WINDOW_HPP
#define GUINOX_WINDOW_HPP


#include "../Utils/Timer.hpp"
#include "../DOM/EventTarget.hpp"
#include "../DOM/Document.hpp"
#include "Screen.hpp"
#include "../RenderContext/Presentation.hpp"
#include <thread>
#include <queue>

#ifndef __ANDROID__
#include <GLFW/glfw3.h>
#endif


class Window :
        public EventTarget,
        protected Presentation {


#ifndef __ANDROID__
    friend class GLFW_Interface;


    static std::deque<GLFWwindow *> glfwWindows;
    static std::deque<Window *> windows;
    GLFWwindow *glfwWindow;


    static GLFWwindow * makeGLFWWindow();
    static std::pair<std::string,std::string> getFeatureValue(const std::string & feature);
    static void setHints(const std::vector<std::string> & features);
    static Window *getWindow(GLFWwindow *glfwWindow);

    static void character_callback(GLFWwindow* window, unsigned int codepoint);
    static void window_size_callback(GLFWwindow* window, int width, int height);
    static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
    static void window_content_scale_callback(GLFWwindow* window, float xscale, float yscale);
    static void window_pos_callback(GLFWwindow* window, int xpos, int ypos);
    static void window_iconify_callback(GLFWwindow* window, int iconified);
    static void window_maximize_callback(GLFWwindow* window, int maximized);
    static void window_focus_callback(GLFWwindow* window, int focused);
    static void drop_callback(GLFWwindow* window, int count, const char** paths);


    void setFeatures(const std::vector<std::string> & features);
    void setCallbacks();


public:
    void maximize();
    void minimize();
    void moveBy(int deltaX, int deltaY);
    void moveTo(int x, int y);
    void requestAttention();
    void resizeBy(int xDelta, int yDelta);
    void resizeTo(int width, int height);
    void restore();
    void run();
    void sizeToContent();
    void setOpacity(float v);
#else
    ;
#endif

private:

    bool _closed = false;
    double _devicePixelRatio = 1;
    bool _fullScreen = false;
    int _innerHeight;
    int _innerWidth;
    int _length;
    int _innerScreenX;
    int _innerScreenY;
    float _opacity = 1;
    int _outerHeight;
    int _outerWidth;
    Screen _screen;
    int _scrollX;
    int _scrollY;
    int _screenX;
    int _screenY;
    bool _visible = true;

    double deltaTime;
    Timer _timer;






    Window(std::string  windowName, const std::string & windowFeatures = "");

    void focus();
    void mainLoop();
    void setSizes();
    void setTitle(const std::string & title);
    void setMediumFeatures();
    void updateMediumFeatures();

public:

    const bool & closed = _closed;
    const double & devicePixelRatio = _devicePixelRatio;
    const int & innerHeight = _innerHeight;
    const int & innerWidth = _innerWidth;
    const int & length = _length;
    //std::string location;
    //menubar
    const int & innerScreenX = _innerScreenX;
    const int & innerScreenY = _innerScreenY;
    const bool & isFullScreen = _fullScreen;
    const bool & isVisible = _visible;
    std::string name;
    //Navigator navigator
    const float & opacity = _opacity;
    const int & outerHeight = _outerHeight;
    const int & outerWidth = _outerWidth;
    const int & scrollX = _scrollX;
    const int & scrollY = _scrollY;
    const int & pageXOffset = scrollX;
    const int & pageYOffset = scrollY;
    const Screen & screen = _screen;
    const int & screenX = _screenX;
    const int & screenLeft = _screenX;
    const int & screenY = _screenY;
    const int & screenTop = _screenY;
    //scrollbars;
    //const VisualViewport & visualViewport = _visualViewport;

    sp_Attr operator[] (size_t index);
    sp_Attr operator[] (size_t index) const ;


    static std::shared_ptr<Window> open(const std::string & url, const std::string & windowName, const std::string & windowFeatures = "");

    ~Window() override;

    void alert(const std::string & message);
    void blur();
    bool confirm(const std::string & message);
    void fullScreen(bool f);
    //CSSStyleDeclaration getComputedStyle(DOM_Node::sp_Element, const std::string & pseudoElt = "");
    //Selection getSelection();
    void hide();
    //MediaQueryList matchMedia(const std::string & mediaQueryString);
    //void postMessage(const std::string & message, Window targetOrigin, Transferable transfer);
    std::string prompt(const std::string & message, const std::string & defaulte);
    //void print();

    void scroll(int xCoord, int yCoord);
    //void scroll(ScrollToOptions options);
    void scrollBy(int xCoord, int yCoord);
    //void scrollBy(ScrollToOptions options);
    void scrollTo(int xCoord, int yCoord);
    //void scrollTo(ScrollToOptions options);
    void show();
    //void stop();


    CSSStyleDeclaration getComputedStyle(DOM_Node::sp_Element * elt, const std::string & pseudoElt = "");

};


#endif //GUINOX_WINDOW_HPP
