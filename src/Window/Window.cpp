#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include "GLFW_Interface.hpp"
#endif

#include "Window.hpp"
#include <DOM/DOMParser.hpp>
#include <iostream>
#include <Utils/Utils.hpp>
#include <utility>



#ifndef __ANDROID__

std::deque<GLFWwindow *> Window::glfwWindows;
std::deque<Window *> Window::windows;


//########################### GLOBALS ###########################//


static void error_callback(int error, const char* description) {
    fprintf(stderr, "Error %d: %s\n", error, description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    printf("Key Event: key %s ,%c,%d, scancode = %d, action = %d, mods = %d\n", glfwGetKeyName(key, scancode),key,key,scancode,action,mods);
}

void Window::character_callback(GLFWwindow* glfwWindow, unsigned int codepoint) {
    printf("Text Event: key = %c, %d\n", codepoint, codepoint);

    Window * window = getWindow(glfwWindow);

    if (window) {
        window->fullScreen(!(window->isFullScreen));
    }
}

void Window::window_size_callback(GLFWwindow *glfwWindow, int width, int height) {
    printf("window size Event: w = %d, h = %d\n", width, height);

    Window *window = getWindow(glfwWindow);

    int top, left, right,  bottom;
    glfwGetWindowFrameSize(glfwWindow, &left, &top, &right, &bottom);

    window->_outerHeight = height + top + bottom;
    window->_outerWidth = width + left + right;
}

void Window::framebuffer_size_callback(GLFWwindow *glfwWindow, int width, int height) {
    printf("framebuffer size Event: w = %d, h = %d\n", width, height);

    Window *window = getWindow(glfwWindow);

    window->_innerHeight = height;
    window->_innerWidth = width;

    window->updateMediumFeatures();

    window->viewport.updateSizes(width, height);
    window->viewport.updateResolution(25.4*(window->_h_resolution), 25.4*(window->_v_resolution));

    //todo, para mejorar el recargado de fullreflow por cambio de tamaño en pantalla.
    // 1.- mantener el contexto del Viewport, hasta que Reflow lo sustituya
    // 2.- mantener un bloque padre (padre de la raiz del HTML) que permanezca con las mismas dimensiones hasta que la ventana detecte que se dejo de mover o un intervalo,
    //     construir arbol nuevo y reemplazar por el viejo, para que no tenga que estar actualizando todo el tiempo
    auto htmlElement = std::dynamic_pointer_cast<HTMLElement>(window->document->documentElement);
    window->fullReFlow();
}

void Window::window_content_scale_callback(GLFWwindow *window, float xscale, float yscale) {
    printf("window content scale Event: xscale = %d, yscale = %d\n", xscale, yscale);
}

void Window::window_pos_callback(GLFWwindow *glfwWindow, int xpos, int ypos) {
    printf("window pos Event: x = %d, y = %d\n", xpos, ypos);

    Window *window = getWindow(glfwWindow);

    window->_innerScreenX = xpos;
    window->_innerScreenY = ypos;

    int top, left, right,  bottom;
    glfwGetWindowFrameSize(glfwWindow, &left, &top, &right, &bottom);

    window->_screenX = xpos;
    window->_screenY = ypos - top;

    printf("window pos: ix = %d, iy = %d, sx = %d, sy = %d\n", window->_innerScreenX, window->_innerScreenY, window->_screenX, window->_screenY);
}

void Window::window_iconify_callback(GLFWwindow *window, int iconified) {
    printf("window iconify Event: minimized = %d\n", iconified);
}

void Window::window_maximize_callback(GLFWwindow *window, int maximized) {
    printf("window maximize Event: maximized = %d\n", maximized);
}

void Window::window_focus_callback(GLFWwindow *window, int focused) {
    printf("window focus Event: focused = %d\n", focused);
}

void Window::drop_callback(GLFWwindow *window, int count, const char **paths) {
    for (int i = 0;  i < count;  i++) {
        printf("window drop files event: Path[%d] = %s\n", i, paths[i]);
    }
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) {
    std::cout << "Cursor at x:" << xpos << " y:" << ypos <<std::endl;
}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    std::string btn;
    switch (button) {
        case GLFW_MOUSE_BUTTON_4: btn = "4";
            break;
        case GLFW_MOUSE_BUTTON_5: btn = "5";
            break;
        case GLFW_MOUSE_BUTTON_6: btn = "6";
            break;
        case GLFW_MOUSE_BUTTON_7: btn = "7";
            break;
        case GLFW_MOUSE_BUTTON_LAST: btn = "LAST"; //GLFW_MOUSE_BUTTON_8
            break;
        case GLFW_MOUSE_BUTTON_LEFT: btn = "LEFT"; //GLFW_MOUSE_BUTTON_1
            break;
        case GLFW_MOUSE_BUTTON_MIDDLE: btn = "MIDDLE"; //GLFW_MOUSE_BUTTON_3
            break;
        case GLFW_MOUSE_BUTTON_RIGHT: btn = "RIGHT"; //GLFW_MOUSE_BUTTON_2
            break;
    }

    std::string act;
    switch (action) {
        case GLFW_PRESS: act = "PRESSED";
            break;
        case GLFW_RELEASE: act = "RELEASED";
            break;
    }

    std::cout << "Mouse Event: The button " << btn << " was " << act << " with mods " << mods << std::endl;
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    std::cout << "xoffset: " << xoffset << ", yoffset: " << yoffset << std::endl;
}

//########################### END GLOBALS ###########################//



//############################# Window #############################//

//----------------------------- STATICS -----------------------------//

//----------------------------- PRIVATE -----------------------------//

GLFWwindow* Window::makeGLFWWindow() {
    GLFWwindow * glfwWindow = nullptr;

    GLFW_Interface::doInMainGLFWThread([&] {
        /* Create a windowed mode window and its OpenGL context */
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

        glfwWindow = glfwCreateWindow(800, 600, "Hello World", nullptr, nullptr);

        glfwDefaultWindowHints();
    }, true);

    glfwWindows.push_back(glfwWindow);

    /* Make the window's context current */
    glfwMakeContextCurrent(glfwWindow);

    gladLoadGL(glfwGetProcAddress);
    glfwSwapInterval(1);

    glfwMakeContextCurrent(nullptr);

    return glfwWindow;
}

std::pair<std::string,std::string> Window::getFeatureValue(const std::string &feature) {
    Parser parser(feature);

    std::pair<std::string,std::string> pair;

    pair.first = Utils::toLower( parser.getUntilFirstOf("=") );

    if (++parser) {
        auto value = Utils::toLower( parser.getUntilFirstOf(",") );

        if (value == "no" || value == "0" || value == "off") {
            pair.second = "off";

        } else if (value == "yes" || value == "1" || value == "on") {
            pair.second = "on";

        } else {
            pair.second = value;
        }

    } else {
        pair.second = "on";
    }

    return pair;
}

void Window::setHints(const std::vector<std::string> &features) {
    std::vector<std::pair<int, int>> hints;

    for (const auto & feature : features) {
        auto pair = getFeatureValue(feature);

        if (pair.first == "minimizable") {
            if (pair.second == "off") {
                hints.emplace_back(GLFW_DECORATED, GLFW_FALSE);
            }

        } else if (pair.first == "maximized") {
            if (pair.second == "on") {
                hints.emplace_back(GLFW_MAXIMIZED, GLFW_TRUE);
            }

        } else if (pair.first == "resizable") {
            if (pair.second == "off") {
                hints.emplace_back(GLFW_RESIZABLE, GLFW_FALSE);
            }

        } else if (pair.first == "titlebar") {
            if (pair.second == "off") {
                hints.emplace_back(GLFW_DECORATED, GLFW_FALSE);
            }

        } else if (pair.first == "alwaysOnTop" || pair.first == "z-lock"){
            if (pair.second == "on") {
                hints.emplace_back(GLFW_FLOATING, GLFW_TRUE);
            }

        } else if (pair.first == "close") {
            if (pair.second == "off") {
                hints.emplace_back(GLFW_DECORATED, GLFW_FALSE);
            }

        } else if (pair.first == "alwaysLowered") {
            if (pair.second == "off") {
                hints.emplace_back(GLFW_FLOATING, GLFW_TRUE);
            }

        } else if (pair.first == "transparent_framebuffer") {
            if (pair.second == "on") {
                hints.emplace_back(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE);
            }
        }
    }

    if (!hints.empty()) {
        GLFW_Interface::doInMainGLFWThread([=] {
            for (auto const &hint : hints) {
                glfwWindowHint(hint.first, hint.second);
            }
        });
    }
}

Window* Window::getWindow(GLFWwindow *glfwWindow) {
    for (auto & window : windows) {
        if (window->glfwWindow == glfwWindow) {
            return window;
        }
    }

    return nullptr;
}

#else
;
#endif


//----------------------------- PUBLIC -----------------------------//

std::shared_ptr<Window> Window::open(const std::string &url, const std::string &windowName, const std::string &windowFeatures) {
    auto window = std::shared_ptr<Window>( new Window(windowName, windowFeatures) );

    auto document = DOMParser::parseFromFile(url);

    document->addPresentation(window.get());

    return window;
}

//+++++++++++++++++++++++++++++ CLASS ++++++++++++++++++++++++++++++//

//++++++++++++++++++++++++++++ PRIVATE +++++++++++++++++++++++++++++//

Window::Window(std::string windowName, const std::string &windowFeatures) :
        name(std::move(windowName)),
#ifndef __ANDROID__
        Presentation(
                Presentation::SCREEN,
                [&]{ glfwMakeContextCurrent(glfwWindow); },
                [&]{ glfwMakeContextCurrent(nullptr); }
                ),
#else
        Presentation(Presentation::SCREEN),
#endif
        _timer(true) {



    std::vector<std::string> features;

    if (!windowFeatures.empty()) {
        features = Utils::split_string_by(windowFeatures, ',');

        ///setHints(features);
    }

    ///glfwWindow = makeGLFWWindow();

    setSizes();
    ///if (!windowFeatures.empty()) setFeatures(features);
    //setSizes();
    ///setCallbacks();

    ///windows.push_back(this);
}

void Window::focus() {
    /*GLFW_Interface::doInMainGLFWThread([=] {
        glfwFocusWindow(glfwWindow);
    });*/
}

void Window::mainLoop() {
    //viewport.lockContext();

    //glEnable(GL_CULL_FACE);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//GL_ONE
    //glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    ////glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_DST_ALPHA);//GL_ONE

    ////glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &gMaxRenderbufferSize);

    ///* Loop until the user closes the window */
    //while (!glfwWindowShouldClose(glfwWindow)) {
    //    viewport.lockContext();
    //    /* Render here */
    //    glViewport(0, 0, _innerWidth, _innerHeight);
    //    glClear(GL_COLOR_BUFFER_BIT);

    //    deltaTime = _timer.getMsFromLastCall();
    //    if (frameTree) {
    //        auto temp = frameTree; //fixme race problem, destroyed while been copying
    //        temp->fullRender();
    //    }

    //    /* Swap front and back buffers */
    //    glfwSwapBuffers(glfwWindow);
    //    viewport.freeContext();

    //    //std::this_thread::sleep_for(std::chrono::milliseconds(20));
    //}

    //auto end = glfwWindows.end();
    //for (auto it = glfwWindows.begin(); it != end; it++) {
    //    if ((*it) == glfwWindow) {
    //        glfwWindows.erase(it);
    //        break;
    //    }
    //}

    //GLFW_Interface::doInMainGLFWThread([&] {
    //    glfwDestroyWindow(glfwWindow);
    //}, true);

    //if (GLFW_Interface::initialized && glfwWindows.empty()) {
    //    GLFW_Interface::terminate();
    //}
}

void Window::setSizes() {
    /*GLFW_Interface::doInMainGLFWThread([this] {
        glfwGetWindowSize(glfwWindow, &_innerWidth, &_innerHeight);

        int top, left, right,  bottom;
        glfwGetWindowFrameSize(glfwWindow, &left, &top, &right, &bottom);

        _outerHeight = _innerHeight + top + bottom;
        _outerWidth = _innerWidth + left + right;

        _devicePixelRatio = ((double)screen.width/screen.widthMM) / ((double)screen.height/screen.heightMM);

        updateMediumFeatures();

        viewport.updateSizes(_innerWidth, _innerHeight);
        viewport.updateResolution(25.4*(_h_resolution), 25.4*(_v_resolution));
    });*/
}

void Window::setTitle(const std::string &title) {
    /*GLFW_Interface::doInMainGLFWThread([=]() {
        glfwSetWindowTitle(glfwWindow, title.data());
    });*/
}

void Window::setMediumFeatures() {
    features["any-hover"] = "hover";
    features["any_pointer"] = "fine";
    features["aspect-ratio"] = std::to_string(((double)_innerWidth)/_innerHeight); //
    features["color"] = std::to_string(screen.colorDepth[0]);
    features["color-gamut"] = "srgb";
    features["color-index"] = "0";
    features["grid"] = "0";
    features["height"] = std::to_string(_innerHeight) + "px"; //
    features["display-mode"] = "standalone"; //
    features["hover"] = "hover";
    features["inverted-colors"] = "none";
    features["light-level"] = "normal";
    features["monochrome"] = "0";
    features["max-aspect-ratio"] = std::to_string(screen.width); //
    features["max-color"] = std::to_string(screen.colorDepth[0]);
    features["max-color-index"] = "0";
    features["max-height"] = std::to_string(screen.height) + "px"; //
    features["max-monochrome"] = "0";
    features["max-resolution"] = std::to_string(25.4*screen.width/((double)screen.widthMM)) + "dpi";
    features["max_width"] = std::to_string(screen.width) + "px"; //
    features["min_aspect_ratio"] = "0";
    features["min_color"] = "1";
    features["min_color_index"] = "0";
    features["min_height"] = "0px";
    features["min_monochrome"] = "0";
    features["min_resolution"] = "0dpi";
    features["orientation"] = ((double)_innerWidth)/_innerHeight >= 1 ? "landscape" : "portrait" ;
    features["overflow_block"] = "none";
    features["overflow_inline"] = "none";
    features["pointer"] = "fine";
    features["resolution"] = std::to_string((25.4*screen.width/((double)screen.widthMM))) + "dpi";
    features["scan"] = "progressive";
    features["scripting"] = "none"; ///
    features["update"] = "fast";
    features["width"] = std::to_string(_innerWidth) + "px"; //

    _h_resolution = screen.width/((double)screen.widthMM);
    _v_resolution = screen.height/((double)screen.heightMM);
}

void Window::updateMediumFeatures() {
    features["aspect-ratio"] = std::to_string(((double)_innerWidth)/_innerHeight); //
    features["height"] = std::to_string(_innerHeight) + "px"; //
    features["display-mode"] = "standalone"; //
    features["max-aspect-ratio"] = std::to_string(screen.width); //
    features["max-height"] = std::to_string(screen.height) + "px"; //
    features["max-resolution"] = std::to_string(25.4*screen.width/((double)screen.widthMM)) + "dpi";
    features["max_width"] = std::to_string(screen.width) + "px"; //
    features["orientation"] = ((double)_innerWidth)/_innerHeight >= 1 ? "landscape" : "portrait" ;
    features["resolution"] = std::to_string(25.4*screen.width/((double)screen.widthMM)) + "dpi";
    features["width"] = std::to_string(_innerWidth) + "px"; //

    _h_resolution = screen.width/((double)screen.widthMM);
    _v_resolution = screen.height/((double)screen.heightMM);
}

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

Window::~Window() {
    /*auto end = windows.end();
    for (auto it = windows.begin(); it != end; it++) {
        if ((*it) == this) {
            windows.erase(it);
            break;
        }
    }*/
}

void Window::fullScreen(bool f) {
    /*if (f && !_fullScreen) {
        GLFW_Interface::doInMainGLFWThread([=] {
            const GLFWvidmode *mode = glfwGetVideoMode(screen.monitor);

            glfwSetWindowMonitor(glfwWindow, screen.monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
        });

        _fullScreen = true;

    } else if (!f && _fullScreen) {
        GLFW_Interface::doInMainGLFWThread([=] {
            glfwSetWindowMonitor(glfwWindow, nullptr, 40, 40, 800, 600, 0);
        });

        _fullScreen = false;
    }*/
}

void Window::hide() {
    /*GLFW_Interface::doInMainGLFWThread([=] {
        glfwHideWindow(glfwWindow);
    });*/
}



#ifndef __ANDROID__

void Window::setFeatures(const std::vector<std::string> & features) {
    for (const auto & feature : features) {
        auto pair = getFeatureValue(feature);

        if (pair.first == "fullscreen") {
            if (pair.second == "on") {
                fullScreen(true);
            }

        } else if (pair.first == "left") {


        } else if (pair.first == "top") {


        } else if (pair.first == "height") {
            //resizeTo(_innerWidth, std::stoi(pair.second));

        } else if (pair.first == "width") {
            //resizeTo(std::stoi(pair.second), _innerHeight);

        } else if (pair.first == "opacity") {
            float v = std::stof(pair.second);
            setOpacity(v);

        } else if (pair.first == "ratio") {
            //glfwSetWindowAspectRatio(window, width, height);

        } else if (pair.first == "visible") {
            if (pair.second == "off") {
                hide();
            }
        }
    }
}

void Window::setCallbacks() {
    /* Make the window's context current */
    glfwMakeContextCurrent(glfwWindow);

    // pass cb to watch events
    glfwSetKeyCallback(glfwWindow, key_callback);
    glfwSetMouseButtonCallback(glfwWindow, mouse_button_callback);
    glfwSetCursorPosCallback(glfwWindow, cursor_position_callback);
    glfwSetCharCallback(glfwWindow, character_callback);
    glfwSetScrollCallback(glfwWindow, scroll_callback);
    glfwSetWindowSizeCallback(glfwWindow, window_size_callback);
    glfwSetFramebufferSizeCallback(glfwWindow, framebuffer_size_callback);
    glfwSetWindowContentScaleCallback(glfwWindow, window_content_scale_callback);
    glfwSetWindowPosCallback(glfwWindow, window_pos_callback);
    glfwSetWindowIconifyCallback(glfwWindow, window_iconify_callback);
    glfwSetWindowMaximizeCallback(glfwWindow, window_maximize_callback);
    glfwSetWindowFocusCallback(glfwWindow, window_focus_callback);
    glfwSetDropCallback(glfwWindow, drop_callback);

    glfwMakeContextCurrent(nullptr);
}

void Window::maximize() {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwMaximizeWindow(glfwWindow);
    });
}

void Window::minimize() {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwIconifyWindow(glfwWindow);
    });
}

void Window::moveBy(int deltaX, int deltaY) {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwSetWindowPos(glfwWindow, _innerScreenX + deltaX, _innerScreenY + deltaY);
    });
}

void Window::moveTo(int x, int y) {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwSetWindowPos(glfwWindow, x, y);
    });
}

void Window::requestAttention() {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwRequestWindowAttention(glfwWindow);
    });
}

void Window::resizeTo(int width, int height) {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwSetWindowSize(glfwWindow, width, height);
    });
}

void Window::resizeBy(int xDelta, int yDelta) {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwSetWindowSize(glfwWindow, _innerWidth + xDelta, _innerHeight + yDelta);
    });
}

void Window::restore() {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwRestoreWindow(glfwWindow);
    });
}

void Window::run() {
    std::thread([this] { mainLoop(); }).detach();
}

void Window::setOpacity(float v) {
    GLFW_Interface::doInMainGLFWThread([=] {
        glfwSetWindowOpacity(glfwWindow, v);
    });

    _opacity = v;
}
#endif

void Window::show() {
    /*GLFW_Interface::doInMainGLFWThread([=] {
        glfwShowWindow(glfwWindow);
    });*/
}

//############################ END WINDOW ############################//
