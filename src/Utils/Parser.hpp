/**
* @brief Linear parser that does not generate a tree, it just goes through a chain and finds matches.
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
*/

#ifndef GUINOX_PARSER_HPP
#define GUINOX_PARSER_HPP


#include <vector>
#include <string>

class Parser {
    const char *str_data = nullptr;
    const char **p;
    const char *const end;

    std::vector< std::pair<std::string, std::string> > commentsTags;

    bool skipComment();

public:
    Parser(const char **buff, size_t size) : str_data(*buff), p(buff), end((*buff)+size) { }
    Parser(const std::string & str) : str_data(str.data()), p(&str_data), end(str_data+str.size()) {}

    inline explicit operator bool() const {
        return *p && *p < end;
    }

    inline Parser & operator++() { // prefix
        (*p)++;
        return *this;
    }

    inline const Parser operator++(int) { // postfix
        Parser tmp(*this);
        operator++(); // prefix-increment this instance
        return tmp;   // return value before increment
    }

    inline Parser & operator +=(const int & n) {
        (*p) += n;
        return *this;
    }

    inline char getChar(int n = 0) const { return *((*p) + n); }
    bool charIsOneOf(const char *cc);
    bool isString(const std::string & str);
    inline Parser & skipSpaces() { return skip(" \t"); }
    inline Parser & skipBlanks() { return skip(" \n\r\t"); } // \f?
    Parser & skip(const char *cc);
    Parser & goUntil(const char *cc);
    std::string getUntilEnd();
    std::string getUntilFirstOf(const char *cc);
    std::string getUntilStr(const std::string & str);
    std::string getInsideParentheses();
    std::string getInsideQuotes();
    std::vector<std::string> splitBy(const char *cc);
    void addCommentTag(const std::string & open, const std::string & close);

    Parser & goBackUntilNotBlank();


};


#endif //GUINOX_PARSER_HPP
