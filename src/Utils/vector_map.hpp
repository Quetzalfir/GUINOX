#ifndef GUINOX_VECTOR_MAP_HPP
#define GUINOX_VECTOR_MAP_HPP

#include <vector>
#include <utility>


template <class T, class U>
class vector_map {
    typedef typename std::vector<std::pair<T, U>>::iterator iterator;
    typedef typename std::vector<std::pair<T, U>>::const_iterator const_iterator;

    std::vector<std::pair<T, U>> container;


public:

    U &operator[](const T & t) {
        for (auto & p : container ){
            if (p.first == t) return p.second;
        }

        std::pair<T,U> aux = {t,U()};
        container.push_back(aux);

        return container.back().second;
    }

    void reserve(size_t size) {
        container.reserve(size);
    }

    inline bool empty() const { return container.empty(); }
    inline size_t size() const { return container.size(); }


    inline iterator begin() noexcept { return container.begin(); }
    //inline const_iterator cbegin() const noexcept { return container.cbegin(); }
    inline iterator end() noexcept { return container.end(); }
    //inline const_iterator cend() const noexcept { return container.cend(); }

};


#endif //GUINOX_VECTOR_MAP_HPP
