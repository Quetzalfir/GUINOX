#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#endif


#include "PNG.hpp"



PNG::~PNG() {
    delete[] pixels;
    if (png && !error) { png_destroy_read_struct(&png, &info, nullptr); }
}

PNG::PNG(const std::string& path) {
    FILE *f = std::fopen(path.c_str(), "rb");
    if (f == nullptr) {
        error = FILE_NOT_OPENED;
        return;
    }

    // check that the PNG signature is in the file header
    unsigned char sig[8];
    if (fread(sig, 1, sizeof(sig), f) < 8) {
        std::fclose(f);
        error = ERROR_;
        return;
    }

    onError = [&](){ std::fclose(f); };

    if (!checkPNG(sig)) { return; }

    if (!init()) { return; }

    // set libpng error handling mechanism
    if (setjmp(png_jmpbuf(png))) {
        png_destroy_read_struct(&png, &info, nullptr);
        onError();

        if (pixels != nullptr) {
            delete [] pixels;
            pixels = nullptr;
        }
        error = ERROR_;
        return;
    }

    // pass open file to png struct
    png_init_io(png, f);

    // skip signature bytes (we already read those)
    png_set_sig_bytes(png, sizeof(sig));

    // get image information
    png_read_info(png, info);

    width = png_get_image_width(png, info);
    height = png_get_image_height(png, info);

    applyTransforms();

    if (!getGLFormat()) { return; }

    png_uint_32 bpp = (png_uint_32)png_get_rowbytes(png, info) / width;

    // allocate pixel buffer
    pixels = new unsigned char[width * height * bpp];

    // setup array with row pointers into pixel buffer
    png_bytep rows[height];
    unsigned char *p = pixels;
    for(int i = 0; i < height; i++) {
        rows[i] = p;
        p += width * bpp;
    }

    // read all rows (data goes into 'pixels' buffer)
    // Note that any decoding errors will jump to the
    // setjmp point and eventually return false
    png_read_image(png, rows);

    png_read_end(png, nullptr);

    ///png_destroy_read_struct(&png, &info, nullptr);
    std::fclose(f);
    //delete [] rows;
}

/*static void readFromBuffer(png_structp pngPtr, png_bytep data, png_size_t length) {
    //Here we get our IO pointer back from the read struct.
    //This is the parameter we passed to the png_set_read_fn() function.
    //Our Asset pointer.
    const Asset* asset = (Asset *)png_get_io_ptr(pngPtr);
    //Cast the pointer to Asset* and read 'length' bytes into 'data'
    asset->read(data, length);
}*/

/*PNG::PNG(const Asset &asset) {
    png_byte buff[8];
    asset.read(buff, 8);
    if (!checkPNG((unsigned char *)buff)) { return; }

    if (!init()) { return; }

    // set libpng error handling mechanism
    if (setjmp(png_jmpbuf(png))) {
        png_destroy_read_struct(&png, &info, nullptr);

        if (pixels != nullptr) {
            delete [] pixels;
            pixels = nullptr;
        }
        error = ERROR_;
        return;
    }

    png_set_read_fn(png, (png_voidp)&asset, readFromBuffer);

    // skip signature bytes (we already read those)
    png_set_sig_bytes(png, 8);

    // get image information
    png_read_info(png, info);

    width = png_get_image_width(png, info);
    height = png_get_image_height(png, info);

    applyTransforms();

    if (!getGLFormat()) { return; }

    png_uint_32 bpp = (uint)png_get_rowbytes(png, info) / width;

    // allocate pixel buffer
    pixels = new unsigned char[width * height * bpp];

    // setup array with row pointers into pixel buffer
    png_bytep rows[height];
    unsigned char *p = pixels;
    for(int i = 0; i < height; i++) {
        rows[i] = p;
        p += width * bpp;
    }

    // read all rows (data goes into 'pixels' buffer)
    // Note that any decoding errors will jump to the
    // setjmp point and eventually return false
    png_read_image(png, rows);

    png_read_end(png, nullptr);

    ///png_destroy_read_struct(&png, &info, nullptr);
}*/

bool PNG::checkPNG(const unsigned char *buff) {
    // check that the PNG signature is in the file header
    if (png_sig_cmp(buff, 0, 8)) {
        onError();
        error = NOT_PNG;
        return false;
    }

    return true;
}

bool PNG::init() {
    // create two data structures: 'png_struct' and 'png_info'
    png = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (png == nullptr) {
        onError();
        error = CANT_PNG_STRUCT;
        return false;
    }

    info = png_create_info_struct(png);
    if (info == nullptr) {
        png_destroy_read_struct(&png, nullptr, nullptr);
        onError();
        error = CANT_INFO_STRUCT;
        return false;
    }

    return true;
}

void PNG::applyTransforms() {
    // if transparency, convert it to alpha
    if (png_get_valid(png, info, PNG_INFO_tRNS)) {
        png_set_tRNS_to_alpha(png);
    }

    if (png_get_bit_depth(png, info) == 16)
        png_set_strip_16(png);

    // set least one byte per channel
    if (png_get_bit_depth(png, info) < 8) {
        png_set_packing(png);
    }

    // Just for Android
    //png_set_bgr(png);

    png_set_interlace_handling(png);

    png_read_update_info(png, info);
}

bool PNG::getGLFormat() {
    if (error) { return false; }

    switch(png_get_color_type(png, info)) {
        case PNG_COLOR_TYPE_GRAY:
            format = GL_LUMINANCE;
            break;

        case PNG_COLOR_TYPE_GRAY_ALPHA:
            format = GL_LUMINANCE_ALPHA;
            break;

        case PNG_COLOR_TYPE_PALETTE:
            format = GL_RGB;
            png_set_expand(png);
            break;

        case PNG_COLOR_TYPE_RGB:
            format = GL_RGB;
            break;

        case PNG_COLOR_TYPE_RGBA:
            format = GL_RGBA;
            break;

        default:
            format = -1;
    }
    if (format == -1) {
        png_destroy_read_struct(&png, &info, nullptr);
        onError();
        error = FORMAT_UNDEFINED;
        return false;
    }

    return true;
}

char PNG::write(FILE *file, void *data) {
    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (!png) { return CANT_PNG_STRUCT; }

    png_infop info = png_create_info_struct(png);
    if (!info) {
        png_destroy_write_struct(&png, nullptr);
        return CANT_INFO_STRUCT;
    }

    if (setjmp(png_jmpbuf(png))) {
        png_destroy_write_struct(&png, &info);
        return ERROR_;
    }

    png_init_io(png, file);


    return ALL_OK;
}