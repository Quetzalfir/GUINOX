#ifndef GUINOX_SYNCHRONIZER_HPP
#define GUINOX_SYNCHRONIZER_HPP

#include <thread>
#include <mutex>
#include <deque>
#include <functional>


class Synchronizer {
    bool locked = false;
    std::mutex mutex;
    std::thread::id blockingThread;
    std::thread::id goThread;
    std::deque<std::thread::id> waitingThreads;

    std::function<void(void)> blockingContext, freeingContext;


public:
    Synchronizer();
    Synchronizer(std::function<void ()> block, std::function<void ()> free);

    void lockContext();
    void freeContext();
};


#endif //GUINOX_SYNCHRONIZER_HPP
