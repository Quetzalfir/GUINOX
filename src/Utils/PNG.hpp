#ifndef GUINOX_PNG_HPP
#define GUINOX_PNG_HPP

#include <png.h>
#include <string>
#include <functional>
//#include "../Asset.h"

class PNG {
    enum ERROR : char {
        ALL_OK = 0,
        FILE_NOT_OPENED = 1,
        NOT_PNG = 2,
        ERROR_ = 3,
        FORMAT_UNDEFINED = 4,
        CANT_PNG_STRUCT = 5,
        CANT_INFO_STRUCT = 6
    };

    png_structp png;
    png_infop info;
    ERROR error = ALL_OK;
    int format;
    png_uint_32 height = 0;
    png_uint_32 width = 0;
    unsigned char *pixels = nullptr;

    std::function<void (void)> onError = [](){};

    bool checkPNG(const unsigned char * buff);
    bool init();
    void applyTransforms();
    bool getGLFormat();

public:
    PNG(const std::string& path);
    //PNG(const Asset &asset);
    ~PNG();

    static char write(FILE *file, void *data);

    char getError() const { return error;}
    int getFormat() const { return format;}
    png_uint_32 getHeight() const { return height;}
    png_uint_32 getWidth() const { return width;}
    unsigned char *getPixels() { return pixels;}

};


#endif //GUINOX_PNG_HPP
