#include "Timer.hpp"


long long Timer::dt;

Timer::Timer(bool auto_begin) {
    if (auto_begin) { start(); }
}

void Timer::setPeriod(double millis) {
    period = std::chrono::microseconds((long long)(millis*1000));
}

bool Timer::periodCompleted(double millis) {
    setPeriod(millis);
    return periodCompleted();
}

bool Timer::periodCompleted() {
    auto elapsed = duration_cast<microseconds>(steady_clock::now() - chkPoint);

    if ((elapsed) >= period) {
        chkPoint = (chkPoint + period); // asi es mas preciso
        //chkPoint = steady_clock::now();
        return true;
    }

    return false;
}

/*void Timer::setInterval(double millis) {
    interval = std::chrono::microseconds((long long)(millis*1000));
}

void Timer::onInterval(std::function<void(void)> cb) {
    if ( running() ) {
        auto elapsed = duration_cast<microseconds>(steady_clock::now() - chkPoint);

        if ((elapsed) >= interval) {
            // asi es mas preciso en lugar de actualizarlo co el tiempo actual
            chkPoint = (chkPoint + interval);

            cb();
        }
    }
}*/

long long Timer::getElapsedTime(Period period) {
    if ( running() ) {

        switch ( period ) {
            case SECONDS :
                return duration_cast<seconds>(steady_clock::now() - startPoint).count();
            case MILLISECONDS :
                return duration_cast<milliseconds>(steady_clock::now() - startPoint).count();
            case MICROSECONDS:
                return duration_cast<microseconds>(steady_clock::now() - startPoint).count();
        }
    }

    return 0;
}

void Timer::start() {
    if ( !started ) {
        restart();
    }
}

void Timer::restart() {
    chkPoint = delta = startPoint = steady_clock::now();
    stopped = false;
    started = true;
}

void Timer::stop() {
    if (!stopped) {
        end = steady_clock::now();
        stopped = true;
        started = false;
    }
}

void Timer::wait(long millis) {
    auto beg = steady_clock::now();
    long long elapsed;

    do {
        elapsed = duration_cast<milliseconds>(steady_clock::now() - beg).count();
    } while (elapsed < millis);
}

double Timer::getMsFromLastCall() {
    if ( running() ) {
        auto micros = duration_cast<microseconds>(steady_clock::now() - delta).count();

        delta = steady_clock::now();

        double millis = micros / 1000.0;

        return millis;
    } else {
        return 0;
    }

}