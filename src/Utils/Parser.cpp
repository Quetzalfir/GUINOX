#include "Parser.hpp"

bool Parser::skipComment() {
    for (const auto & comTag : commentsTags) {
        if ( isString(comTag.first) ) {
            while (*p < end && !isString(comTag.second)) {
                ++(*p);
            }

            break;
        }
    }

    return *p < end;
}

Parser & Parser::skip(const char *cc) {
    while (*p < end && skipComment()) {

        if (charIsOneOf(cc)) {
            ++(*p);
        } else {
            break;
        }
    }

    return *this;
}

Parser & Parser::goBackUntilNotBlank() {
    --(*p);
    if (charIsOneOf(" \n\r\t")) {
        while (*p >= str_data) {
            if (charIsOneOf(" \n\r\t")) {
                --(*p);

            } else {
                ++(*p);
                break;
            }
        }

    } else {
        ++(*p);
    }

    return *this;
}

bool Parser::charIsOneOf(const char *cc) {
    for (int i = 0; cc[i]; i++) {
        if (**p == cc[i]) {
            return true;
        }
    }

    return false;
}

bool Parser::isString(const std::string &str) {
    unsigned long length = str.length();

    if (end - *p >= length) {
        if (std::string(*p, length) == str) {
            *p += length;
            return true;
        }
    }

    return false;
}

Parser & Parser::goUntil(const char *cc) {
    while (*p < end && skipComment()) {
        if (charIsOneOf(cc)) {
            break;
        }

        ++(*p);
    }

    return *this;
}

std::string Parser::getUntilEnd() {
    const char *q = *p;
    std::string res;

    while (*p < end) {

        // ignore comments
        const char *o = *p;
        for (const auto & comTag : commentsTags) {

            if ( isString(comTag.first) ) {
                res += std::string(q, o-q);

                while (*p < end && !isString(comTag.second)) {
                    ++(*p);
                }

                q = *p;
                break;
            }
        }

        ++(*p);
    }

    return res + std::string(q, (*p)-q);
}

std::string Parser::getUntilFirstOf(const char *cc) {
    const char *q = *p;
    std::string res;

    while (*p < end) {

        // ignore comments
        const char *o = *p;
        for (const auto & comTag : commentsTags) {

            if ( isString(comTag.first) ) {
                res += std::string(q, o-q);

                while (*p < end && !isString(comTag.second)) {
                    ++(*p);
                }

                q = *p;
                break;
            }
        }

        if (charIsOneOf(cc)) {
            break;
        }

        ++(*p);
    }

    return res + std::string(q, (*p)-q);
}

std::string Parser::getUntilStr(const std::string & str) {
    const char *q = *p;
    std::string res;

    while (*p < end) {

        // ignore comments
        const char *o = *p;
        for (const auto & comTag : commentsTags) {

            if ( isString(comTag.first) ) {
                res += std::string(q, o-q);

                while (*p < end && !isString(comTag.second)) {
                    ++(*p);
                }

                q = *p;
                break;
            }
        }

        if (isString(str)) {
            break;
        }

        ++(*p);
    }

    return res + std::string(q, ((*p)-q)-str.length());
}

std::string Parser::getInsideParentheses() {
    char c = **p;
    if (c != '(' && c != '[' && c != '{' && c != '<' && c != '"' && c != '\'') {
        return "";
    }

    char close = ')';
    switch (c) {
        case '(': close = ')'; break;
        case '[': close = ']'; break;
        case '{': close = '}'; break;
        case '<': close = '>'; break;
    }

    const char *q = *p;
    std::string res;
    size_t n = 0;

    while (*p < end) {

        // ignore comments
        const char *o = *p;
        for (const auto & comTag : commentsTags) {

            if ( isString(comTag.first) ) {
                res += std::string(q, o-q);

                while (*p < end && !isString(comTag.second)) {
                    ++(*p);
                }

                q = *p;
                break;
            }
        }

        if (**p == c) n++;
        else if (**p == close) n--;

        if (!n) {
            break;
        }

        ++(*p);
    }

    ++(*p);

    return res + std::string(q+1, (*p)-q-2);
}

std::string Parser::getInsideQuotes() {
    char c = **p;
    if (c != '"' && c != '\'') {
        return "";
    }

    ++(*p);

    const char *q = *p;
    std::string res;

    while (*p < end) {

        // ignore comments
        const char *o = *p;
        for (const auto & comTag : commentsTags) {

            if ( isString(comTag.first) ) {
                res += std::string(q, o-q);

                while (*p < end && !isString(comTag.second)) {
                    ++(*p);
                }

                q = *p;
                break;
            }
        }

        if (**p == c) {
            break;
        }

        ++(*p);
    }

    ++(*p);

    return res + std::string(q, (*p)-q-1);
}

std::vector<std::string> Parser::splitBy(const char *cc) {
    std::vector<std::string> vec;

    skip(cc);
    while (*p < end) {
        vec.push_back(getUntilFirstOf(cc));
        skip(cc);
    }

    return vec;
}

void Parser::addCommentTag(const std::string &open, const std::string &close) {
    commentsTags.emplace_back(open, close);
}