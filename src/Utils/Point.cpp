#include "Point.hpp"



/*void Point::print() {
    SFLOGI("Point: x = %f, y = %f", x, y);
}*/

bool Point::operator==(const Point &b) const {
    return x == b.x && y == b.y;
}

bool Point::operator!=(const Point &b) const {
    return !(this->operator==(b));
}

Point Point::operator+(const Point &b) const {
    return {x + b.x, y + b.y};
}

Point Point::operator-(const Point &b) const {
    return {x - b.x, y - b.y};
}

Point Point::operator*(const int & k) const {
    return {x * k, y * k};
}

Point Point::operator*(const double &k) const {
    return {x * k, y * k};
}

Point Point::operator*(const Point &b) const {
    return {x * b.x, y * b.y};
}

Point& Point::operator+=(const Point &b) {
    x += b.x; y += b.y;
    return *this;
}