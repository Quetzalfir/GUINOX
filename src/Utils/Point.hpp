#ifndef GUINOX_POINT_HPP
#define GUINOX_POINT_HPP


class Point {
public:
    double x = 0, y = 0;

    Point() : Point(0,0) {}
    Point(double x_, double y_) : x(x_), y(y_) {}

    //void print();

    bool operator==(const Point & b) const ;
    bool operator!=(const Point& b) const ;
    Point operator+(const Point & b) const ;
    Point & operator+=(const Point & b);
    Point operator-(const Point & b) const ;
    Point operator*(const int & k) const ;
    Point operator*(const double & k) const ;
    Point operator*(const Point & b) const ;
};


#endif //GUINOX_POINT_HPP
