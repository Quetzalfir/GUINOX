#ifndef GUINOX_COLOR_HPP
#define GUINOX_COLOR_HPP


#include <string>
#include <map>


class Color {
    static std::map<std::string, unsigned int> colors;


    float rgba[4] = {1};
    // variables for animating color
    //float speed_A[4] = {0};
    //unsigned int colorTarget = 0;
    //bool onAnimating = false;


public:

    static unsigned int getColorValueByName(const std::string & name);
    static Color fromTo(const Color& A, const Color& B, int step, int interval);
    static float *colorForMultiply(unsigned int c);


    //static bool changeColor(Texture *texture, unsigned int c);
    Color() : Color(0xeeeeeeff) {}
    Color(unsigned int color);
    Color(const std::string & val);

    //void toColor(unsigned int color, long millis = 4000);
    //void toColor(Color color, long millis = 4000) { return this->toColor(color.getHex(), millis);}
    //bool animateColor();
    //bool colorInChange() { return onAnimating;}

    unsigned int getHex() const;
    const float *getFloat() const { return rgba;}
    void setColor(unsigned int color_);

    inline bool operator==(const Color& b) const {
        return (this->getHex() == b.getHex());
    }
    inline bool operator==(const unsigned int& c) const {
        return (this->getHex() == c);
    }
    inline bool operator!=(const Color& b) const { return !(this->operator==(b)); }
    inline bool operator!=(const unsigned int& c) const { return !(this->operator==(c)); }
    inline Color& operator+=(const unsigned int& c) {
        setColor(getHex()+c);
        return *this;
    }

};


#endif //GUINOX_COLOR_HPP
