#include "Synchronizer.hpp"
#include <utility>


Synchronizer::Synchronizer() : Synchronizer([](){},[](){}) {}

Synchronizer::Synchronizer(
        std::function<void ()> block,
        std::function<void ()> free)
        {

    blockingContext = std::move(block);
    freeingContext = std::move(free);
}


void Synchronizer::lockContext() {
    auto currentThread = std::this_thread::get_id();


    if (locked) {

        if (blockingThread == currentThread) {
            return;

        } else {
            waitingThreads.push_back(currentThread);
            while (goThread != currentThread) ;
            waitingThreads.pop_front();
        }


    } else {
        if (!waitingThreads.empty()) {
            waitingThreads.push_back(currentThread);
            while (goThread != currentThread) ;
            waitingThreads.pop_front();
        }
    }

    while (!(mutex.try_lock())) {
        waitingThreads.push_back(currentThread);
        while (goThread != currentThread) ;
        waitingThreads.pop_front();
    }

    goThread = currentThread;

    locked = true;
    blockingThread = std::this_thread::get_id();

    blockingContext();
}

void Synchronizer::freeContext() {
    if (!locked) return;
    if (std::this_thread::get_id() != blockingThread) return;

    freeingContext();

    mutex.unlock();

    locked = false;

    if (!waitingThreads.empty()) {
        goThread = waitingThreads.front();
    }
}