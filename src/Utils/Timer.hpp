#ifndef GUINOX_TIMER_HPP
#define GUINOX_TIMER_HPP

#include <chrono>
#include <functional>

using namespace std::chrono;

class Timer {
    //std::chrono::microseconds interval;
    std::chrono::microseconds period;
    steady_clock::time_point startPoint;
    steady_clock::time_point chkPoint;
    steady_clock::time_point end;
    steady_clock::time_point delta;
    double elapsedTime;
    //double period = 0;

    bool started = false;
    bool stopped = true;
    bool paused = false;
    static long long dt;

public:
    enum Period : char {
        SECONDS, MILLISECONDS, MICROSECONDS
    };


    Timer(bool auto_begin = false);

    double getMsFromLastCall();
    long long getElapsedTime(Period period = MILLISECONDS);
    void setPeriod(double millis);
    bool periodCompleted(double millis);
    bool periodCompleted();
    //void setInterval(double millis);
    //void onInterval(std::function<void(void)> cb);
    bool running() const { return !stopped; }
    void start();
    void restart();
    void stop();
    void pause() { paused = true; }
    //void stopIn(double ms);

    void wait(long millis);
};


#endif //GUINOX_TIMER_HPP
