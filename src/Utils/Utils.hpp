/**
* @brief Utils
*
* @author GUINOX
* @version 1.0
* @date 24/02/2020
*
*/
#ifndef GUINOX_UTILS_HPP
#define GUINOX_UTILS_HPP

#include <string>
#include <vector>

class Utils {

public:

    static std::string toLower(const std::string & str);
    static bool isEmptyElement(const std::string & tagname);
    static std::vector<std::string> split_string_by(const std::string &str, const char c);
    static std::string trim(const std::string& str);
    static double parseMath(const std::string & arithmetic);

};


#endif //GUINOX_UTILS_HPP
