#include "Utils.hpp"
#include <algorithm>
#include <vector>
#include <peglib.h>

std::string Utils::toLower(const std::string &str) {
    std::string aux = str;
    std::transform(aux.begin(), aux.end(), aux.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    return aux;
}

bool Utils::isEmptyElement(const std::string & tagname) {
    const static std::vector<std::string> empty_elements{
            "area"      ,
            "base"      ,
            "br"        ,
            "col"       ,
            "!doctype"   ,
            "embed"     ,
            "hr"        ,
            "img"       ,
            "input"     ,
            //"keygen"  , // HTML 5.2 Draft removed
            "link"      ,
            "meta"      ,
            "param"     ,
            "source"    ,
            "track"     ,
            "wbr"
    };


    for (const auto & tag : empty_elements) {
        if (tagname == tag) {
            return true;
        }
    }

    return false;
}

std::vector<std::string> Utils::split_string_by(const std::string &str, const char c) {
    std::vector<std::string> vec;

    if (str.empty()) return vec;

    size_t found = str.find(c);
    size_t pos = 0;

    while (found != std::string::npos) {
        vec.push_back( trim( str.substr(pos, found-pos) ) );
        pos = found+1;
        found = str.find(c, pos);
    }

    vec.push_back( trim( str.substr(pos, std::string::npos) ) );

    return vec;
}

std::string Utils::trim(const std::string &str) {
    if (str.empty()) { return ""; }

    size_t first = str.find_first_not_of(" \n\t\r");
    if (first == std::string::npos) {
        return str;
    }

    size_t last = str.find_last_not_of(" \n\t\r");
    return str.substr(first, (last - first + 1));
}

double Utils::parseMath(const std::string &arithmetic) {
    static peg::parser parser(R"(
            EXPRESSION               <-  INFIX_EXPRESSION(ATOM, OPERATOR)
            ATOM                     <-  NUMBER / '(' EXPRESSION ')'
            OPERATOR                 <-  < [-+/*] >
            NUMBER                   <-  < ('+'|'-')? [0-9]* '.' [0-9]+ >
            %whitespace              <-  [ \t]*

            # Declare order of precedence
            INFIX_EXPRESSION(A, O) <-  A (O A)* {
              precedence
                L + -
                L * /
            })");

    static bool init = false;

    if (!init) {
        init = true;


        parser["INFIX_EXPRESSION"] = [](const peg::SemanticValues& sv) -> double {
            auto result = peg::any_cast<double>(sv[0]);
            if (sv.size() > 1) {
                auto ope = peg::any_cast<char>(sv[1]);
                auto num = peg::any_cast<double>(sv[2]);
                switch (ope) {
                    case '+': result += num; break;
                    case '-': result -= num; break;
                    case '*': result *= num; break;
                    case '/': result /= num; break;
                }
            }
            return result;
        };
        parser["OPERATOR"] = [](const peg::SemanticValues& sv) { return *sv.c_str(); };
        parser["NUMBER"] = [](const peg::SemanticValues& sv) { return std::stod(sv.c_str()); };
    }


    double res;
    parser.parse(arithmetic.data(), res);

    return res;

}