#include "GUINOX/guinox.hpp"
#include "Window/GLFW_Interface.hpp"
#include <iostream>

namespace GUINOX {

    std::shared_ptr<Window> createWindow(const std::string &url, const std::string &windowName, const std::string &windowFeatures) {
        return Window::open(url, windowName, windowFeatures);
    }

    void waitForTerminate() {
        GLFW_Interface::waitForOut();
    }

    DOM_Node::sp_Document createDocumentFromFile(const std::string &filePath) {
        return DOMParser::parseFromFile(filePath);
    }


}