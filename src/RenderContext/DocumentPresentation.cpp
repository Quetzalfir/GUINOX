#include "DocumentPresentation.hpp"
#include "../DOM/Document.hpp"



DocumentPresentation::DocumentPresentation(DOM_Node::Document *document) :
        _document(document)
        {

}

void DocumentPresentation::addPresentation(Presentation * presentation) {
    for (const auto & p : presentations) {
        if ( p == presentation) return;
    }

    presentation->_ownerDocument = std::dynamic_pointer_cast<DOM_Node::Document>( _document->shared_from_this() );
    presentations.push_back(presentation);

    presentation->fullReFlow();
}

void DocumentPresentation::removePresentation(Presentation *presentation) {
    auto end = presentations.end();
    for (auto it = presentations.begin(); it != end; it++) {
        if ((*it) == presentation) {
            presentations.erase(it);
            return;
        }
    }
}

void DocumentPresentation::fullReFlow() {
    for (auto & p : presentations) {
        p->fullReFlow();
    }
}