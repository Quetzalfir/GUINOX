#include "ElementFrame.hpp"
#include "Presentation.hpp"
#include "../DOM/HTMLElement.hpp"


ElementFrame::ElementFrame(HTMLElement *owner) : _ownerElement(owner) {}


void ElementFrame::childrenDirty() {
    auto node = _ownerElement->shared_from_this();
    auto element = std::dynamic_pointer_cast<HTMLElement>(node);

    for (auto & frame : frames) {
        frame->ownerPresentation->reFlow(element);
    }
}

void ElementFrame::dirty() {
    auto node = _ownerElement->shared_from_this();
    sp_HTMLElement element = std::dynamic_pointer_cast<HTMLElement>(node);

    if (auto parent = node->parentElement()) {
        element = std::dynamic_pointer_cast<HTMLElement>(parent);
    }

    for (auto & frame : frames) {
        frame->ownerPresentation->reFlow(element);
    }
}