#include "FrameRegistry.hpp"
#include <DOM/HTMLElement.hpp>
#include <DOM/Text.hpp>

#include "Frames/InLine.hpp"
#include "Frames/Block.hpp"
#include "Frames/Text.hpp"

std::map<CSSModule::Display::DisplayType, std::function<RenderFrame::sp_Frame(DOM_Node::sp_Node, sp_StyleContext, Presentation *)>> FrameRegistry::frames;



RenderFrame::sp_Frame
FrameRegistry::get(
        DOM_Node::sp_Node node,
        sp_StyleContext style,
        Presentation *presentation
        ) {

    if (!node || !style) return nullptr;

    RenderFrame::sp_Frame frame;

    if (node->nodeType == DOM_Node::Node::TEXT_NODE) {
        auto it = frames.find(CSSModule::Display::TEXT);
        if (it != frames.end()) {
            frame = (*it).second(node, style, presentation);
        }

    } else {
        auto module = style->getModule(CSSModule::DISPLAY);
        auto display = std::dynamic_pointer_cast<CSSModule::Display>(module);

        if (display->display == CSSModule::Display::NONE) return nullptr;


        auto it = frames.find(display->display);
        if (it != frames.end()) {
            frame = (*it).second(node, style, presentation);

        } else {
            auto it = frames.find(CSSModule::Display::BLOCK);
            frame = (*it).second(node, style, presentation);
        }
    }

    return frame;
}



FrameRegistry::FrameRegistry(
        CSSModule::Display::DisplayType type,
        const std::function<RenderFrame::sp_Frame(DOM_Node::sp_Node, const sp_StyleContext &, Presentation *)>& constructor
        ) {


    auto it = frames.find(type);
    if (it == frames.end()) {
        frames[type] = constructor;
    }
}




const FrameRegistry RenderFrame::Block::regist(CSSModule::Display::BLOCK,[](DOM_Node::sp_Node node, const sp_StyleContext &style, Presentation *presentation) -> RenderFrame::sp_Frame {
    return std::make_shared<Block>(node, style, presentation);
});

const FrameRegistry RenderFrame::InLine::regist(CSSModule::Display::INLINE, [](DOM_Node::sp_Node node, const sp_StyleContext &style, Presentation *presentation) -> RenderFrame::sp_Frame {
    return std::make_shared<InLine>(node, style, presentation);
});

const FrameRegistry RenderFrame::Text::regist(CSSModule::Display::TEXT, [](DOM_Node::sp_Node node, const sp_StyleContext &style, Presentation *presentation) -> RenderFrame::sp_Frame {
    return std::make_shared<Text>(node, style, presentation);
});