#ifndef GUINOX_REFLOW_HPP
#define GUINOX_REFLOW_HPP

#include <thread>
#include "Frames/Frame.hpp"


class ReFlow {

    std::thread reflowThread;
    std::deque<std::function<void(void)>> commands;
    //std::mutex mtx;
    Presentation * presentation;

    [[noreturn]] void threadFunc();
    static RenderFrame::sp_Frame getAncientBlock(RenderFrame::sp_Frame frame, bool inclusive);


public:

    ReFlow(Presentation * presentation);

    void append(RenderFrame::sp_Frame & parent, RenderFrame::sp_Frame & frame);
    void insertBefore(RenderFrame::sp_Frame & frame, RenderFrame::sp_Frame & child);
    void replace(RenderFrame::sp_Frame & frame, RenderFrame::sp_Frame & child);
    void substituteRoot(RenderFrame::sp_Frame & newFrame);


};


#endif //GUINOX_REFLOW_HPP
