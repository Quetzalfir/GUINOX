#ifndef GUINOX_CSS_MODULE_FONT_HPP
#define GUINOX_CSS_MODULE_FONT_HPP

#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Font : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST parser;


        static std::vector<std::pair<std::string, std::string>> mapFontShorthand(const std::string &value);


    public:

        enum Stretch {
            ULTRA_CONDENSED,
            EXTRA_CONDENSED,
            CONDENSED,
            SEMI_CONDENSED,
            NORMAL,
            SEMI_EXPANDED,
            EXPANDED,
            EXTRA_EXPANDED,
            ULTRA_EXPANDED
        };

        enum FontStyle {
            NORMAL_STYLE, ITALIC, OBLIQUE
        };

        enum Variant {
            NORMAL_VARIANT, SMALL_CAPS
        };

        enum VariantCaps {
            NORMAL_VARIANT_CAPS,
            SMALL_CAPS_CAPS,
            ALL_SMALL_CAPS,
            PETITE_CAPS,
            ALL_PETITE_CAPS,
            UNICASE,
            TITLING_CAPS,
            UNSET
        };


    private:

        std::vector<std::string> _family;
        Length _size;
        Length _sizeAdjust;
        Stretch _stretch = NORMAL;
        FontStyle _style = NORMAL_STYLE;
        Variant _variant = NORMAL_VARIANT;
        VariantCaps _variantCaps = NORMAL_VARIANT_CAPS;
        Length _weight;

        void clonePropertyFrom(const std::string &name, Module *ref) override;
        void setter(const std::string &name, const std::string &value) override;


        void setFamily(const std::string &value);
        void setFontSize(const std::string &value);
        void setSizeAdjust(const std::string &value);
        void setStretch(const std::string &value);
        void setFontStyle(const std::string &value);
        void setVariant(const std::string &value);
        void setVariantCaps(const std::string &value);
        void setWeight(const std::string &value);

    public:

        const std::vector<std::string> &family = _family;
        const Length &size = _size;
        const Length &sizeAdjust = _sizeAdjust;
        const Stretch &stretch = _stretch;
        const FontStyle &style = _style;
        const Variant &variant = _variant;
        const VariantCaps &variantCaps = _variantCaps;
        const Length &weight = _weight;

        Font(StyleContext *parentContext);
    };

}

#endif //GUINOX_CSS_MODULE_FONT_HPP
