#ifndef GUINOX_CSS_MODULE_TEXT_HPP
#define GUINOX_CSS_MODULE_TEXT_HPP

#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"

namespace CSSModule{

    class Text : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST parser;

        static std::vector<std::pair<std::string, std::string>> mapTextShorthand(const std::string & value);

    public:

        enum Align { LEFT, RIGHT, CENTER, JUSTIFY };
        enum AlignLast{ AUTO, LEFT_LAST, RIGHT_LAST, CENTER_LAST, JUSTIFY_LAST, START, END };
        enum Decoration{ TEXT_DECORATION_LINE , TEXT_DECORATION_COLOR, TEXT_DECORATION_STYLE };
        enum DecorationLine{ NONE, UNDERLINE, OVERLINE, LINE_THROUGH };
        enum DecorationStyle{ SOLID, DOUBLE, DOTTED, DASHED, WAVY };
        enum Justify{ AUTO_JUSTIFY, INTER_WORD, INTER_CHARACTER, NONE_JUSTIFY };
        enum Overflow{ CLIP, ELLIPSIS, STRING };
        enum Transform{ NONE_TRANS, CAPITALIZE, UPPERCASE, LOWERCASE };

        struct Shadow {
            std::vector<Length> length;
            Color color;
        };

    private:

        Align _align ;
        AlignLast _alignLast = AUTO;
        Color _decorationColor;
        DecorationLine _decorationLine = NONE ;
        DecorationStyle _decorationStyle = SOLID;
        Length _indent;
        Justify _justify = AUTO_JUSTIFY;
        Overflow _overflow = CLIP;
        std::vector<Shadow> _shadow;
        Transform _transform = NONE_TRANS;
        Color _color;

        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;

        void setAlign(const std::string & value);
        void setColor(const std::string & value);
        void setAlignLast(const std::string & value);
        void setDecorationColor(const std::string & value);
        void setDecorationLine(const std::string & value);
        void setDecorationStyle(const std::string & value);
        void setJustify(const std::string & value);
        void setOverflow(const std::string & value);
        void setShadow(const std::string & value);
        void setTransform(const std::string & value);

    public:

        const Align &align = _align ;
        const AlignLast &alignLast = _alignLast;
        const Color &decorationColor = _decorationColor;
        const DecorationLine &decorationLine = _decorationLine;
        const DecorationStyle &decorationStyle = _decorationStyle;
        const Length &indent = _indent;
        const Justify &justify = _justify;
        const Overflow &overflow = _overflow;
        const std::vector<Shadow> &shadow = _shadow;
        const Transform &transform = _transform;
        const Color &color = _color;

        Text(StyleContext *parentContext);
    };

}//END NAMESPACE

#endif //GUINOX_CSS_MODULE_TEXT_HPP
