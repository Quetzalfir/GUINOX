#include <Utils/Utils.hpp>
#include "Grid.hpp"

namespace CSSModule{

    const PEGParserAST Grid::gridParser(R"(
        GRID                   <- NONE /
                                  GRID_TEMPLATE_ROWS '/' GRID_TEMPLATE_COLUMNS /
                                  GRID_TEMPLATE_AREAS /
                                  GRID_TEMPLATE_ROWS '/' '[' GRID_AUTO_FLOW ']' GRID_AUTO_COLUMNS /
                                  '[' GRID_AUTO_FLOW ']' GRID_AUTO_ROWS '/' GRID_TEMPLATE_COLUMNS /
                                  INITIAL / INHERIT

        GRID_AUTO_FLOW         <- "row" | "column" | "dense" | "row dense" | "column dense"
        GRID_TEMPLATE_AREAS    <- < NONE / STRING+ >

        GRID_TEMPLATE          <- NONE /
                                  GRID_TEMPLATE_ROWS '/' GRID_TEMPLATE_COLUMNS /
                                  (LINE_NAMES? ITEM TRACK_SIZE? LINE_NAMES?)+ ('/' EXPLICIT_TRACK_LIST)?

        GRID_TEMPLATE_ROWS     <- < NONE / TRACK_LIST / AUTO_TRACK_LIST >
        GRID_TEMPLATE_COLUMNS  <- < NONE / TRACK_LIST / AUTO_TRACK_LIST >

        GRID_AUTO_ROWS         <- < TRACK_SIZE+ >
        GRID_AUTO_COLUMNS      <- < TRACK_SIZE+ >

        TRACK_LIST             <- (LINE_NAMES? (TRACK_SIZE / TRACK_REPEAT))+ LINE_NAMES?
        AUTO_TRACK_LIST        <- (LINE_NAMES? (FIXED_SIZE / FIXED_REPEAT))* LINE_NAMES? AUTO_REPEAT
                                  (LINE_NAMES? (FIXED_SIZE / FIXED_REPEAT))* LINE_NAMES?
        EXPLICIT_TRACK_LIST    <- (LINE_NAMES? TRACK_SIZE)+ LINE_NAMES?

        TRACK_SIZE             <- TRACK_BREADTH / "minmax(" INFLEXIBLE_BREADTH ','? TRACK_BREADTH ')' / "fit_content(" LENGTH_PERCENTAGE ')'
        FIXED_SIZE             <- TRACK_BREADTH / "minmax(" FIXED_BREADTH ','? TRACK_BREADTH ')' / "minmax(" INFLEXIBLE_BREADTH ','? FIXED_BREADTH ')'
        TRACK_BREADTH          <- LENGTH_PERCENTAGE / FLEX / "min-content" / "max-content" / "auto"
        INFLEXIBLE_BREADTH     <- LENGTH_PERCENTAGE / "min-content" / "max-content" / "auto"
        FIXED_BREADTH          <- LENGTH_PERCENTAGE
        LINE_NAMES             <- '[' IDENT* ']'

        TRACK_REPEAT           <- "repeat(" INTEGER ','? (LINE_NAMES? TRACK_SIZE)+ LINE_NAMES? ')'
        AUTO_REPEAT            <- "repeat(" ("auto-fill" / "auto-fit") ','? (LINE_NAMES? FIXED_SIZE)+ LINE_NAMES? ')'
        FIXED_REPEAT           <- "repeat(" INTEGER ','? (LINE_NAMES? FIXED_SIZE)+ LINE_NAMES? ')'

        LENGTH_PERCENTAGE      <- LENGTH / PERCENTAGE
        FLEX                   <- NUMBER "fr"

        INHERIT                <- "inherit"
        INITIAL                <- "initial"
        NONE 			       <- "none"

        LENGTH                 <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
        LENGTH_UNIT            <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

        PERCENTAGE             <- <NUMBER> '%'
        NUMBER                 <- <FLOAT / INTEGER>
        FLOAT                  <- ('+'|'-')? [0-9]* '.' [0-9]+
        INTEGER                <- ('+'|'-')? [0-9]+

        IDENT			       <- < ("--" / '-'? [a-zA-Z_]) [a-zA-Z0-9-_]* >
        STRING                 <- < $q<QUOTE> (!$q .)* $q >
        ITEM                   <- < WORD / PHRASE >
        WORD    		       <- [a-zA-Z]+
        PHRASE    		       <- $q<QUOTE> (!$q .)* $q
        QUOTE     		       <- "'" | '"'

        %whitespace            <- [ \t\r\n]*
    )");

    const PEGParserAST Grid::gridGapParser(R"(
        GRID_GAP               <- GRID_ROW_GAP GRID_COLUMN_GAP?

        GRID_ROW_GAP           <- < "normal" / LENGTH_PERCENTAGE >
        GRID_COLUMN_GAP        <- < "normal" / LENGTH_PERCENTAGE >

        LENGTH_PERCENTAGE      <- LENGTH / PERCENTAGE

        LENGTH                 <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
        LENGTH_UNIT            <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

        PERCENTAGE             <- <NUMBER> '%'
        NUMBER                 <- <FLOAT / INTEGER>
        FLOAT                  <- ('+'|'-')? [0-9]* '.' [0-9]+
        INTEGER                <- ('+'|'-')? [0-9]+

        %whitespace            <- [ \t\r\n]*
    )");

    const PEGParserAST Grid::gridRoCParser(R"(
        GRID_ROW               <- GRID_START ( '/' GRID_END )?

        GRID_START             <- < GRID_LINE >
        GRID_END               <- < GRID_LINE >

        GRID_LINE              <- "auto" / IDENT /
                                  INTEGER IDENT? /
                                  "span" (INTEGER IDENT / IDENT INTEGER / INTEGER / IDENT) /
                                  (INTEGER IDENT / IDENT INTEGER / INTEGER / IDENT) "span"

        IDENT			       <- < ("--" / '-'? [a-zA-Z_]) [a-zA-Z0-9-_]* >
        INTEGER                <- ('+'|'-')? [0-9]+

        %whitespace            <- [ \t\r\n]*
    )");

    const PEGParserAST Grid::gridTemplateParser(R"(
        GRID_TEMPLATE          <- NONE /
                                  GRID_TEMPLATE_ROWS '/' GRID_TEMPLATE_COLUMNS /
                                  GRID_TEMPLATE_AREAS

        GRID_TEMPLATE_AREAS    <- < NONE / STRING+ /
                                    (LINE_NAMES? ITEM TRACK_SIZE? LINE_NAMES?)+ ('/' EXPLICIT_TRACK_LIST)? >

        GRID_TEMPLATE_ROWS     <- < NONE / TRACK_LIST / AUTO_TRACK_LIST >
        GRID_TEMPLATE_COLUMNS  <- < NONE / TRACK_LIST / AUTO_TRACK_LIST >

        TRACK_LIST             <- (LINE_NAMES? (TRACK_SIZE / TRACK_REPEAT))+ LINE_NAMES?
        AUTO_TRACK_LIST        <- (LINE_NAMES? (FIXED_SIZE / FIXED_REPEAT))* LINE_NAMES? AUTO_REPEAT
                                  (LINE_NAMES? (FIXED_SIZE / FIXED_REPEAT))* LINE_NAMES?
        EXPLICIT_TRACK_LIST    <- (LINE_NAMES? TRACK_SIZE)+ LINE_NAMES?

        TRACK_SIZE             <- TRACK_BREADTH / "minmax(" INFLEXIBLE_BREADTH ','? TRACK_BREADTH ')' / "fit_content(" LENGTH_PERCENTAGE ')'
        FIXED_SIZE             <- TRACK_BREADTH / "minmax(" FIXED_BREADTH ','? TRACK_BREADTH ')' / "minmax(" INFLEXIBLE_BREADTH ','? FIXED_BREADTH ')'
        TRACK_BREADTH          <- LENGTH_PERCENTAGE / FLEX / "min-content" / "max-content" / "auto"
        INFLEXIBLE_BREADTH     <- LENGTH_PERCENTAGE / "min-content" / "max-content" / "auto"
        FIXED_BREADTH          <- LENGTH_PERCENTAGE
        LINE_NAMES             <- '[' IDENT* ']'

        TRACK_REPEAT           <- "repeat(" INTEGER ','? (LINE_NAMES? TRACK_SIZE)+ LINE_NAMES? ')'
        AUTO_REPEAT            <- "repeat(" ("auto-fill" / "auto-fit") ','? (LINE_NAMES? FIXED_SIZE)+ LINE_NAMES? ')'
        FIXED_REPEAT           <- "repeat(" INTEGER ','? (LINE_NAMES? FIXED_SIZE)+ LINE_NAMES? ')'

        LENGTH_PERCENTAGE      <- LENGTH / PERCENTAGE
        FLEX                   <- NUMBER "fr"

        INHERIT                <- "inherit"
        INITIAL                <- "initial"
        NONE 			       <- "none"

        LENGTH                 <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
        LENGTH_UNIT            <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

        PERCENTAGE             <- <NUMBER> '%'
        NUMBER                 <- <FLOAT / INTEGER>
        FLOAT                  <- ('+'|'-')? [0-9]* '.' [0-9]+
        INTEGER                <- ('+'|'-')? [0-9]+

        IDENT			       <- < ("--" / '-'? [a-zA-Z_]) [a-zA-Z0-9-_]* >
        STRING                 <- < $q<QUOTE> (!$q .)* $q >
        ITEM                   <- < WORD / PHRASE >
        WORD    		       <- [a-zA-Z]+
        PHRASE    		       <- $q<QUOTE> (!$q .)* $q
        QUOTE     		       <- "'" | '"'

        %whitespace            <- [ \t\r\n]*
    )");

    const PEGParserAST Grid::gridAreaParser(R"(
        GRID_AREA              <- GRID_ROW_START '/' GRID_COLUMN_START '/' GRID_ROW_END '/' GRID_COLUMN_END /
                                  GRID_AREA_NAME

        GRID_ROW_START         <- < GRID_LINE >
        GRID_COLUMN_START      <- < GRID_LINE >
        GRID_ROW_END           <- < GRID_LINE >
        GRID_COLUMN_END        <- < GRID_LINE >

        GRID_AREA_NAME         <- < IDENT >

        GRID_LINE              <- "auto" / IDENT /
                                  INTEGER IDENT? /
                                  "span" (INTEGER IDENT / IDENT INTEGER / INTEGER / IDENT) /
                                  (INTEGER IDENT / IDENT INTEGER / INTEGER / IDENT) "span"

        IDENT			       <- < ("--" / '-'? [a-zA-Z_]) [a-zA-Z0-9-_]* >
        INTEGER                <- ('+'|'-')? [0-9]+

        %whitespace            <- [ \t\r\n]*
    )");


    std::vector<std::pair<std::string, std::string> > Grid::mapGridShorthand(const std::string &value, const PEGParserAST & parser) {
        auto properties = parser.getPairs(value);

        for (auto & p : properties) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return properties;
    }

    std::vector<std::pair<std::string, std::string> > Grid::mapGridRoCShorthand(const std::string &value, bool row) {
        auto properties = gridRoCParser.getPairs(value);
        std::string x = row ? "-row" : "-column" ;

        for (auto & p : properties) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);

            size_t pos = p.first.find_last_of('-');
            p.first.insert(pos, x);
        }

        return properties;
    }

    //----------------------------- PRIVATE -----------------------------//

    void Grid::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto grid = dynamic_cast<Grid *>(ref)) {
            if      (name == "grid-auto-columns")       _autoColumn = grid->_autoColumn;
            else if (name == "grid-auto-flow")          _autoFlow   = grid->_autoFlow;
            else if (name == "grid-auto-rows")          _autoRows  = grid->_autoRows;
            else if (name == "grid-column-end")         _columnEnd  = grid->_columnEnd;
            else if (name == "grid-column-gap")         _columnGap  = grid->_columnGap;
            else if (name == "grid-column-start")       _columnsStart  = grid->_columnsStart;
            else if (name == "grid-row-end")            _rowEnd  = grid->_rowEnd;
            else if (name == "grid-row-gap")            _rowGap  = grid->_rowGap;
            else if (name == "grid-row-start")          _rowStart  = grid->_rowStart;
            //else if (name == "grid-template-areas")     _templateAreas  = grid->_templateAreas;
            else if (name == "grid-template-columns")   _templateColumns  = grid->_templateColumns;
            else if (name == "grid-template-rows")      _templateRows  = grid->_templateRows;
            else if (name == "grid-area-name")          _templateRows  = grid->_templateRows;
        }
    }

    void Grid::setter(const std::string &name, const std::string &value) {
        //if      (name == "grid-auto-columns")       setAutoColumns(value);
        //else if (name == "grid-auto-flow")          setAutoFlow(value);
        //else if (name == "grid-auto-rows")          setAutoRows(value);
        if (name == "grid-column-end")         _columnEnd = value;
        else if (name == "grid-column-gap")         _columnGap = getLength(value);
        else if (name == "grid-column-start")       _columnsStart = value;
        else if (name == "grid-row-end")            _rowEnd = value;
        else if (name == "grid-row-gap")            _rowGap = getLength(value);
        else if (name == "grid-row-start")          _rowStart = value;
        //else if (name == "grid-template-areas")     setTemplateAreas(value);
        //else if (name == "grid-template-columns")   setTemplateColumns(value);
        //else if (name == "grid-template-rows")      setTemplateRows(value);
        else if (name == "grid-area-name")          _areaName = value;

    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Grid::Grid(StyleContext *parentContext_) :
            Module(GRID, parentContext_) {

        init(properties);
    }

}  //END NAMESPACE
