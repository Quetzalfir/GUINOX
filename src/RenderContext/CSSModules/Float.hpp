#ifndef GUINOX_FLOAT_HPP
#define GUINOX_FLOAT_HPP


#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Float : public Module {
        const static PropertyMap properties;
        const static CSSModuleRegistry regist;


    public:
        enum EFloat { NONE_F, LEFT_F, RIGHT_F };
        enum Clear { NONE_C, LEFT_C, RIGHT_C, BOTH};


    private:
        EFloat _eFloat;
        Clear _clear;


        void setFloat(const std::string &value);
        void setClear(const std::string &value);
        void clonePropertyFrom(const std::string &name, Module *ref) override;
        void setter(const std::string &name, const std::string &value) override;


    public:
        const EFloat & eFloat = _eFloat;
        const Clear & clear = _clear;


        Float(StyleContext *parentContext);

    };

}

#endif //GUINOX_FLOAT_HPP
