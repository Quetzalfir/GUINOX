#include "Position.hpp"



namespace CSSModule {


//----------------------------- STATICS -----------------------------//



//----------------------------- PRIVATE -----------------------------//

    void Position::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto position = dynamic_cast<Position *>(ref)) {
            if      (name == "bottom")   _coordinates.y = position->_coordinates.y;
            else if (name == "top")      _coordinates.y = position->_coordinates.y;
            else if (name == "left")     _coordinates.x = position->_coordinates.x;
            else if (name == "right")    _coordinates.x = position->_coordinates.x;
            else if (name == "position") _position = position->_position;
            else if (name == "z-index")  _z_index = position->_z_index;
        }
    }

    void Position::setter(const std::string &name, const std::string &value) {
        if      (name == "bottom")   setCoord("bottom", value);
        else if (name == "top")      setCoord("top", value);
        else if (name == "left")     setCoord("left", value);
        else if (name == "right")    setCoord("right", value);
        else if (name == "position") setPosition(value);
        else if (name == "z-index")  _z_index = getLength(value);
    }

    void Position::setCoord(const std::string &coord, const std::string &value) {
        if      (coord == "left")    _coordinates.x = { LEFT, getLength(value) };
        else if (coord == "right")   _coordinates.x = { RIGHT, getLength(value) };
        else if (coord == "top")     _coordinates.y = { TOP, getLength(value) };
        else if (coord == "bottom")  _coordinates.y = { BOTTOM, getLength(value) };
    }

    void Position::setPosition(const std::string &value) {
        if      (value == "static")     _position = STATIC;
        else if (value == "absolute")   _position = ABSOLUTE;
        else if (value == "fixed")      _position = FIXED;
        else if (value == "relative")   _position = RELATIVE;
        else if (value == "sticky")     _position = STICKY;
    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Position::Position(StyleContext *parentContext_) :
            Module(POSITION, parentContext_) {

        init(properties);
    }

}