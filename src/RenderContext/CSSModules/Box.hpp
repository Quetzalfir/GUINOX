#ifndef GUINOX_BOX_HPP
#define GUINOX_BOX_HPP


#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {

    class Box : public Module {
        const static PropertyMap properties;
        const static CSSModuleRegistry regist;


    public:
        enum Sizing { CONTENT_BOX, BORDER_BOX };


    private:

        Length _width;
        Length _height;
        Length _max_width;
        Length _max_height;
        Length _min_width;
        Length _min_height;
        Length _img_ratio;
        Sizing _box_sizing;


        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;
        void setSizing(const std::string & value);
        static Length getMax(const std::string & value);


    public:

        const Length & width = _width;
        const Length & height = _height;
        const Length & max_width = _max_width;
        const Length & max_height = _max_height;
        const Length & min_width = _min_width;
        const Length & min_height = _min_height;
        const Length & img_ratio = _img_ratio;
        const Sizing & box_sizing = _box_sizing;


        Box(StyleContext *parentContext);

    };

}


#endif //GUINOX_BOX_HPP
