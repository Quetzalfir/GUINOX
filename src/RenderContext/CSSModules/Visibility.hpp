#ifndef GUINOX_VISIBILITY_HPP
#define GUINOX_VISIBILITY_HPP



#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Visibility : public Module {
        const static PropertyMap properties;
        const static CSSModuleRegistry regist;


    public:

        enum Visibility_Type { VISIBLE, HIDDEN, COLLAPSE };


    private:

        Visibility_Type _visibility;


        void clonePropertyFrom(const std::string &name, Module *ref) override;
        void setter(const std::string &name, const std::string &value) override;
        void setVisibility(const std::string &value);


    public:

        const Visibility_Type & visibility = _visibility;

        Visibility(StyleContext *parentContext);

    };

}


#endif //GUINOX_VISIBILITY_HPP
