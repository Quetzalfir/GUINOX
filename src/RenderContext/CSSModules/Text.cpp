#include <Utils/Utils.hpp>
#include "Text.hpp"

namespace CSSModule{

    //----------------------------- STATICS -----------------------------//

    const PEGParserAST Text::parser(R"(
         TEXT_DECORATION       <- TEXT_DECORATION_LINE TEXT_DECORATION_STYLE TEXT_DECORATION_COLOR /
                                  TEXT_DECORATION_LINE TEXT_DECORATION_COLOR TEXT_DECORATION_STYLE /
                                  TEXT_DECORATION_STYLE TEXT_DECORATION_LINE TEXT_DECORATION_COLOR /
                                  TEXT_DECORATION_STYLE TEXT_DECORATION_COLOR TEXT_DECORATION_LINE /
                                  TEXT_DECORATION_COLOR TEXT_DECORATION_LINE TEXT_DECORATION_STYLE /
                                  TEXT_DECORATION_COLOR TEXT_DECORATION_STYLE TEXT_DECORATION_LINE /
                                  TEXT_DECORATION_LINE TEXT_DECORATION_STYLE /
                                  TEXT_DECORATION_LINE TEXT_DECORATION_COLOR /
                                  TEXT_DECORATION_STYLE TEXT_DECORATION_LINE /
                                  TEXT_DECORATION_STYLE TEXT_DECORATION_COLOR /
                                  TEXT_DECORATION_COLOR TEXT_DECORATION_LINE /
                                  TEXT_DECORATION_COLOR TEXT_DECORATION_STYLE /
                                  TEXT_DECORATION_LINE /
                                  TEXT_DECORATION_STYLE /
                                  TEXT_DECORATION_COLOR

         TEXT_DECORATION_LINE  <- "underline" | "overline" | "line-through" | "blink"  |
                                  "spelling-error" | "grammar-error" | "none"

         TEXT_DECORATION_STYLE <- "solid" | "double" | "dotted" | "dashed" | "wavy"

         TEXT_DECORATION_COLOR <- < RGB / RGBA / HSL / HSLA / HEX_COLOR / NAMED_COLOR / "currentcolor" >

         NAMED_COLOR           <- "aliceblue" | "antiquewhite" | "aqua" | "aquamarine" | "azure" | "beige" |
                                  "bisque" | "black" | "blanchedalmond" | "blue" | "blueviolet" | "brown" | "burlywood" |
                                  "cadetblue" | "chartreuse" | "chocolate" | "coral" | "cornflowerblue" | "cornsilk" |
                                  "crimson" | "cyan" | "darkblue" | "darkcyan" | "darkgoldenrod" | "darkgray" | "darkgrey" |
                                  "darkgreen" | "darkkhaki" | "darkmagenta" | "darkolivegreen" | "darkorange" | "darkorchid" |
                                  "darkred" | "darksalmon" | "darkseagreen" | "darkslateblue" | "darkslategray" | "darkslategrey" |
                                  "darkturquoise" | "darkviolet" | "deeppink" | "deepskyblue" | "dimgray" | "dimgrey" | "dodgerblue" |
                                  "firebrick" | "floralwhite" | "forestgreen" | "fuchsia" | "gainsboro" | "ghostwhite" | "gold" |
                                  "goldenrod" | "gray" | "grey" | "green" | "greenyellow" | "honeydew" | "hotpink" | "indianred " |
                                  "indigo " | "ivory" | "khaki" | "lavender" | "lavenderblush" | "lawngreen" | "lemonchiffon" |
                                  "lightblue" | "lightcoral" | "lightcyan" | "lightgoldenrodyellow" | "lightgray" | "lightgrey" |
                                  "lightgreen" | "lightpink" | "lightsalmon" | "lightseagreen" | "lightskyblue" | "lightslategray" |
                                  "lightslategrey" | "lightsteelblue" | "lightyellow" | "lime" | "limegreen" | "linen" |
                                  "magenta" | "maroon" | "mediumaquamarine" | "mediumblue" | "mediumorchid" | "mediumpurple" |
                                  "mediumseagreen" | "mediumslateblue" | "mediumspringgreen" | "mediumturquoise" | "mediumvioletred" |
                                  "midnightblue" | "mintcream" | "mistyrose" | "moccasin" | "navajowhite" | "navy" | "oldlace" |
                                  "olive" | "olivedrab" | "orange" | "orangered" | "orchid" | "palegoldenrod" | "palegreen" |
                                  "paleturquoise" | "palevioletred" | "papayawhip" | "peachpuff" | "peru" | "pink" | "plum" |
                                  "powderblue" | "purple" | "rebeccapurple" | "red" | "rosybrown" | "royalblue" | "saddlebrown" |
                                  "salmon" | "sandybrown" | "seagreen" | "seashell" | "sienna" | "silver" | "skyblue" | "slateblue" |
                                  "slategray" | "slategrey" | "snow" | "springgreen" | "steelblue" | "tan" | "teal" | "thistle" |
                                  "tomato" | "turquoise" | "transparent" | "violet" | "wheat" | "white" | "whitesmoke" |
                                  "yellow" | "yellowgreen"

         HEX_COLOR             <- '#' (HEX_DIGIT{8} /
                                        HEX_DIGIT{6} /
                                        HEX_DIGIT{3,4}  ) ' '?

         RGB                   <- "rgb"  "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                  "rgb"  "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                  "rgb"  "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                  "rgb"  "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         RGBA                  <- "rgba" "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                  "rgba" "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                  "rgba" "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                  "rgba" "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         HSL                   <- "hsl"  "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                  "hsl"  "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'
         HSLA                  <- "hsla" "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                  "hsla" "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'

         ALPHA_VALUE           <- NUMBER / PERCENTAGE
         HUE                   <- NUMBER / ANGLE

         ANGLE                 <- NUMBER ANGLE_UNIT
         ANGLE_UNIT            <- "deg" | "grad" | "rad" | "turn"

         PERCENTAGE            <- <NUMBER> '%'
         NUMBER                <- <FLOAT / INTEGER>
         FLOAT                 <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER               <- ('+'|'-')? [0-9]+
         HEX_DIGIT             <- [a-fA-F0-9]

         %whitespace           <-  [ \t\r\n]*
    )");

    std::vector<std::pair<std::string, std::string> > Text::mapTextShorthand(const std::string &value) {
        auto properties = parser.getPairs(value);

        for (auto & p : properties) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return properties;
    }

    //----------------------------- PRIVATE -----------------------------//

    void Text::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto text = dynamic_cast<Text *>(ref)) {
            if      (name == "text-align")              _align = text->_align;
            else if (name == "color")                   _color = text->_color;
            else if (name == "text-align-last")         _alignLast = text->_alignLast;
            else if (name == "text-decoration-color")   _decorationColor = text->_decorationColor;
            else if (name == "text-decoration-line")    _decorationLine = text->_decorationLine;
            else if (name == "text-decoration-style")   _decorationStyle = text->_decorationStyle;
            else if (name == "text-indent")             _indent = text->_indent;
            else if (name == "text-justify")            _justify = text->_justify;
            else if (name == "text-overflow")           _overflow = text->_overflow;
            else if (name == "text-shadow")             _shadow = text->_shadow;
            else if (name == "text-transform")          _transform = text->_transform;
        }
    }

    void Text::setter(const std::string &name, const std::string &value) {
        if      (name == "text-align")              setAlign(value);
        else if (name == "color")                   setColor(value);
        else if (name == "text-align-last")         setAlignLast(value);
        else if (name == "text-decoration-color")   setDecorationColor(value);
        else if (name == "text-decoration-line")    setDecorationLine(value);
        else if (name == "text-decoration-style")   setDecorationStyle(value);
        else if (name == "text-indent")             _indent = getLength(value);
        else if (name == "text-justify")            setJustify(value);
        else if (name == "text-overflow")           setOverflow(value);
        else if (name == "text-shadow")             setShadow(value);
        else if (name == "text-transform")          setTransform(value);
    }

    void Text::setAlign(const std::string &value) {
        if      (value == "left")    _align = LEFT;
        else if (value == "right")   _align = RIGHT;
        else if (value == "center")  _align = CENTER;
        else if (value == "justify") _align = JUSTIFY;
    }

    void Text::setAlignLast(const std::string &value) {
        if      (value == "auto")    _alignLast = AUTO;
        else if (value == "left")    _alignLast = LEFT_LAST;
        else if (value == "right")   _alignLast = RIGHT_LAST;
        else if (value == "center")  _alignLast = CENTER_LAST;
        else if (value == "justify") _alignLast = JUSTIFY_LAST;
        else if (value == "start")   _alignLast = START;
        else if (value == "end")     _alignLast = END;
    }

    void Text::setColor(const std::string &value) {
        Color *aux = getColor(value);

        _color = *aux;

        delete aux;
    }

    void Text::setDecorationColor(const std::string &value) {
        Color *aux = getColor(value);

        _decorationColor = *aux;

        delete aux;
    }

    void Text::setDecorationLine(const std::string &value) {
        if      (value == "none")          _decorationLine = NONE;
        else if (value == "underline")     _decorationLine = UNDERLINE;
        else if (value == "overline")      _decorationLine = OVERLINE;
        else if (value == "line-through")  _decorationLine = LINE_THROUGH;
    }

    void Text::setDecorationStyle(const std::string &value) {
        if      (value == "solid")  _decorationStyle = SOLID;
        else if (value == "double") _decorationStyle = DOUBLE;
        else if (value == "dotted") _decorationStyle = DOTTED;
        else if (value == "dashed") _decorationStyle = DASHED;
        else if (value == "wavy")   _decorationStyle = WAVY;
    }

    void Text::setJustify(const std::string &value) {
        if      (value == "auto")            _justify = AUTO_JUSTIFY;
        else if (value == "inter-word")      _justify = INTER_WORD;
        else if (value == "inter-character") _justify = INTER_CHARACTER;
        else if (value == "none")            _justify = NONE_JUSTIFY;
    }

    void Text::setOverflow(const std::string &value) {
        if      (value == "clip")     _overflow = CLIP;
        else if (value == "ellipsis") _overflow = ELLIPSIS;
        else if (value == "string")   _overflow = STRING;
    }

    void Text::setShadow(const std::string &value) {
        const static PEGParserAST parserAST(R"(
            SHADOW_T          <- LENGTH{2,3} COLOR? /
                                 COLOR? LENGTH{2,3} /
                                 "none"

            COLOR             <- < RGB / RGBA / HSL / HSLA / HEX_COLOR / NAMED_COLOR / "currentcolor" >

            NAMED_COLOR       <- "aliceblue" | "antiquewhite" | "aqua" | "aquamarine" | "azure" | "beige" |
                                 "bisque" | "black" | "blanchedalmond" | "blue" | "blueviolet" | "brown" | "burlywood" |
                                 "cadetblue" | "chartreuse" | "chocolate" | "coral" | "cornflowerblue" | "cornsilk" |
                                 "crimson" | "cyan" | "darkblue" | "darkcyan" | "darkgoldenrod" | "darkgray" | "darkgrey" |
                                 "darkgreen" | "darkkhaki" | "darkmagenta" | "darkolivegreen" | "darkorange" | "darkorchid" |
                                 "darkred" | "darksalmon" | "darkseagreen" | "darkslateblue" | "darkslategray" | "darkslategrey" |
                                 "darkturquoise" | "darkviolet" | "deeppink" | "deepskyblue" | "dimgray" | "dimgrey" | "dodgerblue" |
                                 "firebrick" | "floralwhite" | "forestgreen" | "fuchsia" | "gainsboro" | "ghostwhite" | "gold" |
                                 "goldenrod" | "gray" | "grey" | "green" | "greenyellow" | "honeydew" | "hotpink" | "indianred " |
                                 "indigo " | "ivory" | "khaki" | "lavender" | "lavenderblush" | "lawngreen" | "lemonchiffon" |
                                 "lightblue" | "lightcoral" | "lightcyan" | "lightgoldenrodyellow" | "lightgray" | "lightgrey" |
                                 "lightgreen" | "lightpink" | "lightsalmon" | "lightseagreen" | "lightskyblue" | "lightslategray" |
                                 "lightslategrey" | "lightsteelblue" | "lightyellow" | "lime" | "limegreen" | "linen" |
                                 "magenta" | "maroon" | "mediumaquamarine" | "mediumblue" | "mediumorchid" | "mediumpurple" |
                                 "mediumseagreen" | "mediumslateblue" | "mediumspringgreen" | "mediumturquoise" | "mediumvioletred" |
                                 "midnightblue" | "mintcream" | "mistyrose" | "moccasin" | "navajowhite" | "navy" | "oldlace" |
                                 "olive" | "olivedrab" | "orange" | "orangered" | "orchid" | "palegoldenrod" | "palegreen" |
                                 "paleturquoise" | "palevioletred" | "papayawhip" | "peachpuff" | "peru" | "pink" | "plum" |
                                 "powderblue" | "purple" | "rebeccapurple" | "red" | "rosybrown" | "royalblue" | "saddlebrown" |
                                 "salmon" | "sandybrown" | "seagreen" | "seashell" | "sienna" | "silver" | "skyblue" | "slateblue" |
                                 "slategray" | "slategrey" | "snow" | "springgreen" | "steelblue" | "tan" | "teal" | "thistle" |
                                 "tomato" | "turquoise" | "transparent" | "violet" | "wheat" | "white" | "whitesmoke" |
                                 "yellow" | "yellowgreen"

            HEX_COLOR         <- '#' (HEX_DIGIT{8} /
                                       HEX_DIGIT{6} /
                                       HEX_DIGIT{3,4}  ) ' '?

            RGB               <- "rgb"  "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgb"  "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgb"  "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                 "rgb"  "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
            RGBA              <- "rgba" "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgba" "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgba" "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                 "rgba" "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
            HSL               <- "hsl"  "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                 "hsl"  "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'
            HSLA              <- "hsla" "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                 "hsla" "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'

            ALPHA_VALUE       <- NUMBER / PERCENTAGE
            HUE               <- NUMBER / ANGLE

            LENGTH            <- < '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT >
            LENGTH_UNIT       <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

            ANGLE             <- NUMBER ANGLE_UNIT
            ANGLE_UNIT        <- "deg" | "grad" | "rad" | "turn"

            PERCENTAGE        <- <NUMBER> '%'
            NUMBER            <- <FLOAT / INTEGER>
            FLOAT             <- ('+'|'-')? [0-9]* '.' [0-9]+
            INTEGER           <- ('+'|'-')? [0-9]+
            HEX_DIGIT         <- [a-fA-F0-9]

            %whitespace       <-  [ \t\r\n]*
        )");



        auto values = Utils::split_string_by(value, ',');

        for (auto & val : values) {
            Shadow sh;
            auto properties = parserAST.getPairs(val);

            for (auto & prop : properties) {
                if        (prop.first == "LENGTH") {
                    sh.length.push_back(getLength(prop.second));

                } else if (prop.first == "COLOR") {
                    Color *aux = getColor(prop.second);
                    sh.color = *aux;
                    delete aux;
                }
            }

            _shadow.push_back(sh);
        }
    }

    void Text::setTransform(const std::string &value) {
        if      (value == "none")        _transform = NONE_TRANS;
        else if (value == "capitalize")  _transform = CAPITALIZE;
        else if (value == "uppercase")   _transform = UPPERCASE;
        else if (value == "lowercase")   _transform = LOWERCASE;
    }

    //+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Text::Text(StyleContext *parentContext_) :
            Module(TEXT, parentContext_) {

        init(properties);
    }

}//END NAMESPACE
