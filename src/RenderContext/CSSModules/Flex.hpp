#ifndef GUINOX_FLEX_HPP
#define GUINOX_FLEX_HPP


#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Flex : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST flexParser;
        const static PEGParserAST flexFlowParser;


        static std::vector<std::pair<std::string, std::string>> mapFlexShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapFlexFlowShorthand(const std::string & value);


    public:

        enum Direction { ROW, ROW_REVERSE, COLUMN, COLUMN_REVERSE };
        enum Wrap { NOWRAP, WRAP, WRAP_REVERSE };
        enum Justify { FLEX_START, FLEX_END, CENTER, SPACE_BETWEEN, SPACE_AROUND };
        enum Align { AUTO_A, STRETCH, CENTER_A, FLEX_START_A, FLEX_END_A, BASELINE };


    private:

        Direction _direction = ROW;
        Wrap _wrap = NOWRAP;
        Length _basis, _grow, _shrink;
        Justify _justify;
        Align _align_items;
        Align _align_content;
        Align _align_self;
        Length _order;


        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;

        static Direction getDirection(const std::string & value);
        static Justify getJustify(const std::string & value);
        static Align getAlign(const std::string & value);
        static Wrap getWrap(const std::string & value);


    public:

        const Direction & direction = _direction;
        const Wrap & wrap = _wrap;
        const Length & basis = _basis;
        const Length & grow = _grow;
        const Length & shrink = _shrink;
        const Justify & justify = _justify;
        const Align & align_items = _align_items;
        const Align & align_content = _align_content;
        const Align & align_self = _align_self;
        const Length & order = _order;


        Flex(StyleContext *parentContext);

    };

}


#endif //GUINOX_FLEX_HPP
