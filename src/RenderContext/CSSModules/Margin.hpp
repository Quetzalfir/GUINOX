#ifndef GUINOX_MARGIN_HPP
#define GUINOX_MARGIN_HPP

#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {

    class Margin : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST parser;


        static std::vector<std::pair<std::string, std::string>> mapMarginShorthand(const std::string & value);


        Length _top;
        Length _bottom;
        Length _left;
        Length _right;

        void setProp(const std::string & name, Length & length);
        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;


    public:
        const Length & top = _top;
        const Length & bottom = _bottom;
        const Length & left = _left;
        const Length & right = _right;


        Margin(StyleContext *parentContext);

    };

}


#endif //GUINOX_MARGIN_HPP
