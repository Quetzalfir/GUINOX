#include "Float.hpp"


namespace CSSModule {


//----------------------------- STATICS -----------------------------//



//----------------------------- PRIVATE -----------------------------//

    void Float::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto floatt = dynamic_cast<Float *>(ref)) {
            if      (name == "float")  _eFloat = floatt->_eFloat;
            else if (name == "clear")  _clear = floatt->_clear;
        }
    }

    void Float::setter(const std::string &name, const std::string &value) {
        if      (name == "float")  setFloat(value);
        else if (name == "clear")  setClear(value);
    }

    void Float::setFloat(const std::string &value) {
        if      (value == "none")   _eFloat = NONE_F;
        else if (value == "left")   _eFloat = LEFT_F;
        else if (value == "right")  _eFloat = RIGHT_F;
    }

    void Float::setClear(const std::string &value) {
        if      (value == "none")   _clear = NONE_C;
        else if (value == "left")   _clear = LEFT_C;
        else if (value == "right")  _clear = RIGHT_C;
        else if (value == "both")   _clear = BOTH;
    }


//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Float::Float(StyleContext *parentContext_) :
            Module(FLOAT, parentContext_) {

        init(properties);
    }

}