#include "Margin.hpp"


namespace CSSModule {


//----------------------------- STATICS -----------------------------//

    const PEGParserAST Margin::parser(R"(
         MARGIN              <- GLOBAL_VALUE / (LENGTH / PERCENTAGE / AUTO){1,4}

         GLOBAL_VALUE        <- INHERIT / INITIAL / UNSET

         INHERIT             <- "inherit"
         INITIAL             <- "initial"
         UNSET               <- "unset"
         AUTO                <- "auto"

         LENGTH              <- < '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT >
         LENGTH_UNIT         <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

         PERCENTAGE          <- < NUMBER '%' >
         NUMBER              <- <FLOAT / INTEGER>
         FLOAT               <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER             <- ('+'|'-')? [0-9]+

         %whitespace         <-  [ \t\r\n]*
    )");




    std::vector<std::pair<std::string, std::string> > Margin::mapMarginShorthand(const std::string &value) {
        static const std::string property = "margin";
        auto tokens = parser.getPairs(value);

        std::vector<std::pair<std::string, std::string>> props;

        switch (tokens.size()) {
            case 4:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[1].second);
                props.emplace_back(property + "-bottom", tokens[2].second);
                props.emplace_back(property + "-left", tokens[3].second);

                break;

            case 3:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[1].second);
                props.emplace_back(property + "-bottom", tokens[1].second);
                props.emplace_back(property + "-left", tokens[2].second);

                break;

            case 2:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[1].second);
                props.emplace_back(property + "-bottom", tokens[0].second);
                props.emplace_back(property + "-left", tokens[1].second);

                break;

            case 1:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[0].second);
                props.emplace_back(property + "-bottom", tokens[0].second);
                props.emplace_back(property + "-left", tokens[0].second);

                break;
        }

        return props;
    }

//----------------------------- PRIVATE -----------------------------//

    void Margin::setProp(const std::string & name, Length &length) {
        if      (name == "margin-top")      _top    = length;
        else if (name == "margin-bottom")   _bottom = length;
        else if (name == "margin-left")     _left   = length;
        else if (name == "margin-right")    _right  = length;
    }

    void Margin::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto margin = dynamic_cast<Margin *>(ref)) {
            if      (name == "margin-top")     _top    = margin->_top;
            else if (name == "margin-bottom")  _bottom = margin->_bottom;
            else if (name == "margin-left")    _left   = margin->_left;
            else if (name == "margin-right")   _right  = margin->_right;
        }
    }

    void Margin::setter(const std::string &name, const std::string &value) {
        if (value == "auto") {
            if      (name == "margin-top")     { _top.num    = 0; _top.unit    = PX;   }
            else if (name == "margin-bottom")  { _bottom.num = 0; _bottom.unit = PX;   }
            else if (name == "margin-left")    { _left = getLength("auto"); }
            else if (name == "margin-right")   { _right = getLength("auto"); }

        } else {
            Length length = getLength(value);
            setProp(name, length);
        }
    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Margin::Margin(StyleContext *parentContext_) :
            Module(MARGIN, parentContext_) {

        init(properties);
    }

}