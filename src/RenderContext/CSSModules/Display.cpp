#include "Display.hpp"



namespace CSSModule {


//----------------------------- STATICS -----------------------------//



//----------------------------- PRIVATE -----------------------------//

    void Display::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto display = dynamic_cast<Display *>(ref)) {
            if (name == "display") _display = display->_display;
        }
    }

    void Display::setter(const std::string &name, const std::string &value) {
        if (name == "display") setDisplay(value);
    }

    void Display::setDisplay(const std::string &value) {
        if      (value == "inline")             _display = INLINE;
        else if (value == "block")              _display = BLOCK;
        else if (value == "contents")           _display = CONTENTS;
        else if (value == "flex")               _display = FLEX_D;
        else if (value == "grid")               _display = GRID_D;
        else if (value == "inline-block")       _display = INLINE_BLOCK;
        else if (value == "inline-flex")        _display = INLINE_FLEX;
        else if (value == "inline-grid")        _display = INLINE_GRID;
        else if (value == "inline-table")       _display = INLINE_TABLE;
        else if (value == "list-item")          _display = LIST_ITEM;
        else if (value == "run-in")             _display = RUN_IN;
        else if (value == "table")              _display = TABLE_D;
        else if (value == "table-caption")      _display = TABLE_CAPTION;
        else if (value == "table-column-group") _display = TABLE_COLUMN_GROUP;
        else if (value == "table-header-group") _display = TABLE_HEADER_GROUP;
        else if (value == "table-footer-group") _display = TABLE_FOOTER_GROUP;
        else if (value == "table-row-group")    _display = TABLE_ROW_GROUP;
        else if (value == "table-cell")         _display = TABLE_CELL;
        else if (value == "table-column")       _display = TABLE_COLUMN;
        else if (value == "table-row")          _display = TABLE_ROW;
        else if (value == "none")               _display = NONE;
    }


//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Display::Display(StyleContext *parentContext_) :
            Module(DISPLAY, parentContext_) {

        init(properties);
    }

}