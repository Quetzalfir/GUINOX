#include "Border.hpp"
#include <Utils/Utils.hpp>



static
std::vector<std::pair<std::string, std::string>>
completeCoordinate(
        const std::string & coord,
        const std::vector<std::pair<std::string, std::string>> & tokens
        ) {

    std::vector<std::pair<std::string, std::string>> props = tokens;


    for (auto & p : props) {
        size_t pos = p.first.find_last_of('-');
        p.first.insert(pos, "-"+coord);
    }

    return props;
}

static
std::vector<std::pair<std::string, std::string>>
completeType(
        const std::string & type,
        const std::vector<std::pair<std::string, std::string>> & tokens
        ) {

    std::vector<std::pair<std::string, std::string>> props;


    switch (tokens.size()) {
        case 4:
            props.emplace_back("border-top-"+type, tokens[0].second);
            props.emplace_back("border-right-"+type, tokens[1].second);
            props.emplace_back("border-bottom-"+type, tokens[2].second);
            props.emplace_back("border-left-"+type, tokens[3].second);

            break;

        case 3:
            props.emplace_back("border-top-"+type, tokens[0].second);
            props.emplace_back("border-right-"+type, tokens[1].second);
            props.emplace_back("border-bottom-"+type, tokens[1].second);
            props.emplace_back("border-left-"+type, tokens[2].second);

            break;

        case 2:
            props.emplace_back("border-top-"+type, tokens[0].second);
            props.emplace_back("border-right-"+type, tokens[1].second);
            props.emplace_back("border-bottom-"+type, tokens[0].second);
            props.emplace_back("border-left-"+type, tokens[1].second);

            break;

        case 1:
            props.emplace_back("border-top-"+type, tokens[0].second);
            props.emplace_back("border-right-"+type, tokens[0].second);
            props.emplace_back("border-bottom-"+type, tokens[0].second);
            props.emplace_back("border-left-"+type, tokens[0].second);

            break;
    }

    return props;
}



namespace CSSModule {

//----------------------------- STATICS -----------------------------//

    const PEGParserAST Border::borderParser(R"(
         BORDER              <- INHERIT /
                                (BORDER_WIDTH BORDER_STYLE BORDER_COLOR  /
                                BORDER_COLOR BORDER_STYLE BORDER_WIDTH  /
                                BORDER_COLOR BORDER_WIDTH BORDER_STYLE  /
                                BORDER_STYLE BORDER_COLOR BORDER_WIDTH  /
                                BORDER_STYLE BORDER_WIDTH BORDER_COLOR  /
                                BORDER_WIDTH BORDER_COLOR BORDER_STYLE  /
                                BORDER_COLOR BORDER_STYLE  /
                                BORDER_COLOR BORDER_WIDTH  /
                                BORDER_STYLE BORDER_COLOR  /
                                BORDER_STYLE BORDER_WIDTH  /
                                BORDER_WIDTH BORDER_COLOR  /
                                BORDER_WIDTH BORDER_STYLE  /
                                BORDER_COLOR  /
                                BORDER_STYLE  /
                                BORDER_WIDTH  )

        BORDER_WIDTH         <- < BORDER_WIDTH_VALUE{1,4} / INHERIT >

        BORDER_WIDTH_VALUE   <- LENGTH / VALUE

        VALUE                <- "thin" | "medium" | "thick"

        BORDER_STYLE         <- < LINE_STYLE{1,4} / GLOBAL_VALUE >

        LINE_STYLE           <- "none" | "hidden" | "dotted" | "dashed" | "solid" | "double" | "groove" | "ridge" | "inset" | "outset"

        BORDER_COLOR         <- < COLOR{1,4} / INHERIT >

        COLOR                <- RGB / RGBA / HSL / HSLA / HEX_COLOR / NAMED_COLOR / "currentcolor"

        NAMED_COLOR          <- "aliceblue" | "antiquewhite" | "aqua" | "aquamarine" | "azure" | "beige" |
                                "bisque" | "black" | "blanchedalmond" | "blue" | "blueviolet" | "brown" | "burlywood" |
                                "cadetblue" | "chartreuse" | "chocolate" | "coral" | "cornflowerblue" | "cornsilk" |
                                "crimson" | "cyan" | "darkblue" | "darkcyan" | "darkgoldenrod" | "darkgray" | "darkgrey" |
                                "darkgreen" | "darkkhaki" | "darkmagenta" | "darkolivegreen" | "darkorange" | "darkorchid" |
                                "darkred" | "darksalmon" | "darkseagreen" | "darkslateblue" | "darkslategray" | "darkslategrey" |
                                "darkturquoise" | "darkviolet" | "deeppink" | "deepskyblue" | "dimgray" | "dimgrey" | "dodgerblue" |
                                "firebrick" | "floralwhite" | "forestgreen" | "fuchsia" | "gainsboro" | "ghostwhite" | "gold" |
                                "goldenrod" | "gray" | "grey" | "green" | "greenyellow" | "honeydew" | "hotpink" | "indianred " |
                                "indigo " | "ivory" | "khaki" | "lavender" | "lavenderblush" | "lawngreen" | "lemonchiffon" |
                                "lightblue" | "lightcoral" | "lightcyan" | "lightgoldenrodyellow" | "lightgray" | "lightgrey" |
                                "lightgreen" | "lightpink" | "lightsalmon" | "lightseagreen" | "lightskyblue" | "lightslategray" |
                                "lightslategrey" | "lightsteelblue" | "lightyellow" | "lime" | "limegreen" | "linen" |
                                "magenta" | "maroon" | "mediumaquamarine" | "mediumblue" | "mediumorchid" | "mediumpurple" |
                                "mediumseagreen" | "mediumslateblue" | "mediumspringgreen" | "mediumturquoise" | "mediumvioletred" |
                                "midnightblue" | "mintcream" | "mistyrose" | "moccasin" | "navajowhite" | "navy" | "oldlace" |
                                "olive" | "olivedrab" | "orange" | "orangered" | "orchid" | "palegoldenrod" | "palegreen" |
                                "paleturquoise" | "palevioletred" | "papayawhip" | "peachpuff" | "peru" | "pink" | "plum" |
                                "powderblue" | "purple" | "rebeccapurple" | "red" | "rosybrown" | "royalblue" | "saddlebrown" |
                                "salmon" | "sandybrown" | "seagreen" | "seashell" | "sienna" | "silver" | "skyblue" | "slateblue" |
                                "slategray" | "slategrey" | "snow" | "springgreen" | "steelblue" | "tan" | "teal" | "thistle" |
                                "tomato" | "turquoise" | "transparent" | "violet" | "wheat" | "white" | "whitesmoke" |
                                "yellow" | "yellowgreen"

        HEX_COLOR            <- '#' (HEX_DIGIT{8} /
                                      HEX_DIGIT{6} /
                                      HEX_DIGIT{3,4}  ) ' '?

        RGB                  <- "rgb"  "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                "rgb"  "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                "rgb"  "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                "rgb"  "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
        RGBA                 <- "rgba" "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                "rgba" "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                "rgba" "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                "rgba" "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
        HSL                  <- "hsl"  "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                "hsl"  "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'
        HSLA                 <- "hsla" "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                "hsla" "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'

        ALPHA_VALUE          <- NUMBER / PERCENTAGE
        HUE                  <- NUMBER / ANGLE

        GLOBAL_VALUE         <- INHERIT / INITIAL / UNSET

        INHERIT              <- "inherit"
        INITIAL              <- "initial"
        UNSET                <- "unset"
        NONE                 <- "none"
        AUTO                 <- "auto"

        LENGTH               <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
        LENGTH_UNIT          <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

        ANGLE                <- NUMBER ANGLE_UNIT
        ANGLE_UNIT           <- "deg" | "grad" | "rad" | "turn"

        PERCENTAGE           <- <NUMBER> '%'
        NUMBER               <- <FLOAT / INTEGER>
        FLOAT                <- ('+'|'-')? [0-9]* '.' [0-9]+
        INTEGER              <- ('+'|'-')? [0-9]+
        HEX_DIGIT            <- [a-fA-F0-9]

        %whitespace          <-  [ \t\r\n]*
    )");

    const PEGParserAST Border::borderCoordParser(R"(
         BORDER_TOP          <- BORDER_WIDTH BORDER_STYLE BORDER_COLOR  /
                                BORDER_COLOR BORDER_STYLE BORDER_WIDTH  /
                                BORDER_COLOR BORDER_WIDTH BORDER_STYLE  /
                                BORDER_STYLE BORDER_COLOR BORDER_WIDTH  /
                                BORDER_STYLE BORDER_WIDTH BORDER_COLOR  /
                                BORDER_WIDTH BORDER_COLOR BORDER_STYLE  /
                                BORDER_WIDTH BORDER_STYLE  /
                                BORDER_STYLE BORDER_WIDTH  /
                                BORDER_COLOR BORDER_STYLE  /
                                BORDER_COLOR BORDER_WIDTH  /
                                BORDER_STYLE BORDER_COLOR  /
                                BORDER_WIDTH BORDER_COLOR  /
                                BORDER_WIDTH  /
                                BORDER_STYLE  /
                                BORDER_COLOR

         BORDER_WIDTH        <- < LENGTH / LITERALS_WIDTH >

         LITERALS_WIDTH      <- "thin" | "medium" | "thick"

         BORDER_STYLE        <- < "none" | "hidden" | "dotted" | "dashed" | "solid" | "double" | "groove" | "ridge" | "inset" | "outset" >

         BORDER_COLOR        <- < RGB / RGBA / HSL / HSLA / HEX_COLOR / NAMED_COLOR / "currentcolor" >

         NAMED_COLOR         <- "aliceblue" | "antiquewhite" | "aqua" | "aquamarine" | "azure" | "beige" |
                                "bisque" | "black" | "blanchedalmond" | "blue" | "blueviolet" | "brown" | "burlywood" |
                                "cadetblue" | "chartreuse" | "chocolate" | "coral" | "cornflowerblue" | "cornsilk" |
                                "crimson" | "cyan" | "darkblue" | "darkcyan" | "darkgoldenrod" | "darkgray" | "darkgrey" |
                                "darkgreen" | "darkkhaki" | "darkmagenta" | "darkolivegreen" | "darkorange" | "darkorchid" |
                                "darkred" | "darksalmon" | "darkseagreen" | "darkslateblue" | "darkslategray" | "darkslategrey" |
                                "darkturquoise" | "darkviolet" | "deeppink" | "deepskyblue" | "dimgray" | "dimgrey" | "dodgerblue" |
                                "firebrick" | "floralwhite" | "forestgreen" | "fuchsia" | "gainsboro" | "ghostwhite" | "gold" |
                                "goldenrod" | "gray" | "grey" | "green" | "greenyellow" | "honeydew" | "hotpink" | "indianred " |
                                "indigo " | "ivory" | "khaki" | "lavender" | "lavenderblush" | "lawngreen" | "lemonchiffon" |
                                "lightblue" | "lightcoral" | "lightcyan" | "lightgoldenrodyellow" | "lightgray" | "lightgrey" |
                                "lightgreen" | "lightpink" | "lightsalmon" | "lightseagreen" | "lightskyblue" | "lightslategray" |
                                "lightslategrey" | "lightsteelblue" | "lightyellow" | "lime" | "limegreen" | "linen" |
                                "magenta" | "maroon" | "mediumaquamarine" | "mediumblue" | "mediumorchid" | "mediumpurple" |
                                "mediumseagreen" | "mediumslateblue" | "mediumspringgreen" | "mediumturquoise" | "mediumvioletred" |
                                "midnightblue" | "mintcream" | "mistyrose" | "moccasin" | "navajowhite" | "navy" | "oldlace" |
                                "olive" | "olivedrab" | "orange" | "orangered" | "orchid" | "palegoldenrod" | "palegreen" |
                                "paleturquoise" | "palevioletred" | "papayawhip" | "peachpuff" | "peru" | "pink" | "plum" |
                                "powderblue" | "purple" | "rebeccapurple" | "red" | "rosybrown" | "royalblue" | "saddlebrown" |
                                "salmon" | "sandybrown" | "seagreen" | "seashell" | "sienna" | "silver" | "skyblue" | "slateblue" |
                                "slategray" | "slategrey" | "snow" | "springgreen" | "steelblue" | "tan" | "teal" | "thistle" |
                                "tomato" | "turquoise" | "transparent" | "violet" | "wheat" | "white" | "whitesmoke" |
                                "yellow" | "yellowgreen"

         HEX_COLOR           <- '#' (HEX_DIGIT{8} /
                                     HEX_DIGIT{6} /
                                     HEX_DIGIT{3,4}  ) ' '?

         RGB                 <- "rgb"  "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                "rgb"  "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                "rgb"  "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                "rgb"  "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         RGBA                <- "rgba" "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                "rgba" "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                "rgba" "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                "rgba" "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         HSL                 <- "hsl"  "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                "hsl"  "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'
         HSLA                <- "hsla" "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                "hsla" "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'

         ALPHA_VALUE         <- NUMBER / PERCENTAGE
         HUE                 <- NUMBER / ANGLE

         GLOBAL_VALUE        <- INHERIT / INITIAL / UNSET

         INHERIT             <- "inherit"
         INITIAL             <- "initial"
         UNSET               <- "unset"
         NONE                <- "none"
         AUTO                <- "auto"

         LENGTH              <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
         LENGTH_UNIT         <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

         ANGLE               <- NUMBER ANGLE_UNIT
         ANGLE_UNIT          <- "deg" | "grad" | "rad" | "turn"

         PERCENTAGE          <- <NUMBER> '%'
         NUMBER              <- <FLOAT / INTEGER>
         FLOAT               <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER             <- ('+'|'-')? [0-9]+
         HEX_DIGIT           <- [a-fA-F0-9]

         %whitespace         <-  [ \t\r\n]*
    )");

    const PEGParserAST Border::borderWidthParser(R"(
         BORDER_WIDTH  		<- BORDER_WIDTH_VALUE{1,4} / INHERIT

         BORDER_WIDTH_VALUE	<- < LENGTH / VALUE >

         VALUE 			    <- "thin" | "medium" | "thick"

         GLOBAL_VALUE       <- INHERIT / INITIAL / UNSET

         INHERIT            <- "inherit"
         INITIAL            <- "initial"
         UNSET              <- "unset"
         NONE 				<- "none"
         AUTO               <- "auto"

         LENGTH             <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
         LENGTH_UNIT        <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

         NUMBER             <- <FLOAT / INTEGER>
         FLOAT              <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER            <- ('+'|'-')? [0-9]+

         %whitespace        <-  [ \t\r\n]*
    )");

    const PEGParserAST Border::borderColorParser(R"(
         BORDER_COLOR		<- COLOR{1,4} / INHERIT

         COLOR				<- < RGB / RGBA / HSL / HSLA / HEX_COLOR / NAMED_COLOR / "currentcolor" >

         NAMED_COLOR		<- "aliceblue" | "antiquewhite" | "aqua" | "aquamarine" | "azure" | "beige" |
                 			   "bisque" | "black" | "blanchedalmond" | "blue" | "blueviolet" | "brown" | "burlywood" |
                 			   "cadetblue" | "chartreuse" | "chocolate" | "coral" | "cornflowerblue" | "cornsilk" |
                 			   "crimson" | "cyan" | "darkblue" | "darkcyan" | "darkgoldenrod" | "darkgray" | "darkgrey" |
                 			   "darkgreen" | "darkkhaki" | "darkmagenta" | "darkolivegreen" | "darkorange" | "darkorchid" |
                 			   "darkred" | "darksalmon" | "darkseagreen" | "darkslateblue" | "darkslategray" | "darkslategrey" |
                 			   "darkturquoise" | "darkviolet" | "deeppink" | "deepskyblue" | "dimgray" | "dimgrey" | "dodgerblue" |
                 			   "firebrick" | "floralwhite" | "forestgreen" | "fuchsia" | "gainsboro" | "ghostwhite" | "gold" |
                 			   "goldenrod" | "gray" | "grey" | "green" | "greenyellow" | "honeydew" | "hotpink" | "indianred " |
                 			   "indigo " | "ivory" | "khaki" | "lavender" | "lavenderblush" | "lawngreen" | "lemonchiffon" |
                 			   "lightblue" | "lightcoral" | "lightcyan" | "lightgoldenrodyellow" | "lightgray" | "lightgrey" |
                 			   "lightgreen" | "lightpink" | "lightsalmon" | "lightseagreen" | "lightskyblue" | "lightslategray" |
                 			   "lightslategrey" | "lightsteelblue" | "lightyellow" | "lime" | "limegreen" | "linen" |
                 			   "magenta" | "maroon" | "mediumaquamarine" | "mediumblue" | "mediumorchid" | "mediumpurple" |
                 			   "mediumseagreen" | "mediumslateblue" | "mediumspringgreen" | "mediumturquoise" | "mediumvioletred" |
                 			   "midnightblue" | "mintcream" | "mistyrose" | "moccasin" | "navajowhite" | "navy" | "oldlace" |
                 			   "olive" | "olivedrab" | "orange" | "orangered" | "orchid" | "palegoldenrod" | "palegreen" |
                 			   "paleturquoise" | "palevioletred" | "papayawhip" | "peachpuff" | "peru" | "pink" | "plum" |
                 			   "powderblue" | "purple" | "rebeccapurple" | "red" | "rosybrown" | "royalblue" | "saddlebrown" |
                 			   "salmon" | "sandybrown" | "seagreen" | "seashell" | "sienna" | "silver" | "skyblue" | "slateblue" |
                 			   "slategray" | "slategrey" | "snow" | "springgreen" | "steelblue" | "tan" | "teal" | "thistle" |
                 			   "tomato" | "turquoise" | "transparent" | "violet" | "wheat" | "white" | "whitesmoke" |
                 			   "yellow" | "yellowgreen"

         HEX_COLOR          <- '#' (HEX_DIGIT{8} /
                                    HEX_DIGIT{6} /
                                    HEX_DIGIT{3,4}  ) ' '?

         RGB                <- "rgb"  "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                               "rgb"  "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                               "rgb"  "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                               "rgb"  "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         RGBA               <- "rgba" "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                               "rgba" "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                               "rgba" "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                               "rgba" "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         HSL                <- "hsl"  "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                               "hsl"  "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'
         HSLA               <- "hsla" "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                               "hsla" "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'

         ALPHA_VALUE        <- PERCENTAGE / NUMBER
         HUE                <- ANGLE / NUMBER

         GLOBAL_VALUE       <- INHERIT / INITIAL / UNSET

         INHERIT            <- "inherit"
         INITIAL            <- "initial"
         UNSET              <- "unset"
         NONE 				<- "none"
         AUTO               <- "auto"

         LENGTH             <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
         LENGTH_UNIT        <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

         ANGLE              <- NUMBER ANGLE_UNIT
         ANGLE_UNIT         <- "deg" | "grad" | "rad" | "turn"

         PERCENTAGE         <- <NUMBER> '%'
         NUMBER             <- <FLOAT / INTEGER>
         FLOAT              <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER            <- ('+'|'-')? [0-9]+
         HEX_DIGIT			<- [a-fA-F0-9]

         %whitespace        <-  [ \t\r\n]*
    )");

    const PEGParserAST Border::borderStyleParser(R"(
         BORDER_STYLE        <- LINE_STYLE{1,4} / GLOBAL_VALUE

         LINE_STYLE          <- < "none" | "hidden" | "dotted" | "dashed" | "solid" | "double" | "groove" | "ridge" | "inset" | "outset" >

         GLOBAL_VALUE        <- INHERIT / INITIAL / UNSET

         INHERIT             <- "inherit"
         INITIAL             <- "initial"
         UNSET               <- "unset"

         %whitespace         <-  [ \t\r\n]*
    )");




    std::vector<std::pair<std::string, std::string> > Border::mapBorderShorthand(const std::string &value) {
        auto tokens = borderParser.getPairs(value);
        std::vector<std::pair<std::string, std::string>> props;

        for (auto & p : tokens) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
            auto values = Utils::split_string_by(p.second, ' ');
            std::vector<std::pair<std::string, std::string>> aux;
            aux.reserve(values.size());

            for (auto & v : values) {
                aux.emplace_back(p.first, v);
            }

            auto pos = p.first.find_last_of('-');
            std::string type = p.first.substr(pos+1);

            auto aux2 = completeType(type, aux);

            props.insert(props.end(), aux2.begin(), aux2.end());
        }

        return props;
    }

    std::vector<std::pair<std::string, std::string> > Border::mapBorderTopShorthand(const std::string &value) {
        std::vector<std::pair<std::string, std::string>> props;
        auto tokens = borderCoordParser.getPairs(value);

        for (auto & p : tokens) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return completeCoordinate("top", tokens);
    }

    std::vector<std::pair<std::string, std::string> > Border::mapBorderLeftShorthand(const std::string &value) {
        std::vector<std::pair<std::string, std::string>> props;
        auto tokens = borderCoordParser.getPairs(value);

        for (auto & p : tokens) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return completeCoordinate("left", tokens);
    }

    std::vector<std::pair<std::string, std::string> > Border::mapBorderRightShorthand(const std::string &value) {
        std::vector<std::pair<std::string, std::string>> props;
        auto tokens = borderCoordParser.getPairs(value);

        for (auto & p : tokens) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return completeCoordinate("right", tokens);
    }

    std::vector<std::pair<std::string, std::string> > Border::mapBorderBottomShorthand(const std::string &value) {
        std::vector<std::pair<std::string, std::string>> props;
        auto tokens = borderCoordParser.getPairs(value);

        for (auto & p : tokens) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return completeCoordinate("bottom", tokens);
    }

    std::vector<std::pair<std::string, std::string> > Border::mapBorderWidthShorthand(const std::string &value) {
        auto tokens = borderWidthParser.getPairs(value);

        return completeType("width", tokens);
    }

    std::vector<std::pair<std::string, std::string> > Border::mapBorderColorShorthand(const std::string &value) {
        auto tokens = borderColorParser.getPairs(value);

        return completeType("color", tokens);
    }

    std::vector<std::pair<std::string, std::string> > Border::mapBorderStyleShorthand(const std::string &value) {
        auto tokens = borderStyleParser.getPairs(value);

        return completeType("style", tokens);
    }

//----------------------------- PRIVATE -----------------------------//



    void Border::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto border = dynamic_cast<Border *>(ref)) {

            if      (name == "border-bottom-color") _bottom.color = border->_bottom.color;
            else if (name == "border-bottom-style") _bottom.style = border->_bottom.style;
            else if (name == "border-bottom-width") _bottom.width = border->_bottom.width;
            else if (name == "border-left-color")   _left.color = border->_left.color;
            else if (name == "border-left-style")   _left.style = border->_left.style;
            else if (name == "border-left-width")   _left.width = border->_left.width;
            else if (name == "border-right-color")  _right.color = border->_right.color;
            else if (name == "border-right-style")  _right.style = border->_right.style;
            else if (name == "border-right-width")  _right.width = border->_right.width;
            else if (name == "border-top-color")    _top.color = border->_top.color;
            else if (name == "border-top-style")    _top.style = border->_top.style;
            else if (name == "border-top-width")    _top.width = border->_top.width;
        }
    }

    void Border::setter(const std::string &name, const std::string &value) {

        if      (name == "border-bottom-color") setColor(value, "bottom");
        else if (name == "border-bottom-style") setStyle(value, "bottom");
        else if (name == "border-bottom-width") setWidth(value, "bottom");
        else if (name == "border-left-color")   setColor(value, "left");
        else if (name == "border-left-style")   setStyle(value, "left");
        else if (name == "border-left-width")   setWidth(value, "left");
        else if (name == "border-right-color")  setColor(value, "right");
        else if (name == "border-right-style")  setStyle(value, "right");
        else if (name == "border-right-width")  setWidth(value, "right");
        else if (name == "border-top-color")    setColor(value, "top");
        else if (name == "border-top-style")    setStyle(value, "top");
        else if (name == "border-top-width")    setWidth(value, "top");

    }

    void Border::setStyle(const std::string &value, const std::string &coord) {
        if      (coord == "top")    _top.style = getStyle(value);
        else if (coord == "left")   _left.style = getStyle(value);
        else if (coord == "right")  _right.style = getStyle(value);
        else if (coord == "bottom") _bottom.style = getStyle(value);
    }

    void Border::setColor(const std::string &value, const std::string &coord) {
        if      (coord == "top")    { delete _top.color; _top.color = getColor(value); }
        else if (coord == "left")   { delete _left.color; _left.color = getColor(value); }
        else if (coord == "right")  { delete _right.color; _right.color = getColor(value); }
        else if (coord == "bottom") { delete _bottom.color; _bottom.color = getColor(value); }
    }

    void Border::setWidth(const std::string &value, const std::string &coord) {
        if      (coord == "top")    _top.width = getWidth(value);
        else if (coord == "left")   _left.width = getWidth(value);
        else if (coord == "right")  _right.width = getWidth(value);
        else if (coord == "bottom") _bottom.width = getWidth(value);
    }

    Border::STYLE Border::getStyle(const std::string &value) {
        if (value == "none")    return NONE;
        if (value == "hidden")  return HIDDEN;
        if (value == "dotted")  return DOTTED;
        if (value == "dashed")  return DASHED;
        if (value == "solid")   return SOLID;
        if (value == "double")  return DOUBLE;
        if (value == "groove")  return GROOVE;
        if (value == "ridge")   return RIDGE;
        if (value == "inset")   return INSET;
        if (value == "outset")  return OUTSET;

        return NONE;
    }

    Length Border::getWidth(const std::string &value) {
        if (value == "thin") return getLength("1px");
        if (value == "medium") return getLength("3px");
        if (value == "thick") return getLength("6px");

        return getPositiveLength(value);
    }



//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Border::Border(StyleContext *parentContext_) :
            Module(BORDER, parentContext_) {

        init(properties);
    }

}