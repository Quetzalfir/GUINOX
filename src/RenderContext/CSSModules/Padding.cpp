#include "RenderContext/CSSModules/Padding.hpp"



namespace CSSModule {


//----------------------------- STATICS -----------------------------//

    const PEGParserAST Padding::parser(R"(
         PADDING             <- (LENGTH / PERCENTAGE){1,4}

         LENGTH              <- < '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT >
         LENGTH_UNIT         <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

         ANGLE               <- NUMBER ANGLE_UNIT
         ANGLE_UNIT          <- "deg" | "grad" | "rad" | "turn"

         PERCENTAGE          <- < NUMBER '%' >
         NUMBER              <- <FLOAT / INTEGER>
         FLOAT               <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER             <- ('+'|'-')? [0-9]+

         %whitespace         <-  [ \t\r\n]*
    )");



    std::vector<std::pair<std::string, std::string> > Padding::mapPaddingShorthand(const std::string &value) {
        static const std::string property = "padding";
        auto tokens = parser.getPairs(value);

        std::vector<std::pair<std::string, std::string>> props;

        switch (tokens.size()) {
            case 4:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[1].second);
                props.emplace_back(property + "-bottom", tokens[2].second);
                props.emplace_back(property + "-left", tokens[3].second);

                break;

            case 3:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[1].second);
                props.emplace_back(property + "-bottom", tokens[1].second);
                props.emplace_back(property + "-left", tokens[2].second);

                break;

            case 2:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[1].second);
                props.emplace_back(property + "-bottom", tokens[0].second);
                props.emplace_back(property + "-left", tokens[1].second);

                break;

            case 1:
                props.emplace_back(property + "-top", tokens[0].second);
                props.emplace_back(property + "-right", tokens[0].second);
                props.emplace_back(property + "-bottom", tokens[0].second);
                props.emplace_back(property + "-left", tokens[0].second);

                break;
        }

        return props;
    }

//----------------------------- PRIVATE -----------------------------//

    void Padding::setProp(const std::string & name, Length &length) {
        if      (name == "padding-top")      _top    = length;
        else if (name == "padding-bottom")   _bottom = length;
        else if (name == "padding-left")     _left   = length;
        else if (name == "padding-right")    _right  = length;
    }

    void Padding::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto padding = dynamic_cast<Padding *>(ref)) {
            if      (name == "padding-top")     _top    = padding->_top;
            else if (name == "padding-bottom")  _bottom = padding->_bottom;
            else if (name == "padding-left")    _left   = padding->_left;
            else if (name == "padding-right")   _right  = padding->_right;
        }
    }

    void Padding::setter(const std::string &name, const std::string &value) {
        Length length = getLength(value);

        if (length.num < 0) length.num = 0;

        setProp(name, length);
    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Padding::Padding(StyleContext *parentContext_) :
            Module(PADDING, parentContext_) {

        init(properties);
    }

}