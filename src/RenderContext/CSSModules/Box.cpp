#include "Box.hpp"


namespace CSSModule {


//----------------------------- STATICS -----------------------------//



//----------------------------- PRIVATE -----------------------------//

    void Box::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto box = dynamic_cast<Box *>(ref)) {
            if      (name == "width")       _width = box->_width;
            else if (name == "height")      _height = box->_height;
            else if (name == "max-width")   _max_width = box->_max_width;
            else if (name == "max-height")  _max_height = box->_max_height;
            else if (name == "min-width")   _min_width = box->_min_width;
            else if (name == "min-height")  _min_height = box->_min_height;
            else if (name == "box-sizing")  _box_sizing = box->_box_sizing;
            else if (name == "img-ratio")   _img_ratio = box->_img_ratio;
        }
    }

    void Box::setter(const std::string &name, const std::string &value) {
        if      (name == "width")       _width = getPositiveLength(value);
        else if (name == "height")      _height = getPositiveLength(value);
        else if (name == "max-width")   _max_width = getMax(value);
        else if (name == "max-height")  _max_height = getMax(value);
        else if (name == "min-width")   _min_width = getPositiveLength(value);
        else if (name == "min-height")  _min_height = getPositiveLength(value);
        else if (name == "box-sizing")  setSizing(value);
        else if (name == "img-ratio")   _img_ratio = getPositiveLength(value);
    }

    void Box::setSizing(const std::string &value) {
        if (value == "border-box") _box_sizing = BORDER_BOX;
        else _box_sizing = CONTENT_BOX;
    }

    Length Box::getMax(const std::string &value) {
        Length aux;

        if (value == "none") {
            aux.num = std::numeric_limits<double>::max();

        } else {
            aux = getPositiveLength(value);
        }

        return aux;
    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Box::Box(StyleContext *parentContext_) :
            Module(BOX, parentContext_) {

        init(properties);
    }

}