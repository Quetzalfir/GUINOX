#ifndef GUINOX_GRID_H
#define GUINOX_GRID_H

#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"

namespace CSSModule {

    class Grid : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST gridParser;
        const static PEGParserAST gridGapParser;
        const static PEGParserAST gridRoCParser;
        const static PEGParserAST gridTemplateParser;
        const static PEGParserAST gridAreaParser;

        static std::vector<std::pair<std::string, std::string>> mapGridShorthand(const std::string & value, const PEGParserAST & parser);
        static std::vector<std::pair<std::string, std::string>> mapGridRoCShorthand(const std::string & value, bool row);



    public:

        enum AutoFlow{ ROW, COLUMN, DENSE, ROW_DENSE, COLUMN_DENSE };



    private:

        std::string _autoColumn ;
        AutoFlow _autoFlow = ROW;
        std::string _autoRows ;
        std::string _columnEnd;
        Length _columnGap; //0
        std::string _columnsStart;
        std::string _rowEnd;
        Length _rowGap;//0
        std::string  _rowStart;
        //TemplateAreas _templateAreas = NONE_TAREA;
        std::string _templateColumns;
        std::string _templateRows;
        std::string _areaName;

        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;


        //void setAutoColumns(const std::string & value);
        //void setAutoFlow(const std::string & value);
        //void setAutoRows(const std::string & value);
        //void setTemplateAreas(const std::string & value);
        //void setTemplateColumns(const std::string & value);
        //void setTemplateRows(const std::string & value);

    public:

        const std::string &autoColumn = _autoColumn;
        const AutoFlow &autoFlow = _autoFlow;
        const std::string &autoRows = _autoRows;
        const std::string &columnEnd = _columnEnd;
        const std::string &columnStart = _columnsStart;
        const std::string &rowEnd = _rowEnd;
        const Length &rowGap = _rowGap;
        const std::string &rowStart = _rowStart;
        //const TemplateAreas &templateAreas = _templateAreas;
        const std::string &templateColumns = _templateColumns;
        const std::string &templateRows = _templateRows;
        const std::string &areaName = _areaName;

        Grid(StyleContext *parentContext);

    };



}//END NAMESPACE




#endif //GUINOX_GRID_H
