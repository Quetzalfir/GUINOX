#ifndef GUINOX_POSITION_HPP
#define GUINOX_POSITION_HPP



#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Position : public Module {
        const static PropertyMap properties;
        const static CSSModuleRegistry regist;


    public:

        enum Position_Type { STATIC, ABSOLUTE, FIXED, RELATIVE, STICKY } ;


    private:

        Position_Type _position;
        Coordinate _coordinates;
        Length _z_index;


        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;
        void setCoord(const std::string & coord, const std::string & value);
        void setPosition(const std::string & value);


    public:

        const Position_Type & position = _position;
        const Coordinate & coordinates = _coordinates;
        const Length & z_index = _z_index;


        Position(StyleContext *parentContext);
    };

}


#endif //GUINOX_POSITION_HPP
