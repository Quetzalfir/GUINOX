#include "Flex.hpp"



namespace CSSModule {


//----------------------------- STATICS -----------------------------//

    const PEGParserAST Flex::flexParser(R"(
         FLEX                <- NONE /
                                (FLEX_GROW FLEX_SHRINK?) FLEX_BASIS  /
                                FLEX_BASIS (FLEX_GROW FLEX_SHRINK?)  /
                                (FLEX_GROW FLEX_SHRINK?)  /
                                FLEX_BASIS

         FLEX_BASIS          <- < "content" / WIDTH >

         WIDTH               <- (LENGTH / PERCENTAGE) WIDTH_BOX? /
                                WIDTH_LITERALS /
                                GLOBAL_VALUE

         WIDTH_LITERALS      <- "available" | "min-content" | "max-content" | "fit-content" | "auto"
         WIDTH_BOX           <- "border-box" | "content-box"

         FLEX_SHRINK         <- < NUMBER / GLOBAL_VALUE >
         FLEX_GROW           <- < NUMBER / GLOBAL_VALUE >

         GLOBAL_VALUE        <- INHERIT / INITIAL / UNSET

         INHERIT             <- "inherit"
         INITIAL             <- "initial"
         UNSET               <- "unset"
         NONE                <- "none"

         LENGTH              <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
         LENGTH_UNIT         <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

         PERCENTAGE          <- <NUMBER> '%'
         NUMBER              <- <FLOAT / INTEGER>
         FLOAT               <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER             <- ('+'|'-')? [0-9]+

         %whitespace         <-  [ \t\r\n]*
    )");

    const PEGParserAST Flex::flexFlowParser(R"(
         FLEX_FLOW           <- FLEX_DIRECTION FLEX_WRAP /
                                FLEX_WRAP FLEX_DIRECTION /
                                FLEX_DIRECTION /
                                FLEX_WRAP

         FLEX_DIRECTION      <- "row" | "row-reverse" | "column" | "column-reverse"

         FLEX_WRAP           <- "nowrap" | "wrap" | "wrap-reverse"
    )");


    std::vector<std::pair<std::string, std::string> > Flex::mapFlexShorthand(const std::string &value) {
        auto properties = flexParser.getPairs(value);

        for (auto & p : properties) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return properties;
    }

    std::vector<std::pair<std::string, std::string> > Flex::mapFlexFlowShorthand(const std::string &value) {
        auto properties = flexFlowParser.getPairs(value);

        for (auto & p : properties) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return properties;
    }

//----------------------------- PRIVATE -----------------------------//

    void Flex::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto flex = dynamic_cast<Flex *>(ref)) {
            if      (name == "flex-direction")  _direction = flex->_direction;
            else if (name == "flex-wrap")       _wrap = flex->_wrap;
            else if (name == "justify-content") _justify = flex->_justify;
            else if (name == "align-items")     _align_items = flex->_align_items;
            else if (name == "align-content")   _align_content = flex->_align_content;
            else if (name == "order")           _order = flex->_order;
            else if (name == "flex-grow")       _grow = flex->_grow;
            else if (name == "flex-shrink")     _shrink = flex->_shrink;
            else if (name == "flex-basis")      _basis = flex->_basis;
            else if (name == "align-self")      _align_self = flex->_align_self;
        }
    }

    void Flex::setter(const std::string &name, const std::string &value) {
        if      (name == "flex-direction")  _direction = getDirection(value);
        else if (name == "flex-wrap")       _wrap = getWrap(value);
        else if (name == "justify-content") _justify = getJustify(value);
        else if (name == "align-items")     _align_items = getAlign(value);
        else if (name == "align-content")   _align_content = getAlign(value);
        else if (name == "order")           _order = getLength(value);
        else if (name == "flex-grow")       _grow = getPositiveLength(value);
        else if (name == "flex-shrink")     _shrink = getPositiveLength(value);
        else if (name == "flex-basis")      _basis = getPositiveLength(value);
        else if (name == "align-self")      _align_self = getAlign(value);
    }

    Flex::Direction Flex::getDirection(const std::string &value) {
        if (value == "row")             return ROW;
        if (value == "row-reverse")     return ROW_REVERSE;
        if (value == "column")          return COLUMN;
        if (value == "column-reverse")  return COLUMN_REVERSE;

        return ROW;
    }

    Flex::Justify Flex::getJustify(const std::string &value) {
        if (value == "flex-start")      return FLEX_START;
        if (value == "flex-end")        return FLEX_END;
        if (value == "center")          return CENTER;
        if (value == "space-between")   return SPACE_BETWEEN;
        if (value == "space-around")    return SPACE_AROUND;

        return FLEX_START;
    }

    Flex::Align Flex::getAlign(const std::string &value) {
        if (value == "auto")        return AUTO_A;
        if (value == "stretch")     return STRETCH;
        if (value == "center")      return CENTER_A;
        if (value == "flex-start")  return FLEX_START_A;
        if (value == "flex-end")    return FLEX_END_A;
        if (value == "baseline")    return BASELINE;

        return AUTO_A;
    }

    Flex::Wrap Flex::getWrap(const std::string &value) {
        if (value == "nowrap")          return NOWRAP;
        if (value == "wrap")            return WRAP;
        if (value == "wrap-reverse")    return WRAP_REVERSE;

        return NOWRAP;
    }


//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Flex::Flex(StyleContext *parentContext_) :
            Module(FLEX, parentContext_) {

        init(properties);
    }

}