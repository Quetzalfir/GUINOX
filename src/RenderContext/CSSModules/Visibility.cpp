#include "Visibility.hpp"





namespace CSSModule {


//----------------------------- STATICS -----------------------------//



//----------------------------- PRIVATE -----------------------------//

    void Visibility::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto display = dynamic_cast<Visibility *>(ref)) {
            if (name == "visibility")  _visibility = display->_visibility;
        }
    }

    void Visibility::setter(const std::string &name, const std::string &value) {
        if (name == "visibility")  setVisibility(value);
    }

    void Visibility::setVisibility(const std::string &value) {
        if      (value == "visible")    _visibility = VISIBLE;
        else if (value == "hidden")     _visibility = HIDDEN;
        else if (value == "collapse")   _visibility = COLLAPSE;
    }


//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Visibility::Visibility(StyleContext *parentContext_) :
            Module(VISIBILITY, parentContext_) {

        init(properties);
    }

}