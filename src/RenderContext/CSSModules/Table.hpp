#ifndef GUINOX_TABLE_HPP
#define GUINOX_TABLE_HPP

#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"

namespace CSSModule {

    class Table : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST parser;

    public:

        enum TableLayout{ AUTO, FIXED };

    private:

        TableLayout _tableLayout = AUTO;

        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;

        //void setTableLayout(const std::string & value);

    public:

        const TableLayout &tableLayout = _tableLayout;

        Table(StyleContext *parentContext);

    };

}//end namespace


#endif //GUINOX_TABLE_HPP
