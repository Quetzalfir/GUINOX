#include <Utils/Utils.hpp>
#include "Overflow.hpp"

namespace CSSModule {

    //----------------------------- STATICS -----------------------------//

   //----------------------------- PRIVATE -----------------------------//

    void Overflow::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto overflow = dynamic_cast<Overflow *>(ref)) {
            if      (name == "overflow-x")  _ox = overflow->_ox;
            else if (name == "overflow-y")  _oy = overflow->_oy;

        }
    }

    void Overflow::setter(const std::string &name, const std::string &value) {
        if      (name == "overflow-x") _ox = getOX(value);
        else if (name == "overflow-y") _oy = getOX(value);
    }

    Overflow::OX Overflow::getOX(const std::string &value) {
        if (value == "visible") return VISIBLE;
        if (value == "hidden")  return HIDDEN;
        if (value == "scroll")  return SCROLL;
        return AUTO;
    }


//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Overflow::Overflow(StyleContext *parentContext_) :
            Module(OVERFLOW_M, parentContext_) {

        init(properties);
    }


}//END NAMESPACE
