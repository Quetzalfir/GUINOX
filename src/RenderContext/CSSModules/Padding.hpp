#ifndef GUINOX_PADDING_HPP
#define GUINOX_PADDING_HPP


#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Padding : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST parser;


        static std::vector<std::pair<std::string, std::string>> mapPaddingShorthand(const std::string & value);


        Length _top;
        Length _bottom;
        Length _left;
        Length _right;


        void setProp(const std::string & name, Length & length);
        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;


    public:
        const Length & top = _top;
        const Length & bottom = _bottom;
        const Length & left = _left;
        const Length & right = _right;


        Padding(StyleContext *parentContext);

    };

}


#endif //GUINOX_PADDING_HPP
