#include "Font.hpp"
#include <Utils/Utils.hpp>


namespace CSSModule {

//----------------------------- STATICS -----------------------------//

    const PEGParserAST Font::parser(R"(
        FONT                <- FONT_COMB? FONT_SIZE ('/' LINE_HEIGHT)? FONT_FAMILY
        FONT_COMB           <- (FONT_STYLE   FONT_VARIANT FONT_WEIGTH)  /
                               (FONT_STYLE   FONT_WEIGTH  FONT_VARIANT) /
                               (FONT_VARIANT FONT_STYLE   FONT_WEIGTH)  /
                               (FONT_VARIANT FONT_WEIGTH  FONT_STYLE)   /
                               (FONT_WEIGTH  FONT_STYLE   FONT_VARIANT) /
                               (FONT_WEIGTH  FONT_VARIANT FONT_STYLE)   /
                               (FONT_STYLE   FONT_VARIANT) /
                               (FONT_STYLE   FONT_WEIGTH)  /
                               (FONT_VARIANT FONT_STYLE)   /
                               (FONT_VARIANT FONT_WEIGTH)  /
                               (FONT_WEIGTH  FONT_STYLE)   /
                               (FONT_WEIGTH  FONT_VARIANT) /
                               FONT_STYLE /  FONT_VARIANT  / FONT_WEIGTH


        LINE_HEIGHT         <- < "normal" / PERCENTAGE / LENGTH / NUMBER / GLOBAL_VALUE >

        FONT_SIZE           <- < FONT_SIZE_VALUES / LENGTH / PERCENTAGE / GLOBAL_VALUE >
        FONT_SIZE_VALUES    <- "xx-small" | "x-small" | "small" | "medium" | "large" | "x-large" | "xx-large" | "smaller" | "larger"

        FONT_WEIGTH         <- < FONT_WEIGTH_VALUES / FONT_WEIGTH_NUMBERS / GLOBAL_VALUE >
        FONT_WEIGTH_VALUES  <- "normal" | "bold" | "lighter" | "bolder"
        FONT_WEIGTH_NUMBERS <- "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900"

        FONT_VARIANT        <- < FONT_VARIANT_VALUES / GLOBAL_VALUE >
        FONT_VARIANT_VALUES <- "small-caps" | "normal"

        FONT_STYLE          <- < FONT_STYLE_VALUES / GLOBAL_VALUE >
        FONT_STYLE_VALUES   <- "oblique" ANGLE? / "italic" / "normal"

        FONT_FAMILY         <- < GLOBAL_VALUE / (GENERIC_FONT_FAMILY / ITEM) (',' (GENERIC_FONT_FAMILY / ITEM))* >
        GENERIC_FONT_FAMILY <- "serif" | "sans-serif" | "cursive" | "fantasy" | "monospace"

        GLOBAL_VALUE        <- INHERIT / INITIAL / UNSET

        INHERIT             <- "inherit"
        INITIAL             <- "initial"
        UNSET               <- "unset"

        LENGTH              <- '0' LENGTH_UNIT? / NUMBER LENGTH_UNIT
        LENGTH_UNIT         <- "em" | "ex" | "ch" | "vh" | "vw" | "vmin" | "vmax" | "px" | "mm" | "q" | "cm" | "in" | "pt" | "pc"

        ANGLE               <- NUMBER ANGLE_UNIT
        ANGLE_UNIT          <- "deg" | "grad" | "rad" | "turn"

        PERCENTAGE          <- <NUMBER> '%'
        NUMBER              <- <FLOAT / INTEGER>
        FLOAT               <- ('+'|'-')? [0-9]* '.' [0-9]+
        INTEGER             <- ('+'|'-')? [0-9]+

        ITEM                <- < WORD / PHRASE >
        WORD                <- [a-zA-Z]+
        PHRASE              <- $q<QUOTE> (!$q .)* $q
        QUOTE               <- "'" | '"'

        %whitespace         <-  [ \t\r\n]*
    )");


    std::vector<std::pair<std::string, std::string> > Font::mapFontShorthand(const std::string &value) {
        auto properties = parser.getPairs(value);

        for (auto & p : properties) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return properties;
    }

//----------------------------- PRIVATE -----------------------------//

    void Font::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto font = dynamic_cast<Font *>(ref)) {
            if      (name == "font-family")         _family = font->_family;
            else if (name == "font-size")           _size = font->_size;
            else if (name == "font-size-adjust")    _sizeAdjust = font->_sizeAdjust;
            else if (name == "font-stretch")        _stretch = font->_stretch;
            else if (name == "font-style")          _style = font->_style;
            else if (name == "font-variant")        _variant = font->_variant;
            else if (name == "font-variant-caps")   _variantCaps = font->_variantCaps;
            else if (name == "font-weight")         _weight = font->_weight;
        }
    }

    void Font::setter(const std::string &name, const std::string &value) {
        if      (name == "font-family")         setFamily(value);
        else if (name == "font-size")           setFontSize(value);
        else if (name == "font-size-adjust")    setSizeAdjust(value);
        else if (name == "font-stretch")        setStretch(value);
        else if (name == "font-style")          setFontStyle(value);
        else if (name == "font-variant")        setVariant(value);
        else if (name == "font-variant-caps")   setVariantCaps(value);
        else if (name == "font-weight")         setWeight(value);
    }

    void Font::setFamily(const std::string &value) {
        _family = Utils::split_string_by(value, ',');
    }

    void Font::setFontSize(const std::string &value) {
        if      (value == "xx-small")   _size = getLength("8px");
        else if (value == "x-small")    _size = getLength("10px");
        else if (value == "small")      _size = getLength("12px");
        else if (value == "medium")     _size = getLength("16px");
        else if (value == "large")      _size = getLength("18px");
        else if (value == "x-large")    _size = getLength("24px");
        else if (value == "xx-large")   _size = getLength("32px");
        else if (value == "smaller")    _size = getLength("13px");
        else if (value == "larger")     _size = getLength("19px");
        else                            _size = getLength(value);
    }

    void Font::setSizeAdjust(const std::string &value) {
        _sizeAdjust = getLength(value);
    }

    void Font::setStretch(const std::string &value) {
        if      (value == "ultra-condensed") _stretch = ULTRA_CONDENSED;
        else if (value == "extra-condensed") _stretch = EXTRA_CONDENSED;
        else if (value == "condensed")       _stretch = CONDENSED;
        else if (value == "semi-condensed")  _stretch = SEMI_CONDENSED;
        else if (value == "normal")          _stretch = NORMAL;
        else if (value == "semi-expanded")   _stretch = SEMI_EXPANDED;
        else if (value == "expanded")        _stretch = EXPANDED;
        else if (value == "extra-expanded")  _stretch = EXTRA_EXPANDED;
        else if (value == "ultra-expanded")  _stretch = ULTRA_EXPANDED;
    }

    void Font::setFontStyle(const std::string &value) {
        if      (value == "normal")  _style = NORMAL_STYLE;
        else if (value == "italic")  _style = ITALIC;
        else if (value == "oblique") _style = OBLIQUE;
    }

    void Font::setVariant(const std::string &value) {
        if      (value == "normal")     _variant = NORMAL_VARIANT;
        else if (value == "small-caps") _variant = SMALL_CAPS;
    }

    void Font::setVariantCaps(const std::string &value) {
        if      (value == "normal")          _variantCaps = NORMAL_VARIANT_CAPS;
        else if (value == "small-caps")      _variantCaps = SMALL_CAPS_CAPS;
        else if (value == "all-small-caps")  _variantCaps = ALL_SMALL_CAPS;
        else if (value == "petite-caps")     _variantCaps = PETITE_CAPS;
        else if (value == "all-petite-caps") _variantCaps = ALL_PETITE_CAPS;
        else if (value == "unicase")         _variantCaps = UNICASE;
        else if (value == "titling-caps")    _variantCaps = TITLING_CAPS;
    }

    void Font::setWeight(const std::string &value) {
        if      (value == "lighter") _weight = getLength("200");
        else if (value == "normal")  _weight = getLength("400");
        else if (value == "bold")    _weight = getLength("600");
        else if (value == "bolder")  _weight = getLength("800");
        else                         _weight = getLength(value);
    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Font::Font(StyleContext *parentContext_) :
            Module(FONT, parentContext_) {

        init(properties);
    }

}