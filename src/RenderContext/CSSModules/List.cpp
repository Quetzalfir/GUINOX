#include <Utils/Utils.hpp>
#include "List.hpp"

namespace CSSModule {

    //----------------------------- STATICS -----------------------------//

    const PEGParserAST List::parser(R"(
         LIST_STYLE              <- INHERIT /
                                ( LIST_STYLE_TYPE LIST_STYLE_POSITION LIST_STYLE_IMAGE /
                                  LIST_STYLE_TYPE LIST_STYLE_IMAGE LIST_STYLE_POSITION /
                                  LIST_STYLE_POSITION LIST_STYLE_TYPE LIST_STYLE_IMAGE /
                                  LIST_STYLE_POSITION LIST_STYLE_IMAGE LIST_STYLE_TYPE /
                                  LIST_STYLE_IMAGE LIST_STYLE_TYPE LIST_STYLE_POSITION /
                                  LIST_STYLE_IMAGE LIST_STYLE_POSITION LIST_STYLE_TYPE /
                                  LIST_STYLE_TYPE LIST_STYLE_POSITION /
                                  LIST_STYLE_TYPE LIST_STYLE_IMAGE /
                                  LIST_STYLE_POSITION LIST_STYLE_TYPE /
                                  LIST_STYLE_POSITION LIST_STYLE_IMAGE /
                                  LIST_STYLE_IMAGE LIST_STYLE_TYPE /
                                  LIST_STYLE_IMAGE LIST_STYLE_POSITION /
                                  LIST_STYLE_TYPE /
                                  LIST_STYLE_POSITION /
                                  LIST_STYLE_IMAGE )

        LIST_STYLE_TYPE      <- < LIST_STYLE_TYPE_NAME / NONE / INHERIT >

        LIST_STYLE_TYPE_NAME <- "disc" | "circle" | "square" | "decimal" | "decimal-leading-zero" |
                                "lower-roman" | "upper-roman" | "lower-greek" | "lower-latin" |
                                "upper-latin" | "armenian" | "georgian" | "lower-alpha" | "upper-alpha"

        LIST_STYLE_POSITION  <- "inside" | "outside" | "inherit"

        LIST_STYLE_IMAGE     <- < URL / NONE / INHERIT >

        INHERIT              <- "inherit"
        NONE 				 <- "none"

        URL 				 <- "url(" ITEM ')'

        ITEM                 <- < WORD / PHRASE >
        WORD    			 <- [a-zA-Z]+
        PHRASE    			 <- $q<QUOTE> (!$q .)* $q
        QUOTE     			 <- "'" | '"'

        %whitespace          <-  [ \t\r\n]*
    )");

    std::vector<std::pair<std::string, std::string> > List::mapListShorthand(const std::string &value) {
        auto properties = parser.getPairs(value);

        for (auto & p : properties) {
            p.first = PEGParserAST::changeToCSSStyle(p.first);
        }

        return properties;
    }

    //----------------------------- PRIVATE -----------------------------//

    void List::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto list = dynamic_cast<List *>(ref)) {
            if      (name == "list-style-position") _position = list->_position;
            //else if (name == "list-style-image")    _styleImage = list->_styleImage;
            else if (name == "list-style-type")     _type = list->_type;
        }
    }

    void List::setter(const std::string &name, const std::string &value) {
        if      (name == "list-style-position") setPosition(value);
        //else if (name == "list-style-image")    setStyleImage(value);
        else if (name == "list-style-type")     setType(value);
    }

    void List::setPosition(const std::string &value) {
        if      (value == "inside")  _position = INSIDE;
        else if (value == "outside") _position = OUTSIDE;
    }

    void List::setType(const std::string &value) {
        if      (value == "disc")            _type = DISC;
        else if (value == "armenian")        _type = ARMENIAN;
        else if (value == "circle")          _type = CIRCLE;
        else if (value == "cjk-ideographic") _type = CJK_IDEOGRAPHIC;
        else if (value == "decimal")         _type = DECIMAL;
        else if (value == "decimal-leading-zero")   _type = DECIMAL_LEADING_ZERO;
        else if (value == "georgian")        _type = GEORGIAN;
        else if (value == "hebrew")          _type = HEBREW;
        else if (value == "hiragana")        _type = HIRAGANA;
        else if (value == "hiragana-iroha")  _type = HIRAGANA_IROHA;
        else if (value == "katakana")        _type = KATAKANA;
        else if (value == "katakana-iroha")  _type = KATAKANA_IROHA;
        else if (value == "lower-alpha")     _type = LOWER_ALPHA;
        else if (value == "lower-greek")     _type = LOWER_GREEK;
        else if (value == "lower-latin")     _type = LOWER_LATIN;
        else if (value == "lower-roman")     _type = LOWER_ROMAN;
        else if (value == "none")            _type = NONE;
        else if (value == "square")          _type = SQUARE;
        else if (value == "upper-alpha")     _type = UPPER_ALPHA;
        else if (value == "upper-greek")     _type = UPPER_GREEK;
        else if (value == "upper-latin")     _type = UPPER_LATIN;
        else if (value == "upper-roman")     _type = UPPER_ROMAN;
    }

    //+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    List::List(StyleContext *parentContext_) :
            Module(LIST, parentContext_) {

        init(properties);
    }
}//END NAMESPACE
