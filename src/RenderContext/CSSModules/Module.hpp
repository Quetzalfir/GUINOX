#ifndef GUINOX_MODULE_HPP
#define GUINOX_MODULE_HPP


#include <string>
#include <map>
#include <memory>
#include <functional>
#include <vector>
#include <peglib.h>
#include "../../Utils/Color.hpp"


class StyleContext;
class CSSModuleRegistry;


namespace CSSModule {

//################################################################//
//########################### GLOBALS ###########################//

    // The types has to be pows of two in order do to fast checks
    //Margin, Padding, Background, Border, Flex, List Style, Font, Display , Box, Text, Grid, Positions, Overflow, Table, Float
    //potencias de 2, ejemplo el 16 es potencia de 2 y en HEX es 0x10
    enum Type : unsigned {
        UNKNOWN    = 0x00000000,
        MARGIN     = 0x00000001, //1
        PADDING    = 0x00000002, //2
        BACKGROUND = 0x00000004, //4
        BORDER     = 0x00000008, //8
        FLEX       = 0x00000010, //16
        LIST       = 0x00000020, //32
        FONT       = 0x00000040, //64
        DISPLAY    = 0x00000080, //128
        BOX        = 0x00000100, //256
        TEXT       = 0x00000200, //512
        GRID       = 0x00000400, //1024
        POSITION   = 0x00000800, //2048
        OVERFLOW_M = 0x00001000, //4096
        TABLE      = 0x00002000, //8192
        FLOAT      = 0x00004000, //16384
        VISIBILITY = 0x00008000
    };

    enum Unit {
        PERCENTAGE, PX, MM, EM, EX, CH, REM,
        VW, VH, VMIN, VMAX
    };

    enum Positions { TOP, LEFT, RIGHT, BOTTOM };

    struct Length {
        std::string text;
        double num = 0;
        Unit unit = PX;

        Length() {}
        Length(const std::string & text_, double num_, Unit unit_) : text(text_), num(num_), unit(unit_) {}
    };

    struct Property {
        Type module;
        std::string initial;
        bool inherit;
    };

    struct ShorthandProperty {
        std::function<std::vector<std::pair<std::string, std::string>>(const std::string &)> getter;
        std::vector<std::string> longHands;
    };

    struct Coordinate {
        std::pair<Positions, Length> x, y;
    };

    class PEGParserAST {

        static std::vector<std::pair<std::string, std::string>> getTokens(std::shared_ptr<peg::Ast> &ast);


        peg::parser parser;


    public:

        static std::string changeToCSSStyle(const std::string & tag);


        PEGParserAST(const char * syntax);

        std::vector<std::pair<std::string, std::string>> getPairs(const std::string & value) const ;

    };

    typedef std::map<std::string, Property> PropertyMap;
    typedef std::map<std::string, ShorthandProperty> ShorthandMap;


    Length getLength(const std::string & value);
    Length getPositiveLength(const std::string & value);
    Positions getPosition(const std::string & value);
    Color * getColor(const std::string & value);


//########################### END GLOBALS ###########################//
//##################################################################//



    class Module {

        Type _type;
        StyleContext * _parentContext = nullptr;


        void inherit(const std::string & name);
        void resetProperty(const std::string & name);
        bool checkII(const std::string & name, const std::string & value);


    protected:
        Module(Type type, StyleContext * parentContext);

        void init(const PropertyMap & properties);

        virtual void clonePropertyFrom(const std::string & name, Module * ref) = 0;
        virtual void setter(const std::string & name, const std::string & value) = 0;


    public:

        bool inLine = false;
        const Type &type = _type;


        virtual ~Module() = default;

        void setProperty(const std::string & name, const std::string & value);

    };


    typedef std::shared_ptr<Module> sp_Module;
    typedef std::weak_ptr<Module> wp_Module;

}


#endif //GUINOX_MODULE_HPP
