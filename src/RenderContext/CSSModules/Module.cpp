#include <cmath>
#include "Module.hpp"
#include "../StyleContext.hpp"
#include "../../Utils/Utils.hpp"
#include "../CSSModuleRegistry.hpp"
#include <array>

#ifndef M_PI
# define M_PI           3.14159265358979323846  /* pi */
#endif


namespace CSSModule {


    static std::array<double, 3> hslToRgb(double h, double s, double l) {
        double C = (1 - std::abs((2*l)-1)) * s;
        double X = C * (1 - std::abs(((int)(h/60) % 2) - 1));
        double m = l - (C/2);

        std::array<double, 3> aux{};

        if      (0   <= h && h <  60) aux = {C,X,0};
        else if (60  <= h && h < 120) aux = {X,C,0};
        else if (120 <= h && h < 180) aux = {0,C,X};
        else if (180 <= h && h < 240) aux = {0,X,C};
        else if (240 <= h && h < 300) aux = {X,0,C};
        else if (300 <= h && h < 360) aux = {C,0,X};

        return {(aux[0]+m)*255, (aux[1]+m)*255, (aux[2]+m)*255};
    }


    static peg::parser createColorParser() {
        peg::parser parser(R"(
         COLOR				  <- RGB / RGBA / HSL / HSLA / HEX_COLOR / NAMED_COLOR

         NAMED_COLOR		  <- "aliceblue" | "antiquewhite" | "aqua" | "aquamarine" | "azure" | "beige" |
                                 "bisque" | "black" | "blanchedalmond" | "blue" | "blueviolet" | "brown" | "burlywood" |
                                 "cadetblue" | "chartreuse" | "chocolate" | "coral" | "cornflowerblue" | "cornsilk" |
                                 "crimson" | "cyan" | "darkblue" | "darkcyan" | "darkgoldenrod" | "darkgray" | "darkgrey" |
                                 "darkgreen" | "darkkhaki" | "darkmagenta" | "darkolivegreen" | "darkorange" | "darkorchid" |
                                 "darkred" | "darksalmon" | "darkseagreen" | "darkslateblue" | "darkslategray" | "darkslategrey" |
                                 "darkturquoise" | "darkviolet" | "deeppink" | "deepskyblue" | "dimgray" | "dimgrey" | "dodgerblue" |
                                 "firebrick" | "floralwhite" | "forestgreen" | "fuchsia" | "gainsboro" | "ghostwhite" | "gold" |
                                 "goldenrod" | "gray" | "grey" | "green" | "greenyellow" | "honeydew" | "hotpink" | "indianred " |
                                 "indigo " | "ivory" | "khaki" | "lavender" | "lavenderblush" | "lawngreen" | "lemonchiffon" |
                                 "lightblue" | "lightcoral" | "lightcyan" | "lightgoldenrodyellow" | "lightgray" | "lightgrey" |
                                 "lightgreen" | "lightpink" | "lightsalmon" | "lightseagreen" | "lightskyblue" | "lightslategray" |
                                 "lightslategrey" | "lightsteelblue" | "lightyellow" | "lime" | "limegreen" | "linen" |
                                 "magenta" | "maroon" | "mediumaquamarine" | "mediumblue" | "mediumorchid" | "mediumpurple" |
                                 "mediumseagreen" | "mediumslateblue" | "mediumspringgreen" | "mediumturquoise" | "mediumvioletred" |
                                 "midnightblue" | "mintcream" | "mistyrose" | "moccasin" | "navajowhite" | "navy" | "oldlace" |
                                 "olive" | "olivedrab" | "orange" | "orangered" | "orchid" | "palegoldenrod" | "palegreen" |
                                 "paleturquoise" | "palevioletred" | "papayawhip" | "peachpuff" | "peru" | "pink" | "plum" |
                                 "powderblue" | "purple" | "rebeccapurple" | "red" | "rosybrown" | "royalblue" | "saddlebrown" |
                                 "salmon" | "sandybrown" | "seagreen" | "seashell" | "sienna" | "silver" | "skyblue" | "slateblue" |
                                 "slategray" | "slategrey" | "snow" | "springgreen" | "steelblue" | "tan" | "teal" | "thistle" |
                                 "tomato" | "turquoise" | "transparent" | "violet" | "wheat" | "white" | "whitesmoke" |
                                 "yellow" | "yellowgreen"

         HEX_COLOR            <- < '#' HEX_DIGIT{8} ' '? > /
                                 < '#' HEX_DIGIT{6} ' '? > /
                                 < '#' HEX_DIGIT{4} ' '? > /
                                 < '#' HEX_DIGIT{3} ' '? >

         RGB                  <- "rgb"  "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgb"  "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgb"  "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                 "rgb"  "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         RGBA                 <- "rgba" "(" PERCENTAGE{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgba" "(" NUMBER{3} ('/' ALPHA_VALUE)? ')' /
                                 "rgba" "(" PERCENTAGE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')' /
                                 "rgba" "(" NUMBER (',' NUMBER){2} (',' ALPHA_VALUE)? ')'
         HSL                  <- "hsl"  "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                 "hsl"  "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'
         HSLA                 <- "hsla" "(" HUE PERCENTAGE{2} ('/' ALPHA_VALUE)? ')' /
                                 "hsla" "(" HUE (',' PERCENTAGE){2} (',' ALPHA_VALUE)? ')'

         ALPHA_VALUE          <- < PERCENTAGE / NUMBER >
         HUE                  <- ANGLE / NUMBER

         ANGLE                <- < NUMBER ANGLE_UNIT >
         ANGLE_UNIT           <- "deg" | "grad" | "rad" | "turn"

         PERCENTAGE           <- <NUMBER> '%'
         NUMBER               <- <FLOAT / INTEGER>
         FLOAT                <- ('+'|'-')? [0-9]* '.' [0-9]+
         INTEGER              <- ('+'|'-')? [0-9]+
         HEX_DIGIT			  <- [a-fA-F0-9]
         DIGIT				  <- [0-9]

         %whitespace          <-  [ \t\r\n]*)");

        struct Alpha {
            bool percentage = false;
            double val = 0;
        };

        parser.enable_packrat_parsing();

        parser["COLOR"] = [] (const peg::SemanticValues& sv) -> Color * {
            return peg::any_cast<Color *>(sv[0]);
        };

        parser["NAMED_COLOR"] = [] (const peg::SemanticValues& sv) -> Color * {
            return new Color(sv.token(0));
        };

        parser["HEX_COLOR"] = [] (const peg::SemanticValues& sv) -> Color * {
            std::string aux = sv.token(0).substr(1);
            unsigned int hex = (unsigned int)std::stoul(aux, nullptr,16);

            switch (sv.choice()) {
                case 1:
                    hex = (hex << 8) | 0xFF;
                    break;

                case 2: {
                    hex = 0;
                    for (int i = 0; i < 4; i++) {
                        std::string c = aux.substr(i,1);
                        unsigned int num = (unsigned int)std::stoul(c, nullptr,16);

                        num *= ((double)0xFF) / 0xF;

                        hex = (hex << 8) | num;
                    }

                    break;
                }

                case 3: {
                    hex = 0;
                    for (int i = 0; i < 3; i++) {
                        std::string c = aux.substr(i,1);
                        unsigned int num = (unsigned int)std::stoul(c, nullptr,16);

                        num *= ((double)0xFF) / 0xF;

                        hex = (hex << 8) | num;
                    }

                    hex = (hex << 8) | 0xFF;

                    break;
                }
            }

            return new Color(hex);
        };

        parser["RGB"] = [] (const peg::SemanticValues& sv) -> Color * {
            size_t  size = sv.size();
            unsigned int hex = 0;

            switch (sv.choice()) {
                case 0:
                case 2: {
                    for (int i = 0; i < 3; i++) {
                        double num = peg::any_cast<double>(sv[i]);
                        if (num > 1) num = 1;
                        if (num < 0) num = 0;

                        hex = (hex << 8) | ((int)num * 255);
                    }

                    break;
                }

                case 3:
                case 1: {
                    for (int i = 0; i < 3; i++) {
                        hex = (hex << 8) | ((int)peg::any_cast<double>(sv[i]) % 256);
                    }

                    break;
                }
            }

            if (size == 4) {
                Alpha alpha = peg::any_cast<Alpha>(sv[3]);

                if (alpha.percentage) {
                    if (alpha.val > 1) alpha.val = 1;
                    if (alpha.val < 0) alpha.val = 0;
                    hex = (hex << 8) | (unsigned int)(alpha.val * 255);

                } else {
                    hex = (hex << 8) | ((unsigned int)alpha.val % 256);

                }

            } else {
                hex = (hex << 8) | 0xFF;
            }

            return new Color(hex);
        };

        parser["RGBA"] = [] (const peg::SemanticValues& sv) -> Color * {
            size_t  size = sv.size();
            unsigned int hex = 0;

            switch (sv.choice()) {
                case 0:
                case 2: {
                    for (int i = 0; i < 3; i++) {
                        double num = peg::any_cast<double>(sv[i]);
                        if (num > 1) num = 1;
                        if (num < 0) num = 0;

                        hex = (hex << 8) | ((int)num * 255);
                    }

                    break;
                }

                case 3:
                case 1: {
                    for (int i = 0; i < 3; i++) {
                        hex = (hex << 8) | ((int)peg::any_cast<double>(sv[i]) % 256);
                    }

                    break;
                }
            }

            if (size == 4) {
                Alpha alpha = peg::any_cast<Alpha>(sv[3]);

                if (alpha.percentage) {
                    if (alpha.val > 1) alpha.val = 1;
                    if (alpha.val < 0) alpha.val = 0;
                    hex = (hex << 8) | (unsigned int)(alpha.val * 255);

                } else {
                    hex = (hex << 8) | ((unsigned int)alpha.val % 256);

                }

            } else {
                hex = (hex << 8) | 0xFF;
            }

            return new Color(hex);
        };

        parser["HSL"] = [] (const peg::SemanticValues& sv) -> Color * {
            size_t  size = sv.size();
            unsigned int hex = 0;

            double h = peg::any_cast<double>(sv[0]);
            double s = peg::any_cast<double>(sv[1]);
            double l = peg::any_cast<double>(sv[2]);

            if (s > 1) s = 1;
            if (s < 0) s = 0;

            if (l > 1) l = 1;
            if (l < 0) l = 0;

            auto rgb = hslToRgb(h,s,l);

            for (int i = 0; i < 3; i++) {
                hex = (hex << 8) | ((int)(rgb[i]) % 256);
            }

            if (size == 4) {
                Alpha alpha = peg::any_cast<Alpha>(sv[3]);

                if (alpha.percentage) {
                    if (alpha.val > 1) alpha.val = 1;
                    hex = (hex << 8) | (unsigned int)(alpha.val * 255);

                } else {
                    hex = (hex << 8) | ((unsigned int)alpha.val % 256);

                }

            } else {
                hex = (hex << 8) | 0xFF;
            }

            return new Color(hex);
        };

        parser["HSLA"] = [] (const peg::SemanticValues& sv) -> Color * {
            size_t  size = sv.size();
            unsigned int hex = 0;

            double h = peg::any_cast<double>(sv[0]);
            double s = peg::any_cast<double>(sv[1]);
            double l = peg::any_cast<double>(sv[2]);

            if (s > 1) s = 1;
            if (s < 0) s = 0;

            if (l > 1) l = 1;
            if (l < 0) l = 0;

            auto rgb = hslToRgb(h,s,l);

            for (int i = 0; i < 3; i++) {
                hex = (hex << 8) | ((int)(rgb[i]) % 256);
            }

            if (size == 4) {
                Alpha alpha = peg::any_cast<Alpha>(sv[3]);

                if (alpha.percentage) {
                    if (alpha.val > 1) alpha.val = 1;
                    hex = (hex << 8) | (unsigned int)(alpha.val * 255);

                } else {
                    hex = (hex << 8) | ((unsigned int)alpha.val % 256);

                }

            } else {
                hex = (hex << 8) | 0xFF;
            }

            return new Color(hex);
        };

        parser["ALPHA_VALUE"] = [] (const peg::SemanticValues& sv) -> Alpha {
            Alpha alpha;
            switch (sv.choice()) {
                case 0: alpha.percentage = true;  break;
                case 1: alpha.percentage = false; break;
            }

            alpha.val = peg::any_cast<double>(sv[0]);

            return alpha;
        };

        parser["HUE"] = [] (const peg::SemanticValues& sv) -> double {
            return peg::any_cast<double>(sv[0]);
        };

        parser["ANGLE"] = [] (const peg::SemanticValues& sv) -> double {
            auto unit = peg::any_cast<std::string>(sv[1]);
            double num = peg::any_cast<double>(sv[0]);

            if (unit == "rad") return num * (180.0/ M_PI);
            if (unit == "grad") return num * 0.9;
            if (unit == "turn") return num * 360;

            while (num < 0) num += 360;

            return (int)num % 360;
        };

        parser["ANGLE_UNIT"] = [] (const peg::SemanticValues& sv) -> std::string {
            return sv.token(0);
        };

        parser["NUMBER"] = [] (const peg::SemanticValues& sv) -> double {
            return std::stod(sv.token(0));
        };

        parser["PERCENTAGE"] = [] (const peg::SemanticValues& sv) -> double {
            return std::stod(sv.token(0)) / 100.0;
        };

        return parser;
    }


    const static peg::parser colorParser = createColorParser();



    std::vector<std::pair<std::string, std::string>> PEGParserAST::getTokens(std::shared_ptr<peg::Ast> &ast) {
        std::vector<std::pair<std::string, std::string>> properties;

        for (auto & node : ast->nodes) {
            if (node->is_token) {
                properties.emplace_back(node->name, Utils::trim( node->token ));

            } else {
                auto aux = getTokens(node);
                properties.insert(properties.end(), aux.begin(), aux.end());
            }
        }

        return  properties;
    }

    std::string PEGParserAST::changeToCSSStyle(const std::string & tag) {
        std::string aux = tag;
        std::transform(aux.begin(), aux.end(), aux.begin(),
                       [](unsigned char c) -> unsigned char {
                           if (c == '_') return '-';
                           return std::tolower(c);
                       });
        return aux;
    }


    PEGParserAST::PEGParserAST(const char *syntax) : parser(syntax) {
        parser.enable_ast();
    }

    std::vector<std::pair<std::string, std::string> > PEGParserAST::getPairs(const std::string & value) const {
        std::vector<std::pair<std::string, std::string> > properties;
        std::shared_ptr<peg::Ast> ast;

        if (parser.parse(value.data(), ast)) {
            properties = getTokens(ast);
        }

        return properties;
    }




    Length getLength(const std::string & value) {
        Length length;

        if (value.empty()) return length;

        if (!std::isdigit(value[0])) {
            return {value, 0, PX};
        }

        size_t sz;
        length.num = std::stod(value, &sz);
        std::string unit = value.substr(sz);

        if      (unit == "mm") length.unit = MM;
        else if (unit == "cm") { length.num *= 10;  length.unit = MM; }
        else if (unit == "in") { length.num *= 254; length.unit = MM; }
        else if (unit == "pt") { length.num *= (254.0/72); length.unit = MM; }
        else if (unit == "pc") { length.num *= (12*(254.0/72)); length.unit = MM; }
        else if (unit == "em") length.unit = EM;
        else if (unit == "ex") length.unit = EX;
        else if (unit == "ch") length.unit = CH;
        else if (unit == "rem") length.unit = REM;
        else if (unit == "vw") length.unit = VW;
        else if (unit == "vh") length.unit = VH;
        else if (unit == "vmin") length.unit = VMIN;
        else if (unit == "vmax") length.unit = VMAX;
        else if (unit == "%") length.unit = PERCENTAGE;


        return length;
    }

    Length getPositiveLength(const std::string &value) {
        auto aux = getLength(value);

        if (aux.num < 0) aux.num = 0;

        return aux;
    }

    Positions getPosition(const std::string & value) {
        if      (value == "top")    return TOP;
        else if (value == "left")   return LEFT;
        else if (value == "right")  return RIGHT;
        else if (value == "bottom") return BOTTOM;

        return TOP;
    }

    Color * getColor(const std::string & value) {
        if (value == "currentcolor" || value == "actual") return nullptr;

        Color *color = nullptr;
        colorParser.parse(value.data(), color);

        return color;
    }






//----------------------------- PRIVATE -----------------------------//

    void Module::inherit(const std::string &name) {
        if (_parentContext) {
            auto module = _parentContext->getModule(_type);

            if (!module) return;

            clonePropertyFrom(name, module.get());

        } else {
            const Property &prop = CSSModuleRegistry::_propertiesMap.at(name);
            setter(name, prop.initial);
        }
    }

    void Module::resetProperty(const std::string &name) {
        const Property &prop = CSSModuleRegistry::_propertiesMap.at(name);

        if (prop.inherit) {
            inherit(name);

        } else {
            setter(name, prop.initial);
        }
    }

    bool Module::checkII(const std::string &name, const std::string &value) {
        if (value == "inherit") {
            inherit(name);
            return true;

        } else if (value == "initial" || value == "unset") {
            resetProperty(name);
            return true;
        }

        return false;
    }

    Module::Module(Type type, StyleContext *parentContext) :
            _type(type), _parentContext(parentContext) {

    }

    void Module::init(const PropertyMap &properties) {
        for (const auto & p : properties) {
            resetProperty(p.first);
        }
    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    void Module::setProperty(const std::string &name, const std::string &value) {
        if (checkII(name, value)) return;

        setter(name, value);
    }


}