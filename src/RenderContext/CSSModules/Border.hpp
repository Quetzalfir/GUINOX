#ifndef GUINOX_BORDER_HPP
#define GUINOX_BORDER_HPP

#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Border : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;

        const static PEGParserAST borderParser;
        const static PEGParserAST borderCoordParser;
        const static PEGParserAST borderWidthParser;
        const static PEGParserAST borderStyleParser;
        const static PEGParserAST borderColorParser;


        static std::vector<std::pair<std::string, std::string>> mapBorderShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapBorderTopShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapBorderLeftShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapBorderBottomShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapBorderRightShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapBorderWidthShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapBorderColorShorthand(const std::string & value);
        static std::vector<std::pair<std::string, std::string>> mapBorderStyleShorthand(const std::string & value);


    public:

        enum STYLE { NONE, HIDDEN, DOTTED, DASHED, SOLID, DOUBLE, GROOVE, RIDGE, INSET, OUTSET };


        struct BorderProperties{
            Length width;
            STYLE style;
            Color *color = nullptr;

            ~BorderProperties() { delete color; color = nullptr; }
        };

    private:

        BorderProperties _top, _left, _right, _bottom;

        void clonePropertyFrom(const std::string &name, Module *ref) override;
        void setter(const std::string &name, const std::string &value) override;


        void setStyle(const std::string & value, const std::string & coord);
        void setColor(const std::string & value, const std::string & coord);
        void setWidth(const std::string & value, const std::string & coord);


        static STYLE getStyle(const std::string & value);
        static Length getWidth(const std::string & value);


    public:

        const BorderProperties & top = _top;
        const BorderProperties & left = _left;
        const BorderProperties & bottom = _bottom;
        const BorderProperties & right = _right;

        Border(StyleContext *parentContext);

    };

}


#endif //GUINOX_BORDER_HPP
