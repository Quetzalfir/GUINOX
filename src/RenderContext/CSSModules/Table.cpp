#include <Utils/Utils.hpp>
#include "Table.hpp"

namespace CSSModule {

    //----------------------------- STATICS -----------------------------//

    const PropertyMap Table::properties = {
            {"table-layout",     {TABLE, "auto", false} }

    };

    const CSSModuleRegistry Table::regist(TABLE, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Table>(parentContext);
    },properties);

//----------------------------- PRIVATE -----------------------------//

    void Table::clonePropertyFrom(const std::string &name, Module *ref) {
        if (auto table = dynamic_cast<Table *>(ref)) {
            if      (name == "table-layout")  _tableLayout = table->_tableLayout;

        }
    }

    void Table::setter(const std::string &name, const std::string &value) {
        //if      (name == "table-layout")   setTableLayout(value);
    }

//+++++++++++++++++++++++++++++ PUBLIC +++++++++++++++++++++++++++++//

    Table::Table(StyleContext *parentContext_) :
            Module(TABLE, parentContext_) {

    }

}//END NAMESPACE
