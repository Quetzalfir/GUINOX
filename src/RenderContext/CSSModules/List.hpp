#ifndef GUINOX_LIST_HPP
#define GUINOX_LIST_HPP


#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"

namespace CSSModule{

    class List : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST parser;

        static std::vector<std::pair<std::string, std::string>> mapListShorthand(const std::string & value);


    public:

        enum StylePosition { INSIDE, OUTSIDE };
        enum StyleType { DISC, ARMENIAN, CIRCLE, CJK_IDEOGRAPHIC, DECIMAL, DECIMAL_LEADING_ZERO, GEORGIAN, HEBREW, HIRAGANA, HIRAGANA_IROHA, KATAKANA, KATAKANA_IROHA, LOWER_ALPHA, LOWER_GREEK, LOWER_LATIN, LOWER_ROMAN, NONE, SQUARE, UPPER_ALPHA, UPPER_GREEK, UPPER_LATIN, UPPER_ROMAN };


    private:

        //Image _image;
        StylePosition _position = OUTSIDE ;
        StyleType _type = DISC ;

        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;

        //void setStyleImage(const std::string & value);
        void setPosition(const std::string & value);
        void setType(const std::string & value);

    public:

        //const StyleImage & styleImage = _styleImage;
        const StylePosition & position = _position;
        const StyleType & type = _type;

        List(StyleContext *parentContext);



    };


}  //END NAMESPACE

#endif