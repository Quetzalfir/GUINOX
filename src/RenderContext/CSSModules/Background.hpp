#ifndef GUINOX_BACKGROUND_HPP
#define GUINOX_BACKGROUND_HPP


#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {


    class Background : public Module {
        const static PropertyMap properties;
        const static ShorthandMap shorthands;
        const static CSSModuleRegistry regist;
        const static PEGParserAST parser;


        static std::vector<std::pair<std::string, std::string>> mapBackgroundShorthand(const std::string & value);

    public:
        enum Attachment { SCROLL, FIXED, LOCAL};
        enum BlendMode { NORMAL, MULTIPLY, SCREEN, OVERLAY, DARKEN, LIGHTEN, COLOR_DODGE, SATURATION, COLOR, LUMINOSITY };
        enum Clip { BORDER_BOX, PADDING_BOX, CONTENT_BOX };
        enum Origin { PADDING, BORDER, CONTENT };
        enum Repeat { REPEAT, REPEAT_X, REPEAT_Y, NO_REPEAT, SPACE, ROUND };

        struct Size { Length width, height; };



    private:
        Attachment _attachment = SCROLL;
        BlendMode _blend_mode = NORMAL;
        Clip _clip = BORDER_BOX;
        Color * _color = nullptr;
        //Image
        Origin _origin = Origin::PADDING;
        Coordinate _position;
        Repeat _repeat = REPEAT;
        Size _size;


        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;

        void setAttachment(const std::string & value);
        void setBlendMode(const std::string & value);
        void setClip(const std::string & value);
        void setOrigin(const std::string & value);
        void setPosition(const std::string & value);
        void setRepeat(const std::string & value);
        void setSize(const std::string & value);
        void setColor(const std::string & value);


        static bool transform2PositionPercentage(std::pair<std::string, std::string> &pair);
        static void getPercentageValue(std::string &str);
        static bool isPercentageWord(const std::string & str);


    public:
        const Attachment & attachment = _attachment;
        const BlendMode & blend_mode = _blend_mode;
        const Clip & clip = _clip;
        Color * const & color = _color;
        const Origin & origin = _origin;
        const Coordinate & position = _position;
        const Repeat & repeat = _repeat;
        const Size & size = _size;



        Background(StyleContext *parentContext);
        ~Background() noexcept override ;
    };

}


#endif //GUINOX_BACKGROUND_HPP
