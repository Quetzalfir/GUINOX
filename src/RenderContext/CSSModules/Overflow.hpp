#ifndef GUINOX_OVERFLOW_H
#define GUINOX_OVERFLOW_H

#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"

namespace CSSModule {

    class Overflow : public Module {
        const static PropertyMap properties;
        const static CSSModuleRegistry regist;


    public:

        enum OX { VISIBLE, HIDDEN, SCROLL, AUTO };

    private:

        OX _ox = VISIBLE;
        OX _oy = VISIBLE;

        void clonePropertyFrom(const std::string & name, Module * ref) override ;
        void setter(const std::string & name, const std::string & value) override ;


        OX getOX(const std::string & value);

    public:

        const OX &ox = _ox;
        const OX &oy = _oy;

        Overflow(StyleContext *parentContext);

};

}//END NAMESPACE

#endif //GUINOX_OVERFLOW_H
