#ifndef GUINOX_DISPLAY_HPP
#define GUINOX_DISPLAY_HPP


#include "Module.hpp"
#include "../CSSModuleRegistry.hpp"


namespace CSSModule {

    class Display : public Module {
        const static PropertyMap properties;
        const static CSSModuleRegistry regist;


    public:
        enum DisplayType {
            INLINE, BLOCK, CONTENTS, FLEX_D, GRID_D,
            INLINE_BLOCK, INLINE_FLEX, INLINE_GRID,
            INLINE_TABLE, LIST_ITEM, RUN_IN, TABLE_D,
            TABLE_CAPTION, TABLE_COLUMN_GROUP,
            TABLE_HEADER_GROUP, TABLE_FOOTER_GROUP,
            TABLE_ROW_GROUP, TABLE_CELL, TABLE_COLUMN,
            TABLE_ROW, TEXT, NONE
        };


    private:

        DisplayType _display;


        void clonePropertyFrom(const std::string &name, Module *ref) override;
        void setter(const std::string &name, const std::string &value) override;
        void setDisplay(const std::string &value);



    public:

        const DisplayType & display = _display;



        Display(StyleContext *parentContext);

    };

}


#endif //GUINOX_DISPLAY_HPP
