#include "Presentation.hpp"

#include <Utils/Utils.hpp>
#include "../DOM/Document.hpp"
#include "../DOM/Text.hpp"
#include "../CSSOM/CSSOMTravel.hpp"
#include "../DOM/NodeTravel.hpp"
#include "FrameRegistry.hpp"


static const int NUM_OF_NODES_TO_ANALYZE = 100;
static const int REFRESH_RATE = 200; //200ms



static
std::map<sp_HTMLElement, std::vector<std::pair<sp_CSSStyleRule, std::string>>>
getCSSRules(
        const std::vector<DOM_Node::sp_Node> &nodes,
        const CSSStyleSheetList &styleSheets,
        const Presentation * presentation
) {


    std::map<std::shared_ptr<HTMLElement>, std::vector<std::pair<sp_CSSStyleRule, std::string>>> map;

    if (nodes.empty()) return map;

    for (const auto & sheet : styleSheets) {
        CSSOMTravel(sheet, presentation).forEachCSSStyleRule([&](const sp_CSSStyleRule& styleRule) {
            auto selectors = Utils::split_string_by(styleRule->selectorText, ',');

            for (const auto & node : nodes) {
                if (auto element = std::dynamic_pointer_cast<HTMLElement>(node)) {
                    for (const auto &selector : selectors) {
                        if (element->matches(selector)) {
                            map[element].emplace_back(styleRule, selector);
                            break;
                        }
                    }
                }
            }
        });
    }

    return map;
}







RenderFrame::sp_Frame Presentation::getFrameWidthRoot(const sp_HTMLElement &element, const RenderFrame::sp_Frame &root) {
    for (const auto & frame : element->frames) {
        if (frame->root() == root)
            return frame->shared_from_this();
    }

    return nullptr;
}






void Presentation::updateTree(RenderFrame::sp_Frame &root, const std::vector<DOM_Node::sp_Node> & nodes) {

    auto map = getCSSRules(nodes, _ownerDocument->styleSheets, this);

    for (auto & node : nodes) {
        sp_RuleTree rule = nullptr;

        if (auto element = std::dynamic_pointer_cast<HTMLElement>(node)) {
            auto it = map.find(element);
            rule = it != map.end() ? ruleTree->appendPath((*it).second) : ruleTree;
        }

        if (auto parent = node->parentElement()) {
            if (auto htmlParent = std::dynamic_pointer_cast<HTMLElement>(parent)) {
                if (auto parentFrame = getFrameWidthRoot(htmlParent, root)) {
                    parentFrame->emplace_back(node, rule, this);
                }
            }
        }
    }
}

RenderFrame::sp_Frame Presentation::getFrameFor(const sp_HTMLElement& element) const {
    for (const auto & frame : element->frames) {
        if (frame->_ownerPresentation == this)
            return frame->shared_from_this();
    }

    return nullptr;
}

RenderFrame::sp_Frame Presentation::createFrameRoot(sp_HTMLElement &element) {
    auto map = getCSSRules({element}, _ownerDocument->styleSheets, this);
    auto it = map.find(element);

    sp_RuleTree rule = it != map.end() ? ruleTree->appendPath((*it).second) : ruleTree ;

    auto style = std::make_shared<StyleContext>(rule, &(element->style));

    if (auto parent = element->parentElement()) {
        if (auto htmlParent = std::dynamic_pointer_cast<HTMLElement>(parent)) {
            if (auto parentFrame = getFrameWidthRoot(htmlParent, frameTree)) {
                style = parentFrame->style->append(style);
            }
        }

    } else {
        style->compute();
    }


    auto frame = FrameRegistry::get(element, style, this);

    return frame;
}

RenderFrame::sp_Frame Presentation::buildFrameTreeFor(sp_HTMLElement &root) {
    std::cout << "Presentation buildFrameTreeFor 1: " << std::endl;

    auto frameRoot = createFrameRoot(root);

    std::cout << "Presentation buildFrameTreeFor 2: " << std::endl;


    auto treeWalker = DOM_Node::Document::createTreeWalker(root, NodeFilter::SHOW_ELEMENT | NodeFilter::SHOW_TEXT);
    std::vector<DOM_Node::sp_Node> nodes;
    nodes.reserve(NUM_OF_NODES_TO_ANALYZE);

    auto currentNode = treeWalker.nextNode();
    while (currentNode) {
        nodes.push_back(currentNode);

        if (nodes.size() == NUM_OF_NODES_TO_ANALYZE) {
            updateTree(frameRoot, nodes);
            nodes.clear();
        }

        currentNode = treeWalker.nextNode();
    }
    std::cout << "Presentation buildFrameTreeFor 3: " << std::endl;

    if (!nodes.empty()) {
        updateTree(frameRoot, nodes);
    }
    std::cout << "Presentation buildFrameTreeFor 4: " << std::endl;


    //ruleTree->print();
    //frameRoot->style->print();
    //frameRoot->print();

    return frameRoot;
}

RenderFrame::sp_Frame Presentation::getNextFrameSibling(const sp_HTMLElement &element) const {
    auto aux = element->nextElementSibling();

    while (aux) {
        auto htmlElement = std::dynamic_pointer_cast<HTMLElement>(aux);
        auto frame = getFrameFor(htmlElement);
        if (frame) return frame;
        aux = aux->nextElementSibling();
    }

    return nullptr;
}

[[noreturn]] void Presentation::buildTreeFunc() {
    while (true) {
        while (dirtyElements.begin() != dirtyElements.end()) {
            auto element = dirtyElements.front();
            std::cout << "Presentation buildTreeFunc 1: " << std::endl;
            auto newFrameTree = buildFrameTreeFor(element);
            std::cout << "Presentation buildTreeFunc 2: " << std::endl;


            if (auto frame = getFrameFor(element)) {
                std::cout << "Presentation buildTreeFunc 3: " << std::endl;

                if (frame->parent()) {
                    std::cout << "Presentation buildTreeFunc 4: " << std::endl;

                    reflow.replace(newFrameTree, frame);

                } else {
                    std::cout << "Presentation buildTreeFunc 5: " << std::endl;

                    reflow.substituteRoot(newFrameTree);
                    std::cout << "Presentation buildTreeFunc 6: " << std::endl;

                }

            } else {
                if (auto parent = element->parentElement()) {
                    if (auto parentHTML = std::dynamic_pointer_cast<HTMLElement>(parent)) {
                        if (auto parentFrame = getFrameFor(parentHTML)) {
                            auto refFrame = getNextFrameSibling(element);

                            if (refFrame) {
                                reflow.insertBefore(newFrameTree, refFrame);
                            } else {
                                reflow.append(parentFrame, refFrame);
                            }
                        }
                    }

                } else {
                    reflow.substituteRoot(newFrameTree);
                }
            }
            std::cout << "Presentation buildTreeFunc 7: " << std::endl;


            dirtyElements.pop_front();
        }
    }
}

[[noreturn]] void Presentation::petitionFunc() {
    std::deque<sp_HTMLElement> deque;
    while (true) {
        while (tempDirtyElements.begin() != tempDirtyElements.end()) {
            std::cout << "Presentation petitionFunc 1: " << std::endl;
            auto aux = tempDirtyElements.front();
            if (!isInDirtyBranch(aux)) {
                std::cout << "Presentation petitionFunc 2: " << std::endl;
                deque.push_back(aux);
            }

            std::cout << "Presentation petitionFunc 3: " << std::endl;
            tempDirtyElements.pop_front();
        }

        if (timer.getElapsedTime(Timer::MILLISECONDS) > REFRESH_RATE) {
            std::cout << "Presentation petitionFunc 4: " << std::endl;
            if (!deque.empty()) {
                std::cout << "Presentation petitionFunc 5: " << deque.size() << std::endl;
                dirtyElements.insert(dirtyElements.end(), deque.begin(), deque.end());
                deque.clear();
            }
            timer.restart();
        }
    }
}

bool Presentation::isInDirtyBranch(sp_HTMLElement &element) const {
    for (auto & de : dirtyElements) {
        if (NodeTravel(de).inclusive_ancestor(element)) {
            return true;
        }
    }

    return false;
}





Presentation::Presentation(unsigned type_) :
            Medium(type_),
            FontPresentation(this),
            reflow(this) {

    ruleTree = std::make_shared<RuleTree>();
    timer.start();

    petitionThread = std::thread([this]{ petitionFunc(); });
    petitionThread.detach();

    buildTreeThread = std::thread([this]{ buildTreeFunc(); });
    buildTreeThread.detach();
}

Presentation::Presentation(
        unsigned int type_,
        std::function<void ()> block,
        std::function<void ()> free)
        :
        Medium(type_),
        FontPresentation(this),
        reflow(this),
        viewport(block, free) {

    ruleTree = std::make_shared<RuleTree>();
    timer.start();

    petitionThread = std::thread([this]{ petitionFunc(); });
    petitionThread.detach();

    buildTreeThread = std::thread([this]{ buildTreeFunc(); });
    buildTreeThread.detach();
}

Presentation::~Presentation() {
    if (_ownerDocument) {
        _ownerDocument->removePresentation(this);
    }
}


void Presentation::reFlow(sp_HTMLElement root) {
    if (!root) return;
    if (root->ownerDocument() != _ownerDocument) return;

    std::cout << "Presentation Reflow : 1" << std::endl;
    tempDirtyElements.push_back(root);
    std::cout << "Presentation Reflow : 2" << std::endl;
}

void Presentation::fullReFlow() {
    ruleTree->clear();

    getUserFonts();

    if (auto element = _ownerDocument->documentElement) {
        if (auto htmlElement = std::dynamic_pointer_cast<HTMLElement>(element)) {
            reFlow(htmlElement);
        }
    }
}