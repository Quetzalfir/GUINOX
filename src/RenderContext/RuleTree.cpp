#include <CSSOM/CSSSelector.hpp>
#include "RuleTree.hpp"
#include <algorithm>
#include <iostream>



std::vector<RuleTree::RuleSpecificity> RuleTree::sortRules(const std::vector<std::pair<sp_CSSStyleRule, std::string>> & rules) {
    std::vector<RuleSpecificity> specificities;
    specificities.reserve(rules.size());

    for (size_t i = 0; i < rules.size(); i++) {
        specificities.emplace_back(rules[i], i);
    }

    std::stable_sort(specificities.begin(), specificities.end());

    return specificities;
}



RuleTree::RuleSpecificity::RuleSpecificity(const std::pair<sp_CSSStyleRule, std::string> &rule_, size_t position) {
    rule = rule_.first;
    specificity = CSSSelector::getSpecificity(rule_.second);
    pos = position;
}

bool RuleTree::RuleSpecificity::operator<(const RuleSpecificity &rhs) const {
    return (specificity == rhs.specificity) ? pos < rhs.pos : specificity < rhs.specificity;
}



sp_RuleTree RuleTree::append(const RuleSpecificity &ruleSpecificity) {
    auto node = std::make_shared<RuleTree>();

    node->_parent = shared_from_this();
    node->_rule = ruleSpecificity.rule;
    node->specificity = ruleSpecificity.specificity;
    node->position = ruleSpecificity.pos;

    children.push_back(node);

    return node;
}

sp_RuleTree RuleTree::lookForChild(const sp_CSSStyleRule &rule_) const {
    for (const auto & child : children) {
        if (child->rule == rule_) {
            return child;
        }
    }

    return nullptr;
}

void RuleTree::printIdent(int n) const {
    for (int i = 0; i < n; i++) {
        std::cout << "_";
    }

    std::cout << rule->cssText << std::endl;

    for (const auto & r : children) {
        r->printIdent(n+4);
    }
}




sp_RuleTree
RuleTree::appendPath(
        const std::vector<std::pair<sp_CSSStyleRule, std::string>> & rules
        ) {


    auto path = sortRules(rules);

    sp_RuleTree current = this->shared_from_this();
    auto it = path.begin();
    auto end = path.end();

    while (it != end) {
        if (auto aux = current->lookForChild((*it).rule)) {
            current = aux;
            it++;
        } else {
            break;
        }
    }

    for ( ; it != end; it++) {
        current = current->append(*it);
    }

    return current;
}

sp_RuleTree RuleTree::parent() const {
    if (auto p = _parent.lock()) return p;
    return nullptr;
}

void RuleTree::clear() {
    children.clear();
}

void RuleTree::print() const {
    std::cout << "___________________________ RULE TREE ___________________________" << std::endl;

    for (const auto & r : children) {
        r->printIdent(1);
    }

    std::cout << "_________________________ END RULE TREE _________________________" << std::endl;
}