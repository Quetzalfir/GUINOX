#ifndef GUINOX_FRAMEREGISTRY_HPP
#define GUINOX_FRAMEREGISTRY_HPP


#include "CSSModules/Display.hpp"
#include "Frames/Frame.hpp"
#include "../DOM/dom_declarations.hpp"
#include <functional>
#include <map>

class FrameRegistry {

    static std::map<CSSModule::Display::DisplayType, std::function<RenderFrame::sp_Frame(DOM_Node::sp_Node, sp_StyleContext, Presentation *)>> frames;


public:

    FrameRegistry(
            CSSModule::Display::DisplayType type,
            const std::function<RenderFrame::sp_Frame(DOM_Node::sp_Node, const sp_StyleContext &, Presentation *)>& constructor);



    static RenderFrame::sp_Frame get(DOM_Node::sp_Node node, sp_StyleContext style, Presentation *presentation);
};


#endif //GUINOX_FRAMEREGISTRY_HPP
