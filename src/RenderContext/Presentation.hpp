#ifndef GUINOX_PRESENTATION_HPP
#define GUINOX_PRESENTATION_HPP


/**
 * this class is based on https://developer.mozilla.org/en-US/docs/Web/CSS/@media#Media_features
 */


#include <string>
#include "RuleTree.hpp"
#include "../CSSOM/Medium.hpp"
#include "../DOM/dom_declarations.hpp"
#include "Frames/Frame.hpp"
#include "StyleContext.hpp"
#include "ReFlow.hpp"
#include "../OpenGL/ViewPort.hpp"
#include "../Utils/Timer.hpp"
#include "../Utils/Synchronizer.hpp"
#include "FontPresentation.hpp"


class Presentation :
        public Medium,
        public FontPresentation {

    friend class DocumentPresentation;
    friend class ReFlow;
    friend class RenderFrame::Frame;


    static RenderFrame::sp_Frame getFrameWidthRoot(const sp_HTMLElement& element, const RenderFrame::sp_Frame &root);



    std::deque<sp_HTMLElement> tempDirtyElements;
    std::deque<sp_HTMLElement> dirtyElements;
    std::thread petitionThread;
    std::thread buildTreeThread;


    DOM_Node::sp_Document _ownerDocument;
    Timer timer;

    // Frame Context
    sp_RuleTree ruleTree;
    ReFlow reflow;
    FloatManager floatManager;


    void updateTree(RenderFrame::sp_Frame &root, const std::vector<DOM_Node::sp_Node> & nodes);
    RenderFrame::sp_Frame getFrameFor(const sp_HTMLElement& element) const ;
    RenderFrame::sp_Frame createFrameRoot(sp_HTMLElement & element);
    RenderFrame::sp_Frame buildFrameTreeFor(sp_HTMLElement & element);
    RenderFrame::sp_Frame getNextFrameSibling(const sp_HTMLElement & element) const ;

    [[noreturn]] void buildTreeFunc();
    [[noreturn]] void petitionFunc();
    bool isInDirtyBranch(sp_HTMLElement & element) const ;

    //static RenderFrame::sp_Frame getAncientBlock(RenderFrame::sp_Frame frame, bool inclusive);



protected:
    RenderFrame::sp_Frame frameTree = nullptr;
    //Synchronizer synchronizer;


    Presentation(unsigned type);

    //Constructor with a blocking context in hte viewport
    Presentation(unsigned type, std::function<void(void)> block, std::function<void(void)> free);


public:

    ViewPort viewport;
    const DOM_Node::sp_Document & document = _ownerDocument;


    virtual ~Presentation();

    void reFlow(sp_HTMLElement element);
    void fullReFlow();

};


typedef std::shared_ptr<Presentation> sp_Presentation;


#endif //GUINOX_PRESENTATION_HPP
