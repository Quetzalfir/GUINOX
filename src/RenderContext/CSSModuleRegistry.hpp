#ifndef GUINOX_CSSMODULEREGISTRY_HPP
#define GUINOX_CSSMODULEREGISTRY_HPP


#include "CSSModules/Module.hpp"
#include <functional>
#include <vector>


class CSSModuleRegistry {
    friend CSSModule::Module;

    struct Module {
        std::function<std::shared_ptr<CSSModule::Module>(StyleContext *)> constructor;
        bool inherited = false;
        bool full_inherited = true;
    };

    static std::map<CSSModule::Type, Module> modules;
    static std::vector<CSSModule::sp_Module > defaultModules;
    static CSSModule::ShorthandMap _shorthands;
    static CSSModule::PropertyMap _propertiesMap;
    static unsigned int _accumulatedType;


    static void push_back(CSSModule::Type type,
                          const std::function< CSSModule::sp_Module (StyleContext *)>& constructor,
                          const std::map<std::string, CSSModule::Property> & properties);


public:
    CSSModuleRegistry(
            CSSModule::Type type,
            const std::function<CSSModule::sp_Module(StyleContext *)> &constructor,
            const std::map<std::string, CSSModule::Property> & properties);

    CSSModuleRegistry(
            CSSModule::Type type,
            const std::function<CSSModule::sp_Module(StyleContext *)> &constructor,
            const std::map<std::string, CSSModule::Property> & properties,
            const CSSModule::ShorthandMap & shorthands);


    static CSSModule::sp_Module get(CSSModule::Type type, StyleContext *parentContext) ;
    static CSSModule::Type getModuleTypeOf(const std::string &property);
    static std::vector<CSSModule::sp_Module> getMissingDefaultModule(unsigned int mod, StyleContext * parentContext);
    static std::vector<std::pair<std::string,std::string>> mapProperty(const std::string & property, const std::string & value);
    static std::vector<std::string> getLonghandsProperty(const std::string & property);

    const static unsigned int & accumulatedType;

};


#endif //GUINOX_CSSMODULEREGISTRY_HPP
