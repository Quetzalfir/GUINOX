#ifndef GUINOX_ELEMENTFRAME_HPP
#define GUINOX_ELEMENTFRAME_HPP

#include "Frames/Frame.hpp"
#include <vector>


class ElementFrame {

    HTMLElement *_ownerElement;


protected:
    std::vector<RenderFrame::Frame *> frames;


public:
    ElementFrame(HTMLElement * owner);


    void childrenDirty();
    void dirty();

};


#endif //GUINOX_ELEMENTFRAME_HPP
