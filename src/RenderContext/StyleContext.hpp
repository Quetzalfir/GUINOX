#ifndef GUINOX_STYLECONTEXT_HPP
#define GUINOX_STYLECONTEXT_HPP

#include <memory>
#include "RuleTree.hpp"
#include "CSSModules/Module.hpp"


class RenderContext;


class StyleContext;
typedef std::weak_ptr<StyleContext> wp_StyleContext;
typedef std::shared_ptr<StyleContext> sp_StyleContext;


class StyleContext :
            public std::enable_shared_from_this<StyleContext>
            {


    wp_RuleTree _ruleTree;
    StyleContext * _parent = nullptr;
    wp_StyleContext _prevRuleSibling;
    wp_StyleContext _nextRuleSibling;
    wp_StyleContext _prevSibling;
    wp_StyleContext _nextSibling;
    std::list<StyleContext *> _children;
    std::vector<CSSModule::sp_Module> modules;
    CSSStyleDeclaration * _styleInLine = nullptr;



    void printIdent(int n) const ;

    CSSStyleDeclaration getDeclarations() const ;
    CSSModule::sp_Module getOwnModule(CSSModule::Type);


    sp_StyleContext findPrevRuleSibling(const sp_StyleContext& context, bool inclusive) const ;
    sp_StyleContext findNextRuleSibling(const sp_StyleContext& context, bool inclusive) const ;



public:

    StyleContext(sp_RuleTree rule, CSSStyleDeclaration * styleInLine);
    virtual ~StyleContext();


    sp_RuleTree ruleTree() const ;
    inline sp_StyleContext prevRuleSibling() const { return _prevRuleSibling.lock(); }
    inline sp_StyleContext nextRuleSibling() const { return _nextRuleSibling.lock(); }
    inline sp_StyleContext prevSibling() const { return _prevSibling.lock(); }
    inline sp_StyleContext nextSibling() const { return _nextSibling.lock(); }
    inline sp_StyleContext lastChild() const { return _children.empty() ? nullptr : _children.back()->shared_from_this() ; }
    inline sp_StyleContext firstChild() const { return _children.empty() ? nullptr : _children.front()->shared_from_this() ; }
    inline sp_StyleContext parent() const { return _parent ? _parent->shared_from_this() : nullptr; }
    inline bool inLine() const { return !(_styleInLine->empty()); }


    void clear();
    inline bool empty() const { return _children.empty(); }
    sp_StyleContext append(sp_StyleContext context);
    sp_StyleContext insert(const sp_StyleContext& context, const sp_StyleContext& child);
    sp_StyleContext replace(const sp_StyleContext& context, const sp_StyleContext& child);
    sp_StyleContext remove(sp_StyleContext context);
    CSSModule::sp_Module getModule(CSSModule::Type type) const ;
    void print() const ;
    void compute();


    sp_StyleContext findPrevRuleSibling(const sp_RuleTree &rule, bool inclusive) const ;
    sp_StyleContext findNextRuleSibling(const sp_RuleTree &rule, bool inclusive) const ;

};




#endif //GUINOX_STYLECONTEXT_HPP
