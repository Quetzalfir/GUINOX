#ifndef GUINOX_DOCUMENTPRESENTATION_HPP
#define GUINOX_DOCUMENTPRESENTATION_HPP


#include "Presentation.hpp"


class DocumentPresentation {

    DOM_Node::Document * _document;
    std::vector<Presentation *> presentations;


public:

    DocumentPresentation(DOM_Node::Document * document);

    void addPresentation(Presentation * presentation);
    void removePresentation(Presentation * presentation);

    void fullReFlow();

};


#endif //GUINOX_DOCUMENTPRESENTATION_HPP
