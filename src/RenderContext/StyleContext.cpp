#include "StyleContext.hpp"
#include "CSSModuleRegistry.hpp"



void StyleContext::printIdent(int n) const {
    for (int i = 0; i < n; i++) {
        std::cout << "_";
    }

    //std::cout << "ch: " << _children.size() << std::endl;
    auto rule = ruleTree()->rule;
    std::cout << (rule ? rule->selectorText : "") << "\n" << getDeclarations().cssText << std::endl;

    for (const auto & r : _children) {
        r->printIdent(n+4);
    }
}

void StyleContext::compute() {
    auto sibling = prevRuleSibling();

    unsigned int mod = 0;

    // get the modules that are inline in this context
    unsigned int inlined = 0;
    if (_styleInLine) {
        for (const auto &p : *_styleInLine) {
            auto type = CSSModuleRegistry::getModuleTypeOf(p->name);
            inlined |= type;
        }
    }


    // check for all the modules that can be shared
    while (sibling && mod != CSSModuleRegistry::accumulatedType) {
        for (const auto & module : sibling->modules) {
            if (!module->inLine && !(mod & module->type) && !(inlined & module->type)) {
                modules.push_back(module);
                mod |= module->type;
            }
        }

        sibling = sibling->prevRuleSibling();
    }

    // add all modules that are missing in the rules
    if (mod != CSSModuleRegistry::accumulatedType) {
        auto style = getDeclarations();
        unsigned int aux = 0;

        for (const auto & p : style) {
            auto type = CSSModuleRegistry::getModuleTypeOf(p->name);

            if (!(mod & type)) {
                if (auto module = getOwnModule(type)) {
                    module->setProperty(p->name, p->value);
                    aux |= type;
                }
            }
        }

        mod |= aux;
    }

    // override all properties that are in the style inline
    if (_styleInLine) {
        for (const auto &p : *_styleInLine) {
            auto type = CSSModuleRegistry::getModuleTypeOf(p->name);

            if (auto module = getOwnModule(type)) {
                module->setProperty(p->name, p->value);
                module->inLine = true;
                mod |= type;
            }
        }
    }

    // full filled all the missing modules left with the default values
    if (mod != CSSModuleRegistry::accumulatedType) {
        for (const auto &m : CSSModuleRegistry::getMissingDefaultModule(mod, _parent)) {
            modules.push_back(m);
        }
    }
}

CSSStyleDeclaration StyleContext::getDeclarations() const {
    CSSStyleDeclaration style;
    std::deque<sp_CSSStyleRule> rules;

    auto aux = ruleTree();
    while (aux && aux->rule) {
        rules.push_front(aux->rule);
        aux = aux->parent();
    }

    for (const auto & r : rules) {
        style += r->style;
    }

    return style;
}

CSSModule::sp_Module StyleContext::getOwnModule(CSSModule::Type type) {
    // search if module has been added
    for (auto & module : modules) {
        if (module->type == type) return module;
    }

    // if not creat a new one and append it
    CSSModule::sp_Module module = CSSModuleRegistry::get(type, _parent);

    if (module) {
        modules.push_back(module);
    }

    return module;
}

sp_StyleContext StyleContext::findPrevRuleSibling(const sp_StyleContext &context, bool inclusive) const {
    if (!context) return nullptr;

    sp_StyleContext aux;
    if (inclusive) {
        aux = std::const_pointer_cast<StyleContext>( this->shared_from_this() );

    } else {
        aux = prevSibling();
    }

    auto rule = context->ruleTree();
    while (aux && aux->ruleTree() != rule) aux = aux->prevSibling();

    return aux;
}

sp_StyleContext StyleContext::findNextRuleSibling(const sp_StyleContext &context, bool inclusive) const {
    if (!context) return nullptr;

    sp_StyleContext aux;
    if (inclusive) {
        aux = std::const_pointer_cast<StyleContext>( this->shared_from_this() );

    } else {
        aux = nextSibling();
    }

    auto rule = context->ruleTree();
    while (aux && aux->ruleTree() != rule) aux = aux->nextSibling();

    return aux;
}




StyleContext::StyleContext(
        sp_RuleTree rule,
        CSSStyleDeclaration * styleInLine_
        ) :
        _ruleTree(rule),
        _styleInLine(styleInLine_)
        {

}

StyleContext::~StyleContext() {
    if (_parent) {
        auto end = _parent->_children.end();

        for (auto it = _parent->_children.begin(); it != end; it++) {
            if ((*it) == this) {
                _parent->_children.erase(it);
                break;
            }
        }
    }
}

sp_RuleTree StyleContext::ruleTree() const {
    return _ruleTree.lock();
}

void StyleContext::clear() {
    _children.clear();
}

sp_StyleContext StyleContext::append(sp_StyleContext context) {
    return insert(context, nullptr);
}

sp_StyleContext StyleContext::insert(const sp_StyleContext& context, const sp_StyleContext& child) {
    if (!context) return nullptr;
    if (context->_parent == this) return context; // keep push order

    sp_StyleContext previousSibling = child ? child->prevSibling() : this->lastChild() ;

    auto end = this->_children.end();
    auto it = this->_children.begin();

    if (child) {
        for (; it != end; it++) {
            if ((*it) == child.get())
                break;
        }

    } else {
        it = this->_children.end();
    }

    remove(context);

    context->_prevSibling = previousSibling;
    context->_nextSibling = child;

    sp_StyleContext prevRuleSibling;
    sp_StyleContext nextRuleSibling;

    if (child) {
        prevRuleSibling = child->findPrevRuleSibling(context, false);
        nextRuleSibling = prevRuleSibling ? prevRuleSibling->nextRuleSibling() : child->findNextRuleSibling(context, true) ;

    } else {
        if (auto last = lastChild()) {
            prevRuleSibling = last->findPrevRuleSibling(context, true);
        }
    }



    context->_prevRuleSibling = prevRuleSibling;
    context->_nextRuleSibling = nextRuleSibling;

    if (prevRuleSibling) {
        prevRuleSibling->_nextRuleSibling = context;
    }

    if (nextRuleSibling) {
        nextRuleSibling->_prevRuleSibling = context;
    }


    if (child) {
        child->_prevSibling = context;
    }

    if (previousSibling) {
        previousSibling->_nextSibling = context;
    }

    context->_parent = this;

    this->_children.insert(it, context.get());

    context->compute();

    return context;
}

sp_StyleContext StyleContext::replace(const sp_StyleContext &context, const sp_StyleContext &child) {
    if (!child || !context) return nullptr;

    if (child == context) return child;

    auto aux = this->_parent;
    while (aux && aux != context.get()) aux = aux->_parent;

    if (aux) return nullptr;

    if (child->_parent != this) return nullptr;


    auto referenceChild = child->nextSibling();

    if (referenceChild == context) {
        referenceChild = context->nextSibling();
    }

    auto previousSibling = child->prevSibling();

    remove(child);

    insert(context, referenceChild);

    return child;
}

sp_StyleContext StyleContext::remove(sp_StyleContext child) {
    if (!child || child->_parent != this) return nullptr;

    auto oldPreviousSibling = child->prevSibling();
    auto oldNextSibling = child->nextSibling();

    if (oldPreviousSibling) {
        oldPreviousSibling->_nextSibling = oldNextSibling;
    }

    if (oldNextSibling) {
        oldNextSibling->_prevSibling = oldPreviousSibling;
    }

    auto oldPrevRuleSibling = child->prevRuleSibling();
    auto oldNextRuleSibling = child->nextRuleSibling();

    if (oldPrevRuleSibling) {
        oldPrevRuleSibling->_nextRuleSibling = oldNextRuleSibling;
    }

    if (oldNextRuleSibling) {
        oldNextRuleSibling->_prevRuleSibling = oldPrevRuleSibling;
    }

    child->_prevSibling.reset();
    child->_nextSibling.reset();
    child->_prevRuleSibling.reset();
    child->_nextRuleSibling.reset();
    child->_parent = nullptr;
    child->modules.clear();

    return child;
}

CSSModule::sp_Module StyleContext::getModule(CSSModule::Type type) const {
    for (const auto & module : modules) {
        if (module->type == type) return module;
    }

    return nullptr;
}

void StyleContext::print() const {
    std::cout << "___________________________ CONTEXT TREE ___________________________" << std::endl;

    printIdent(1);

    std::cout << "_________________________ END CONTEXT TREE _________________________" << std::endl;
}

sp_StyleContext StyleContext::findPrevRuleSibling(const sp_RuleTree &rule, bool inclusive) const {
    if (!rule) return nullptr;

    sp_StyleContext aux;
    if (inclusive) {
        aux = std::const_pointer_cast<StyleContext>( this->shared_from_this() );

    } else {
        aux = prevSibling();
    }

    while (aux && aux->ruleTree() != rule) aux = aux->prevSibling();

    return aux;
}

sp_StyleContext StyleContext::findNextRuleSibling(const sp_RuleTree &rule, bool inclusive) const {
    if (!rule) return nullptr;

    sp_StyleContext aux;
    if (inclusive) {
        aux = std::const_pointer_cast<StyleContext>( this->shared_from_this() );

    } else {
        aux = nextSibling();
    }

    while (aux && aux->ruleTree() != rule) aux = aux->nextSibling();

    return aux;
}