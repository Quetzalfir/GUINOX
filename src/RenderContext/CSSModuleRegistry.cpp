#include <Utils/Utils.hpp>
#include "CSSModuleRegistry.hpp"
#include "StyleContext.hpp"

#include "CSSModules/Background.hpp"
#include "CSSModules/Border.hpp"
#include "CSSModules/Box.hpp"
#include "CSSModules/Display.hpp"
#include "CSSModules/Flex.hpp"
#include "CSSModules/Float.hpp"
#include "CSSModules/Font.hpp"
#include "CSSModules/Grid.hpp"
#include "CSSModules/List.hpp"
#include "CSSModules/Margin.hpp"
#include "CSSModules/Overflow.hpp"
#include "CSSModules/Padding.hpp"
#include "CSSModules/Position.hpp"
#include "CSSModules/Text.hpp"
#include "CSSModules/Visibility.hpp"


std::map<CSSModule::Type,CSSModuleRegistry::Module> CSSModuleRegistry::modules;
std::vector<CSSModule::sp_Module > CSSModuleRegistry::defaultModules;
CSSModule::ShorthandMap CSSModuleRegistry::_shorthands;
CSSModule::PropertyMap CSSModuleRegistry::_propertiesMap;
unsigned int CSSModuleRegistry::_accumulatedType = 0;
const unsigned int & CSSModuleRegistry::accumulatedType = _accumulatedType;


void CSSModuleRegistry::push_back(
        CSSModule::Type type,
        const std::function<CSSModule::sp_Module (StyleContext *)>& constructor,
        const std::map<std::string, CSSModule::Property> &properties
        ) {

    Module module;

    module.constructor = constructor;

    for (auto & p : properties) {
        module.inherited |= p.second.inherit;
        module.full_inherited &= p.second.inherit;
    }

    modules[type] = module;

    _accumulatedType |= type;

    _propertiesMap.insert(properties.begin(), properties.end());

    defaultModules.push_back(constructor(nullptr));

}

CSSModuleRegistry::CSSModuleRegistry(
        CSSModule::Type type,
        const std::function<CSSModule::sp_Module(StyleContext *)> &constructor,
        const std::map<std::string, CSSModule::Property> & properties
        ) {

    if (type && !(_accumulatedType & type)) { // if the module has not been registered
        push_back(type, constructor, properties);
    }
}

CSSModuleRegistry::CSSModuleRegistry(
        CSSModule::Type type,
        const std::function<CSSModule::sp_Module(StyleContext *)> &constructor,
        const std::map<std::string, CSSModule::Property> & properties,
        const CSSModule::ShorthandMap & shorthands
) {

    if (type && !(_accumulatedType & type)) { // if the module has not been registered
        push_back(type, constructor, properties);

        _shorthands.insert(shorthands.begin(), shorthands.end());
    }
}

CSSModule::sp_Module CSSModuleRegistry::get(CSSModule::Type type, StyleContext *parentContext) {
    auto it = modules.find(type);
    if (it != modules.end()) {

        return (*it).second.constructor(parentContext);
    }

    return nullptr;
}

CSSModule::Type CSSModuleRegistry::getModuleTypeOf(const std::string &property) {
    auto it = _propertiesMap.find(property);
    if (it != _propertiesMap.end()) {
        return (*it).second.module;
    }

    return CSSModule::UNKNOWN;
}

std::vector<CSSModule::sp_Module> CSSModuleRegistry::getMissingDefaultModule(unsigned int mod, StyleContext * parentContext) {
    std::vector<CSSModule::sp_Module > missing;

    for (const auto & m : defaultModules) {
        if (!(m->type & mod)) {
            auto & module = modules[m->type];

            if (module.full_inherited) {
                if (parentContext) {
                    if (auto inherit = parentContext->getModule(m->type)) {
                        missing.push_back(inherit);
                    }
                } else {
                    missing.push_back(module.constructor(parentContext));
                }

            } else if (module.inherited) {
                missing.push_back(module.constructor(parentContext));

            } else {
                missing.push_back(m);
            }
        }
    }

    return missing;
}

std::vector<std::pair<std::string, std::string> > CSSModuleRegistry::mapProperty(const std::string &property, const std::string &value) {
    std::vector<std::pair<std::string, std::string>> properties;
    std::string name_l = Utils::toLower(property);

    auto it = _shorthands.find(name_l);
    if (it != _shorthands.end()) {
        properties = (*it).second.getter(value);

    } else {
        properties.emplace_back(name_l, value);

    }

    for (auto & p : properties) {
        p.second = Utils::trim(p.second);
    }

    return properties;
}

std::vector<std::string> CSSModuleRegistry::getLonghandsProperty(const std::string &property) {
    auto it = _shorthands.find(property);
    if (it != _shorthands.end()) {
        return (*it).second.longHands;
    }

    return { property };
}



namespace CSSModule {


    const PropertyMap Background::properties = {
            {"background-attachment", {BACKGROUND, "scroll",      false}},
            {"background-blend-mode", {BACKGROUND, "normal",      false}},
            {"background-clip",       {BACKGROUND, "border-box",  false}},
            {"background-color",      {BACKGROUND, "transparent", false}},
            {"background-image",      {BACKGROUND, "none",        false}},
            {"background-origin",     {BACKGROUND, "padding-box", false}},
            {"background-position",   {BACKGROUND, "0% 0%",       false}},
            {"background-repeat",     {BACKGROUND, "repeat",      false}},
            {"background-size",       {BACKGROUND, "auto",        false}}
    };

    const ShorthandMap Background::shorthands = {
            {"background", { Background::mapBackgroundShorthand, {"background-color",
                                                             "background-image",
                                                             "background-repeat",
                                                             "background-attachment",
                                                             "background-position"} } }
    };

    const CSSModuleRegistry Background::regist(BACKGROUND, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Background>(parentContext);
    }, Background::properties, Background::shorthands);









    const PropertyMap Border::properties = {
            {"border-bottom-color", {BORDER, "actual", false}},
            {"border-bottom-style", {BORDER, "none",   false}},
            {"border-bottom-width", {BORDER, "0", false}},
            {"border-left-color",   {BORDER, "actual", false}},
            {"border-left-style",   {BORDER, "none",   false}},
            {"border-left-width",   {BORDER, "0", false}},
            {"border-right-color",  {BORDER, "black",  false}},
            {"border-right-style",  {BORDER, "none",   false}},
            {"border-right-width",  {BORDER, "0", false}},
            {"border-top-color",    {BORDER, "actual", false}},
            {"border-top-style",    {BORDER, "none",   false}},
            {"border-top-width",    {BORDER, "0", false}},
    };

    const ShorthandMap Border::shorthands = {
            {"border", { Border::mapBorderShorthand, {"border-top-width",
                                                     "border-right-width",
                                                     "border-bottom-width",
                                                     "border-left-width",
                                                     "border-top-style",
                                                     "border-right-style",
                                                     "border-bottom-style",
                                                     "border-left-style",
                                                     "border-top-color",
                                                     "border-right-color",
                                                     "border-bottom-color",
                                                     "border-left-color"} } },

            {"border-top", {Border::mapBorderTopShorthand, {"border-top-width",
                                                     "border-top-style",
                                                     "border-top-color"}}},

            {"border-left", {Border::mapBorderLeftShorthand, {"border-left-width",
                                                     "border-left-style",
                                                     "border-left-color"}}},

            {"border-bottom", {Border::mapBorderBottomShorthand, {"border-bottom-width",
                                                     "border-bottom-style",
                                                     "border-bottom-color"}}},

            {"border-right", {Border::mapBorderRightShorthand, {"border-right-width",
                                                     "border-right-style",
                                                     "border-right-color"}}},

            {"border-width", {Border::mapBorderWidthShorthand, {"border-top-width",
                                                     "border-right-width",
                                                     "border-bottom-width",
                                                     "border-left-width"}}},

            {"border-color", {Border::mapBorderColorShorthand, {"border-top-color",
                                                     "border-right-color",
                                                     "border-bottom-color",
                                                     "border-left-color"}}},

            {"border-style", {Border::mapBorderStyleShorthand, {"border-top-style",
                                                     "border-right-style",
                                                     "border-bottom-style",
                                                     "border-left-style"}}},
    };

    const CSSModuleRegistry Border::regist(BORDER, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Border>(parentContext);
    }, Border::properties, Border::shorthands);









    const PropertyMap Box::properties = {
            {"width",     {BOX, "auto", false} },
            {"height",    {BOX, "auto", false} },
            {"max-width", {BOX, "none", false} },
            {"max-height",{BOX, "none", false} },
            {"min-width", {BOX, "0", false} },
            {"min-height",{BOX, "0", false} },
            {"box-sizing",{BOX, "content-box", false} },
            {"img-ratio", {BOX, "none", false} }
    };

    const CSSModuleRegistry Box::regist(BOX, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Box>(parentContext);
    }, Box::properties);










    const PropertyMap Display::properties = {
            {"display",     {DISPLAY, "inline", false} } };

    const CSSModuleRegistry Display::regist(DISPLAY, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Display>(parentContext);
    }, Display::properties);







    const PropertyMap Flex::properties = {
            {"flex-direction",  {FLEX, "row", false}},
            {"flex-wrap",       {FLEX, "nowrap", false}},
            {"justify-content", {FLEX, "flex-start", false}},
            {"align-items",     {FLEX, "stretch", false}},
            {"align-content",   {FLEX, "stretch", false}},
            {"order",           {FLEX, "0", false}},
            {"flex-grow",       {FLEX, "0", false}},
            {"flex-shrink",     {FLEX, "1", false}},
            {"flex-basis",      {FLEX, "auto", false}},
            {"align-self",      {FLEX, "auto", false}}
    };

    const ShorthandMap Flex::shorthands = {
            {"flex", {Flex::mapFlexShorthand, {"flex-grow",
                                                "flex-shrink",
                                                "flex-basis"}}},
            {"flex-flow", {Flex::mapFlexFlowShorthand, {"flex-direction",
                                                "flex-wrap"}}}
    };

    const CSSModuleRegistry Flex::regist(FLEX, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Flex>(parentContext);
    }, Flex::properties, Flex::shorthands);








    const PropertyMap Float::properties = {
            {"float",  {FLOAT, "none", false}},
            {"clear",  {FLOAT, "none", false}},
    };

    const CSSModuleRegistry Float::regist(FLOAT, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Float>(parentContext);
    }, Float::properties);







    const PropertyMap Font::properties = {
            {"font-family",       {FONT, "sans-serif",  true}},
            {"font-size",         {FONT, "medium", true}},
            {"font-size-adjust",  {FONT, "none",   true}},
            {"font-stretch",      {FONT, "normal", true}},
            {"font-style",        {FONT, "normal", true}},
            {"font-variant",      {FONT, "normal", true}},
            {"font-variant-caps", {FONT, "normal", true}},
            {"font-weight",       {FONT, "normal", true}}
    };

    const ShorthandMap Font::shorthands = {
            {"font", { mapFontShorthand, {"font-style",
                                                 "font-variant",
                                                 "font-weight",
                                                 "font-size",
                                                 "line-height",
                                                 "font-family"} } }
    };

    const CSSModuleRegistry Font::regist(FONT, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Font>(parentContext);
    }, Font::properties, Font::shorthands);






    const PropertyMap Grid::properties = {
            {"grid-auto-columns",     {GRID, "auto", false} },
            {"grid-auto-flow",        {GRID, "row", false} },
            {"grid-auto-rows",        {GRID, "auto", false} },
            {"grid-column-end",       {GRID, "auto", false} },
            {"grid-column-gap",       {GRID, "0px", false} },
            {"grid-column-start",     {GRID, "auto", false} },
            {"grid-row-end",          {GRID, "auto", false} },
            {"grid-row-gap",          {GRID, "0px", false} },
            {"grid-row-start",        {GRID, "auto", false} },
            {"grid-template-areas",   {GRID, "none", false} },
            {"grid-template-columns", {GRID, "none", false} },
            {"grid-template-rows",    {GRID, "none", false} },
            {"grid-area-name",        {GRID, "", false} }
    };

    const ShorthandMap Grid::shorthands = {
            {"grid",{[](const std::string & value) { return mapGridShorthand(value, gridParser);}, {"grid-template-rows", "grid-template-columns", "grid-template-areas", "grid-auto-rows", "grid-auto-columns", "grid-auto-flow"}}},
            {"grid-gap",{[](const std::string & value) { return mapGridShorthand(value, gridGapParser);}, {"grid-row-gap", "grid-column-gap"}}},
            {"grid-row",{[](const std::string & value) { return mapGridRoCShorthand(value, true);}, {"grid-row-start", "grid-row-end"}}},
            {"grid-column",{[](const std::string & value) { return mapGridRoCShorthand(value, false);}, {"grid-column-start", "grid-column-end"}}},
            {"grid-template",{[](const std::string & value) { return mapGridShorthand(value, gridTemplateParser);}, {"grid-template-rows", "grid-template-columns", "grid-template-areas"}}},
            {"grid-area",{[](const std::string & value) { return mapGridShorthand(value, gridAreaParser);}, {"grid-row-start", "grid-column-start", "grid-row-end", "grid-column-end", "grid-area-name"}}}
    };

    const CSSModuleRegistry Grid::regist(GRID, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Grid>(parentContext);
    }, properties, shorthands);







    const PropertyMap List::properties = {
            {"list-style-image",   {LIST, "none", true} },
            {"list-style-position",{LIST, "outside", true} },
            {"list-style-type",    {LIST, "disc", true} }
    };

    const ShorthandMap List::shorthands = {
            {"list-style", {mapListShorthand, {"list-style-type", "list-style-position", "list-style-image"}}}
    };

    const CSSModuleRegistry List::regist(LIST, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<List>(parentContext);
    }, properties, shorthands);








    const PropertyMap Margin::properties = {
            {"margin-top",      {MARGIN, "0px", false} },
            {"margin-right",    {MARGIN, "0px", false} },
            {"margin-bottom",   {MARGIN, "0px", false} },
            {"margin-left",     {MARGIN, "0px", false} }
    };

    const ShorthandMap Margin::shorthands = {
            {"margin", { mapMarginShorthand, {"margin-top",
                                                     "margin-right",
                                                     "margin-bottom",
                                                     "margin-left"} } }
    };

    const CSSModuleRegistry Margin::regist(MARGIN, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Margin>(parentContext);
    }, Margin::properties, Margin::shorthands);







    const PropertyMap Overflow::properties = {
            {"overflow-x",   {OVERFLOW_M, "visible", false} },
            {"overflow-y",   {OVERFLOW_M, "visible", false} }
    };


    const CSSModuleRegistry Overflow::regist(OVERFLOW_M, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Overflow>(parentContext);
    },properties);








    const PropertyMap Padding::properties = {
            {"padding-top",    {PADDING, "0px", false} },
            {"padding-right",  {PADDING, "0px", false} },
            {"padding-bottom", {PADDING, "0px", false} },
            {"padding-left",   {PADDING, "0px", false} }
    };

    const ShorthandMap Padding::shorthands = {
            {"padding", { mapPaddingShorthand, {"padding-top",
                                                       "padding-right",
                                                       "padding-bottom",
                                                       "padding-left"} } }
    };

    const CSSModuleRegistry Padding::regist(PADDING, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Padding>(parentContext);
    }, Padding::properties, Padding::shorthands);







    const PropertyMap Position::properties = {
            {"bottom",  {POSITION, "auto", false} },
            {"top",     {POSITION, "auto", false} },
            {"left",    {POSITION, "auto", false} },
            {"right",   {POSITION, "auto", false} },
            {"position",{POSITION, "static", false} },
            {"z-index", {POSITION, "auto", false} }
    };

    const CSSModuleRegistry Position::regist(POSITION, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Position>(parentContext);
    }, Position::properties);





    const PropertyMap Text::properties = {
            {"text-align",           {TEXT, "left", true} }, //left if direction is ltr, and right if direction is rtl
            {"color",                {TEXT, "black", true} },
            {"text-align-last",      {TEXT, "auto", true} },
            {"text-decoration-color",{TEXT, "currentColor", false} },
            {"text-decoration-line", {TEXT, "none", false} },
            {"text-decoration-style",{TEXT, "solid", false} },
            {"text-indent",          {TEXT, "0px", true} },
            {"text-justify",         {TEXT, "auto", true} },
            {"text-overflow",        {TEXT, "clip", false} },
            {"text-shadow",          {TEXT, "none", true} },
            {"text-transform",       {TEXT, "none", true} }
    };

    const ShorthandMap Text::shorthands = {
            {"text-decoration", {mapTextShorthand, {"text-decoration-line", "text-decoration-color", "text-decoration-style"}}}
    };

    const CSSModuleRegistry Text::regist(TEXT, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Text>(parentContext);
    },properties, shorthands);






    const PropertyMap Visibility::properties = {
            {"visibility",     {VISIBILITY, "visible", true} } };

    const CSSModuleRegistry Visibility::regist(VISIBILITY, [](StyleContext *parentContext) -> sp_Module {
        return std::make_shared<Visibility>(parentContext);
    }, Visibility::properties);

}


