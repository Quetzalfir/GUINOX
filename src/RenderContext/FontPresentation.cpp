#include "FontPresentation.hpp"
#include <DOM/Document.hpp>
#include <CSSOM/CSSOMTravel.hpp>



bool FontPresentation::Font_Properties::operator==(const Font_Properties &rhs) const {
    return (stretch == rhs.stretch ||
            style == rhs.style ||
            weight == rhs.weight);
}





FontPresentation::Stretch FontPresentation::getStretch(const std::string &value) {
    if (value == "condensed")       return CONDENSED;
    if (value == "ultra-condensed") return ULTRA_CONDENSED;
    if (value == "extra-condensed") return EXTRA_CONDENSED;
    if (value == "semi-condensed")  return SEMI_CONDENSED;
    if (value == "expanded")        return EXPANDED;
    if (value == "semi-expanded")   return SEMI_EXPANDED;
    if (value == "extra-expanded")  return EXTRA_EXPANDED;
    if (value == "ultra-expanded")  return ULTRA_EXPANDED;

    return NORMAL;
}

FontPresentation::Style FontPresentation::getStyle(const std::string &value) {
    if (value == "italic")   return ITALIC;
    if (value == "oblique")  return OBLIQUE;

    return NORMAL_S;
}

int FontPresentation::getWeight(const std::string &value) {
    if (value.empty())      return 400;
    if (value == "lighter") return 200;
    if (value == "normal")  return 400;
    if (value == "bold")    return 600;
    if (value == "bolder")  return 800;

    return std::stoi(value);
}


FontPresentation::FontPresentation(Presentation *owner) : _ownerPresentation(owner) {

}

void FontPresentation::getUserFonts() {
    _fonts.clear();

    auto styleSheets = _ownerPresentation->document->styleSheets;

    for (auto &sheet : styleSheets) {
        CSSOMTravel(sheet, _ownerPresentation).forEachFontRule([&](sp_CSSFontFaceRule rule) {
            std::string name = rule->style["font-family"];

            if (name.empty()) return ;

            Font_Properties properties;

            std::string url = rule->style["src"];

            url = url.substr(5, url.length()-5-2);
            auto styleURL = rule->parentStyleSheet()->url;
            auto pos = styleURL.find_last_of('/');

            url = styleURL.substr(0,pos+1) + url;

            properties.url = url;
            properties.stretch = getStretch(rule->style["font-stretch"]);
            properties.style = getStyle(rule->style["font-style"]);
            properties.weight = getWeight(rule->style["font-weight"]);
            properties.range = rule->style["unicode-range"];

            bool found = false;
            auto & vec = _fonts[name];

            for (auto & prop : vec) {
                if (prop == properties) {
                    found = true;
                    prop = properties;
                    break;
                }
            }

            if (!found) {
                vec.push_back(properties);
            }
        });
    }
}

const std::vector<FontPresentation::Font_Properties> & FontPresentation::getFontFamily(const std::string &name) {
    static const std::vector<Font_Properties> T;

    auto it = _fonts.find(name);
    if (it != _fonts.end()) {
        return (*it).second;
    }

    return T;
}