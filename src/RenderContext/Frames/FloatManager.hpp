#ifndef GUINOX_FLOATMANAGER_HPP
#define GUINOX_FLOATMANAGER_HPP

#include <memory>
#include <vector>
#include "../../Utils/Point.hpp"
#include "ContentFrameStructures.hpp"


namespace RenderFrame {
    class Frame;

    typedef std::shared_ptr<Frame> sp_Frame ;
    typedef std::weak_ptr<Frame> wp_Frame ;
}




class FloatManager {
    //friend class ViewPort;

    //std::vector<RenderFrame::Frame *> parentFrames;


    struct Float {
        bool left;
        RenderFrame::Frame *frame;
    };

    std::vector<Float> floatsR, floatsL;


    static bool intersect(const DOMRect &A, const DOMRect &B);
    static bool intersectTop(const DOMRect & content, double top);


public:

    static bool isFloating(RenderFrame::sp_Frame & frame);


    void positionFloat(RenderFrame::sp_Frame & frame, const Point & pen, RenderFrame::Frame *content);
    double getAvailableSpaceFor(double top, const DOMRect & content) const ;
    double getLeftSpace(double top, const DOMRect & content) const ;
    bool clear(RenderFrame::sp_Frame & frame, const DOMRect & content) const ;

    void removeFloat(RenderFrame::Frame * frame);

};


#endif //GUINOX_FLOATMANAGER_HPP
