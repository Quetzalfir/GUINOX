#ifndef GUINOX_CONTENTFRAMESTRUCTURES_HPP
#define GUINOX_CONTENTFRAMESTRUCTURES_HPP

#include "../../DOM/DOMRect.hpp"
#include "../../OpenGL/ViewPort.hpp"
#include <vector>


namespace RenderFrame {

    class ContentFrameStructures {


    public:

        class Content {
        public:
            DOMRect rect;
            double baseline = 0;

            virtual void render(ViewPort & viewport) = 0;
            virtual void move(double x, double y) = 0;
            void moveTo(double x, double y);
        };

        class Line {
            double _advanced = 0;
            std::vector<Content *> contents;


        public:
            DOMRect rect;
            bool positioned = false;
            const double & advanced = _advanced;

            bool fits(double width) const ;
            //void moveTo(double x, double y);
            void advance(double);
            void setWidth(double);
            void append(Content * content);

        };


        virtual Line * getLine() = 0;
        virtual Line * createNewLine() = 0 ;
        virtual void appendToLine(Content * content) = 0;
        virtual void advanceLine(double) = 0;

    };

}


#endif //GUINOX_CONTENTFRAMESTRUCTURES_HPP
