#include <RenderContext/ElementRenderizable.hpp>
#include "Block.hpp"

#include "../CSSModules/Box.hpp"
#include "../CSSModules/Padding.hpp"
#include "../CSSModules/Border.hpp"
#include "../CSSModules/Background.hpp"
#include "../Presentation.hpp"
#include <DOM/HTMLElement.hpp>
#include <RenderContext/CSSModules/Margin.hpp>
#include <RenderContext/CSSModules/Position.hpp>


namespace RenderFrame {


    void Block::setSizing() {
        auto module = style->getModule(CSSModule::BOX);
        auto boxModule = std::dynamic_pointer_cast<CSSModule::Box>(module);

        switch (boxModule->box_sizing) {
            case CSSModule::Box::CONTENT_BOX:
                FrameBoxModel::mode = FrameBoxModel::CONTENT;
                break;
            case CSSModule::Box::BORDER_BOX:
                FrameBoxModel::mode = FrameBoxModel::BORDER;
                break;
        };
    }

    void Block::setBox() {
        auto module = style->getModule(CSSModule::BOX);
        auto boxModule = std::dynamic_pointer_cast<CSSModule::Box>(module);

        double width = 0, height = 0;

        double minWidth = compute(boxModule->min_width, HORIZONTAL);
        double maxWidth = compute(boxModule->max_width, HORIZONTAL);

        double maxHeight = compute(boxModule->max_height, VERTICAL);
        double minHeight = compute(boxModule->min_height, VERTICAL);


        if (auto htmlElement = dynamic_cast<HTMLElement*>(_ownerNode)) {
            if (htmlElement->renderizable) {
                double ratio = htmlElement->width/htmlElement->height;
                width = htmlElement->width;
                height = htmlElement->height;

                if (boxModule->width.text == "auto" && boxModule->height.text != "auto") {
                    height = compute(boxModule->height, VERTICAL);
                    width = height * ratio;

                } else if (boxModule->width.text != "auto" && boxModule->height.text == "auto") {
                    width = compute(boxModule->width, HORIZONTAL);
                    height = width / ratio;

                } else if (boxModule->width.text == "auto" && boxModule->height.text == "auto") {

                    if (width < minWidth || height < minHeight) {
                        if (ratio < minWidth/minHeight) {
                            width = minWidth;
                            height = width/ratio;
                        } else {
                            height = minHeight;
                            width = height*ratio;
                        }
                    }

                    if (width > maxWidth || height > maxHeight) {
                        if (ratio < maxWidth/maxHeight) {
                            height = maxHeight;
                            width = height*ratio;
                        } else {
                            width = maxWidth;
                            height = width/ratio;
                        }
                    }

                } else {
                    width = compute(boxModule->width, HORIZONTAL);
                    height = compute(boxModule->height, VERTICAL);
                }

                width = std::max(std::min(width, maxWidth), minWidth);
                height = std::max(std::min(height, maxHeight), minHeight);



            } else { // ################# No renderizable content
                if (boxModule->width.text == "auto") {
                    CSSModule::Length length;
                    length.num = 100;
                    length.unit = CSSModule::PERCENTAGE;

                    width = compute(length, HORIZONTAL);

                } else {
                    width = compute(boxModule->width, HORIZONTAL);
                }

                width = std::max(std::min(width, maxWidth), minWidth);


                if (boxModule->height.text == "auto") {
                    FrameBoxModel::setBox(width, 0);

                    measureChildren();

                    height = getHeightOfContent();

                } else {
                    height = compute(boxModule->height, VERTICAL);
                }

                height = std::max(std::min(height, maxHeight), minHeight);
            }
        }

        FrameBoxModel::setBox(width, height);
    }

    void Block::setPadding() {
        auto module = style->getModule(CSSModule::PADDING);
        auto paddingModule = std::dynamic_pointer_cast<CSSModule::Padding>(module);

        double top = compute(paddingModule->top, VERTICAL);
        double left = compute(paddingModule->left, HORIZONTAL);
        double bottom = compute(paddingModule->bottom, VERTICAL);
        double right = compute(paddingModule->right, HORIZONTAL);

        FrameBoxModel::setPadding(top, right, bottom, left);
    }

    void Block::setBorder() {
        auto module = style->getModule(CSSModule::BORDER);
        auto borderModule = std::dynamic_pointer_cast<CSSModule::Border>(module);

        double top = compute(borderModule->top.width, VERTICAL);
        double left = compute(borderModule->left.width, HORIZONTAL);
        double bottom = compute(borderModule->bottom.width, VERTICAL);
        double right = compute(borderModule->right.width, HORIZONTAL);

        FrameBoxModel::setBorder(top, right, bottom, left);
    }

    void Block::setMargin() {
        auto module = style->getModule(CSSModule::MARGIN);
        auto marginModule = std::dynamic_pointer_cast<CSSModule::Margin>(module);

        double marginRight = 0;
        if (marginModule->right.text.empty())
            marginRight = compute(marginModule->right, HORIZONTAL);

        double marginLeft = 0;
        if (marginModule->left.text.empty())
            marginRight = compute(marginModule->left, HORIZONTAL);


        double marginTop = 0;
        double marginBottom = 0;


        if (marginModule->top.text.empty())
            marginTop = compute(marginModule->top, VERTICAL);

        if (marginModule->bottom.text.empty())
            marginBottom = compute(marginModule->bottom, VERTICAL);


        FrameBoxModel::setMargin(marginTop,marginRight,marginBottom,marginLeft);
    }

    void Block::setPosition() {
        //todo position auto margin
        auto content = FrameBoxModel::content;
        double accumulative = 0;

        for (auto & line : lines) {
            line->positioned = false;
        }

        for (auto & child : _children) {
            //if (child->prevSibling())
                //pen = child->bottom;

            child->moveTo(content.left, content.top+accumulative);

            accumulative += height;
        }
    }

    void Block::setBackground() {
        resetBg();
        setBgClip();
        setBgColor();
    }

    void Block::measureChildren() {
        if (_childrenMeasured) return;
        _childrenMeasured = true;

        auto parentBox = getContentBox();
        auto fm = getFloatManager();

        for (auto & child : _children) {

            bool cleared = false;

            // pre-position
            if (child->positionAsBlock) {
                if (FloatManager::isFloating(child)) {
                    double space = fm->getAvailableSpaceFor(pen.y, content);
                    if (space - pen.x < child->width) {
                        breakLine();
                        child->moveTo(parentBox.left, parentBox.top+pen.y);
                    }

                } else {
                    breakLine();
                    child->moveTo(parentBox.left, parentBox.top+pen.y);

                    if (fm->clear(child, content)) {
                        cleared = true;
                        pen.y = child->top;
                    }
                }

            } else {
                child->moveTo(parentBox.left + pen.x,parentBox.top+pen.y);
            }

            //measure
            child->fullMeasure();

            // post-position
            if (child->positionAsBlock) {

                // Setting Float elements
                if (FloatManager::isFloating(child)) {
                    fm->positionFloat(child, pen, this);

                } else {
                    if (pen.x != 0) {
                        breakLine();
                    }

                    if (!cleared) {
                        FrameBoxModel::collapseMargin(child->prevSibling(), child);
                    }

                    pen.x = 0;
                    pen.y = child->bottom; //todo go for bottom not height
                }

            } else { // positioned as a Line

            }
        }
    }

    void Block::breakLine() {
        if (!lines.empty()) {
            auto &line = lines.back();
            pen.y += line->rect.height();
        }

        pen.x = 0;
    }

    double Block::getHeightOfContent() {
        // if overflow == auto, cover all the children

        double bottom = 0;

       /* for (auto & child : _children) {
            if (child->positionAsBlock) {
                if (child.isFloating) {
                    if (child.height > bottom) {
                        bottom = child.heigh;
                    }
                }
            }
        }*/

        breakLine();

        return pen.y;
    }

    void Block::resetBg() {
        FrameBackground::clear();
    }

    void Block::setBgClip() {
        auto module = style->getModule(CSSModule::BACKGROUND);
        auto backgroundModule = std::dynamic_pointer_cast<CSSModule::Background>(module);

        switch (backgroundModule->clip) {
            case CSSModule::Background::PADDING_BOX:
                FrameBackground::setClip(FrameBoxModel::padding);
                break;
            case CSSModule::Background::BORDER_BOX:
                FrameBackground::setClip(FrameBoxModel::border);
                break;
            default:
                FrameBackground::setClip(FrameBoxModel::content);
                break;
        }
    }

    void Block::setBgColor() {
        auto module = style->getModule(CSSModule::BACKGROUND);
        auto backgroundModule = std::dynamic_pointer_cast<CSSModule::Background>(module);

        FrameBackground::setColor(backgroundModule->color);
    }


    Block::Block(
            DOM_Node::sp_Node node,
            const sp_StyleContext &style,
            Presentation *presentation)
            :
            Frame(node, style, presentation),
            FrameBackground(&(presentation->viewport)) {

        if (auto htmlElement = dynamic_cast<HTMLElement*>(_ownerNode)) {
            if (auto renderizable = dynamic_cast<ElementRenderizable *>(htmlElement)) {
                renderizable->init(_ownerPresentation->viewport);
            }
        }

    }

    Block::~Block() noexcept {
        for (auto & line : lines) {
            delete line;
        }
    }

    Frame::Line * Block::getLine() {
        if (lines.empty()) {
            createNewLine();
        }

        auto *line = lines.back();

        auto fm = getFloatManager();
        double available = fm->getAvailableSpaceFor(pen.y, content);
        line->setWidth(available);

        return lines.back();
    }

    Frame::Line * Block::createNewLine() {
        breakLine();

        Line *line = new Line();

        auto fm = getFloatManager();
        double available = fm->getAvailableSpaceFor(pen.y, content);
        line->setWidth(available);

        double space = fm->getLeftSpace(content.top + pen.y, content);
        line->rect.moveTo(content.left + pen.x + space, content.top + pen.y);

        lines.push_back(line);

        return lines.back();
    }

    void Block::advanceLine(double x) {
        getLine()->advance(x);
        pen.x += x;
    }

    void Block::appendToLine(Content *content) {
        auto *line = getLine();
        line->append(content);
    }

    void Block::move(double x, double y) {
        FrameBoxModel::move(x,y);

        setBgClip();
        FrameBackground::move(x,y);

        auto sp_this = this->shared_from_this();

        if (!FloatManager::isFloating(sp_this)) {
            for (auto & child : _children) {
                child->move(x,y); //todo move children that are only in context of this block
            }

        } else {
            for (auto & child : _children) {
                child->moveTo(x,y); //todo move children that are only in context of this block
            }
        }
    }

    void Block::measure() {
        _childrenMeasured = false,

        setSizing();
        setMargin();
        setBorder();
        setPadding();
        setBox();

        measureChildren();
    }

    void Block::position() {
        //setPosition();
        setBackground();
    }

    void Block::render() {
        FrameBackground::render();

        if (auto htmlElement = dynamic_cast<HTMLElement*>(_ownerNode)) {
            if (auto renderizable = dynamic_cast<ElementRenderizable *>(htmlElement)) {
                renderizable->render(_ownerPresentation->viewport);
            }
        }
    }

}