#ifndef GUINOX_FRAME_INLINE_HPP
#define GUINOX_FRAME_INLINE_HPP

#include "../FrameRegistry.hpp"


namespace RenderFrame {

    class InLine : public Frame {
        const static FrameRegistry regist;


        struct ContentLine {
            Frame * node;
            FrameRect frameRect;

            ContentLine(Frame * node);
        };

        std::vector<ContentLine> lines;
        void setBox(ContentLine & cl);


        bool _leftOccupied = false;
        bool _rightOccupied = false;
        Frame * processingChild = nullptr;




    protected:
        void measure() override ;
        void position() override {}
        void render() override ;


    public:
        InLine(DOM_Node::sp_Node node, const sp_StyleContext &style, Presentation *presentation);
        ~InLine() override = default;

        virtual double getLeftSpace() const;
        virtual double getRightSpace() const;
        inline void leftSpaceOccupied() { _leftOccupied = true; }
        inline void rightSpaceOccupied() { _rightOccupied = true; }

        // inherited from ContentFrameStructures
        Line * getLine() override ;
        Line * createNewLine() override ;
        void advanceLine(double) override ;
        void appendToLine(Content * content) override ;

        // inherited from fraBoxModel
        void move(double x, double y) override ;


    };

}


#endif //GUINOX_FRAME_INLINE_HPP
