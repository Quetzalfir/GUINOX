#include "FrameBoxModel.hpp"



void FrameBoxModel::collapseMargin(RenderFrame::sp_Frame above, RenderFrame::sp_Frame below) {

}





void FrameBoxModel::setData() {
    _right = margin.right;
    _left = margin.left;
    _top = margin.top;
    _bottom = margin.bottom;
    _width = margin.width();
    _height = margin.height();
}


void FrameBoxModel::setBox(double width_, double height_) {
    auto dw = width_ - (mode == CONTENT ? content.width() : border.width());
    auto dh = height_ - (mode == CONTENT ? content.height() : border.height());

    _content.width(_content.width() + dw);
    _content.height(_content.height() + dh);

    _padding.width(_padding.width() + dw);
    _padding.height(_padding.height() + dh);

    _border.width(_border.width() + dw);
    _border.height(_border.height() + dh);

    _margin.width(_margin.width() + dw);
    _margin.height(_margin.height() + dh);

    setData();
    positionRelativeToMargin();
}

void FrameBoxModel::setPadding(double top_, double right_, double bottom_, double left_) {
    double width_ = left_ + right_;
    double height_ = top_ + bottom_;
    double dw = width_ - (padding.width() - content.width());
    double dh = height_ - (padding.height() - content.height());

    if (mode == CONTENT) {
        _padding.width(_padding.width() + dw);
        _padding.height(_padding.height() + dh);

        _border.width(_border.width() + dw);
        _border.height(_border.height() + dh);

        _margin.width(_margin.width() + dw);
        _margin.height(_margin.height() + dh);

    } else {
        _content.width(_content.width() - dw);
        _content.height(_content.height() - dh);
    }

    _pp = {left_, top_};

    setData();
    positionRelativeToMargin();
}

void FrameBoxModel::setBorder(double top_, double right_, double bottom_, double left_) {
    double width_ = left_ + right_;
    double height_ = top_ + bottom_;
    double dw = width_ - (border.width() - padding.width());
    double dh = height_ - (border.height() - padding.height());

    if (mode == CONTENT) {
        _border.width(_border.width() + dw);
        _border.height(_border.height() + dh);

        _margin.width(_margin.width() + dw);
        _margin.height(_margin.height() + dh);

    } else {
        _content.width(_content.width() - dw);
        _content.height(_content.height() - dh);

        _padding.width(_padding.width() + dw);
        _padding.height(_padding.height() + dh);
    }

    _pb = {left_, top_};

    setData();
    positionRelativeToMargin();
}

void FrameBoxModel::setMargin(double top_, double right_, double bottom_, double left_) {
    double width_ = left_ + right_;
    double height_ = top_ + bottom_;
    double dw = width_ - (border.width() - padding.width());
    double dh = height_ - (border.height() - padding.height());

    _margin.width(_margin.width() + dw);
    _margin.height(_margin.height() + dh);

    _pm = {left_, top_};

    setData();
    positionRelativeToMargin();
}

void FrameBoxModel::positionRelativeToMargin() {
    _border.moveTo(margin.left + _pm.x, margin.top + _pm.y);
    _padding.moveTo(border.left + _pb.x, border.top + _pb.y);
    _content.moveTo(padding.left + _pp.x, padding.top + _pp.y);
}

void FrameBoxModel::move(double x, double y) {
    //background.move(x, y);

    _content.move(x,y);
    _padding.move(x,y);
    _border.move(x,y);
    _margin.move(x,y);

    setData();
}

void FrameBoxModel::moveTo(double x, double y) {
    double u = x - left;
    double v = y - top;

    move(u, v);
}