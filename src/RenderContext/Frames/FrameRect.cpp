#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#endif

#include "FrameRect.hpp"

const std::string FrameRect::Background::shader = "GUINOX_background_shader";


/////////////////////////////////////////////////////////////////////
//                       Background class
/////////////////////////////////////////////////////////////////////


void FrameRect::Background::render() {
    // render color
    //clip->setVertex();
    if ( color && color->getHex() & 0xFF ) {
        viewport->draw(shader,this);
    }


    if (!imgs.empty()) {
        // clip rect
        //viewport->pushScissors(*clip);


        // render images
        /*for (auto &img : imgs) {
            for (auto &rect : img.rects) {
                img.image.render(rect.setVertex());
            }
        }*/

        //viewport->popScissors();
    }
}

void FrameRect::Background::move(double x, double y) {
    for (auto & img : imgs) {
        if (img.attachment != FIXED) {
            for (auto &rect : img.rects) {
                rect.move(x, y);
            }
        }
    }
}

FrameRect::Background::Background(ViewPort *viewport_) : viewport(viewport_) {
    viewport->appendShader(shader,
               "geometry.vert", "fragShader1.frag", "static", 2,
               [] (Shader * shader) {
                   shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                   glBufferData(GL_ARRAY_BUFFER, 4*2 * sizeof(GLfloat), NULL, GL_STATIC_DRAW);
                   glEnableVertexAttribArray(shader->getAttribute("position"));

                   const static GLushort indices[] = {0,1,2,3};
                   shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 1);
                   glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLushort), indices, GL_STATIC_DRAW);
               },
               [] (Shader *shader, void *element) {
                   const static float NO_COLOR[4] = {0};
                   auto *bg = (Background *)element;

                   auto viewport = bg->viewport;

                   shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                   glBufferSubData(GL_ARRAY_BUFFER, 0, 4*2 * sizeof(GLfloat), viewport->setVertex(*(bg->clip)));
                   glVertexAttribPointer(shader->getAttribute("position"), 2, GL_FLOAT, GL_FALSE, 0, nullptr);

                   glUniform4fv(shader->getUniform("u_color"), 1, bg->color ? bg->color->getFloat() : NO_COLOR);

                   shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 1);
                   glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, 0);
               });
}

/*FrameRect::Background & FrameRect::Background::operator=(const Background &A) {
    viewport = A.viewport;
}*/

void FrameRect::Background::clear() {
    imgs.clear();
}

void FrameRect::Background::setColor(const Color * color_) {
    color = color_;
}

/////////////////////////////////////////////////////////////////////
//                     END Background class
/////////////////////////////////////////////////////////////////////



void FrameRect::setBox(double width, double height) {
    auto dw = width - (mode == CONTENT ? content.width() : border.width());
    auto dh = height - (mode == CONTENT ? content.height() : border.height());

    _content.width(_content.width() + dw);
    _content.height(_content.height() + dh);

    _padding.width(_padding.width() + dw);
    _padding.height(_padding.height() + dh);

    _border.width(_border.width() + dw);
    _border.height(_border.height() + dh);

    _margin.width(_margin.width() + dw);
    _margin.height(_margin.height() + dh);

    positionRelativeToMargin();

    changed = true;
}

void FrameRect::setPadding(double top, double right, double bottom, double left) {
    double width = left + right;
    double height = top + bottom;
    double dw = width - (padding.width() - content.width());
    double dh = height - (padding.height() - content.height());

    if (mode == CONTENT) {
        _padding.width(_padding.width() + dw);
        _padding.height(_padding.height() + dh);

        _border.width(_border.width() + dw);
        _border.height(_border.height() + dh);

        _margin.width(_margin.width() + dw);
        _margin.height(_margin.height() + dh);

    } else {
        _content.width(_content.width() - dw);
        _content.height(_content.height() - dh);
    }

    _pp = {left, top};

    positionRelativeToMargin();

    changed = true;
}

void FrameRect::setBorder(double top, double right, double bottom, double left) {
    double width = left + right;
    double height = top + bottom;
    double dw = width - (border.width() - padding.width());
    double dh = height - (border.height() - padding.height());

    if (mode == CONTENT) {
        _border.width(_border.width() + dw);
        _border.height(_border.height() + dh);

        _margin.width(_margin.width() + dw);
        _margin.height(_margin.height() + dh);

    } else {
        _content.width(_content.width() - dw);
        _content.height(_content.height() - dh);

        _padding.width(_padding.width() + dw);
        _padding.height(_padding.height() + dh);
    }

    _pb = {left, top};
    positionRelativeToMargin();

    changed = true;
}

void FrameRect::setMargin(double top, double right, double bottom, double left) {
    double width = left + right;
    double height = top + bottom;
    double dw = width - (border.width() - padding.width());
    double dh = height - (border.height() - padding.height());

    _margin.width(_margin.width() + dw);
    _margin.height(_margin.height() + dh);

    _pm = {left, top};
    positionRelativeToMargin();

    changed = true;
}

FrameRect::FrameRect(ViewPort *viewPort) : background(viewPort) {

}

void FrameRect::render() {
    /*if (changed) {
        updateVertex();
    }*/

    background.render();
    //border.render();
}

void FrameRect::positionRelativeToMargin() {
    _border.moveTo(margin.left + _pm.x, margin.top + _pm.y);
    _padding.moveTo(border.left + _pb.x, border.top + _pb.y);
    _content.moveTo(padding.left + _pp.x, padding.top + _pp.y);
}

void FrameRect::updateVertex() {
    //_margin.setVertex();
    //_border.get().setVertex();
    //_padding.setVertex();
    //_content.setVertex();

    changed = false;
}

void FrameRect::move(double x, double y) {
    background.move(x, y);

    _content.move(x,y);
    _padding.move(x,y);
    _border.move(x,y);
    _margin.move(x,y);

    changed = true;
}

void FrameRect::move(const Point &point) {
    move(point.x, point.y);
}

void FrameRect::moveTo(double x, double y) {
    double u = x - margin.left;
    double v = y - margin.top;

    move(u, v);
}

void FrameRect::moveTo(const Point &point) {
    moveTo(point.x, point.y);
}

/*FrameRect& FrameRect::operator=(const FrameRect &A) {
    _margin = A._margin;
    _border = A._border;
    _padding = A._padding;
    _content = A._content;
    changed = A.changed;
    background = A.background;
    mode = A.mode;

    return *this;
}*/