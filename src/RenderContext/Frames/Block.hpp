#ifndef GUINOX_FRAME_BLOCK_HPP
#define GUINOX_FRAME_BLOCK_HPP

#include "../FrameRegistry.hpp"
#include "Frame.hpp"
#include "FloatManager.hpp"
#include "FrameBackground.hpp"


namespace RenderFrame {


    class Block :
            public Frame,
            public FrameBackground {

        const static FrameRegistry regist;


        bool _childrenMeasured = false;

        void resetBg();
        void setBgClip();
        void setBgColor();


        void setSizing();
        void setBox();
        void setPadding();
        void setBorder();
        void setMargin();
        void setPosition();
        void setBackground();

        void measureChildren();
        void breakLine();
        double getHeightOfContent();



    protected:
        std::vector<Frame::Line *> lines;
        Point pen;


        void measure() override;
        void position() override;
        void render() override;


    public:

        Block(DOM_Node::sp_Node node, const sp_StyleContext &style, Presentation *presentation);
        ~Block() noexcept override ;


        Frame::Line * getLine() override ;
        Frame::Line * createNewLine() override ;
        void advanceLine(double) override ;
        void appendToLine(Content * content) override ;

        void move(double x, double y) override ;

    };


}

#endif //GUINOX_FRAME_BLOCK_HPP
