#ifndef GUINOX_FRAMEBACKGROUND_HPP
#define GUINOX_FRAMEBACKGROUND_HPP

#include <vector>
#include "../../DOM/DOMRect.hpp"
#include "../../Utils/Color.hpp"
#include "../../OpenGL/ViewPort.hpp"


class FrameBackground {
public:

    enum Attachment : char {
        FIXED, SCROLL, LOCAL
    };

    struct Image {
        //AutoImage image;
        Attachment attachment;
        std::vector<DOMRect> rects;
    };


private:
    const Color * color = nullptr;
    DOMRect clip;
    std::vector<Image> imgs;
    ViewPort *viewport;


public:
    FrameBackground(ViewPort * viewport);

    void clear();
    void setColor(const Color * color_);
    void setClip(const DOMRect & rect) { clip = rect; }
    void addImage(const Image & image) { imgs.push_back(image); }

    void render();
    void move(double x, double y);

};


#endif //GUINOX_FRAMEBACKGROUND_HPP
