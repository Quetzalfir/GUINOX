#include "Text.hpp"
#include "../CSSModules/Font.hpp"
#include "../Presentation.hpp"
#include <DOM/Text.hpp>
#include <Utils/Utils.hpp>



static const std::string shader = "GUINOX_text_shader";


static void setCompleteVertexInfo(float array[], ftgl::texture_glyph_t * glyph) {
    array[0] = glyph->s0; array[1] = glyph->t0;
    array[2] = glyph->s0; array[3] = glyph->t1;
    array[4] = glyph->s1; array[5] = glyph->t0;
    array[6] = glyph->s1; array[7] = glyph->t1;
}


namespace RenderFrame {

    Text::Glyph::Glyph(ftgl::texture_glyph_t *glyph, GLuint texture_, double factor_) {
        setCompleteVertexInfo(tCoord, glyph);
        texture = texture_;
        factor = factor_;
    }

    void Text::Word::render(ViewPort & viewport) {
        for (auto & glyph : glyphs) {
            viewport.setVertex(glyph.rect);
            viewport.draw(shader, &glyph);
        }
    }

    void Text::Word::move(double x, double y) {
        rect.move(x,y);

        for (auto & glyph : glyphs) {
            glyph.rect.move(x, y);
        }
    }

    Text::Word * Text::createNewWord(bool space) {
        Word *word = new Word();

        word->rect.height(PT2PX(/*fontSize +*/ font->lineHeight * factor));
        word->baseline = PT2PX(font->baseLine * factor);
        word->space = space;

        words.push_back(word);

        return word;
    }

    void Text::getWords() {
        auto textNode = dynamic_cast<DOM_Node::Text *>(_ownerNode);
        std::string data = textNode->data;

        //todo replace HTMLEntities on the data

        for (const auto & c : data) {
            Word *word;

            if (words.empty()) {
                word = createNewWord(c == ' ');

            } else {
                Word *lastWord = words.back();

                if (c == ' ') {
                    word = lastWord->space ? lastWord : createNewWord(true);

                } else {
                    word = !lastWord->space ? lastWord : createNewWord(false);
                }
            }

            word->str += c;
            auto glyph = font->getGlyph(c);

            float kerning = 0.0f;
            if ( word->glyphs.size() > 0 ) {
                kerning = texture_glyph_get_kerning( glyph, word->str.data() + word->str.length() - 2 );
            }

            auto & penX = word->rect.right;
            penX += PT2PX( kerning * factor );

            Glyph glyph2(glyph, texture->handle, factor);

            glyph2.rect.width(PT2PX(glyph->width * factor));
            glyph2.rect.height(PT2PX(glyph->height * factor));

            double yoffset = PT2PX((font->lineHeight - font->baseLine - glyph->offset_y) * factor);
            double xoffset = PT2PX(glyph->offset_x * factor);

            glyph2.rect.moveTo(penX + xoffset, word->rect.top + yoffset);
            glyph2.color = &color;
            word->glyphs.push_back(glyph2);

            double advance = PT2PX(glyph->advance_x * factor);
            penX += advance;
        }
    }


    void Text::getFont() {
        auto module = style->getModule(CSSModule::FONT);
        auto fontModule = std::dynamic_pointer_cast<CSSModule::Font>(module);

        std::string path;

        for (const auto & family : fontModule->family) {
            // todo implement save fonts

            for (auto & fp : ownerPresentation->getFontFamily(family)) {
                path = fp.url;
                break;
            }

            if (!path.empty()) break;
        }

        if (!path.empty()) {
            font = Font::getFont(path);

            auto & atlas = font->atlas;
            texture = ownerPresentation->viewport.addTexture(path, atlas->width, atlas->height, (int *)atlas->data, GL_RED);
        }

        if (font) {
            double fontLineHeight = font->lineHeight ; // pt

            factor = fontSize / fontLineHeight;
        }
    }


    void Text::measure() {
        getFont();
        getWords();


        Line * line = getLine();


        auto rend = words.rend();
        auto rword = words.rbegin();
        double rightSpace = getRightSpace();

        while (rword != rend && (*rword)->space) rword++;


        auto end = words.end();
        auto word = words.begin();
        double leftSpace = getLeftSpace();

        while (word != end && (*word)->space) word++;


        while (word != end) {
            double wordWidth = (*word)->rect.width();

            if ((*word) == (*rword) || (*word) == words.back()) { // if is the last word (not space)
                if (!(line->fits(leftSpace + wordWidth + rightSpace))) {
                    createNewLine();
                }

                if (!(*word)->space) {
                    appendToLine((*word));
                }
                advanceLine(leftSpace + wordWidth + rightSpace);

                leftSpace = 0;
                rightSpace = 0;

                break;

            } else {
                if (!line->fits(leftSpace + wordWidth)) {
                    if (!(*word)->space)
                        line = createNewLine();
                }

                if (!(*word)->space) {
                    appendToLine((*word));
                }

                advanceLine(leftSpace + wordWidth);

                leftSpace = 0;
            }

            word++;
        }

        if (leftSpace == 0) leftSpaceOccupied();
        if (rightSpace == 0) rightSpaceOccupied();
    }

    void Text::render() {
        for (auto & line : lines) {
            for (auto & word : line->words) {
                word->render(ownerPresentation->viewport);
            }
        }
    }

    Text::Text(
            DOM_Node::sp_Node node,
            const sp_StyleContext &style,
            Presentation *presentation)
            :
            InLine(node, style, presentation) {

        color.setColor(0x000000ff);

        presentation->viewport.appendShader(shader,
                 "font.vert", "font.frag", "static", 3,
                 [] (Shader * shader) {
                     static const GLushort indices[] = {0,1,2,3};

                     shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                     glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), 0, GL_STATIC_DRAW);
                     glEnableVertexAttribArray(shader->getAttribute("position"));

                     shader->bindBuffer(GL_ARRAY_BUFFER, 1);
                     glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), 0, GL_STATIC_DRAW);
                     glEnableVertexAttribArray(shader->getAttribute("a_texCoord"));

                     shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 2);
                     glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLushort), indices, GL_STATIC_DRAW);
                 },
                 [] (Shader *shader, void *data) {
                     auto *glyph = (Glyph *)data;

                     glActiveTexture(GL_TEXTURE0);
                     glBindTexture(GL_TEXTURE_2D, glyph->texture);

                     glUniform1i(shader->getUniform("s_texture"), 0);
                     /************ Signed Distance Field *************/
                     glUniform1f(shader->getUniform("smoothing"), .065/glyph->factor); //.05/text->factor
                     glUniform1f(shader->getUniform("outlineDistance"), 0);
                     /************************************************/
                     glUniform4fv(shader->getUniform("u_color"), 1, glyph->color->getFloat());

                     // vertex
                     shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                     glBufferSubData(GL_ARRAY_BUFFER, 0, 8 * sizeof(GLfloat), glyph->rect.getVertex());
                     glVertexAttribPointer(shader->getAttribute("position"), 2, GL_FLOAT, GL_FALSE, 0, 0);

                     // texture coordinates
                     shader->bindBuffer(GL_ARRAY_BUFFER, 1);
                     glBufferSubData(GL_ARRAY_BUFFER, 0, 8 * sizeof(GLfloat), glyph->tCoord);
                     glVertexAttribPointer(shader->getAttribute("a_texCoord"), 2, GL_FLOAT, GL_FALSE, 0, 0);
                     ////GL_HALF_FLOAT_OES

                     // indices
                     shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 2);
                     glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, 0);
                 });
    }

    Text::~Text() noexcept {
        for (auto & word : words) {
            delete word;
        }

        for (auto & line : lines) {
            delete line;
        }
    }

    double Text::getLeftSpace() const {
        auto end = words.end();
        auto word = words.begin();

        double leftSpace = 0;

        while (word != end && (*word)->space) {
            leftSpace += (*word)->rect.width();
            word++;
        }

        if (auto parent_ = std::dynamic_pointer_cast<InLine>(parent())) {
            leftSpace += parent_->getLeftSpace();
        }

        return leftSpace;
    }

    double Text::getRightSpace() const {
        auto rend = words.rend();
        auto rword = words.rbegin();

        double rightSpace = 0;

        while (rword != rend && (*rword)->space) {
            rightSpace += (*rword)->rect.width();
            rword++;
        }

        if (auto parent_ = std::dynamic_pointer_cast<InLine>(parent())) {
            rightSpace += parent_->getRightSpace();
        }

        return rightSpace;
    }


    Frame::Line * Text::createNewLine() {
        auto parentLine = parent()->createNewLine();
        auto & parentRect = parentLine->rect;

        auto *textLine = new TextLine();
        textLine->start.x = parentRect.left;
        textLine->start.y = parentRect.top;


        lines.push_back(textLine);

        return parentLine;
    }

    void Text::advanceLine(double a) {
        parent()->advanceLine(a);

        if (lines.empty()) {
            auto parentLine = parent()->getLine();
            auto & parentRect = parentLine->rect;

            auto *textLine = new TextLine();
            textLine->start.x = parentRect.left;
            textLine->start.y = parentRect.top;

            lines.push_back(textLine);
        }

        auto & line = lines.back();
        line->advanced += a;
    }

    void Text::appendToLine(Content *content) {
        if (lines.empty()) {
            auto parentLine = parent()->getLine();
            auto & parentRect = parentLine->rect;

            auto *textLine = new TextLine();
            textLine->start.x = parentRect.left + parentLine->advanced;
            textLine->start.y = parentRect.top;

            lines.push_back(textLine);
        }

        auto & line = lines.back();
        content->moveTo(line->start.x + line->advanced, line->start.y);
        line->words.push_back(content);

        parent()->appendToLine(content);
    }

    void Text::move(double x, double y) {
        for (auto & line : lines) {
            line->start.x += x;
            line->start.y += y;

            for (auto & word : line->words) {
                word->move(x,y);
            }
        }
    }

}