#ifndef GUINOX_FRAMEBOXMODEL_HPP
#define GUINOX_FRAMEBOXMODEL_HPP

#include <memory>
#include "../../Utils/Point.hpp"
#include "../../DOM/DOMRect.hpp"


namespace RenderFrame {
    class Frame;

    typedef std::shared_ptr<Frame> sp_Frame ;
    typedef std::weak_ptr<Frame> wp_Frame ;
}



class FrameBoxModel {

    double _right = 0;
    double _top = 0;
    double _left = 0;
    double _bottom = 0;
    double _width = 0;
    double  _height = 0;

    Point _pp, _pb, _pm;


    void setData();
    void positionRelativeToMargin();


protected:

    bool _positionAsBlock = true;

    DOMRect _margin;
    DOMRect _border;
    DOMRect _padding;
    DOMRect _content;


public:
    enum Mode { CONTENT, BORDER };


    const double & right = _right;
    const double & left = _left;
    const double & top = _top;
    const double & bottom = _bottom;
    const bool & positionAsBlock = _positionAsBlock;
    const double &width = _width;
    const double &height = _height;
    Mode mode = CONTENT;

    const DOMRect &margin = _margin;
    const DOMRect &border = _border;
    const DOMRect &padding = _padding;
    const DOMRect &content = _content;



    void setBox(double width, double height);
    void setPadding(double top, double right, double bottom, double left);
    void setBorder(double top, double right, double bottom, double left);
    void setMargin(double top, double right, double bottom, double left);



    virtual void move(double x, double y);
    void moveTo(double x, double y);


    static void collapseMargin(RenderFrame::sp_Frame above, RenderFrame::sp_Frame below);

};


#endif //GUINOX_FRAMEBOXMODEL_HPP
