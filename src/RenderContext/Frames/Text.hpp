#ifndef GUINOX_FRAME_TEXT_HPP
#define GUINOX_FRAME_TEXT_HPP

#include "../FrameRegistry.hpp"
#include "InLine.hpp"
#include "OpenGL/Font.hpp"


namespace RenderFrame {

    class Text : public InLine {
        const static FrameRegistry regist;


        struct Glyph {
            Color *color;
            double factor;
            GLuint texture;
            DOMRect rect;
            GLfloat tCoord[8];

            Glyph(ftgl::texture_glyph_t * glyph,
                    GLuint texture,
                    double factor);
        };


        struct Word : public Content {
            friend class Text;

            std::string str;
            Font *font = nullptr;
            bool space = false;
            std::vector<Glyph> glyphs;

            void render(ViewPort & viewport) override ;
            void move(double x, double y) override ;
        };


        struct TextLine {
            Point start;
            double advanced = 0;
            std::vector<Content *> words;
        };


        std::vector<TextLine *> lines;
        double factor = 1;
        Font * font = nullptr;
        Texture *texture = nullptr;
        Color color;


        std::vector<Word *> words;

        Word * createNewWord(bool space);

        void getWords();
        void getFont();



    protected:
        void measure() override ;
        void position() override {}
        void render() override ;


    public:
        Text(DOM_Node::sp_Node node, const sp_StyleContext &style, Presentation *presentation);
        ~Text() noexcept override;

        double getLeftSpace() const override ;
        double getRightSpace() const override ;


        // inherited from ContentFrameStructures
        Line * createNewLine() override ;
        void advanceLine(double) override ;
        void appendToLine(Content * content) override ;


        // inherited from frameBoxModel
        void move(double x, double y) override ;
    };

}


#endif //GUINOX_FRAME_TEXT_HPP
