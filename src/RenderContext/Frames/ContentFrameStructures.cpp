#include <algorithm>
#include "ContentFrameStructures.hpp"


namespace RenderFrame {

    void ContentFrameStructures::Content::moveTo(double x, double y) {
        double dx = x - rect.left;
        double dy = y - rect.top;

        move(dx, dy);
    }


    bool ContentFrameStructures::Line::fits(double width) const {
        return advanced + width <= rect.width();
    }

    void ContentFrameStructures::Line::append(Content *content) {
        rect.height(std::max(rect.height(), content->rect.height()));

        content->rect.moveTo(rect.left + advanced, rect.bottom);

        contents.push_back(content);
    }

    /*void ContentFrameStructures::Line::moveTo(double x, double y) {
        double ax = rect.left;
        double ay = rect.top;

        rect.moveTo(x, y);

        for (auto &content : contents) {
            content->rect.move(x - ax, y - ay);
        }
    }*/

    void ContentFrameStructures::Line::advance(double x) {
        _advanced += x;
    }

    void ContentFrameStructures::Line::setWidth(double w) {
        rect.width(w);
    }

}