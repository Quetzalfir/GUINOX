#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#endif

#include "FrameBackground.hpp"

const std::string SHADER = "GUINOX_background_shader";





void FrameBackground::render() {
    // render color
    //clip->setVertex();
    if ( color && color->getHex() & 0xFF ) {
        viewport->draw(SHADER,this);
    }


    if (!imgs.empty()) {
        // clip rect
        //viewport->pushScissors(*clip);


        // render images
        /*for (auto &img : imgs) {
            for (auto &rect : img.rects) {
                img.image.render(rect.setVertex());
            }
        }*/

        //viewport->popScissors();
    }
}

void FrameBackground::move(double x, double y) {
    for (auto & img : imgs) {
        if (img.attachment != FIXED) {
            for (auto &rect : img.rects) {
                rect.move(x, y);
            }
        }
    }
}

FrameBackground::FrameBackground(ViewPort *viewport_) : viewport(viewport_) {
    viewport->appendShader(SHADER,
                           "geometry.vert", "fragShader1.frag", "static", 2,
                           [] (Shader * shader) {
                               shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                               glBufferData(GL_ARRAY_BUFFER, 4*2 * sizeof(GLfloat), NULL, GL_STATIC_DRAW);
                               glEnableVertexAttribArray(shader->getAttribute("position"));

                               const static GLushort indices[] = {0,1,2,3};
                               shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 1);
                               glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLushort), indices, GL_STATIC_DRAW);
                           },
                           [] (Shader *shader, void *element) {
                               const static float NO_COLOR[4] = {0};
                               auto *bg = (FrameBackground *)element;

                               auto viewport = bg->viewport;

                               shader->bindBuffer(GL_ARRAY_BUFFER, 0);
                               glBufferSubData(GL_ARRAY_BUFFER, 0, 4*2 * sizeof(GLfloat), viewport->setVertex(bg->clip));
                               glVertexAttribPointer(shader->getAttribute("position"), 2, GL_FLOAT, GL_FALSE, 0, nullptr);

                               glUniform4fv(shader->getUniform("u_color"), 1, bg->color ? bg->color->getFloat() : NO_COLOR);

                               shader->bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 1);
                               glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, 0);
                           });
}


void FrameBackground::clear() {
    imgs.clear();
}

void FrameBackground::setColor(const Color * color_) {
    color = color_;
}