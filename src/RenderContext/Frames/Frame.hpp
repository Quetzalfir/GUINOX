#ifndef GUINOX_FRAME_HPP
#define GUINOX_FRAME_HPP

#include <memory>
#include "../StyleContext.hpp"
#include "../../DOM/dom_declarations.hpp"
#include "ContentFrameStructures.hpp"
#include "../CSSModules/Display.hpp"
#include "FrameRect.hpp"
#include "FrameBoxModel.hpp"
#include "FloatManager.hpp"


class Presentation;
class ElementFrame;


namespace RenderFrame {


    enum Direction { HORIZONTAL, VERTICAL };




    class Frame;
    typedef std::shared_ptr<Frame> sp_Frame ;
    typedef std::weak_ptr<Frame> wp_Frame ;


    class Frame :
            public FrameBoxModel,
            public ContentFrameStructures,
            public std::enable_shared_from_this<Frame> {

        friend class ::Presentation;


        static bool isBlock(const Frame * frame);


        sp_StyleContext _style;
        wp_Frame _prevSibling;
        wp_Frame _nextSibling;
        Frame *_parent = nullptr;
        CSSModule::Display::DisplayType _type;
        wp_Frame _shadow;
        double _fontSize;


        sp_StyleContext getStyleContext(const sp_HTMLElement &element, const sp_RuleTree &rule);
        void printIdent(int n) const;
        void getFontSize();

        void fullPosition();


    protected:
        Presentation *_ownerPresentation = nullptr;
        DOM_Node::Node *_ownerNode = nullptr;
        std::list<sp_Frame> _children;


        double PT2PX(double pt);

        double compute(const CSSModule::Length & length, Direction dir) const ;
        DOMRect getContentBox() const ;
        FloatManager *getFloatManager();


        //FlowState * const getFlowState();

        virtual void measure() = 0;
        virtual void position() = 0;
        virtual void render() = 0;


    public:

        const sp_StyleContext &style = _style;
        Presentation *const ownerPresentation = _ownerPresentation;
        const double & fontSize = _fontSize;
        const CSSModule::Display::DisplayType & type = _type;



        Frame(DOM_Node::sp_Node node, sp_StyleContext style, Presentation * presentation);
        virtual ~Frame();

        inline sp_Frame prevSibling() const { return _prevSibling.lock(); }
        inline sp_Frame nextSibling() const { return _nextSibling.lock(); }
        inline sp_Frame lastChild() const { return _children.empty() ? nullptr : _children.back(); }
        inline sp_Frame firstChild() const { return _children.empty() ? nullptr : _children.front(); }
        inline sp_Frame parent() const { return _parent ? _parent->shared_from_this() : nullptr ; }
        sp_Frame root() const;
        inline void clear() { _children.clear(); }
        inline bool empty() const { return _children.empty(); }

        sp_Frame shadow() ;
        inline sp_Frame getShadow() const { return _shadow.lock(); }

        sp_Frame append(sp_Frame frame);
        sp_Frame insert(const sp_Frame &frame, const sp_Frame &child);
        sp_Frame replace(const sp_Frame &frame, const sp_Frame &child);
        sp_Frame remove(sp_Frame frame);

        sp_Frame emplace_back(DOM_Node::sp_Node node, const sp_RuleTree &rule, Presentation *presentation);
        void print() const;


        void reflow();
        void fullRender();
        void fullMeasure();

    };


}

#endif //GUINOX_FRAME_HPP
