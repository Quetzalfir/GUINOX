#ifndef GUINOX_FRAMERECT_HPP
#define GUINOX_FRAMERECT_HPP


#include "../../DOM/DOMRect.hpp"
#include "../../Utils/Color.hpp"
#include <vector>
#include <functional>
#include "../../OpenGL/Shader.hpp"
#include "../../OpenGL/ViewPort.hpp"

namespace RenderFrame {
    class Block;
}

class FrameRect {

    friend class RenderFrame::Block;

public:
    enum Mode : char { CONTENT, BORDER};

    class Background {
        static const std::string shader;


    public:

        enum Attachment : char {
            FIXED, SCROLL, LOCAL
        };

        struct Image {
            //AutoImage image;
            Attachment attachment;
            std::vector<DOMRect> rects;
        };


    private:
        const Color * color = nullptr;
        DOMRect * clip;
        std::vector<Image> imgs;
        ViewPort *viewport;

    public:
        Background(ViewPort * viewport);

        void clear();
        void setColor(const Color * color_);
        void setClip(DOMRect * rect) { clip = rect; }
        void addImage(const Image & image) { imgs.push_back(image); }

        void render();
        void move(double x, double y);

        //Background& operator=(const Background & A) ;

    };

    Mode mode = CONTENT;


private:
    DOMRect _margin;
    DOMRect _border;
    DOMRect _padding;
    DOMRect _content;
    Point _pp, _pb, _pm;
    bool changed = true;

    void positionRelativeToMargin();
    void updateVertex();



public:
    const DOMRect & margin = _margin;
    const DOMRect & content = _content;
    const DOMRect & padding = _padding;
    const DOMRect & border = _border;
    Background background;


    FrameRect(ViewPort * viewPort);

    void render() ;
    void setBox(double width, double height);
    void setPadding(double top, double right, double bottom, double left);
    void setBorder(double top, double right, double bottom, double left);
    void setMargin(double top, double right, double bottom, double left);

    inline double getMarginTop() const { return border.top - margin.top; }
    inline double getMarginBottom() const { return  margin.bottom - border.bottom; }


    void move(double x, double y);
    void move(const Point & point);
    void moveTo(double x, double y);
    void moveTo(const Point & point);

    //FrameRect& operator=(const FrameRect & A) ;

};


#endif //GUINOX_FRAMERECT_HPP
