#include <RenderContext/CSSModules/Margin.hpp>
#include <RenderContext/CSSModules/Border.hpp>
#include <RenderContext/CSSModules/Padding.hpp>
#include "InLine.hpp"
#include "../Presentation.hpp"


namespace RenderFrame {


    InLine::ContentLine::ContentLine(Frame *node_) :
            node(node_),
            frameRect(&(node_->ownerPresentation->viewport)) {

    }

    void InLine::setBox(ContentLine &cl) {
        auto module = style->getModule(CSSModule::MARGIN);
        auto marginM = std::dynamic_pointer_cast<CSSModule::Margin>(module);

        module = style->getModule(CSSModule::BORDER);
        auto borderM = std::dynamic_pointer_cast<CSSModule::Border>(module);

        module = style->getModule(CSSModule::PADDING);
        auto paddingM = std::dynamic_pointer_cast<CSSModule::Padding>(module);

        cl.frameRect.setMargin(
                0,
                compute(marginM->right,HORIZONTAL),
                0,
                compute(marginM->left, HORIZONTAL));

        cl.frameRect.setBorder(
                compute(borderM->top.width, VERTICAL),
                compute(borderM->right.width, HORIZONTAL),
                compute(borderM->bottom.width, VERTICAL),
                compute(borderM->left.width, HORIZONTAL));

        cl.frameRect.setPadding(
                compute(paddingM->top, VERTICAL),
                compute(paddingM->right, HORIZONTAL),
                compute(paddingM->bottom, VERTICAL),
                compute(paddingM->left, HORIZONTAL));
    }


    void InLine::measure() {
        _leftOccupied = _rightOccupied = false;

        for (auto & child : _children) {
            lines.emplace_back(child.get());
            auto & cl = lines.back();

            processingChild = child.get();

            child->fullMeasure();
        }
    }

    void InLine::render() {
        for (auto & fRect : lines) {
            fRect.frameRect.render();
        }
    }

    Frame::Line * InLine::getLine() {
        return parent()->getLine();
    }

    Frame::Line * InLine::createNewLine() {
        if (!lines.empty()) {
            auto &fr = lines.back().frameRect;
            if (fr.content.width() != 0) {
                lines.emplace_back(processingChild);
                auto &cl = lines.back();
            }
        }

        return parent()->createNewLine();
    }

    void InLine::advanceLine(double w) {
        if (!lines.empty()) {
            auto &fr = lines.back().frameRect;
            fr.setBox(
                    fr.content.width() + w,
                    fr.content.height()
            );
        }

        parent()->advanceLine(w);
    }

    void InLine::appendToLine(Content * content) {
        if (!lines.empty()) {
            auto &fr = lines.back().frameRect;
            fr.setBox(
                    fr.content.width(),
                    std::max(fr.content.height(), content->rect.height())
            );
        }

        parent()->appendToLine(content);
    }


    InLine::InLine(
            DOM_Node::sp_Node node,
            const sp_StyleContext &style,
            Presentation *presentation)
            :
            Frame(node, style, presentation) {

        _positionAsBlock = false;
    }

    double InLine::getLeftSpace() const {
        if (_leftOccupied) return 0;

        auto module = style->getModule(CSSModule::MARGIN);
        auto marginM = std::dynamic_pointer_cast<CSSModule::Margin>(module);

        module = style->getModule(CSSModule::BORDER);
        auto borderM = std::dynamic_pointer_cast<CSSModule::Border>(module);

        module = style->getModule(CSSModule::PADDING);
        auto paddingM = std::dynamic_pointer_cast<CSSModule::Padding>(module);

        double space = 0;

        space += compute(marginM->left, HORIZONTAL);
        space += compute(borderM->left.width, HORIZONTAL);
        space += compute(paddingM->left, HORIZONTAL);

        return space;
    }

    double InLine::getRightSpace() const {
        if (_rightOccupied) return 0;

        auto module = style->getModule(CSSModule::MARGIN);
        auto marginM = std::dynamic_pointer_cast<CSSModule::Margin>(module);

        module = style->getModule(CSSModule::BORDER);
        auto borderM = std::dynamic_pointer_cast<CSSModule::Border>(module);

        module = style->getModule(CSSModule::PADDING);
        auto paddingM = std::dynamic_pointer_cast<CSSModule::Padding>(module);

        double space = 0;

        space += compute(marginM->right, HORIZONTAL);
        space += compute(borderM->right.width, HORIZONTAL);
        space += compute(paddingM->right, HORIZONTAL);

        return space;
    }

    void InLine::move(double x, double y) {
        //todo
        //todo override move to move from padding;
    }

}