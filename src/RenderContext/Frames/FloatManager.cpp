#include "FloatManager.hpp"
#include "Frame.hpp"
#include "../CSSModules/Float.hpp"



bool FloatManager::isFloating(RenderFrame::sp_Frame &frame) {
    auto module = frame->style->getModule(CSSModule::FLOAT);
    auto floatM = std::dynamic_pointer_cast<CSSModule::Float>(module);

    return floatM->eFloat != CSSModule::Float::NONE_F;
}

bool FloatManager::intersect(const DOMRect &A, const DOMRect &B) {
    return ((B.left >= A.left && B.left < A.right) || (B.right <= A.right && B.right > A.left));
}

bool FloatManager::intersectTop(const DOMRect &content, double top) {
    return (content.top <= top && content.bottom > top);
}



void FloatManager::positionFloat(RenderFrame::sp_Frame & frame, const Point & pen, RenderFrame::Frame *parent) {

    //clear(pen, frame);
    const auto & content = parent->content;
    double top = content.top + pen.y;

    auto module = frame->style->getModule(CSSModule::FLOAT);
    auto floatM = std::dynamic_pointer_cast<CSSModule::Float>(module);

    if (floatM->eFloat == CSSModule::Float::NONE_F) return;


    bool type = floatM->eFloat == CSSModule::Float::LEFT_F;
    RenderFrame::Frame *above = nullptr;

    auto lastL = floatsL.rbegin();
    auto endL = floatsL.rend();

    auto lastR = floatsR.rbegin();
    auto endR = floatsR.rend();


    while (lastL != endL && !intersect(content, (*lastL).frame->margin)) lastL++;
    while (lastR != endR && !intersect(content, (*lastR).frame->margin)) lastR++;


    std::reverse_iterator<std::vector<Float>::iterator> * active = type ? &lastL : &lastR;


    if (getAvailableSpaceFor(top, content) < frame->width) {
        do {
            active = nullptr;

            if (lastL != endL && lastR != endR) {
                if (lastL->frame->bottom > lastR->frame->bottom) {
                    active = &lastR;

                } else if (lastL->frame->bottom < lastR->frame->bottom) {
                    active = &lastL;

                } else {
                    active = type ? &lastL : &lastR;
                }

            } else if (lastL == endL && lastR != endR) {
                active = &lastR;

            } else if (lastL != endL && lastR == endR) {
                active = &lastL;
            }

            if (active) {
                above = (*active)->frame;
                top = above->bottom;

                if (getAvailableSpaceFor(top, content) >= frame->width) {
                    if ((*active)->left == type) {
                        auto aux = (*active)++;
                        while (
                                (*active) != (aux->left ? endL : endR) &&
                                intersect(content, (*active)->frame->margin) &&
                                (*active)->frame->bottom <= aux->frame->bottom)
                            (*active)++;
                    }

                    break;
                }

                auto aux = (*active)++;
                while (
                        (*active) != (aux->left ? endL : endR) &&
                        intersect(content, (*active)->frame->margin) &&
                        (*active)->frame->bottom <= aux->frame->bottom) {
                    (*active)++;
                }
            }

        } while (active);

    } else {

        auto end = type ? endL : endR ;
        if ((*active) != end) {
            while (
                    (*active) != end &&
                    intersect(content, (*active)->frame->margin) &&
                    (*active)->frame->bottom <= top) {
                (*active)++;
            }
        }
    }


    double h;
    if (type) { // if is left floating
        h = lastL != endL ? lastL->frame->right : content.left ;
    } else {
        h = (lastR != endR ? lastR->frame->left : content.right) - frame->width;
    }


    if (type) { // if left
        floatsL.push_back({true, frame.get()});
    } else {
        floatsR.push_back({false, frame.get()});
    }


    frame->moveTo(h, top);
    FrameBoxModel::collapseMargin(above ? above->shared_from_this() : nullptr, frame);
}

double FloatManager::getAvailableSpaceFor(double top, const DOMRect & content) const {

    top += content.top;
    auto lastL = floatsL.rbegin();
    auto endL = floatsL.rend();

    auto lastR = floatsR.rbegin();
    auto endR = floatsR.rend();

    while (lastL != endL) {
        while (lastL != endL && !intersect(content, lastL->frame->margin)) lastL++;
        if (lastL != endL && intersectTop(lastL->frame->margin, top)) break;
        lastL++;
    }

    while (lastR != endR) {
        while (lastR != endR && !intersect(content, lastR->frame->margin)) lastR++;
        if (lastR != endR && intersectTop(lastR->frame->margin, top)) break;
        lastR++;
    }

    double space = content.width();

    if (lastL != endL) {
        space -= lastL->frame->right - content.left;
    }

    if (lastR != endR) {
        space -= content.right - lastR->frame->left;
    }

    if (space < 0) space = 0;

    return space;
}

double FloatManager::getLeftSpace(double top, const DOMRect &content) const {
    auto lastL = floatsL.rbegin();
    auto endL = floatsL.rend();

    while (lastL != endL) {
        while (lastL != endL && !intersect(content, lastL->frame->margin)) lastL++;
        if (lastL != endL && intersectTop(lastL->frame->margin, top)) break;
        lastL++;
    }

    if (lastL != endL) {
        double space = lastL->frame->right - content.left;;

        if (space < 0) space = 0;

        return space;
    }

    return 0;
}

bool FloatManager::clear(RenderFrame::sp_Frame & frame, const DOMRect & content) const {

    auto module = frame->style->getModule(CSSModule::FLOAT);
    auto floatM = std::dynamic_pointer_cast<CSSModule::Float>(module);
    double bottom = 0;

    switch (floatM->clear) {
        case CSSModule::Float::BOTH: {

            for (auto & fl : floatsL) {
                if (intersect(fl.frame->margin, content) && fl.frame->bottom > bottom)
                    bottom = fl.frame->bottom;
            }

            for (auto & fl : floatsR) {
                if (intersect(fl.frame->margin, content) && fl.frame->bottom > bottom)
                    bottom = fl.frame->bottom;
            }

            break;
        }

        case CSSModule::Float::LEFT_C: {
            for (auto & fl : floatsL) {
                if (intersect(fl.frame->margin, content) && fl.frame->bottom > bottom)
                    bottom = fl.frame->bottom;
            }
            break;
        }

        case CSSModule::Float::RIGHT_C: {
            for (auto & fl : floatsR) {
                if (intersect(fl.frame->margin, content) && fl.frame->bottom > bottom)
                    bottom = fl.frame->bottom;
            }
            break;
        }
        default: ;
    }

    if (bottom > frame->top) {
        frame->moveTo(frame->left, bottom);
    }

    return false;
}

void FloatManager::removeFloat(RenderFrame::Frame *frame) {

    auto fL = floatsL.begin();
    while (fL != floatsL.end()) {
        if ((*fL).frame == frame) {
            fL = floatsL.erase(fL);
            return;
        }
    }

    auto fR = floatsR.begin();
    while (fR != floatsL.end()) {
        if ((*fR).frame == frame) {
            fR = floatsL.erase(fR);
            return;
        }
    }
}