#include "Frame.hpp"
#include <utility>
#include <DOM/HTMLElement.hpp>
#include "../FrameRegistry.hpp"
#include "../Presentation.hpp"
#include "../CSSModules/Font.hpp"
#include "Block.hpp"


namespace RenderFrame {



    bool Frame::isBlock(const Frame *frame) {
        if (!frame) return false;

        return (frame->type == CSSModule::Display::BLOCK);
    }



    sp_StyleContext Frame::getStyleContext(const sp_HTMLElement &element, const sp_RuleTree &rule) {
        if (element->style.empty()) {
            if (auto aux = _style->lastChild()) {
                if (auto ref = aux->findPrevRuleSibling(rule, true)) {
                    if (!ref->inLine()) return ref;
                }
            }
        }

        return std::make_shared<StyleContext>(rule, &(element->style));
    }

    void Frame::printIdent(int n) const {
        for (int i = 0; i < n; i++) {
            std::cout << "_";
        }


        std::string type_;

        if (_ownerNode->nodeType == DOM_Node::Node::TEXT_NODE) {
            type_ = "text";

        } else {
            auto module = style->getModule(CSSModule::DISPLAY);
            auto display = std::dynamic_pointer_cast<CSSModule::Display>(module);

            switch (display->display) {
                case CSSModule::Display::BLOCK:
                    type_ = "block";
                    break;
                case CSSModule::Display::INLINE:
                    type_ = "inline";
                    break;
            }
        }

        std::cout << type_ << std::endl;

        for (const auto &r : _children) {
            r->printIdent(n + 4);
        }
    }

    void Frame::getFontSize() {
        auto module = style->getModule(CSSModule::FONT);
        auto fontModule = std::dynamic_pointer_cast<CSSModule::Font>(module);

        auto & vp = ownerPresentation->viewport;
        double ydpi = vp.ydpi;
        double xdpi = vp.xdpi;
        auto size = fontModule->size;
        auto parentSize = _parent ? _parent->fontSize : 16*72.0/ydpi; // 16px = 1em


        if      (size.unit == CSSModule::MM)          _fontSize = size.num*72.0/254;
        else if (size.unit == CSSModule::PX)          _fontSize = size.num*72.0/ydpi;
        else if (size.unit == CSSModule::PERCENTAGE)  _fontSize = size.num*parentSize/100;
        else if (size.unit == CSSModule::VH)          _fontSize = (size.num*vp.viewportHeight/100)*72/ydpi;
        else if (size.unit == CSSModule::VW)          _fontSize = (size.num*vp.viewportWidth/100)*72/xdpi;
        else if (size.unit == CSSModule::VMIN)        _fontSize = (size.num*std::min(vp.viewportHeight, vp.viewportWidth)/100)*72/xdpi;
        else if (size.unit == CSSModule::VMAX)        _fontSize = (size.num*std::max(vp.viewportHeight, vp.viewportWidth)/100)*72/xdpi;
        else if (size.unit == CSSModule::EM)          _fontSize = size.num*16*72/ydpi;
        else if (size.unit == CSSModule::REM)         _fontSize = size.num*16*72/ydpi;
        else if (size.unit == CSSModule::CH)          _fontSize = size.num*16*72/ydpi;
    }

    DOMRect Frame::getContentBox() const {
        auto aux = _parent;

        while (aux && !isBlock(aux)) aux = aux->_parent;

        if (aux) {
            auto block = dynamic_cast<Block *>(aux);
            return block->content;

        } else {
            const auto & viewport = _ownerPresentation->viewport;
            return DOMRect::fromRect(0,0, viewport.viewportWidth, viewport.viewportHeight);
        }
    }

    void Frame::fullMeasure() {
        getFontSize();
        measure();
    }

    void Frame::fullPosition() {
        position();

        for (auto & child : _children) {
            child->fullPosition();
        }
    }

    double Frame::PT2PX(double pt) {
        auto & vp = _ownerPresentation->viewport;

        return pt * vp.ydpi / 72;
    }

    double Frame::compute(const CSSModule::Length &length, Direction dir) const {
        if (length.text.substr(0, 4) == "calc") {
            //size_t f = value.find_first_of("(");
            //size_t l = value.find_last_of(")");
            //return calculate(value.substr(f, l-f), reference);
        }

        const auto & vp = _ownerPresentation->viewport;
        auto contentBox = getContentBox();
        double ref = dir == VERTICAL ? contentBox.height() : contentBox.width() ;
        double dpi = dir == VERTICAL ? vp.ydpi : vp.xdpi ;


        switch (length.unit) {
            case CSSModule::PERCENTAGE: return length.num * ref / 100;
            case CSSModule::VW:   return length.num * vp.viewportWidth / 100;
            case CSSModule::VH:   return length.num * vp.viewportHeight / 100;
            case CSSModule::VMIN: return length.num * std::min(vp.viewportHeight, vp.viewportWidth) / 100;
            case CSSModule::VMAX: return length.num * std::max(vp.viewportHeight, vp.viewportWidth) / 100;
            case CSSModule::MM:   return length.num * dpi / 25.4;
            //case CSSModule::PX:   return length.num * dpi / 96;
        }

        return length.num;
    }

    FloatManager * Frame::getFloatManager() {
        return &(_ownerPresentation->floatManager);
    }


    Frame::Frame(
            DOM_Node::sp_Node node,
            sp_StyleContext style_,
            Presentation * presentation_)
            :
            _style(std::move(style_)),
            _ownerPresentation(presentation_) {

        auto module = _style->getModule(CSSModule::DISPLAY);
        auto display = std::dynamic_pointer_cast<CSSModule::Display>(module);

        if (node->nodeType == DOM_Node::Node::TEXT_NODE) {
            _type = CSSModule::Display::TEXT;
        } else {
            _type = display->display;
        }

        if (auto element = std::dynamic_pointer_cast<HTMLElement>(node)) {
            element->frames.push_back(this);
        }

        _ownerNode = node.get();
    }

    Frame::~Frame() {
        if (auto element = dynamic_cast<HTMLElement *>(_ownerNode)) {
            auto end = element->frames.end();
            for (auto it = element->frames.begin(); it != end; it++) {
                if ((*it) == this) {
                    element->frames.erase(it);
                    break;
                }
            }
        }

        getFloatManager()->removeFloat(this);

        std::cout << "Frame destroy " << this << std::endl;
    }

    sp_Frame Frame::shadow() {
        sp_Frame clone;

        FrameRegistry::get(_ownerNode->shared_from_this(), _style, _ownerPresentation);

        clone->_parent = _parent;
        _shadow = clone;

        for (auto & child : _children) {
            clone->append(child->shadow());
        }

        return clone;
    }

    sp_Frame Frame::append(sp_Frame frame) {
        return insert(frame, nullptr);
    }

    sp_Frame Frame::insert(const sp_Frame &frame, const sp_Frame &child) {
        if (!frame) return nullptr;

        sp_Frame previousSibling = child ? child->prevSibling() : this->lastChild();

        auto end = this->_children.end();
        auto it = this->_children.begin();

        if (child) {
            for (; it != end; it++) {
                if ((*it) == child)
                    break;
            }

        } else {
            it = this->_children.end();
        }

        remove(frame);

        frame->_prevSibling = previousSibling;
        frame->_nextSibling = child;

        if (child) {
            child->_prevSibling = frame;
        }

        if (previousSibling) {
            previousSibling->_nextSibling = frame;
        }

        frame->_parent = this;
        frame->_ownerPresentation = _ownerPresentation;

        this->_children.insert(it, frame);

        return frame;
    }

    sp_Frame Frame::replace(const sp_Frame &frame, const sp_Frame &child) {
        if (!child || !frame) return nullptr;

        if (child == frame) return child;

        auto aux = this->_parent;
        while (aux && aux != frame.get()) aux = aux->_parent;

        if (aux) return nullptr;

        if (child->_parent != this) return nullptr;


        auto referenceChild = child->nextSibling();

        if (referenceChild == frame) {
            referenceChild = frame->nextSibling();
        }

        auto previousSibling = child->prevSibling();

        remove(child);

        insert(frame, referenceChild);

        return child;
    }

    sp_Frame Frame::remove(sp_Frame child) {
        if (!child || child->_parent != this) return nullptr;

        auto oldPreviousSibling = child->prevSibling();
        auto oldNextSibling = child->nextSibling();

        if (oldPreviousSibling) {
            oldPreviousSibling->_nextSibling = oldNextSibling;
        }

        if (oldNextSibling) {
            oldNextSibling->_prevSibling = oldPreviousSibling;
        }

        child->_prevSibling.reset();
        child->_nextSibling.reset();
        child->_parent = nullptr;
        child->_ownerPresentation = nullptr;

        this->_children.remove(child);

        return child;
    }

    sp_Frame Frame::emplace_back(DOM_Node::sp_Node node, const sp_RuleTree &rule, Presentation *presentation) {
        if (!node) return nullptr;
        sp_StyleContext childStyle;

        if (node->nodeType == DOM_Node::Node::ELEMENT_NODE) {
            if (!rule) return nullptr;

            auto element = std::dynamic_pointer_cast<HTMLElement>(node);
            auto aux = getStyleContext(element, rule);

            childStyle = _style->append(aux);

        } else { // TEXT_NODE
            childStyle = _style;
        }

        if (childStyle) {
            if (auto child = FrameRegistry::get(node, childStyle, presentation)) {
                return append(child);
            }
        }

        return nullptr;
    }

    sp_Frame Frame::root() const {
        auto aux = std::const_pointer_cast<Frame>(shared_from_this());

        while (aux->_parent) aux = aux->parent();

        return aux;
    }

    void Frame::print() const {
        std::cout << "___________________________ FRAME TREE ___________________________" << std::endl;

        printIdent(1);

        std::cout << "_________________________ END FRAME TREE _________________________" << std::endl;
    }

    void Frame::reflow() {
        fullMeasure();
        fullPosition();
    }

    void Frame::fullRender() {
        render();

        for (auto & child : _children) {
            child->fullRender();
        }
    }

}