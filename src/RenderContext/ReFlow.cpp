#include <RenderContext/CSSModules/Display.hpp>
#include "ReFlow.hpp"
#include "Presentation.hpp"


[[noreturn]] void ReFlow::threadFunc() {
    while (true) {
        while (commands.begin() != commands.end()) {
            auto dirty = commands.front();

            (commands.front())();

            commands.pop_front();
        }
    }
}

RenderFrame::sp_Frame ReFlow::getAncientBlock(RenderFrame::sp_Frame frame, bool inclusive) {
    if (!frame) return nullptr;

    RenderFrame::sp_Frame aux = inclusive ? frame : frame->parent() ;

    while (aux) {
        auto module = aux->style->getModule(CSSModule::DISPLAY);
        auto display = std::dynamic_pointer_cast<CSSModule::Display>(module);

        if (display->display == CSSModule::Display::BLOCK)
            return aux;

        aux = aux->parent();
    }

    return frame;
}


ReFlow::ReFlow(Presentation * presentation_) : presentation(presentation_) {
    reflowThread = std::thread([this]{ threadFunc(); });
    reflowThread.detach();
}

void ReFlow::append(RenderFrame::sp_Frame &parent, RenderFrame::sp_Frame &frame) {
    commands.emplace_back([=]() {
        auto ancient = getAncientBlock(parent, true);
        auto shadow = ancient->shadow();

        shadow->append(frame);

        shadow->reflow();

        if (auto shParent = shadow->parent()) {
            shParent->replace(shadow, parent);

        } else {
            presentation->frameTree = shadow;
        }
    });
}

void ReFlow::insertBefore(RenderFrame::sp_Frame &frame, RenderFrame::sp_Frame & child) {
    commands.emplace_back([=]() {
        auto ancient = getAncientBlock(child->parent(), true);
        auto shadow = ancient->shadow();

        shadow->insert(frame, child->getShadow());

        shadow->reflow();

        if (auto parent = shadow->parent()) {
            parent->replace(shadow, child->parent());

        } else {
            presentation->frameTree = shadow;
        }
    });
}

void ReFlow::replace(RenderFrame::sp_Frame &frame, RenderFrame::sp_Frame &child) {
    commands.emplace_back([=]() {
        auto ancient = getAncientBlock(child->parent(), true);
        auto shadow = ancient->shadow();

        shadow->replace(frame, child->getShadow());

        shadow->reflow();

        if (auto parent = shadow->parent()) {
            parent->replace(shadow, child->parent());

        } else {
            presentation->frameTree = shadow;
        }
    });
}

void ReFlow::substituteRoot(RenderFrame::sp_Frame &newFrame) {
    commands.emplace_back([=]() {
        newFrame->reflow();

        presentation->frameTree = newFrame;
    });
}