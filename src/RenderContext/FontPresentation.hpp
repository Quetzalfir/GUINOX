#ifndef GUINOX_FONTPRESENTATION_HPP
#define GUINOX_FONTPRESENTATION_HPP

#include <string>
#include <vector>
#include <map>


class Presentation;

class FontPresentation {

    Presentation * _ownerPresentation = nullptr;


public:
    enum Stretch {
        NORMAL, CONDENSED, ULTRA_CONDENSED, EXTRA_CONDENSED, SEMI_CONDENSED,
        EXPANDED, SEMI_EXPANDED, EXTRA_EXPANDED, ULTRA_EXPANDED
    };

    enum Style { NORMAL_S, ITALIC, OBLIQUE };

    struct Font_Properties {
        std::string url;
        Stretch stretch;
        Style style;
        int weight;
        std::string range;

        bool operator==(const Font_Properties &rhs) const ;
        inline bool operator!=(const Font_Properties &rhs) const { return !(this->operator==(rhs)); }
    };


private:

    static Stretch getStretch(const std::string & value);
    static Style getStyle(const std::string & value);
    static int getWeight(const std::string & value);


    std::map<std::string, std::vector<Font_Properties>> _fonts;


protected:
    void getUserFonts();


public:

    FontPresentation(Presentation * owner);

    const std::vector<Font_Properties> & getFontFamily(const std::string & name);


};


#endif //GUINOX_FONTPRESENTATION_HPP
