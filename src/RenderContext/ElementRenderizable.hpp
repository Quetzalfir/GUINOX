#ifndef GUINOX_ELEMENTRENDERIZABLE_HPP
#define GUINOX_ELEMENTRENDERIZABLE_HPP

#include "../OpenGL/ViewPort.hpp"


class ElementRenderizable {




protected:
    double _width = 0;
    double _height = 0;
    bool _renderizable = false;


public:
    const bool & renderizable = _renderizable;
    const double & width = _width;
    const double & height = _height;





    virtual void init(ViewPort & viewPort) {};
    virtual void render(ViewPort &viewport) const {};

};


#endif //GUINOX_ELEMENTRENDERIZABLE_HPP
