#ifndef GUINOX_RULETREE_HPP
#define GUINOX_RULETREE_HPP

#include <memory>
#include <vector>
#include "../CSSOM/CSSRule.hpp"


class RuleTree;
typedef std::shared_ptr<RuleTree> sp_RuleTree;
typedef std::weak_ptr<RuleTree> wp_RuleTree;



class RuleTree :
        public std::enable_shared_from_this<RuleTree> {


    struct RuleSpecificity {
        unsigned int specificity;
        sp_CSSStyleRule rule;
        size_t pos;

        RuleSpecificity(const std::pair<sp_CSSStyleRule, std::string> & rule, size_t position);

        bool operator<(const RuleSpecificity & rhs) const ;
    };


    static std::vector<RuleSpecificity> sortRules(const std::vector<std::pair<sp_CSSStyleRule, std::string>> & rules);



    wp_RuleTree _parent;
    std::list<sp_RuleTree> children;
    sp_CSSStyleRule _rule = nullptr;
    unsigned int specificity = 0;
    size_t position = 0;


    sp_RuleTree append(const RuleSpecificity & ruleSpecificity);
    sp_RuleTree lookForChild(const sp_CSSStyleRule & rule) const ;
    void printIdent(int n) const ;


public:

    const sp_CSSStyleRule &rule = _rule;


    sp_RuleTree appendPath(const std::vector<std::pair<sp_CSSStyleRule, std::string>> & path);
    sp_RuleTree parent() const ;
    void clear();
    void print() const ;

};


#endif //GUINOX_RULETREE_HPP
