#include "Touch.hpp"
#include "../Events/UIEvents/MouseEvents/MouseDown.hpp"
#include "../Events/UIEvents/MouseEvents/MouseMove.hpp"
#include "../Events/UIEvents/MouseEvents/MouseUp.hpp"
#include "../Events/UIEvents/MouseEvents/Click.hpp"
#include "../Events/UIEvents/MouseEvents/TouchMove.hpp"
#include "../Events/UIEvents/MouseEvents/MouseEnter.hpp"
#include "../Events/UIEvents/MouseEvents/MouseLeave.hpp"
#include "../Events/UIEvents/FocusEvents/Focus.hpp"
#include "../Events/UIEvents/FocusEvents/FocusIn.hpp"
#include "../Events/UIEvents/FocusEvents/Blur.hpp"
#include "../Events/UIEvents/FocusEvents/FocusOut.hpp"

#include <android/log.h>

#define  LOG_TAG    "GUINOX"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)


//std::deque<Touch::Pointer> Touch::pointers;
//std::vector<std::shared_ptr<Element>> Touch::focused;


/*Touch::Pointer::Pointer(int id_, float x_, float y_) {
    id = id_;
    x = x_;
    y = y_;
}*/




void Touch::addPointer(int pId, float x, float y) {
    /*for (const Pointer & pointer : pointers) {
        if (pointer.id == pId) {
            LOGI("Touch: id repetido");
            return ;
        }
    }

    //LOGI("Touch: x = %f, y = %f", x, y);

    Pointer pointer(pId, x, y);

    EventDispatcher * touch = new EventDispatcher(new MouseDown(x, y));

    pointer.initialPath = touch->getTargets();
    pointer.currentPath = pointer.initialPath;

    pointers.push_back(pointer);

    EventDispatcher::addDispatcher(touch);*/
}

void Touch::updatePointerCoords(int pId, float x, float y) {
    /*for (Pointer & pointer : pointers) {
        if (pointer.id == pId) {

            //LOGI("Touch: {%f,%f} | x = %f, y = %f | diff: %f, %f", pointer.x, pointer.y, x, y, x - pointer.x, y - pointer.y);

            //////////////////////////////////////////////////////////
            MouseMove * mouse;
            mouse = new MouseMove(x, y, x - pointer.x, y - pointer.y);
            EventDispatcher::addDispatcher(new EventDispatcher(mouse));

            //////////////////////////////////////////////////////////
            TouchMove * touch;
            touch = new TouchMove(x, y, x - pointer.x, y - pointer.y);

            EventDispatcher * dispatcher = new EventDispatcher(touch);
            Event_Path & currentPath = dispatcher->getTargets();

            ///////////////////////////////
            checkPointerOn(currentPath, pointer, x, y);
            pointer.currentPath = currentPath;

            EventDispatcher::addDispatcher(dispatcher);

            //////////////////////////////////////////////////////////

            pointer.x = x;
            pointer.y = y;

            break;
        }
    }*/
}

/*void Touch::checkPointerOn(const Event_Path &currentPath, const Pointer &pointer, float x, float y) {
    const Event_Path & oldPath = pointer.currentPath;
    int currSize = currentPath.size();
    int oldSize = oldPath.size();

    int minSize = std::min(currSize, oldSize);

    int i = 0;
    while (i < minSize && currentPath[i] == oldPath[i]) { i++; }

    ///////////////////////////////////////////////////////
    if (i != oldSize) {
        Event_Path path(oldPath.begin() + i, oldPath.end());
        MouseLeave *leave = new MouseLeave(x, y, x - pointer.x, y - pointer.y);

        EventDispatcher *dispatcher = new EventDispatcher(leave);
        dispatcher->setTargets(path);

        EventDispatcher::addDispatcher(dispatcher);
    }
    ////////////////////////////////////////////////////////
    if (i != currSize) {
        Event_Path path(currentPath.begin() + i, currentPath.end());

        MouseEnter *enter = new MouseEnter(x, y, x - pointer.x, y - pointer.y);

        EventDispatcher *dispatcher = new EventDispatcher(enter);
        dispatcher->setTargets(path);

        EventDispatcher::addDispatcher(dispatcher);
    }
}*/

void Touch::pointerLeave(int pId, float x, float y) {
    /*auto end = pointers.end();
    for (auto it = pointers.begin(); it != end; it++) {
        if ((*it).id == pId) {

            //LOGI("Touch: x = %f, y = %f", x, y);

            //////////////////////////////////////////////////////////
            MouseUp * eventUp = new MouseUp(x, y);

            EventDispatcher * up = new EventDispatcher(eventUp);
            up->setTargets((*it).currentPath);
            EventDispatcher::addDispatcher(up);

            //////////////////////////////////////////////////////////
            Click * eventClick = new Click(x, y);

            EventDispatcher * click = new EventDispatcher(eventClick);
            Event_Path & targets = click->getTargets();

            int minSize = std::min((*it).initialPath.size(), targets.size());

            int i = 0;
            while (i < minSize && (*it).initialPath[i] == targets[i]) { i++; }
            targets.erase(targets.begin()+i, targets.end());

            Element * focusedElement = targets.back().get();

            EventDispatcher::addDispatcher(click);

            setFocusEvents(focusedElement);

            /////////////////////////////////////////////////////////

            pointers.erase(it);

            break;
        }
    }*/
}

/*void Touch::setFocusEvents(Element * target) {
    std::deque<Element *> targets;

    while (target->tagName != "xml") {
        targets.push_front(target);
        target = target->parentElement;
    }

    int currSize = targets.size();
    int oldSize = focused.size();

    int minSize = std::min(currSize, oldSize);

    int i = 0;
    while (i < minSize && targets[i] == focused[i].get()) { i++; }

    ///////////////////////////////////////////////////////
    if (i != oldSize) {
        Event_Path path(focused.begin() + i, focused.end());
        EventDispatcher * blur = new EventDispatcher(new Blur());
        EventDispatcher * focusOut = new EventDispatcher(new FocusOut());

        blur->setTargets(path);
        focusOut->setTargets(path);

        EventDispatcher::addDispatcher(blur);
        EventDispatcher::addDispatcher(focusOut);
    }
    ////////////////////////////////////////////////////////
    if (i != currSize) {
        Event_Path path;
        int length = targets.size();
        for (int j = i ; j < length; j++) {
            path.push_back(std::static_pointer_cast<Element>(targets[i]->lock()));
        }

        EventDispatcher * focus = new EventDispatcher(new Focus());
        EventDispatcher * focusIn = new EventDispatcher(new FocusIn());

        focus->setTargets(path);
        focusIn->setTargets(path);

        EventDispatcher::addDispatcher(focus);
        EventDispatcher::addDispatcher(focusIn);
    }

    focused.clear();
    for (Element * element : targets) {
        focused.push_back(std::static_pointer_cast<Element>(element->lock()));
    }
}*/

/*void Touch::completeTheTree(Event_Path &path) {
    // lo completa linealmente hay que ocmpletarlo en arbol
    while (true) {
        const size_t length = path.size();
        size_t i = 0;
        while (i+1 < length && path[i].get() == path[i+1]->parentElement) { i++; }

        if (i == length-1) {
            return;

        } else {
            std::deque<std::shared_ptr<Element>> fill;
            Element * elem = path[i+1].get();

            while (path[i].get() != elem->parentElement) {
                elem = elem->parentElement;
                fill.push_front(std::static_pointer_cast<Element>(elem->lock()));
            }

            path.insert(path.begin()+i+1, fill.begin(), fill.end());
        }
    }
}*/