#include "Main_Manager.hpp"
#include <GLES2/gl2.h>

#include <android/log.h>

#define  LOG_TAG    "GUINOX"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)



//bool firstTimeStarted = false;

//double deltaTime;
//double globalRatio;
//int gMaxRenderbufferSize;
//Timer* Main_Manager::timer = new Timer(true);


void Main_Manager::on_created() {

}

void Main_Manager::on_surfaceCreated() {

}

void Main_Manager::on_surfaceChanged() {

}

void Main_Manager::on_changed(int w, int h) {

}

void Main_Manager::initEventThread() {

}

void Main_Manager::init() {

}

void Main_Manager::on_draw() {

}

void Main_Manager::recurrent() {

}

void Main_Manager::on_pause() {

}

void Main_Manager::on_resume() {

}

void Main_Manager::on_backPressed() {

}

void Main_Manager::on_destroy() {

}

void Main_Manager::setRealSize(float xdpi, float ydpi) {

}

void Main_Manager::on_close() {

}

void Main_Manager::on_stop() {
    LOGI("Manager: stopping....");
}