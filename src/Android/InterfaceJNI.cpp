#include "InterfaceJNI.hpp"
#include "Main_Manager.hpp"
#include "Asset.hpp"
#include "Touch.hpp"
#include <android/asset_manager_jni.h>



extern "C" {

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onCreate(JNIEnv *env, jclass obj) {
        Main_Manager::on_created();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onPause(JNIEnv *env, jclass obj) {
        Main_Manager::on_pause();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onResume(JNIEnv *env, jclass obj) {
        Main_Manager::on_resume();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onStop(JNIEnv *env, jclass obj) {
        Main_Manager::on_stop();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onBackPressed(JNIEnv *env, jclass obj) {
        Main_Manager::on_backPressed();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onDestroy(JNIEnv *env, jclass obj) {
        Main_Manager::on_destroy();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_setIfGLContextIsPreserved(JNIEnv *env, jclass obj, jboolean is) {

    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_setRealScreenSize(JNIEnv *env, jclass obj, jfloat xdpi, jfloat ydpi) {
        Main_Manager::setRealSize(xdpi, ydpi);
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onSurfaceCreated(JNIEnv *env, jclass obj) {
        Main_Manager::on_surfaceCreated();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onSurfaceChanged(JNIEnv *env, jclass obj, jint width, jint height) {
        Main_Manager::on_surfaceChanged();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onDrawFrame(JNIEnv *env, jclass obj) {
        Main_Manager::on_draw();
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_passAssetManager(JNIEnv *env, jclass obj, jobject assetManager) {
        Asset::setAssetManager(AAssetManager_fromJava(env, assetManager));
    }

    //JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_addTexture(JNIEnv *env, jclass obj, jint width, jint height, jintArray pixels, jstring name, jchar type);


    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_addPointer(JNIEnv *env, jclass obj, jint id, jfloat x, jfloat y) {
        Touch::addPointer(id, x, y);
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_updatePointerCoords(JNIEnv *env, jclass obj, jint id, jfloat x, jfloat y) {
        Touch::updatePointerCoords(id, x, y);
    }

    JNIEXPORT void JNICALL
    Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_pointerLeave(JNIEnv *env, jclass obj, jint id, jfloat x, jfloat y) {
        Touch::pointerLeave(id, x, y);
    }


}