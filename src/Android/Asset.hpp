#ifndef GUINOX_ANDROID_ASSETS_FILE_HPP
#define GUINOX_ANDROID_ASSETS_FILE_HPP


#include <string>
#include <android/asset_manager.h>

class Asset {
    AAssetDir *m_dir = nullptr;
    AAsset *m_pAsset = nullptr;
    std::string m_filename;
    static AAssetManager *m_pAssermanager;

public:
    struct ResourceDescriptor {
        int32_t mDescriptor;
        off_t mStart;
        off_t mLength;
    };

    Asset() {}
    explicit Asset(const std::string & name): m_filename(name) {}
    ~Asset();

    bool open();
    bool inline open(const std::string & str) {m_filename = str; return open();}
    bool openDir(const std::string & dir);
    std::string getNextFileName();
    void rewind();
    int read(void * const pBuffer, const size_t count) const ;
    void close();
    const void *getBuffer();
    void closeDir();
    long fseek(long offset, int origin);
    off_t length() const;
    int fgetc();
    const unsigned char *getCharArray() const ;
    ResourceDescriptor descript();
    inline std::string getPath() { return m_filename;}

    static inline void setAssetManager(AAssetManager *pAsset) {
        m_pAssermanager = pAsset;
    }
};



#endif //GUINOX_ANDROID_ASSETS_FILE_HPP
