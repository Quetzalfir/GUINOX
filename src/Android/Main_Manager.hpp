#ifndef GUINOX_MAIN_MANAGER_HPP
#define GUINOX_MAIN_MANAGER_HPP


#include <vector>
#include <string>
//#include <src/Utils/Timer.h>

class Main_Manager {

    //static Timer* timer;

    static void init();
    static void recurrent();
    static void initEventThread();

public:

    static void on_created();
    static void on_surfaceCreated();
    static void on_surfaceChanged();
    static void on_changed(int w, int h);
    static void on_draw();
    static void on_pause();
    static void on_resume();
    static void on_backPressed();
    static void on_destroy();
    static void setRealSize(float xdpi, float ydpi);
    static void on_close();
    static void on_stop();
};



#endif //GUINOX_MAIN_MANAGER_HPP
