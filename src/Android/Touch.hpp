#ifndef GUINOX_ANDROID_TOUCH_HPP
#define GUINOX_ANDROID_TOUCH_HPP


#include "../Events/EventDispatcher.hpp"
#include "../Events/UIEvents/MouseEvents/MouseEvent.hpp"
#include <deque>


class Touch {
    //typedef std::deque<std::shared_ptr<Element>> Event_Path;

    /*struct Pointer {
        float x, y;
        int id;

        Event_Path initialPath;
        Event_Path currentPath;


        Pointer(int id, float x, float y);
    };

    static std::deque<Pointer> pointers;
    static std::vector<std::shared_ptr<Element>> focused;


    static void checkPointerOn(const Event_Path & currentPath, const Pointer & pointer, float x, float y);
    static void setFocusEvents(Element * target);
    static void completeTheTree(Event_Path & path);
*/

public:

    static void addPointer(int pId, float x, float y);
    static void updatePointerCoords(int pId, float x, float y);
    static void pointerLeave(int pId, float x, float y);

};


#endif //GUINOX_ANDROID_TOUCH_HPP
