#include <cerrno>
#include "Asset.hpp"

#define  LOG_TAG    "libgl2jni"
#define  SFLOGI(...)  __android_log_print(ANDROID_LOG_INFO,"Super_Fenyx",__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)


AAssetManager *Asset::m_pAssermanager;

bool Asset::open() {
    if (m_filename.empty()) { return false; }
    m_pAsset = AAssetManager_open(m_pAssermanager, m_filename.c_str(), AASSET_MODE_UNKNOWN );

    return m_pAsset != nullptr;
}

bool Asset::openDir(const std::string & dir) {
    m_dir = AAssetManager_openDir(m_pAssermanager, dir.c_str());
    return m_dir != nullptr;
}

void Asset::rewind() {
    if (!m_dir) { return; }
    AAssetDir_rewind(m_dir);
}

std::string Asset::getNextFileName() {
    if (!m_dir) { return ""; }

    const char *fileName = AAssetDir_getNextFileName(m_dir);

    return (fileName ? fileName : "");
}

off_t Asset::length() const {
    return AAsset_getLength(m_pAsset);
}

void Asset::close() {
    if (m_pAsset) {
        AAsset_close(m_pAsset);
        m_pAsset = nullptr;
    }
}

void Asset::closeDir() {
    if (m_dir) {
        AAssetDir_close(m_dir);
        m_dir = nullptr;
    }
}

const void *Asset::getBuffer() {
    if (!m_pAsset) { return nullptr; }
    return AAsset_getBuffer(m_pAsset);
}

const unsigned char* Asset::getCharArray() const {
    off_t size = this->length();
    unsigned char *buff = new unsigned char[size+1];

    read(buff, size);
    buff[size] = 0;

    return buff;
}

long Asset::fseek(long offset, int origin) {
    return AAsset_seek(m_pAsset, offset, origin);
}

int Asset::fgetc() {
    char c[0];

    this->read(c, 1);
    this->fseek(0, SEEK_SET);
    return c[0];
}

Asset::~Asset() {
    this->close();
    this->closeDir();
}

int Asset::read(void *pBuffer, const size_t count) const {
    return AAsset_read(m_pAsset, pBuffer, count);
}

Asset::ResourceDescriptor Asset::descript() {
    ResourceDescriptor lDescriptor = { -1, 0, 0 };
    if (m_pAsset != nullptr) {
        lDescriptor.mDescriptor = AAsset_openFileDescriptor(m_pAsset, &lDescriptor.mStart, &lDescriptor.mLength);
    }
    return lDescriptor;
}