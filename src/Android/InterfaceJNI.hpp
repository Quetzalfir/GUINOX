//
// Created by quetz on 13/07/2020.
//

#ifndef GUINOX_INTERFACEJNI_HPP
#define GUINOX_INTERFACEJNI_HPP

#include <jni.h>

extern "C" {

    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onCreate(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onPause(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onResume(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onStop(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onBackPressed(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onDestroy(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_setIfGLContextIsPreserved(JNIEnv *env, jclass obj, jboolean is);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_setRealScreenSize(JNIEnv *env, jclass obj, jfloat xdpi, jfloat ydpi);


    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onSurfaceCreated(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onSurfaceChanged(JNIEnv *env, jclass obj, jint width, jint height);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_onDrawFrame(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_passAssetManager(JNIEnv *env, jclass obj, jobject assetManager);
    //JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_addTexture(JNIEnv *env, jclass obj, jint width, jint height, jintArray pixels, jstring name, jchar type);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_addPointer(JNIEnv *env, jclass obj, jint id, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_updatePointerCoords(JNIEnv *env, jclass obj, jint id, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_com_quetzalfir_polygondash_GUINOX_1JNI_1Interface_pointerLeave(JNIEnv *env, jclass obj, jint id, jfloat x, jfloat y);

}


#endif //GUINOX_INTERFACEJNI_HPP
