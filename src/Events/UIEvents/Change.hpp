#ifndef GUINOX_EVENT_CHANGE_HPP
#define GUINOX_EVENT_CHANGE_HPP


#include "../Event.hpp"

class Change : public Event {

public:
    std::string newValue;


    //todo, arreglar para trigger()
    Change(Element * target) : Event("change") {
        _bubbles = true;
        _cancelable = false;
        _composed = false;

        deepPath.push_back(std::static_pointer_cast<Element>(target->lock()));
    }

    virtual ~Change() {}
};


#endif //GUINOX_EVENT_CHANGE_HPP
