//
// Created by SAMS 1 on 16/03/2018.
//

#ifndef GUINOX_UIEVENT_HPP
#define GUINOX_UIEVENT_HPP


#include "../Event.hpp"

class UIEvent : public Event {

public:
    UIEvent(const std::string & type) : Event(type) {}
    virtual ~UIEvent() {}

};


#endif //GUINOX_UIEVENT_HPP
