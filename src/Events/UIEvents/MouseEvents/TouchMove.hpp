//
// Created by quetz on 12/05/2018.
//

#ifndef GUINOX_TOUCHMOVE_HPP
#define GUINOX_TOUCHMOVE_HPP


#include "MouseEvent.hpp"

class TouchMove : public MouseEvent {

public:
    TouchMove(float x, float y, float movX, float movY) : MouseEvent("touchmove") {
        _bubbles = true;
        _cancelable = true;
        _composed = false;
        _button = MouseEvent::PRIMARY;
        _buttons = MouseEvent::PRIMARY;
        _clientX = _screenX = x;
        _clientY = _screenY = y;
        _movementX = movX;
        _movementY = movY;
    }

};


#endif //GUINOX_TOUCHMOVE_HPP
