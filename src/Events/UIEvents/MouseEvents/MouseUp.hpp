//
// Created by SAMS 1 on 18/03/2018.
//

#ifndef GUINOX_MOUSEUP_HPP
#define GUINOX_MOUSEUP_HPP


#include "MouseEvent.hpp"

class MouseUp : public MouseEvent {

public:
    MouseUp(float x, float y) : MouseEvent("mouseup") {
        _bubbles = true;
        _cancelable = true;
        _composed = false;
        _clientX = _screenX = x;
        _clientY = _screenY = y;
        _button = MouseEvent::PRIMARY;
        _buttons = MouseEvent::PRIMARY;
    }
};


#endif //GUINOX_MOUSEUP_HP
