//
// Created by SAMS 1 on 17/03/2018.
//

#ifndef GUINOX_MOUSEMOVE_HPP
#define GUINOX_MOUSEMOVE_HPP


#include "MouseEvent.hpp"

class MouseMove : public MouseEvent {

public:
    MouseMove(float x, float y, float movX, float movY) : MouseEvent("mousemove") {
        _bubbles = true;
        _cancelable = true;
        _composed = false;
        _button = MouseEvent::PRIMARY;
        _buttons = MouseEvent::PRIMARY;
        _clientX = _screenX = x;
        _clientY = _screenY = y;
        _movementX = movX;
        _movementY = movY;
    }

};


#endif //GUINOX_MOUSEMOVE_HPP
