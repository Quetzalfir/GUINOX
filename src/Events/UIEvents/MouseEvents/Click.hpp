//
// Created by SAMS 1 on 18/03/2018.
//

#ifndef GUINOX_CLICK_HPP
#define GUINOX_CLICK_HPP


#include "MouseEvent.hpp"

class Click : public MouseEvent {

    void defaultAction(sp_HTMLElement target) override {
            //todo,
    }

public:
    Click(float x, float y) : MouseEvent("click") {
        _bubbles = true;
        _cancelable = true;
        _composed = false;
        _clientX = _screenX = x;
        _clientY = _screenY = y;
        _button = MouseEvent::PRIMARY;
        _buttons = MouseEvent::PRIMARY;
    }
};


#endif //GUINOX_CLICK_HPP
