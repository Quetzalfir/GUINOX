//
// Created by SAMS 1 on 17/03/2018.
//

#ifndef GUINOX_MOUSEDOWN_HPP
#define GUINOX_MOUSEDOWN_HPP


#include "MouseEvent.hpp"

class MouseDown : public MouseEvent {


public:
    MouseDown(float x, float y) : MouseEvent("mousedown") {
        _bubbles = true;
        _cancelable = true;
        _composed = false;
        _clientX = _screenX = x;
        _clientY = _screenY = y;
        _button = MouseEvent::PRIMARY;
        _buttons = MouseEvent::PRIMARY;
    }

};


#endif //GUINOX_MOUSEDOWN_HPP
