//
// Created by quetz on 12/05/2018.
//

#ifndef GUINOX_MOUSELEAVE_HPP
#define GUINOX_MOUSELEAVE_HPP


#include "MouseEvent.hpp"

class MouseLeave : public MouseEvent {

public:
    MouseLeave(float x, float y, float movX, float movY) : MouseEvent("mouseleave") {
        _bubbles = true;
        _cancelable = true;
        _composed = false;
        _button = MouseEvent::PRIMARY;
        _buttons = MouseEvent::PRIMARY;
        _clientX = _screenX = x;
        _clientY = _screenY = y;
        _movementX = movX;
        _movementY = movY;
    }

};


#endif //GUINOX_MOUSELEAVE_HPP
