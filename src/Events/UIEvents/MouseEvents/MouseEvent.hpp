#ifndef GUINOX_MOUSEEVENT_HPP
#define GUINOX_MOUSEEVENT_HPP


#include "../UIEvent.hpp"
#include "DOM/HTMLElement.hpp"
//#include "../../../Stylizable.h"

class MouseEvent : public UIEvent {
    friend class Touch;

public:
    enum BUTTON_NUMBER {
        NONE = 0,
        PRIMARY = 1,
        SECONDARY = 2,
        AUXILARY = 4,
        FOURTH = 8,
        FIFTH = 16
    };


private:
    bool _altKey;
    bool _ctrlKey;
    bool _metaKey;
    double _offsetX;
    double _offsetY;
    //int _region;
    //EventTarget _relatedTarget;
    bool _shiftKey;


protected:
    unsigned int _button;
    unsigned int _buttons;
    double _screenX;
    double _screenY;
    double _clientX;
    double _clientY;
    double _movementX = 0;
    double _movementY = 0;

    virtual bool tracePath(sp_HTMLElement elem) override {
       // return elem->cursorIn(clientX, clientY);
    }

    virtual void customizeEvent(sp_HTMLElement elem) override {
        /*const DOMRect & padd = ((Stylizable *)elem)->getPaddingRect();

        _offsetX = clientX-padd.left;
        _offsetY = clientY-padd.top;*/
    }

public:
    const bool & altKey = _altKey;
    const unsigned int & button = _button;
    const unsigned int & buttons = _buttons;
    const double & clientX = _clientX;
    const double & clientY = _clientY;
    const bool & ctrlKey = _ctrlKey;
    const bool & metaKey = _metaKey;
    const double & movementX = _movementX;
    const double & movementY = _movementY;
    const double & offsetX = _offsetX;
    const double & offsetY = _offsetY;
    //const int & region = _region;
    //const EventTarget & relatedTarget = _relatedTarget;
    const double & screenX = _screenX;
    const double & screenY = _screenY;
    const bool & shiftKey = _shiftKey;


    MouseEvent(const std::string & type) : UIEvent(type) {
        higherZIndex = true;
    }
    virtual ~MouseEvent(){}

    //void getModifierState();

};


#endif //GUINOX_MOUSEEVENT_HPP
