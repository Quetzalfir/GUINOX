//
// Created by quetz on 20/07/2018.
//

#ifndef GUINOX_FOCUS_HPP
#define GUINOX_FOCUS_HPP


#include "FocusEvent.hpp"

class Focus : public FocusEvent {

public:
    Focus() : FocusEvent("focus") {}
    virtual ~Focus() {}

};


#endif //GUINOX_FOCUS_HPP
