//
// Created by quetz on 20/07/2018.
//

#ifndef GUINOX_BLUR_HPP
#define GUINOX_BLUR_HPP


#include "FocusEvent.hpp"

class Blur : public FocusEvent {

public:
    Blur() : FocusEvent("blur") {}
    virtual ~Blur() {}

};


#endif //GUINOX_BLUR_HPP
