//
// Created by quetz on 20/07/2018.
//

#ifndef GUINOX_FOCUSOUT_HPP
#define GUINOX_FOCUSOUT_HPP


#include "FocusEvent.hpp"

class FocusOut : public FocusEvent {

public:
    FocusOut() : FocusEvent("focusin") {
        _bubbles = true;
    }
    virtual ~FocusOut() {}

};


#endif //GUINOX_FOCUSOUT_HPP
