//
// Created by quetz on 20/07/2018.
//

#ifndef GUINOX_FOCUSEVENT_HPP
#define GUINOX_FOCUSEVENT_HPP


#include "../UIEvent.hpp"

class FocusEvent : public UIEvent {

protected:
    virtual void defaultAction(sp_HTMLElement) override {}
    virtual bool tracePath(sp_HTMLElement) override { return false; }

public:
    FocusEvent(const std::string & type) : UIEvent(type) {
        _cancelable = false;
        _bubbles = false;
    }
    virtual ~FocusEvent() {}

};


#endif //GUINOX_FOCUSEVENT_HPP
