//
// Created by quetz on 20/07/2018.
//

#ifndef GUINOX_FOCUSIN_HPP
#define GUINOX_FOCUSIN_HPP


#include "FocusEvent.hpp"

class FocusIn : public FocusEvent {

public:
    FocusIn() : FocusEvent("focusin") {
        _bubbles = true;
    }
    virtual ~FocusIn() {}

};


#endif //GUINOX_FOCUSIN_HPP
