#include "Event.hpp"
#include "../DOM/EventTarget.hpp"
#include "../DOM/Node.hpp"


sp_EventTarget Event::getSpEventTarget(EventTarget *et) {
    auto node = dynamic_cast<DOM_Node::Node *>(et);

    auto sp_node = node->shared_from_this();

    return std::dynamic_pointer_cast<EventTarget>(sp_node);
    //todo, make window::EventTarget
}


Event::Event(const std::string &type, bool bubbles, bool cancelable) :
        _type(type), _bubbles(bubbles), _cancelable(cancelable), _composed(false) {

    //SFLOGI("Event: %s", type.c_str());
    //todo, set timeStamp

    initialized = true;
}

std::deque<sp_EventTarget> Event::composedPath() {
    std::deque<sp_EventTarget> composedPath;

    if (path.empty()) return composedPath;

    composedPath.push_back(getSpEventTarget(_currentTarget));

    int currentTargetIndex = 0;
    int currentTargetHiddenSubtreeLevel = 0;
    int index = path.size() - 1;

    while (index >= 0) {
        if (path[index].root_of_closed_tree) {
            currentTargetHiddenSubtreeLevel++;
        }

        if (path[index].invocation_target == _currentTarget) {
            currentTargetIndex = index;
            break;
        }

        if (path[index].slot_in_closed_tree) {
            currentTargetHiddenSubtreeLevel--;
        }

        index--;
    }

    int currentHiddenLevel = currentTargetHiddenSubtreeLevel;
    int maxHiddenLevel = currentTargetHiddenSubtreeLevel;

    index = currentTargetIndex - 1;

    while (index >= 0) {
        if (path[index].root_of_closed_tree) {
            currentHiddenLevel++;
        }

        if (currentHiddenLevel <= maxHiddenLevel) {
            composedPath.push_front(getSpEventTarget(path[index].invocation_target));
        }

        if (path[index].slot_in_closed_tree) {
            currentHiddenLevel--;
            if (currentHiddenLevel < maxHiddenLevel) {
                maxHiddenLevel = currentHiddenLevel;
            }
        }

        index--;
    }

    currentHiddenLevel = maxHiddenLevel = currentTargetHiddenSubtreeLevel;

    index = currentTargetIndex + 1;

    while (index < path.size()) {
        if (path[index].slot_in_closed_tree) {
            currentHiddenLevel++;
        }

        if (currentHiddenLevel <= maxHiddenLevel) {
            composedPath.push_back(getSpEventTarget(path[index].invocation_target));
        }

        if (path[index].root_of_closed_tree) {
            currentHiddenLevel--;
            if (currentHiddenLevel < maxHiddenLevel) {
                maxHiddenLevel = currentHiddenLevel;
            }
        }

        index++;
    }

    return composedPath;
}