#include <GUINOX/guinox.hpp>

int main() {
    auto window = GUINOX::createWindow("Assets/basic/index.html", "main");

    window->run();

    GUINOX::waitForTerminate();
}